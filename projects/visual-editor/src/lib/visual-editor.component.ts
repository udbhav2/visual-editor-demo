import {
  Component,
  OnInit,
  OnChanges,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  ElementRef,
  HostListener,
  SimpleChanges
} from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
declare const fabric: any;
declare let FontFace: any;
import { MatDialog } from '@angular/material';

import { ActionTypes, ObjectTypes } from './configs/action-types';
import { VisualEditorService } from './services/visual-editor.service';

@Component({
  selector: 'lib-visual-editor',
  templateUrl: './visual-editor.component.html',
  styleUrls: ['./visual-editor.component.scss']
})
export class VisualEditorComponent implements OnInit, OnChanges {
  @Input() originalImageUrl: string;
  @Input() editedImageUrl: string;
  @Input() visualJson: any;
  @Input() visualEditorConfig: any;
  @Input() customFonts = [];
  @Output() outgoingEventTriggered = new EventEmitter<object>();
  @Output() outgoingSelectionEventTriggered = new EventEmitter<object>();

  @ViewChild('wideCanvas') wideCanvas: ElementRef;

  ActionTypes = ActionTypes;
  VisualEditorService = VisualEditorService;

  checkersImageUrl = '@app/../assets/images/checkers_bg2.png';
  originalImageCanvas: any;
  editedImageCanvas: any;
  imageConfig = {
    width: 0,
    height: 0,
    scaleFactor: 1,
    xOffset: 0,
    yOffset: 0
  };

  tools = [];
  activeTool: any;
  objectTypeCount: any;
  isFirstToolSelected: boolean;
  hasCheckersBg: boolean;
  hasHiddenSelectionControls: boolean;
  hasZoom: boolean;
  hasPan: boolean;
  isWideCanvas: boolean;
  isShowFrame: boolean;
  resizeCanvasToBackgroundImage: boolean;
  isCenterBackgroundImage: boolean;
  toolsFlex: any;
  actionsFlex: any;
  selection: any;
  fileUpload: any;
  fileService: any;
  s3Service: any;

  // Keyboard Shortcuts
  hasShortcutsExpansionPanel: boolean;
  hasCloneShortcut: boolean;
  hasRemoveShortcut: boolean;
  hasRemoveAllShortcut: boolean;
  hasSendBackwardsShortcut: boolean;
  hasBringForwardShortcut: boolean;

  // FontAwesome Icons
  faClone;
  faTrash;
  faTrashAlt;
  faArrowUp;
  faArrowDown;

  isDragging: boolean;
  lastPosY: number;
  lastPosX: number;
  minZoom = 1;

  constructor(private dialog: MatDialog) {}

  // Start of ngOnInit function
  ngOnInit() {
    this.addCustomFonts();
    this.loadCustomFonts();

    this.tools = this.visualEditorConfig.tools;
    this.isFirstToolSelected = this.visualEditorConfig.isFirstToolSelected;
    this.hasCheckersBg = this.visualEditorConfig.hasCheckersBg;
    this.hasHiddenSelectionControls = this.visualEditorConfig.hasHiddenSelectionControls;
    this.hasZoom = this.visualEditorConfig.hasZoom;
    this.hasPan = this.visualEditorConfig.hasPan;
    this.isWideCanvas = this.visualEditorConfig.isWideCanvas;
    this.isShowFrame = this.visualEditorConfig.isShowFrame;
    this.resizeCanvasToBackgroundImage = this.visualEditorConfig.resizeCanvasToBackgroundImage;
    this.isCenterBackgroundImage = this.visualEditorConfig.isCenterBackgroundImage;
    this.toolsFlex = this.visualEditorConfig.toolsFlex;
    this.actionsFlex = this.visualEditorConfig.actionsFlex;

    // Keyboard Shortcuts
    this.hasShortcutsExpansionPanel = this.visualEditorConfig.hasShortcutsExpansionPanel;
    this.hasCloneShortcut = this.visualEditorConfig.hasCloneShortcut;
    this.hasRemoveShortcut = this.visualEditorConfig.hasRemoveShortcut;
    this.hasRemoveAllShortcut = this.visualEditorConfig.hasRemoveAllShortcut;
    this.hasSendBackwardsShortcut = this.visualEditorConfig.hasSendBackwardsShortcut;
    this.hasBringForwardShortcut = this.visualEditorConfig.hasBringForwardShortcut;

    // FontAwesome Icons
    this.faClone = this.visualEditorConfig.faClone;
    this.faTrash = this.visualEditorConfig.faTrash;
    this.faTrashAlt = this.visualEditorConfig.faTrashAlt;
    this.faArrowUp = this.visualEditorConfig.faArrowUp;
    this.faArrowDown = this.visualEditorConfig.faArrowDown;

    this.fileService = this.visualEditorConfig.fileService;
    this.s3Service = this.visualEditorConfig.s3Service;

    if (!this.isWideCanvas) {
      this.editedImageCanvas = new fabric.Canvas('editedImageCanvas', {
        isDrawingMode: true
      });
    } else {
      this.editedImageCanvas = new fabric.Canvas('wideCanvas', {
        isDrawingMode: true
      });
    }

    this.loadCanvas();

    if (this.hasHiddenSelectionControls) {
      this.editedImageCanvas.on('selection:created', e => {
        this.selectHiddenControls(e.selected);
      });
      this.editedImageCanvas.on('selection:updated', e => {
        if (
          this.editedImageCanvas.getActiveObject().isType('activeSelection')
        ) {
          this.selectHiddenControls(
            this.editedImageCanvas.getActiveObject().getObjects()
          );
        } else {
          this.selectHiddenControls(e.selected);
        }
      });
      this.editedImageCanvas.on('selection:cleared', e => {
        this.activeTool = this.tools[0]; // Code changed to render the 'Remove All' and 'Submit' buttons all the time
      });
    }

    if (this.isFirstToolSelected) {
      this.activeTool = this.tools[0];
      this.activeTool.canvasConfigs.forEach(confFunc => {
        this.editedImageCanvas = confFunc(this.editedImageCanvas);
      });
    } else {
      this.activeTool = {};
    }

    if (this.hasZoom) {
      this.addZoomControls(this.editedImageCanvas);
      if (this.originalImageUrl) {
        this.addZoomControls(this.originalImageCanvas);
      }
    }

    if (this.hasPan) {
      this.addPanControls(this.editedImageCanvas);
      if (this.originalImageUrl) {
        this.addPanControls(this.originalImageCanvas);
      }
    }
  }
  // End of ngOnInit function

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.visualJson.isFirstChange()) {
      this.loadCanvas();
    }
  }

  public loadCanvas() {
    this.VisualEditorService.resetObjectTypeCount();
    if (this.visualJson) {
      this.loadJson(this.visualJson, this.editedImageCanvas);
      this.editedImageCanvas.requestRenderAll();
    } else {
      this.setScaledImageToCanvas(
        this.editedImageCanvas,
        this.editedImageUrl,
        true
      );
      this.editedImageCanvas.requestRenderAll();
      if (this.originalImageUrl) {
        this.originalImageCanvas = new fabric.StaticCanvas(
          'originalImageCanvas'
        );
        this.setScaledImageToCanvas(
          this.originalImageCanvas,
          this.originalImageUrl
        );
        this.originalImageCanvas.requestRenderAll();
      }
    }
  }

  private addCustomFonts() {
    _.each(this.visualEditorConfig.tools, t => {
      _.each(t.sections, s => {
        _.each(s.actions, a => {
          if (a.label === 'FONT FAMILY') {
            a.dropdownOption = _.sortBy(
              _.union(
                a.dropdownOption,
                _.map(this.customFonts, f => f.fontFamily)
              )
            );
          }
        });
      });
    });
  }

  private loadCustomFonts() {
    // FIXME: IE support
    const customFontFaces = _.map(
      this.customFonts,
      f => new FontFace(f.fontFamily, `url(${f.fontFileUrl})`)
    );
    _.each(customFontFaces, cff => {
      // NOTE: need to specify a font size for check() function
      if (!(document as any).fonts.check(`12px ${cff.family}`)) {
        cff
          .load()
          .then(res => {
            (document as any).fonts.add(res);
          })
          .catch(error => {
            // FIXME: Error display
          });
      }
    });
  }

  private addZoomControls(canvas) {
    canvas.on('mouse:wheel', opt => {
      const delta = opt.e.deltaY;
      let zoom = canvas.getZoom();

      zoom = zoom - delta / _.min([canvas.getWidth(), canvas.getHeight()]);
      if (zoom > 20) {
        zoom = 20;
      }
      if (zoom < this.minZoom) {
        zoom = this.minZoom;
      }

      canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);

      opt.e.preventDefault();
      opt.e.stopPropagation();
    });
  }

  private addPanControls(canvas) {
    canvas.on('mouse:down', opt => {
      const evt = opt.e;
      if (evt.altKey === true) {
        this.isDragging = true;
        canvas.selection = false;
        this.lastPosX = evt.clientX;
        this.lastPosY = evt.clientY;
      }
    });
    canvas.on('mouse:move', opt => {
      if (this.isDragging) {
        const e = opt.e;
        canvas.viewportTransform[4] += e.clientX - this.lastPosX;
        canvas.viewportTransform[5] += e.clientY - this.lastPosY;
        canvas.forEachObject(o => o.setCoords());
        canvas.requestRenderAll();
        this.lastPosX = e.clientX;
        this.lastPosY = e.clientY;
      }
    });
    canvas.on('mouse:up', opt => {
      this.isDragging = false;
      canvas.selection = true;
    });
  }

  // Start of loadJson function
  private loadJson(inputJson, canvas) {
    const img = new Image();
    img.onload = () => {
      // Extracting the images width and height and adding it into the _canvasDimensions property in the inputJson
      if (this.resizeCanvasToBackgroundImage) {
        inputJson._canvasDimensions = { width: img.width, height: img.height };
        inputJson.backgroundImage.width = inputJson._canvasDimensions.width;
        inputJson.backgroundImage.height = inputJson._canvasDimensions.height;
      }

      canvas.loadFromJSON(inputJson, () => {
        const scaleFactor = _.min([
          canvas.getWidth() / inputJson._canvasDimensions.width,
          canvas.getHeight() / inputJson._canvasDimensions.height
        ]);
        this.minZoom = scaleFactor;

        if (!this.isWideCanvas) {
          canvas.setWidth(inputJson._canvasDimensions.width * scaleFactor);
          canvas.setHeight(inputJson._canvasDimensions.height * scaleFactor);
          canvas.setZoom(this.minZoom);

          const isWidthSmaller =
            inputJson._canvasDimensions.width / canvas.backgroundImage.width >
            inputJson._canvasDimensions.height / canvas.backgroundImage.height;
          if (isWidthSmaller) {
            canvas.backgroundImage.scaleToWidth(
              inputJson._canvasDimensions.width
            );
          } else {
            canvas.backgroundImage.scaleToHeight(
              inputJson._canvasDimensions.height
            );
          }
        } else {
          const padding = 5;
          const border = 0;
          const newWidth =
            this.wideCanvas.nativeElement.offsetWidth - 2 * (padding + border);
          const newHeight =
            this.wideCanvas.nativeElement.offsetHeight - 2 * (padding + border);
          canvas.setWidth(newWidth);
          canvas.setHeight(newHeight);
          canvas.setZoom(
            0.8 *
              _.min([
                canvas.getWidth() / inputJson._canvasDimensions.width,
                canvas.getHeight() / inputJson._canvasDimensions.height
              ])
          );
          canvas.viewportTransform[4] =
            (newWidth - inputJson._canvasDimensions.width * canvas.getZoom()) /
            2;
          canvas.viewportTransform[5] =
            (newHeight -
              inputJson._canvasDimensions.height * canvas.getZoom()) /
            2;
          canvas.forEachObject(o => o.setCoords());
        }

        if (this.isCenterBackgroundImage) {
          canvas.viewportCenterObject(canvas.backgroundImage); // Centers the image on the canvas
        }

        if (this.isShowFrame) {
          const strokeWidth = 3;
          const rectangle = new fabric.Rect({
            left: -strokeWidth,
            top: -strokeWidth,
            height: inputJson._canvasDimensions.height + strokeWidth,
            width: inputJson._canvasDimensions.width + strokeWidth,
            stroke: 'rgb(0,255,0)',
            strokeWidth,
            strokeDashArray: [5 * strokeWidth, 5 * strokeWidth],
            fill: 'rgba(0,0,0,0)',
            selectable: false,
            evented: false
          });
          rectangle._key = 'frame_rectangle';
          canvas.add(rectangle);
          rectangle.bringToFront();
        }

        canvas.renderAll();
      });
    };
    img.src = inputJson.backgroundImage.src;
  }
  // End of loadJson function

  private resetCanvas(canvas) {
    canvas.getObjects().forEach(o => canvas.remove(o));
  }

  private setScaledImageToCanvas(canvas, imageUrl, isEditedImage = false) {
    fabric.Image.fromURL(imageUrl, img => {
      const isWidthSmaller =
        canvas.getWidth() / img.width > canvas.getHeight() / img.height;
      if (isWidthSmaller) {
        img.scaleToHeight(canvas.getHeight());
      } else {
        img.scaleToWidth(canvas.getWidth());
      }

      const { width, height } = img;
      const left = isWidthSmaller
        ? (canvas.getWidth() - width * img.scaleX) / 2.0
        : 0;
      const top = isWidthSmaller
        ? 0
        : (canvas.getHeight() - height * img.scaleX) / 2.0;

      img.set({
        left,
        top
      });

      if (isEditedImage) {
        this.imageConfig = {
          width,
          height,
          scaleFactor: img.scaleX,
          xOffset: left,
          yOffset: top
        };
      }

      if (this.hasCheckersBg) {
        canvas.add(img);
        canvas.renderAll();

        fabric.Image.fromURL(this.checkersImageUrl, checkerImg => {
          checkerImg.scaleToWidth(canvas.getWidth());
          canvas.setBackgroundImage(checkerImg, canvas.renderAll.bind(canvas));
        });
      } else {
        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
      }
    });
  }

  private selectHiddenControls(selectedObjects) {
    if (selectedObjects.length === 1) {
      const isShape =
        selectedObjects[0].isType(ObjectTypes.RECT) ||
        selectedObjects[0].isType(ObjectTypes.CIRCLE) ||
        selectedObjects[0].isType(ObjectTypes.POLYGON);
      const isImage = selectedObjects[0].isType(ObjectTypes.IMAGE);
      this.tools.forEach(t => {
        if (t.isHidden) {
          if (isShape) {
            if (t.objectType === ObjectTypes.SHAPE) {
              this.onSelectTool(t);
            }
          } else if (isImage) {
            if (
              selectedObjects[0]._key &&
              selectedObjects[0]._key.includes(ObjectTypes.PRODUCT)
            ) {
              if (t.objectType === ObjectTypes.PRODUCT) {
                this.onSelectTool(t);
              }
            } else {
              // Non product image
              if (selectedObjects[0].isType(t.objectType)) {
                this.onSelectTool(t);
              }
            }
          } else {
            // Non shape and non image object
            if (selectedObjects[0].isType(t.objectType)) {
              this.onSelectTool(t);
            }
          }
        }
      });
    } else if (selectedObjects.length > 1) {
      this.tools.forEach(t => {
        if (t.isHidden && t.objectType === ObjectTypes.GROUP) {
          this.onSelectTool(t);
        }
      });
    }
  }

  public onSelectTool(tool) {
    if (tool.type === ActionTypes.UPLOAD) {
      this.fileUpload.click();
    } else {
      if (!_.isUndefined(tool.onSelect)) {
        tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
        this.activeTool = {};
      }
      if (!this.activeTool || this.activeTool.name !== tool.name) {
        this.activeTool = tool;
        if (tool.canvasConfigs) {
          tool.canvasConfigs.forEach(confFunc => {
            this.editedImageCanvas = confFunc(this.editedImageCanvas);
          });
        }
      }
    }
  }

  // Start of private onToolAction(action) function
  onToolAction(action) {
    switch (action.type) {
      case ActionTypes.BUTTON: {
        let res;

        if (!_.isUndefined(action.onSelectParams)) {
          res = action.action(this.editedImageCanvas, action.onSelectParams);
        } else {
          res = action.action(this.editedImageCanvas);
        }
        this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
        if (!_.isUndefined(res) && !_.isUndefined(action.onActionReturn)) {
          action.onActionReturn(action, res);
        }
        break;
      }
      case ActionTypes.TOGGLE: {
        action.isOn = !action.isOn;
        action.action(
          this.editedImageCanvas,
          action.isOn ? action.onValue : action.offValue
        );
        break;
      }
      case ActionTypes.DROPDOWN: {
        action.action(this.editedImageCanvas, action.dropdownSelectedOption);
        this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
        break;
      }
      case ActionTypes.RELATED_INPUT: {
        const relatedValues = {};
        this.activeTool.sections.forEach(s => {
          s.actions.forEach(a => {
            if (
              a.type === ActionTypes.RELATED_INPUT &&
              a.groupName === action.groupName &&
              a.label !== action.label
            ) {
              relatedValues[a.keyName] = a.inputValue;
            }
          });
        });
        relatedValues[action.keyName] = action.inputValue;
        action.action(this.editedImageCanvas, relatedValues);
        this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
        break;
      }
      case ActionTypes.DIALOG: {
        if (action.dialog) {
          const dialogRef = this.dialog.open(action.dialog.component, {
            autoFocus: false,
            data: action.dialog.data,
            minWidth: '300px',
            panelClass: 'app-dialog'
          });
          dialogRef.afterClosed().subscribe(resDialog => {
            if (!_.isUndefined(resDialog)) {
              const resAction = action.dialog.onClose(
                this.editedImageCanvas,
                resDialog
              );
              if (
                !_.isUndefined(resAction) &&
                !_.isUndefined(action.onActionReturn)
              ) {
                action.onActionReturn(action, resAction);
              }
            }
          });
        }
        break;
      }
      case ActionTypes.OUTGOING_EVENT_TRIGGER: {
        this.outgoingEventTriggered.emit({
          canvas: this.editedImageCanvas,
          transformedCanvas:
            action.label === 'Submit'
              ? action.action(
                  this.visualJson.backgroundImage.src,
                  this.editedImageCanvas.toJSON(['_canvasDimensions'])
                )
              : ''
        });
        break;
      }
      default: {
        action.action(this.editedImageCanvas);
        this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
        break;
      }
    }
  }
  // End of private onToolAction(action) function

  public onUpdate(imgUrl) {
    this.resetCanvas(this.editedImageCanvas);
    this.setScaledImageToCanvas(this.editedImageCanvas, imgUrl, true);
  }

  public getCanvasAndConfig() {
    return {
      canvas: this.editedImageCanvas,
      imageConfig: this.imageConfig
    };
  }

  onFileSelect(res) {
    const { files, caller: tool } = res;
    if (!_.isUndefined(tool)) {
      if (!_.isUndefined(tool.onSelect) && files.length > 0) {
        const file = files[0];
        const filename = _.replace(file.name, /[^A-Z0-9.]+/gi, '_');

        this.fileService
          .getUploadUrl({
            filename,
            contentType: file.type,
            expiresInSecs: 3600
          })
          .subscribe(respUploadUrl => {
            this.s3Service
              .upload(respUploadUrl.url, file, file.type)
              .subscribe(respUpload => {
                const fileUrl = respUploadUrl.url.split('?')[0];
                tool.onSelectParams.fileUrl = fileUrl;
                tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                this.activeTool = {};
              });
          });
      }
    }
  }

  // Keyboard Shortcuts
  @HostListener('window:keydown', ['$event'])
  canvasKeyboardEvent(event: KeyboardEvent) {
    if (
      this.hasCloneShortcut &&
      ((event.ctrlKey || event.metaKey) &&
        event.shiftKey &&
        event.keyCode === 86)
    ) {
      this.VisualEditorService.cloneObject(this.editedImageCanvas);
    }

    if (this.hasRemoveShortcut && (!event.shiftKey && event.keyCode === 8)) {
      this.VisualEditorService.deleteSelection(this.editedImageCanvas);
    }

    if (this.hasRemoveAllShortcut && (event.shiftKey && event.keyCode === 8)) {
      this.VisualEditorService.deleteAllSelections(this.editedImageCanvas);
    }

    if (
      this.hasSendBackwardsShortcut &&
      (event.shiftKey && event.keyCode === 40)
    ) {
      this.VisualEditorService.sendBackwards(this.editedImageCanvas);
    }

    if (
      this.hasBringForwardShortcut &&
      (event.shiftKey && event.keyCode === 38)
    ) {
      this.VisualEditorService.bringForward(this.editedImageCanvas);
    }
  }
}
