import * as _ from 'lodash';

import { VisualEditorService } from '../services/visual-editor.service';
import {
  faClone,
  faTrashAlt,
  faTrash,
  faArrowUp,
  faArrowDown,
  faAlignLeft,
  faAlignCenter,
  faAlignRight
} from '@fortawesome/free-solid-svg-icons';
import { ActionTypes, ObjectTypes } from './action-types';

export class TemplateAnnotatorConfig {
  static readonly addBoundingBoxConfigParams = {
    config: {
      left: 60,
      top: 60,
      height: 100,
      width: 100,
      strokeWidth: 2,
      stroke: 'black',
      originX: 'center',
      originY: 'center'
    }
  };

  static readonly buttonStyles = { 'font-weight': 'bold' };

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = true;
  public hasHiddenSelectionControls = true;
  public hasZoom = false;
  public isWideCanvas = true;
  public toolsFlex = '0 1 13%';
  public actionsFlex = '0 1 20%';
  public hasPan = false;
  public resizeCanvasToBackgroundImage = true;
  public isCenterBackgroundImage = true;
  public hasShortcutsExpansionPanel = true;
  public hasCloneShortcut = true;
  public hasRemoveShortcut = true;
  public hasRemoveAllShortcut = true;
  public hasSendBackwardsShortcut = true;
  public hasBringForwardShortcut = true;

  // FontAwesome Icons
  public faClone = faClone;
  public faTrash = faTrash;
  public faTrashAlt = faTrashAlt;
  public faArrowUp = faArrowUp;
  public faArrowDown = faArrowDown;
  public faAlignLeft = faAlignLeft;
  public faAlignCenter = faAlignCenter;
  public faAlignRight = faAlignRight;

  constructor() {
    this.tools = [
      {
        name: 'config',
        isHidden: true,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Product',
        isHidden: false,
        iconDisplayText: 'Product',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#3cb44b',
            fill: '#3cb44b' + '88'
          },
          additionalProperties: {
            objectType: 'product'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#3cb44b'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Primary Logo',
        isHidden: false,
        iconDisplayText: 'Primary Logo',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#e6194b',
            fill: '#e6194b' + '88'
          },
          additionalProperties: {
            objectType: 'primary_logo'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#e6194b'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Secondary Logo',
        isHidden: false,
        iconDisplayText: 'Secondary Logo',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#f032e6',
            fill: '#f032e6' + '88'
          },
          additionalProperties: {
            objectType: 'secondary_logo'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#f032e6'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Lockup',
        isHidden: false,
        iconDisplayText: 'Lockup',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#bcf60c',
            fill: '#bcf60c' + '88'
          },
          additionalProperties: {
            objectType: 'lockup'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#bcf60c',
          'text-shadow': '0px 0.7px #ffffff'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Headline Textbox',
        isHidden: false,
        iconDisplayText: 'Headline Textbox',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#4363d8',
            fill: '#4363d8' + '88'
          },
          additionalProperties: {
            objectType: 'headline_textbox'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#4363d8'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Other Textbox',
        isHidden: false,
        iconDisplayText: 'Other Textbox',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#f58231',
            fill: '#f58231' + '88'
          },
          additionalProperties: {
            objectType: 'other_textbox'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#f58231'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Graphic Accent',
        isHidden: false,
        iconDisplayText: 'Graphic Accent',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#000075',
            fill: '#000075' + '88'
          },
          additionalProperties: {
            objectType: 'graphic_accent'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#000075'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Photo Accent',
        isHidden: false,
        iconDisplayText: 'Photo Accent',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#46f0f0',
            fill: '#46f0f0' + '88'
          },
          additionalProperties: {
            objectType: 'photo_accent'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#46f0f0'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Button',
        isHidden: false,
        iconDisplayText: 'Button',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#008080',
            fill: '#008080' + '88'
          },
          additionalProperties: {
            objectType: 'button'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#008080'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Banner',
        isHidden: false,
        iconDisplayText: 'Banner',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#ffe119',
            fill: '#ffe119' + '88'
          },
          additionalProperties: {
            objectType: 'banner'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#ffe119',
          'text-shadow': '0px 0.7px #ffffff'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Sticker',
        isHidden: false,
        iconDisplayText: 'Sticker',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#fabebe',
            fill: '#fabebe' + '88'
          },
          additionalProperties: {
            objectType: 'sticker'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#fabebe'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'Frame',
        isHidden: false,
        iconDisplayText: 'Frame',
        onSelect: VisualEditorService.addBoundingBox,
        onSelectParams: {
          config: {
            ...TemplateAnnotatorConfig.addBoundingBoxConfigParams.config,
            stroke: '#e6beff',
            fill: '#e6beff' + '88'
          },
          additionalProperties: {
            objectType: 'frame'
          }
        },
        flex: '33%',
        style: {
          ...TemplateAnnotatorConfig.buttonStyles,
          'background-color': '#e6beff'
        },
        sections: [
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'edit shape',
        isHidden: true,
        objectType: ObjectTypes.SHAPE,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Bring Front',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '33%'
              },
              {
                label: 'Send Back',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Clone',
                type: ActionTypes.BUTTON,
                position: 'RHS',
                iconClass: faClone,
                action: VisualEditorService.cloneObject,
                flex: '33%'
              },
              {
                label: 'Remove',
                type: ActionTypes.BUTTON,
                position: 'RHS',
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteAllSelections,
                flex: '33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '33%'
              }
            ],
            isEndSection: true
          }
        ]
      },
      {
        name: 'edit group',
        isHidden: true,
        objectType: ObjectTypes.GROUP,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Align Left',
                type: ActionTypes.BUTTON,
                iconClass: faAlignLeft,
                action: VisualEditorService.alignSelectionLeft,
                flex: '0 3 33%'
              },
              {
                label: 'Align Horizontal Center',
                type: ActionTypes.BUTTON,
                iconClass: faAlignCenter,
                action: VisualEditorService.alignSelectionHorizontalCenter,
                flex: '0 3 33%'
              },
              {
                label: 'Align Right',
                type: ActionTypes.BUTTON,
                iconClass: faAlignRight,
                action: VisualEditorService.alignSelectionRight,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Align Top',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_top',
                action: VisualEditorService.alignSelectionTop,
                flex: '0 3 33%'
              },
              {
                label: 'Align Vertical Center',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_center',
                action: VisualEditorService.alignSelectionVerticalCenter,
                flex: '0 3 33%'
              },
              {
                label: 'Align Bottom',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_bottom',
                action: VisualEditorService.alignSelectionBottom,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Bring Front',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'Send Back',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteSelectionGroup,
                flex: '0 3 33%'
              },
              {
                label: 'Submit',
                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                action: VisualEditorService.transformCanvas,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      }
    ];
  }
}
