import * as _ from 'lodash';

import { VisualEditorService } from '../services/visual-editor.service';
import { ColorPickerDialogComponent } from '../components/color-picker-dialog/color-picker-dialog.component';
import {
  faFont,
  faTrashAlt,
  faBold,
  faAlignRight,
  faFillDrip,
  faPen,
  faAlignLeft,
  faItalic,
  faUnderline,
  faAlignCenter,
  faRetweet,
  faArrowUp,
  faArrowDown
} from '@fortawesome/free-solid-svg-icons';

import { ActionTypes, ObjectTypes } from './action-types';

export class DesignEditorConfig {
  static readonly defaultFontFamily = 'Times New Roman';
  static readonly availableFontFamilies = [
    DesignEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
  ];
  static readonly defaultFontSize = 34;
  static readonly availableFontSizes = _.range(8, 50 + 1);
  static readonly defaultLineHeight = 1;
  static readonly availableLineHeights = _.range(0, 11, 0.5);
  static readonly defaultCharSpacing = 0;
  static readonly availableCharSpacings = _.range(0, 801, 100);
  static readonly addTextConfigParams = {
    text: '<text>',
    config: {
      left: 10,
      top: 10,
      fontFamily: DesignEditorConfig.defaultFontFamily,
      fontSize: DesignEditorConfig.defaultFontSize,
      cornerColor: '#5c59f0',
      cornerSize: 8,
      transparentCorners: false
    }
  };

  static readonly defaultStrokeWidth = 0;
  static readonly defaultLineStrokeWidth = 5;
  static readonly availableStrokeWidths = _.range(0, 50 + 1);

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = false;
  public hasHiddenSelectionControls = true;
  public hasZoom = true;
  public hasPan = true;
  public isWideCanvas = true;
  public isShowFrame = true;
  public toolsFlex = '0 1 13%';
  public actionsFlex = '0 1 20%';
  public hasShortcutsExpansionPanel = true;
  public hasCloneShortcut = false;
  public hasRemoveShortcut = true;
  public hasRemoveAllShortcut = true;
  public hasBringForwardShortcut = true;
  public hasSendBackwardsShortcut = true;

  // FontAwesome Icons
  public faTrashAlt = faTrashAlt;
  public faFont = faFont;
  public faBold = faBold;
  public faAlignRight = faAlignRight;
  public faFillDrip = faFillDrip;
  public faPen = faPen;
  public faAlignLeft = faAlignLeft;
  public faItalic = faItalic;
  public faUnderline = faUnderline;
  public faAlignCenter = faAlignCenter;
  public faRetweet = faRetweet;
  public faArrowUp = faArrowUp;
  public faArrowDown = faArrowDown;

  constructor() {
    this.tools = [
      {
        name: 'config',
        isHidden: true,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ]
      },
      {
        name: 'edit shape',
        isHidden: true,
        objectType: ObjectTypes.SHAPE,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Fill color',
                type: ActionTypes.LABEL,
                flex: '3 3 67%'
              },
              {
                label: 'FILL COLOR',
                type: ActionTypes.DIALOG,
                iconClass: faFillDrip,
                dialog: {
                  component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setFillColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = {
                    'border-bottom': `3px solid ${val}`,
                    'padding-bottom': '2px'
                  };
                },
                iconStyle: {
                  'border-bottom': '3px solid black',
                  'padding-bottom': '2px'
                },
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Shadow blur',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW BLUR',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'blur',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow angle',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW ANGLE',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'angle',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow width',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW LENGTH',
                type: ActionTypes.RELATED_INPUT,
                groupName: 'SHADOW',
                keyName: 'length',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow color',
                type: ActionTypes.LABEL,
                flex: '3 3 67%'
              },
              {
                label: 'SHADOW COLOR',
                type: ActionTypes.DIALOG,
                iconClass: faPen,
                dialog: {
                  component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setShadowColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = {
                    'border-bottom': `3px solid ${val}`,
                    'padding-bottom': '2px'
                  };
                },
                iconStyle: {
                  'border-bottom': '3px solid black',
                  'padding-bottom': '2px'
                },
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          // NOTE: functionality turned off
          // {actions: [
          //   {
          //     label: 'Stroke width',
          //     type: ActionTypes.LABEL,
          //     flex: '75%'
          //   },
          //   {
          //     label: 'STROKE WIDTH',
          //     type: ActionTypes.DROPDOWN,
          //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
          //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
          //     iconClass: 'fas fa-font',
          //     action: VisualEditorService.setStrokeWidth,
          //     flex: '25%'
          //   }
          // ]},
          // {actions: [
          //   {
          //     label: 'Stroke color',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE COLOR',
          //     type: ActionTypes.DIALOG,
          //     iconClass: 'fas fa-pen',
          //     dialog: {
          //       component: ColorPickerDialogComponent,
          //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
          //       onClose: VisualEditorService.setStrokeColor
          //     },
          //     onActionReturn: (action, val) => {
          //       action.dialog.data.color = val;
          //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
          //     },
          //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
          //     flex: '33%'
          //   },
          // ]},
          {
            actions: [
              {
                label: 'BRING FRONT',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'SEND BACK',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'CLEAR SELECTION',
                type: ActionTypes.BUTTON,
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      },
      {
        name: 'edit line',
        isHidden: true,
        objectType: ObjectTypes.LINE,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Stroke width',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'STROKE WIDTH',
                type: ActionTypes.DROPDOWN,
                dropdownOption: DesignEditorConfig.availableStrokeWidths,
                dropdownSelectedOption:
                  DesignEditorConfig.defaultLineStrokeWidth,
                iconClass: faFont,
                action: VisualEditorService.setStrokeWidth,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Stroke color',
                type: ActionTypes.LABEL,
                flex: '3 3 67%'
              },
              {
                label: 'STROKE COLOR',
                type: ActionTypes.DIALOG,
                iconClass: faPen,
                dialog: {
                  component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setStrokeColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = {
                    'border-bottom': `3px solid ${val}`,
                    'padding-bottom': '2px'
                  };
                },
                iconStyle: {
                  'border-bottom': '3px solid black',
                  'padding-bottom': '2px'
                },
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'BRING FRONT',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'SEND BACK',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'CLEAR SELECTION',
                type: ActionTypes.BUTTON,
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      },
      {
        name: 'edit text',
        isHidden: true,
        objectType: ObjectTypes.TEXBOX,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Font Family',
                type: ActionTypes.DROPDOWN,
                dropdownOption: DesignEditorConfig.availableFontFamilies,
                dropdownSelectedOption: DesignEditorConfig.defaultFontFamily,
                iconClass: faFont,
                action: VisualEditorService.setFontFamily,
                flex: '0 1 75%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Font Size',
                type: ActionTypes.DROPDOWN,
                dropdownOption: DesignEditorConfig.availableFontSizes,
                dropdownSelectedOption: DesignEditorConfig.defaultFontSize,
                iconClass: faFont,
                action: VisualEditorService.setFontSize,
                flex: '0 1 75%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Line Height',
                type: ActionTypes.DROPDOWN,
                dropdownOption: DesignEditorConfig.availableLineHeights,
                dropdownSelectedOption: DesignEditorConfig.defaultLineHeight,
                action: VisualEditorService.setLineHeight,
                flex: '0 3 75%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Character Spacing',
                type: ActionTypes.DROPDOWN,
                dropdownOption: DesignEditorConfig.availableCharSpacings,
                dropdownSelectedOption: DesignEditorConfig.defaultCharSpacing,
                action: VisualEditorService.setCharacterSpacing,
                flex: '0 3 75%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Align Left',
                type: ActionTypes.BUTTON,
                iconClass: faAlignLeft,
                action: VisualEditorService.alignLeft,
                flex: '0 3 33%'
              },
              {
                label: 'Align Center',
                type: ActionTypes.BUTTON,
                iconClass: faAlignCenter,
                action: VisualEditorService.alignCenter,
                flex: '0 3 33%'
              },
              {
                label: 'Align Right',
                type: ActionTypes.BUTTON,
                iconClass: faAlignRight,
                action: VisualEditorService.alignRight,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Bold',
                type: ActionTypes.BUTTON,
                iconClass: faBold,
                action: VisualEditorService.setBold,
                flex: '0 3 33%'
              },
              {
                label: 'Italic',
                type: ActionTypes.BUTTON,
                iconClass: faItalic,
                action: VisualEditorService.setItalic,
                flex: '0 3 33%'
              },
              {
                label: 'Underline',
                type: ActionTypes.BUTTON,
                iconClass: faUnderline,
                action: VisualEditorService.setUnderline,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Font Color',
                type: ActionTypes.DIALOG,
                iconClass: faFont,
                dialog: {
                  component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setFillColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                },
                iconStyle: { 'border-bottom': '3px solid black' },
                flex: '0 3 33%'
              },
              {
                label: 'Bring Front',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'Send Back',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Remove',
                type: ActionTypes.BUTTON,
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      },
      {
        name: 'edit image',
        isHidden: true,
        objectType: ObjectTypes.IMAGE,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Shadow blur',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'Shadow Blur',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'blur',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow angle',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'Shadow Angle',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'angle',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow width',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'Shadow Length',
                type: ActionTypes.RELATED_INPUT,
                groupName: 'SHADOW',
                keyName: 'length',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow color',
                type: ActionTypes.LABEL,
                flex: '3 3 67%'
              },
              {
                label: 'Shadow Color',
                type: ActionTypes.DIALOG,
                iconClass: faPen,
                dialog: {
                  //component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setShadowColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = {
                    'border-bottom': `3px solid ${val}`,
                    'padding-bottom': '2px'
                  };
                },
                iconStyle: {
                  'border-bottom': '3px solid black',
                  'padding-bottom': '2px'
                },
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          // NOTE: functionality turned off
          // {actions: [
          //   {
          //     label: 'Stroke width',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE WIDTH',
          //     type: ActionTypes.DROPDOWN,
          //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
          //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
          //     iconClass: 'fas fa-font',
          //     action: VisualEditorService.setStrokeWidth,
          //     flex: '67%'
          //   }
          // ]},
          // {actions: [
          //   {
          //     label: 'Stroke color',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE COLOR',
          //     type: ActionTypes.DIALOG,
          //     iconClass: 'fas fa-pen',
          //     dialog: {
          //       component: ColorPickerDialogComponent,
          //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
          //       onClose: VisualEditorService.setStrokeColor
          //     },
          //     onActionReturn: (action, val) => {
          //       action.dialog.data.color = val;
          //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
          //     },
          //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
          //     flex: '33%'
          //   },
          // ]},
          {
            actions: [
              {
                label: 'BRING FRONT',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'SEND BACK',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'CLEAR SELECTION',
                type: ActionTypes.BUTTON,
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      },
      {
        name: 'edit product',
        isHidden: true,
        objectType: ObjectTypes.PRODUCT,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Shadow blur',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW BLUR',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'blur',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow angle',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW ANGLE',
                type: ActionTypes.RELATED_INPUT,
                keyName: 'angle',
                groupName: 'SHADOW',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow width',
                type: ActionTypes.LABEL,
                flex: '3 3 75%'
              },
              {
                label: 'SHADOW LENGTH',
                type: ActionTypes.RELATED_INPUT,
                groupName: 'SHADOW',
                keyName: 'length',
                inputValue: 0,
                inputType: 'number',
                iconClass: faFont,
                action: VisualEditorService.setShadow,
                flex: '0 3 25%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Shadow color',
                type: ActionTypes.LABEL,
                flex: '3 3 67%'
              },
              {
                label: 'SHADOW COLOR',
                type: ActionTypes.DIALOG,
                iconClass: faPen,
                dialog: {
                  component: ColorPickerDialogComponent,
                  data: {
                    color: '#000000',
                    titleText: 'SET COLOR',
                    buttonText: 'SET'
                  },
                  onClose: VisualEditorService.setShadowColor
                },
                onActionReturn: (action, val) => {
                  action.dialog.data.color = val;
                  action.iconStyle = {
                    'border-bottom': `3px solid ${val}`,
                    'padding-bottom': '2px'
                  };
                },
                iconStyle: {
                  'border-bottom': '3px solid black',
                  'padding-bottom': '2px'
                },
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'BRING FRONT',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'SEND BACK',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'CLEAR SELECTION',
                type: ActionTypes.BUTTON,
                iconClass: faTrashAlt,
                action: VisualEditorService.deleteSelection,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      },
      {
        name: 'edit group',
        isHidden: true,
        objectType: ObjectTypes.GROUP,
        canvasConfigs: [
          canvas => {
            canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
            return canvas;
          }
        ],
        sections: [
          {
            actions: [
              {
                label: 'Align Left',
                type: ActionTypes.BUTTON,
                iconClass: faAlignLeft,
                action: VisualEditorService.alignSelectionLeft,
                flex: '0 3 33%'
              },
              {
                label: 'Align Horizontal Center',
                type: ActionTypes.BUTTON,
                iconClass: faAlignCenter,
                action: VisualEditorService.alignSelectionHorizontalCenter,
                flex: '0 3 33%'
              },
              {
                label: 'Align Right',
                type: ActionTypes.BUTTON,
                iconClass: faAlignRight,
                action: VisualEditorService.alignSelectionRight,
                flex: '0 3 33%'
              }
            ]
          },
          {
            actions: [
              {
                label: 'Align Top',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_top',
                action: VisualEditorService.alignSelectionTop,
                flex: '0 3 33%'
              },
              {
                label: 'Align Vertical Center',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_center',
                action: VisualEditorService.alignSelectionVerticalCenter,
                flex: '0 3 33%'
              },
              {
                label: 'Align Bottom',
                type: ActionTypes.BUTTON,
                iconMaterial: 'vertical_align_bottom',
                action: VisualEditorService.alignSelectionBottom,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Bring Front',
                type: ActionTypes.BUTTON,
                iconClass: faArrowUp,
                action: VisualEditorService.bringForward,
                flex: '0 3 33%'
              },
              {
                label: 'Send Back',
                type: ActionTypes.BUTTON,
                iconClass: faArrowDown,
                action: VisualEditorService.sendBackwards,
                flex: '0 3 33%'
              }
            ],
            isEndSection: true
          },
          {
            actions: [
              {
                label: 'Remove All',
                type: ActionTypes.BUTTON,
                action: VisualEditorService.deleteSelectionGroup,
                flex: '0 3 33%'
              }
            ]
          }
        ]
      }
    ];
  }
}
