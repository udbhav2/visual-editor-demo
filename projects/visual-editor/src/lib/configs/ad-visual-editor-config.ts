import * as _ from 'lodash';

import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';

export class AdVisualEditorConfig {
  static readonly defaultFontFamily = 'Times New Roman';
  static readonly availableFontFamilies = [
    AdVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
  ];
  static readonly defaultFontSize = 34;
  static readonly availableFontSizes = _.range(8, 50 + 1);
  static readonly addTextConfigParams = {
    text: '<text>',
    config: {
      left: 10,
      top: 10,
      fontFamily: AdVisualEditorConfig.defaultFontFamily,
      fontSize: AdVisualEditorConfig.defaultFontSize,
      cornerColor: '#5c59f0',
      cornerSize: 8,
      transparentCorners: false
    }
  };
  static readonly defaultStrokeWidth = 0;
  static readonly defaultLineStrokeWidth = 5;
  static readonly availableStrokeWidths = _.range(0, 50 + 1);
  static readonly addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: {left: 10, top: 10, stroke: '#000000', strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous'}};

  static readonly addRectangleConfigParams = {
    config: {left: 10, top: 10, height: 100, width: 100, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black'}
  };
  static readonly addLineConfigParams = {
    config: {left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultLineStrokeWidth, stroke: 'black'},
    coords: [0, 0, 100, 100]
  };
  static readonly addCircleConfigParams = {
    config: {left: 10, top: 10, radius: 50, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black'}
  };
  static readonly addTriangleConfigParams = {
    config: {left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black'},
    coords: [{x: 50, y: 0}, {x: 0, y: 86}, {x: 100, y: 86}]
  };

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = false;
  public hasHiddenSelectionControls = true;
  public hasZoom = true;
  public hasPan = true;
  public isWideCanvas = true;
  public isShowFrame = true;
  public toolsFlex = '0 2 10%';
  public actionsFlex = '0 2 20%';

  constructor() {
    this.tools = [
      {
        name: 'config',
        isHidden: true,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }]
      },
      {
        name: 'add shape',
        iconImgUrl: '@app/../assets/images/add-shape-icon.png',
        iconDisplayText: 'ADD SHAPE',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'ADD RECTANGLE',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/shape-rectangle-icon.png',
              action: VisualEditorService.addRectangle,
              onSelectParams: AdVisualEditorConfig.addRectangleConfigParams,
              flex: '0 3 33%'
            },
            {
              label: 'Rectangle',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            }
          ]},
          {actions: [
            {
              label: 'ADD LINE',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/shape-line-icon.png',
              action: VisualEditorService.addLine,
              onSelectParams: AdVisualEditorConfig.addLineConfigParams,
              flex: '0 3 33%'
            },
            {
              label: 'Line',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            }
          ]},
          {actions: [
            {
              label: 'ADD ELLIPSE',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/shape-ellipse-icon.png',
              action: VisualEditorService.addCircle,
              onSelectParams: AdVisualEditorConfig.addCircleConfigParams,
              flex: '0 3 33%'
            },
            {
              label: 'Ellipse',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            }
          ]},
          {actions: [
            {
              label: 'ADD TRIANGLE',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/shape-triangle-icon.png',
              action: VisualEditorService.addTriangle,
              onSelectParams: AdVisualEditorConfig.addTriangleConfigParams,
              flex: '33%'
            },
            {
              label: 'Triangle',
              type: ActionTypes.LABEL,
              flex: '67%'
            }
          ]}
        ]
      },
      {
        name: 'edit shape',
        isHidden: true,
        objectType: ObjectTypes.SHAPE,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'Fill color',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
            {
              label: 'FILL COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-fill-drip',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setFillColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
              },
              iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
              flex: '0 3 33%'
            },
          ], isEndSection: true},
          {actions: [
            {
              label: 'Shadow blur',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW BLUR',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'blur',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow angle',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW ANGLE',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'angle',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow width',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW LENGTH',
              type: ActionTypes.RELATED_INPUT,
              groupName: 'SHADOW',
              keyName: 'length',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow color',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
            {
              label: 'SHADOW COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-pen',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setShadowColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
              },
              iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
              flex: '0 3 33%'
            },
          ], isEndSection: true},
          // NOTE: functionality turned off
          // {actions: [
          //   {
          //     label: 'Stroke width',
          //     type: ActionTypes.LABEL,
          //     flex: '75%'
          //   },
          //   {
          //     label: 'STROKE WIDTH',
          //     type: ActionTypes.DROPDOWN,
          //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
          //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
          //     iconClass: 'fas fa-font',
          //     action: VisualEditorService.setStrokeWidth,
          //     flex: '25%'
          //   }
          // ]},
          // {actions: [
          //   {
          //     label: 'Stroke color',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE COLOR',
          //     type: ActionTypes.DIALOG,
          //     iconClass: 'fas fa-pen',
          //     dialog: {
          //       component: ColorPickerDialogComponent,
          //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
          //       onClose: VisualEditorService.setStrokeColor
          //     },
          //     onActionReturn: (action, val) => {
          //       action.dialog.data.color = val;
          //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
          //     },
          //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
          //     flex: '33%'
          //   },
          // ]},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ],
      },
      {
        name: 'edit line',
        isHidden: true,
        objectType: ObjectTypes.LINE,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'Stroke width',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'STROKE WIDTH',
              type: ActionTypes.DROPDOWN,
              dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
              dropdownSelectedOption: AdVisualEditorConfig.defaultLineStrokeWidth,
              iconClass: 'fas fa-font',
              action: VisualEditorService.setStrokeWidth,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Stroke color',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
            {
              label: 'STROKE COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-pen',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setStrokeColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
              },
              iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
              flex: '0 3 33%'
            },
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ],
      },
      {
        name: 'add text',
        iconImgUrl: '@app/../assets/images/add-text-icon.png',
        iconDisplayText: 'ADD TEXT',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        onSelect: VisualEditorService.addText,
        onSelectParams: AdVisualEditorConfig.addTextConfigParams,
      },
      {
        name: 'edit text',
        isHidden: true,
        objectType: ObjectTypes.TEXBOX,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'FONT FAMILY',
              type: ActionTypes.DROPDOWN,
              dropdownOption: AdVisualEditorConfig.availableFontFamilies,
              dropdownSelectedOption: AdVisualEditorConfig.defaultFontFamily,
              iconClass: 'fas fa-font',
              action: VisualEditorService.setFontFamily,
              flex: '0 3 100%'
            }
          ]},
          {actions: [
            {
              label: 'FONT COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-font',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setFillColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`};
              },
              iconStyle: {'border-bottom': '3px solid black'},
              flex: '0 3 33%'
            },
            {
              label: 'FONT SIZE',
              type: ActionTypes.DROPDOWN,
              dropdownOption: AdVisualEditorConfig.availableFontSizes,
              dropdownSelectedOption: AdVisualEditorConfig.defaultFontSize,
              iconClass: 'fas fa-font',
              action: VisualEditorService.setFontSize,
              flex: '3 3 67%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'ALIGN LEFT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-left',
              action: VisualEditorService.alignLeft,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN CENTER',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-center',
              action: VisualEditorService.alignCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN RIGHT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-right',
              action: VisualEditorService.alignRight,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BOLD',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-bold',
              action: VisualEditorService.setBold,
              flex: '0 3 33%'
            },
            {
              label: 'ITALIC',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-italic',
              action: VisualEditorService.setItalic,
              flex: '0 3 33%'
            },
            {
              label: 'UNDERLINE',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-underline',
              action: VisualEditorService.setUnderline,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ],
      },
      {
        name: 'add image',
        iconImgUrl: '@app/../assets/images/add-image-icon.png',
        iconDisplayText: 'ADD IMAGE',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        type: ActionTypes.UPLOAD,
        onSelect: VisualEditorService.addImage,
        onSelectParams: AdVisualEditorConfig.addImageConfigParams,
      },
      {
        name: 'edit image',
        isHidden: true,
        objectType: ObjectTypes.IMAGE,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'Shadow blur',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW BLUR',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'blur',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow angle',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW ANGLE',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'angle',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow width',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW LENGTH',
              type: ActionTypes.RELATED_INPUT,
              groupName: 'SHADOW',
              keyName: 'length',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow color',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
            {
              label: 'SHADOW COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-pen',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setShadowColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
              },
              iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
              flex: '0 3 33%'
            },
          ], isEndSection: true},
          // NOTE: functionality turned off
          // {actions: [
          //   {
          //     label: 'Stroke width',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE WIDTH',
          //     type: ActionTypes.DROPDOWN,
          //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
          //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
          //     iconClass: 'fas fa-font',
          //     action: VisualEditorService.setStrokeWidth,
          //     flex: '67%'
          //   }
          // ]},
          // {actions: [
          //   {
          //     label: 'Stroke color',
          //     type: ActionTypes.LABEL,
          //     flex: '67%'
          //   },
          //   {
          //     label: 'STROKE COLOR',
          //     type: ActionTypes.DIALOG,
          //     iconClass: 'fas fa-pen',
          //     dialog: {
          //       component: ColorPickerDialogComponent,
          //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
          //       onClose: VisualEditorService.setStrokeColor
          //     },
          //     onActionReturn: (action, val) => {
          //       action.dialog.data.color = val;
          //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
          //     },
          //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
          //     flex: '33%'
          //   },
          // ]},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ]
      },
      {
        name: 'edit product',
        isHidden: true,
        objectType: ObjectTypes.PRODUCT,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'CHANGE PRODUCT',
              type: ActionTypes.OUTGOING_EVENT_TRIGGER,
              iconClass: 'fas fa-retweet',
              flex: '0 3 33%'
            },
            {
              label: 'Change product',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
          ], isEndSection: true},
          {actions: [
            {
              label: 'Shadow blur',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW BLUR',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'blur',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow angle',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW ANGLE',
              type: ActionTypes.RELATED_INPUT,
              keyName: 'angle',
              groupName: 'SHADOW',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow width',
              type: ActionTypes.LABEL,
              flex: '3 3 75%'
            },
            {
              label: 'SHADOW LENGTH',
              type: ActionTypes.RELATED_INPUT,
              groupName: 'SHADOW',
              keyName: 'length',
              inputValue: 0,
              inputType: 'number',
              iconClass: 'fas fa-font',
              action: VisualEditorService.setShadow,
              flex: '0 3 25%'
            }
          ]},
          {actions: [
            {
              label: 'Shadow color',
              type: ActionTypes.LABEL,
              flex: '3 3 67%'
            },
            {
              label: 'SHADOW COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-pen',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setShadowColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
              },
              iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
              flex: '0 3 33%'
            },
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ]
      },
      {
        name: 'edit group',
        isHidden: true,
        objectType: ObjectTypes.GROUP,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'ALIGN LEFT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-left-icon.png',
              action: VisualEditorService.alignSelectionLeft,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN HORIZONTAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
              action: VisualEditorService.alignSelectionHorizontalCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN RIGHT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-right-icon.png',
              action: VisualEditorService.alignSelectionRight,
              flex: '0 3 33%'
            }
          ]},
          {actions: [
            {
              label: 'ALIGN TOP',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-top-icon.png',
              action: VisualEditorService.alignSelectionTop,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN VERTICAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
              action: VisualEditorService.alignSelectionVerticalCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN BOTTOM',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-bottom-icon.png',
              action: VisualEditorService.alignSelectionBottom,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelectionGroup,
              flex: '0 3 33%'
            }
          ]}
        ],
      }
    ];
  }
}
