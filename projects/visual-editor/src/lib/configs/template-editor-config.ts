import * as _ from 'lodash';

import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';


export class TemplateEditorConfig {
  static readonly addRectangleConfigParams = {
    config: {
      left: 60, top: 60,
      height: 100, width: 100,
      strokeWidth: 2, stroke: 'black',
      originX: 'center', originY: 'center',
      fill: 'rgba(0,0,0,0)'
    }
  };

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = true;
  public hasHiddenSelectionControls = true;
  public hasZoom = false;
  public hasPan = false;
  public toolsFlex = '0 1 20%';
  public actionsFlex = '0 1 20%';

  constructor() {
    this.tools = [
      {
        name: 'config',
        isHidden: true,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }]
      },
      {
        name: 'add placeholder',
        iconImgUrl: '@app/../assets/images/add-shape-icon.png', 
        iconDisplayText: 'ADD PLACEHOLDER', 
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'ADD PRODUCT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-square',
              iconStyle: {color: '#5c59f0'},
              action: VisualEditorService.addRectangle,
              onSelectParams: {config: {
                ...TemplateEditorConfig.addRectangleConfigParams.config,
                stroke: '#5c59f0'
              }},
              flex: '33%'
            },
            {
              label: 'Product',
              type: ActionTypes.LABEL,
              flex: '67%'
            }
          ]},
          {actions: [
            {
              label: 'ADD ACCENT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-square',
              iconStyle: {color: '#2bc4b6'},
              action: VisualEditorService.addRectangle,
              onSelectParams: {config: {
                ...TemplateEditorConfig.addRectangleConfigParams.config,
                stroke: '#2bc4b6'
              }},
              flex: '33%'
            },
            {
              label: 'Accent',
              type: ActionTypes.LABEL,
              flex: '67%'
            }
          ]}
        ]
      },
      {
        name: 'edit shape',
        isHidden: true,
        objectType: ObjectTypes.SHAPE,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'SET PRODUCT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-square',
              iconStyle: {color: '#5c59f0'},
              action: VisualEditorService.setStrokeColor,
              onSelectParams: '#5c59f0',
              flex: '33%'
            },
            {
              label: 'Product',
              type: ActionTypes.LABEL,
              flex: '67%'
            }
          ]},
          {actions: [
            {
              label: 'SET ACCENT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-square',
              iconStyle: {color: '#2bc4b6'},
              action: VisualEditorService.setStrokeColor,
              onSelectParams: '#2bc4b6',
              flex: '33%'
            },
            {
              label: 'Accent',
              type: ActionTypes.LABEL,
              flex: '67%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '33%'
            }
          ]}
        ],
      },
      {
        name: 'edit group',
        isHidden: true,
        objectType: ObjectTypes.GROUP,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'ALIGN LEFT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-left-icon.png',
              action: VisualEditorService.alignSelectionLeft,
              flex: '33%'
            },
            {
              label: 'ALIGN HORIZONTAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
              action: VisualEditorService.alignSelectionHorizontalCenter,
              flex: '33%'
            },
            {
              label: 'ALIGN RIGHT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-right-icon.png',
              action: VisualEditorService.alignSelectionRight,
              flex: '33%'
            }
          ]},
          {actions: [
            {
              label: 'ALIGN TOP',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-top-icon.png',
              action: VisualEditorService.alignSelectionTop,
              flex: '33%'
            },
            {
              label: 'ALIGN VERTICAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
              action: VisualEditorService.alignSelectionVerticalCenter,
              flex: '33%'
            },
            {
              label: 'ALIGN BOTTOM',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-bottom-icon.png',
              action: VisualEditorService.alignSelectionBottom,
              flex: '33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelectionGroup,
              flex: '33%'
            }
          ]}
        ],
      }
    ];
  }
}
