import { VisualEditorService } from '../services/visual-editor.service';

import { ActionTypes } from './action-types';

export class SegmentationVisualEditorConfig {
  static readonly colorBackground = '#cb0c93';
  static readonly colorForeground = '#2bc4b6';
  static readonly drawingBrushWidth = 4;

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = true;
  public hasHiddenSelectionControls = false;
  public hasZoom = false;
  public hasPan = false;
  public toolsFlex = '0 1 10%';
  public actionsFlex = '0 1 10%';

  constructor() {
    this.tools = [
      {
        name: 'draw',
        iconImgUrl: '@app/../assets/images/target_icon.png',
        iconDisplayText: 'POINTER',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: true });
          canvas.freeDrawingBrush.color = SegmentationVisualEditorConfig.colorBackground;
          canvas.freeDrawingBrush.width = SegmentationVisualEditorConfig.drawingBrushWidth;
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'TOGGLE',
              type: ActionTypes.TOGGLE,
              action: VisualEditorService.setColor,
              isOn: false,
              onValue: SegmentationVisualEditorConfig.colorForeground,
              offValue: SegmentationVisualEditorConfig.colorBackground,
              onLabel: 'Foreground',
              offLabel: 'Background',
              flex: '100%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'UNDO',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-undo-alt',
              action: VisualEditorService.undoDrawingTransparentBg,
              flex: '50%'
            },
            {
              label: 'CLEAR',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.clearDrawingTransparentBg,
              flex: '50%'
            }
          ]}
        ],
      }
    ];
  }
}
