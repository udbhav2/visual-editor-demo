import * as _ from 'lodash';

import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';

export class BannerVisualEditorConfig {
  static readonly defaultFontFamily = 'Times New Roman';
  static readonly availableFontFamilies = [
    BannerVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'mc-extra-lt',
    'mc-bold',
    'mc-book',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
  ];
  static readonly defaultFontSize = 34;
  static readonly availableFontSizes = _.range(8, 50 + 1);
  static readonly addTextConfigParams = {
    text: '<text>',
    config: {
      left: 10,
      top: 10,
      fontFamily: BannerVisualEditorConfig.defaultFontFamily,
      fontSize: BannerVisualEditorConfig.defaultFontSize,
      cornerColor: '#5c59f0',
      cornerSize: 8,
      transparentCorners: false
    }
  };
  static readonly defaultStrokeWidth = 0;
  static readonly defaultLineStrokeWidth = 5;
  static readonly availableStrokeWidths = _.range(0, 50 + 1);
  static readonly addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: {left: 10, top: 10, stroke: '#000000', strokeWidth: BannerVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous'}};

  public tools: any[];
  public isFirstToolSelected = true;
  public hasCheckersBg = false;
  public hasHiddenSelectionControls = true;
  public hasZoom = true;
  public hasPan = true;
  public isWideCanvas = true;
  public isShowFrame = true;
  public toolsFlex = '0 2 10%';
  public actionsFlex = '0 2 20%';

  constructor() {
    this.tools = [
      {
        name: 'config',
        isHidden: true,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }]
      },
      {
        name: 'add text',
        iconImgUrl: '@app/../assets/images/add-text-icon.png',
        iconDisplayText: 'ADD TEXT',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        onSelect: VisualEditorService.addText,
        onSelectParams: BannerVisualEditorConfig.addTextConfigParams,
      },
      {
        name: 'edit text',
        isHidden: true,
        objectType: ObjectTypes.TEXBOX,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'FONT FAMILY',
              type: ActionTypes.DROPDOWN,
              dropdownOption: BannerVisualEditorConfig.availableFontFamilies,
              dropdownSelectedOption: BannerVisualEditorConfig.defaultFontFamily,
              iconClass: 'fas fa-font',
              action: VisualEditorService.setFontFamily,
              flex: '0 3 100%'
            }
          ]},
          {actions: [
            {
              label: 'FONT COLOR',
              type: ActionTypes.DIALOG,
              iconClass: 'fas fa-font',
              dialog: {
                //component: ColorPickerDialogComponent,
                data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                onClose: VisualEditorService.setFillColor
              },
              onActionReturn: (action, val) => {
                action.dialog.data.color = val;
                action.iconStyle = {'border-bottom': `3px solid ${val}`};
              },
              iconStyle: {'border-bottom': '3px solid black'},
              flex: '0 3 33%'
            },
            {
              label: 'FONT SIZE',
              type: ActionTypes.DROPDOWN,
              dropdownOption: BannerVisualEditorConfig.availableFontSizes,
              dropdownSelectedOption: BannerVisualEditorConfig.defaultFontSize,
              iconClass: 'fas fa-font',
              action: VisualEditorService.setFontSize,
              flex: '3 3 67%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'ALIGN LEFT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-left',
              action: VisualEditorService.alignLeft,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN CENTER',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-center',
              action: VisualEditorService.alignCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN RIGHT',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-align-right',
              action: VisualEditorService.alignRight,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BOLD',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-bold',
              action: VisualEditorService.setBold,
              flex: '0 3 33%'
            },
            {
              label: 'ITALIC',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-italic',
              action: VisualEditorService.setItalic,
              flex: '0 3 33%'
            },
            {
              label: 'UNDERLINE',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-underline',
              action: VisualEditorService.setUnderline,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ],
      },
      {
        name: 'add image',
        iconImgUrl: '@app/../assets/images/add-image-icon.png',
        iconDisplayText: 'ADD IMAGE',
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        type: ActionTypes.UPLOAD,
        onSelect: VisualEditorService.addImage,
        onSelectParams: BannerVisualEditorConfig.addImageConfigParams,
      },
      {
        name: 'edit image',
        isHidden: true,
        objectType: ObjectTypes.IMAGE,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelection,
              flex: '0 3 33%'
            }
          ]}
        ]
      },
      {
        name: 'edit group',
        isHidden: true,
        objectType: ObjectTypes.GROUP,
        canvasConfigs: [(canvas) =>  {
          canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
          return canvas;
        }],
        sections: [
          {actions: [
            {
              label: 'ALIGN LEFT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-left-icon.png',
              action: VisualEditorService.alignSelectionLeft,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN HORIZONTAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
              action: VisualEditorService.alignSelectionHorizontalCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN RIGHT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-right-icon.png',
              action: VisualEditorService.alignSelectionRight,
              flex: '0 3 33%'
            }
          ]},
          {actions: [
            {
              label: 'ALIGN TOP',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-top-icon.png',
              action: VisualEditorService.alignSelectionTop,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN VERTICAL CENTER',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
              action: VisualEditorService.alignSelectionVerticalCenter,
              flex: '0 3 33%'
            },
            {
              label: 'ALIGN BOTTOM',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/align-bottom-icon.png',
              action: VisualEditorService.alignSelectionBottom,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'BRING FRONT',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/bring-front-icon.png',
              action: VisualEditorService.bringForward,
              flex: '0 3 33%'
            },
            {
              label: 'SEND BACK',
              type: ActionTypes.BUTTON,
              iconSrc: '@app/../assets/images/send-back-icon.png',
              action: VisualEditorService.sendBackwards,
              flex: '0 3 33%'
            }
          ], isEndSection: true},
          {actions: [
            {
              label: 'CLEAR SELECTION',
              type: ActionTypes.BUTTON,
              iconClass: 'fas fa-trash-alt',
              action: VisualEditorService.deleteSelectionGroup,
              flex: '0 3 33%'
            }
          ]}
        ],
      }
    ];
  }
}
