export enum ActionTypes  {
  BUTTON,
  TOGGLE,
  DROPDOWN,
  DIALOG,
  UPLOAD,
  LABEL,
  RELATED_INPUT,
  OUTGOING_EVENT_TRIGGER
}

export enum ObjectTypes  {
  TEXBOX = 'textbox',
  IMAGE = 'image',
  SHAPE = 'shape',
  RECT = 'rect',
  CIRCLE = 'circle',
  LINE = 'line',
  POLYGON = 'polygon',
  GROUP = 'group',
  PRODUCT = 'product',
}
