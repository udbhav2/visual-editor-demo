import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
declare const fabric: any;

interface ImageConfig {
  width: number;
  height: number;
  scaleFactor: number;
  xOffset: number;
  yOffset: number;
}

@Injectable({
  providedIn: 'root'
})
export class VisualEditorService {
  static objectTypeCount: {} = {
    product: 0,
    primary_logo: 0,
    secondary_logo: 0,
    lockup: 0,
    headline_textbox: 0,
    other_textbox: 0,
    graphic_accent: 0,
    photo_accent: 0,
    button: 0,
    banner: 0,
    sticker: 0,
    frame: 0
  };

  static setColor(canvas, color) {
    canvas.freeDrawingBrush.color = color;
  }

  static clearDrawingTransparentBg(canvas) {
    VisualEditorService.clearDrawing(canvas, 1); // NOTE: first object is the image itself
  }

  static clearDrawing(canvas, offset = 0) {
    canvas
      .getObjects()
      .slice(offset)
      .forEach(o => canvas.remove(o));
  }

  static undoDrawingTransparentBg(canvas) {
    VisualEditorService.undoDrawing(canvas, 1); // NOTE: first object is the image itself
  }

  static undoDrawing(canvas, offset = 0) {
    const objs = canvas.getObjects();
    if (objs.length > offset) {
      canvas.remove(objs[objs.length - 1]);
    }
  }

  static addText(canvas, params) {
    const { text, config } = params;
    const textbox = new fabric.Textbox(text, config);
    canvas.add(textbox);
    canvas.discardActiveObject();
  }

  static bringForward(canvas) {
    const selectedObject = canvas.getActiveObject();
    canvas.bringForward(selectedObject);
  }

  static sendBackwards(canvas) {
    const selectedObject = canvas.getActiveObject();
    canvas.sendBackwards(selectedObject);
  }

  static setBold(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set(
      'fontWeight',
      selectedObject.fontWeight === 'bold' ? 'normal' : 'bold'
    );
    canvas.requestRenderAll();
  }

  static setItalic(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set(
      'fontStyle',
      selectedObject.fontStyle === 'italic' ? 'normal' : 'italic'
    );
    canvas.requestRenderAll();
  }

  static setUnderline(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('underline', !selectedObject.underline);
    canvas.requestRenderAll();
  }

  static alignLeft(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('textAlign', 'left');
    canvas.requestRenderAll();
  }

  static alignCenter(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('textAlign', 'center');
    canvas.requestRenderAll();
  }

  static alignRight(canvas) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('textAlign', 'right');
    canvas.requestRenderAll();
  }

  static setFontFamily(canvas, fontFamily) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('fontFamily', fontFamily);
    canvas.requestRenderAll();
  }

  static setFillColor(canvas, color) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('fill', color);
    canvas.requestRenderAll();
    return color;
  }

  static setFontSize(canvas, fontSize) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('fontSize', fontSize);
    canvas.requestRenderAll();
  }
  
  static setLineHeight(canvas, lineHeight) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('lineHeight', lineHeight);
    canvas.requestRenderAll();
  }

  static setCharacterSpacing(canvas, characterSpacing) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('charSpacing', characterSpacing);
    canvas.requestRenderAll();
  }

  static addImage(canvas, params) {
    fabric.Image.fromURL(
      params.fileUrl,
      img => {
        if (params.scaling) {
          const scaleFactor =
            img.height > img.width
              ? (canvas.height * params.scaling) / img.height
              : (canvas.width * params.scaling) / img.width;
          img.set({
            scaleX: scaleFactor,
            scaleY: scaleFactor
          });
        }
        canvas.add(img);
        canvas.requestRenderAll();
      },
      params.config
    );

    canvas.discardActiveObject();
  }

  static updateImage(canvas, url, callback) {
    const selectedObject = canvas.getActiveObject();
    const { width, height, scaleX, scaleY } = selectedObject;
    selectedObject.setSrc(
      url,
      img => {
        const scaleFactor = _.min([
          (width * scaleX) / img.width,
          (height * scaleY) / img.height
        ]);
        img.set({
          scaleX: scaleFactor,
          scaleY: scaleFactor
        });
        canvas.requestRenderAll();
        callback();
      },
      { crossOrigin: 'anonymous' }
    );
  }

  static setStrokeColor(canvas, color) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('stroke', color);
    canvas.requestRenderAll();
    return color;
  }

  static setStrokeWidth(canvas, width) {
    const selectedObject = canvas.getActiveObject();
    selectedObject.set('strokeWidth', width);
    canvas.requestRenderAll();
  }

  static deleteSelection(canvas) {
    const selectedObject = canvas.getActiveObject();
    canvas.remove(selectedObject);
  }

  static deleteSelectionGroup(canvas) {
    const selectedObjects = canvas.getActiveObject();
    selectedObjects.forEachObject(o => canvas.remove(o));
    canvas.discardActiveObject();
  }

  static deleteAllSelections(canvas) {
    canvas.remove(...canvas.getObjects());

    VisualEditorService.resetObjectTypeCount;
  }

  static resetObjectTypeCount() {
    VisualEditorService.objectTypeCount = {
      product: 0,
      primary_logo: 0,
      secondary_logo: 0,
      lockup: 0,
      headline_textbox: 0,
      other_textbox: 0,
      graphic_accent: 0,
      photo_accent: 0,
      button: 0,
      banner: 0,
      sticker: 0,
      frame: 0
    };
  }

  static addRectangle(canvas, params) {
    const { config } = params;
    const rectangle = new fabric.Rect(config);
    canvas.add(rectangle);
    canvas.discardActiveObject();
  }

  static addBoundingBox(canvas, params) {
    const { config } = params;
    const rectangle = new fabric.Rect(config);
    canvas.add(rectangle);
    canvas.discardActiveObject();

    const objectType = params.additionalProperties.objectType;
    const objectId = VisualEditorService.createObjectId(objectType);
    VisualEditorService.extendObjectJson(rectangle, objectId, objectType);
  }

  static addLine(canvas, params) {
    const { config, coords } = params;
    const line = new fabric.Line(coords, config);
    canvas.add(line);
    canvas.discardActiveObject();
  }

  static addCircle(canvas, params) {
    const { config } = params;
    const circle = new fabric.Circle(config);
    canvas.add(circle);
    canvas.discardActiveObject();
  }

  static addTriangle(canvas, params) {
    const { config, coords } = params;
    const triangle = new fabric.Polygon(coords, config);
    canvas.add(triangle);
    canvas.discardActiveObject();
  }

  static cloneObject(canvas) {
    const selectedObject = canvas.getActiveObject();
    const clone = new fabric.Rect(selectedObject.toObject());
    clone.set({ left: 0, top: 0 });

    canvas.add(clone);

    VisualEditorService.extendObjectJson(
      clone,
      selectedObject.toObject()._oId,
      selectedObject.toObject().objectType
    );
  }

  static alignSelectionLeft(canvas) {
    VisualEditorService.alignSelection(canvas, 'left');
  }

  static alignSelectionHorizontalCenter(canvas) {
    VisualEditorService.alignSelection(canvas, 'horizontal-center');
  }

  static alignSelectionRight(canvas) {
    VisualEditorService.alignSelection(canvas, 'right');
  }

  static alignSelectionTop(canvas) {
    VisualEditorService.alignSelection(canvas, 'top');
  }

  static alignSelectionVerticalCenter(canvas) {
    VisualEditorService.alignSelection(canvas, 'vertical-center');
  }

  static alignSelectionBottom(canvas) {
    VisualEditorService.alignSelection(canvas, 'bottom');
  }

  static alignSelection(canvas, direction) {
    const group = canvas.getActiveObject();
    const groupBoundingRect = group.getBoundingRect(true);

    group.forEachObject(item => {
      const itemBoundingRect = item.getBoundingRect();

      switch (direction) {
        case 'left': {
          item.set({
            left:
              -groupBoundingRect.width / 2 + (item.left - itemBoundingRect.left)
          });
          break;
        }
        case 'horizontal-center': {
          item.set({
            left: item.left - itemBoundingRect.left - itemBoundingRect.width / 2
          });
          break;
        }
        case 'right': {
          item.set({
            left:
              item.left +
              (groupBoundingRect.width / 2 - itemBoundingRect.left) -
              itemBoundingRect.width
          });
          break;
        }
        case 'top': {
          item.set({
            top:
              -groupBoundingRect.height / 2 + (item.top - itemBoundingRect.top)
          });
          break;
        }
        case 'vertical-center': {
          item.set({
            top: item.top - itemBoundingRect.top - itemBoundingRect.height / 2
          });
          break;
        }
        case 'bottom': {
          item.set({
            top:
              item.top +
              (groupBoundingRect.height / 2 - itemBoundingRect.top) -
              itemBoundingRect.height
          });
          break;
        }
      }
    });

    group.forEachObject(item => {
      group.removeWithUpdate(item).addWithUpdate(item);
    });
    group.setCoords();
    canvas.requestRenderAll();
  }

  static getShadowOffset(length: number, angle: number, pos: string) {
    let x;
    let y;

    switch (pos) {
      case 'BOTTOM':
        x = 0;
        y = 10;
        break;
      case 'BOTTOM_LEFT':
        x = -6;
        y = 10;
        break;
      case 'BOTTOM_RIGHT':
        x = 6;
        y = 10;
        break;
      case 'TOP':
        x = 0;
        y = -10;
        break;
      case 'TOP_LEFT':
        x = -6;
        y = -10;
        break;
      case 'TOP_RIGHT':
        x = 6;
        y = -10;
        break;
      default:
        x = 0;
        y = 0;
        break;
    }

    return fabric.util.rotatePoint(
      new fabric.Point(x * length, y * length),
      new fabric.Point(0, 0),
      fabric.util.degreesToRadians(360 - angle)
    );
  }

  static setShadow(canvas, params) {
    const { angle, length, blur } = params;
    const selectedObject = canvas.getActiveObject();
    const shadowOffset = VisualEditorService.getShadowOffset(
      length,
      -angle,
      'BOTTOM'
    );

    const color =
      selectedObject.shadow && selectedObject.shadow.color
        ? selectedObject.shadow.color
        : 'rgba(0,0,0,0.5)';

    selectedObject.setShadow({
      ...selectedObject.shadow,
      color,
      blur,
      offsetX: shadowOffset.x,
      offsetY: shadowOffset.y,
      affectStroke: false
    });
    canvas.requestRenderAll();
  }

  static setShadowColor(canvas, color) {
    const selectedObject = canvas.getActiveObject();
    const alpha =
      selectedObject.shadow &&
      selectedObject.shadow.color &&
      fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
        ? fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
        : 0.5;
    const rgbaColor = fabric.Color.fromHex(color)
      .setAlpha(alpha)
      .toRgba();
    selectedObject.setShadow({
      ...selectedObject.shadow,
      color: rgbaColor,
      blur: 0.5
    });
    canvas.requestRenderAll();

    return color;
  }

  static transformCanvas(imgUrl, canvasObj): any {
    const backgroundPlaceholderObject = {
      src: 'background_placeholder',
      top: 300,
      _key: 'background_placeholder',
      fill: 'rgb(0,0,0)',
      left: 300,
      type: 'image',
      angle: 0,
      cropX: 0,
      cropY: 0,
      flipX: false,
      flipY: false,
      skewX: 0,
      skewY: 0,
      width: canvasObj.backgroundImage.width,
      clipTo: null,
      height: canvasObj.backgroundImage.height,
      scaleX: 1,
      scaleY: 1,
      shadow: null,
      stroke: null,
      filters: [],
      opacity: 1,
      originX: 'center',
      originY: 'center',
      version: '2.3.6',
      visible: true,
      fillRule: 'nonzero',
      paintFirst: 'fill',
      crossOrigin: '',
      strokeWidth: 0,
      strokeLineCap: 'butt',
      strokeLineJoin: 'miter',
      backgroundColor: '',
      strokeDashArray: null,
      transformMatrix: null,
      strokeMiterLimit: 4,
      globalCompositeOperation: 'source-over'
    };

    canvasObj.objects.forEach(o => {
      o.stroke = null;
      o.strokeWidth = 0;
      o.type = 'image';
      o.width = o.width * o.scaleX;
      o.height = o.height * o.scaleY;
      o.scaleX = 1;
      o.scaleY = 1;
      o.src = o._key

      delete o._oId;
      delete o.objectType;
    });
    canvasObj.objects.unshift(backgroundPlaceholderObject);
    canvasObj.backgroundImage.src = '';
    canvasObj.backgroundImage.crossOrigin = 'anonymous';
    canvasObj._originalImgUrl = imgUrl;

    const canvasJson = JSON.stringify(canvasObj, null, 2);
    console.log(canvasJson);
    return canvasJson;
  }

  static createObjectId(objectType: string) {
    const id = VisualEditorService.objectTypeCount[objectType];
    VisualEditorService.objectTypeCount[objectType] += 1;
    return id;
  }

  static extendObjectJson(obj, id, objectType) {
    obj.toObject = (function(toObject) {
      return function() {
        return fabric.util.object.extend(toObject.call(this), {
          _oId: id,
          _key: `${objectType}_placeholder_${id}`,
          objectType
        });
      };
    })(obj.toObject);
  }
}
