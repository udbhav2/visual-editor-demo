import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'lib-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  @Input() accept: string;
  @Input() multiple = false;
  @Output() selectedFiles = new EventEmitter<File[]>();
  @Output() selectedFilesWithCaller = new EventEmitter<{files: File[], caller: any}>();

  @ViewChild('fileInput') fileInput: ElementRef;

  caller: any;

  click(caller?) {
    if (caller) {
      this.caller = caller;
    }
    this.fileInput.nativeElement.click();
  }

  selected(event) {
    const files: File[] = event.target.files;
    if (this.caller) {
      this.selectedFilesWithCaller.emit({files, caller: this.caller});
    } else {
      this.selectedFiles.emit(files);
    }
  }
}
