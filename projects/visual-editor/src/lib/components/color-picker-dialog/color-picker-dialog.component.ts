import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-color-picker-dialog',
  templateUrl: './color-picker-dialog.component.html',
  styleUrls: ['./color-picker-dialog.component.scss']
})
export class ColorPickerDialogComponent implements OnInit {
  addAll = false;
  color = '#ffffff';
  titleText = 'ADD COLOR';
  buttonText = 'Add';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private colorPickerDialog: MatDialogRef<ColorPickerDialogComponent>
  ) {}

  ngOnInit() {
    if (this.data.color) {
      this.color = this.data.color;
    }
    if (this.data.titleText) {
      this.titleText = this.data.titleText;
    }
    if (this.data.buttonText) {
      this.buttonText = this.data.buttonText;
    }
    if (this.data.addAll) {
      this.addAll = this.data.addAll;
    }
  }

  onAdd(addAll): void {
    this.colorPickerDialog.close({
      addAll,
      colorHex: this.color
    });
  }

  onCancel(): void {
    this.colorPickerDialog.close();
  }
}
