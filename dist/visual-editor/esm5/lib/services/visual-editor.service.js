/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
import * as i0 from "@angular/core";
/**
 * @record
 */
function ImageConfig() { }
if (false) {
    /** @type {?} */
    ImageConfig.prototype.width;
    /** @type {?} */
    ImageConfig.prototype.height;
    /** @type {?} */
    ImageConfig.prototype.scaleFactor;
    /** @type {?} */
    ImageConfig.prototype.xOffset;
    /** @type {?} */
    ImageConfig.prototype.yOffset;
}
var VisualEditorService = /** @class */ (function () {
    function VisualEditorService() {
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    VisualEditorService.setColor = /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    function (canvas, color) {
        canvas.freeDrawingBrush.color = color;
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.clearDrawingTransparentBg = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.clearDrawing(canvas, 1); // NOTE: first object is the image itself
    };
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    VisualEditorService.clearDrawing = /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    function (canvas, offset) {
        if (offset === void 0) { offset = 0; }
        canvas
            .getObjects()
            .slice(offset)
            .forEach((/**
         * @param {?} o
         * @return {?}
         */
        function (o) { return canvas.remove(o); }));
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.undoDrawingTransparentBg = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.undoDrawing(canvas, 1); // NOTE: first object is the image itself
    };
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    VisualEditorService.undoDrawing = /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    function (canvas, offset) {
        if (offset === void 0) { offset = 0; }
        /** @type {?} */
        var objs = canvas.getObjects();
        if (objs.length > offset) {
            canvas.remove(objs[objs.length - 1]);
        }
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addText = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var text = params.text, config = params.config;
        /** @type {?} */
        var textbox = new fabric.Textbox(text, config);
        canvas.add(textbox);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.bringForward = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        canvas.bringForward(selectedObject);
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.sendBackwards = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        canvas.sendBackwards(selectedObject);
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.setBold = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('fontWeight', selectedObject.fontWeight === 'bold' ? 'normal' : 'bold');
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.setItalic = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('fontStyle', selectedObject.fontStyle === 'italic' ? 'normal' : 'italic');
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.setUnderline = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('underline', !selectedObject.underline);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignLeft = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'left');
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignCenter = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'center');
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignRight = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'right');
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} fontFamily
     * @return {?}
     */
    VisualEditorService.setFontFamily = /**
     * @param {?} canvas
     * @param {?} fontFamily
     * @return {?}
     */
    function (canvas, fontFamily) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('fontFamily', fontFamily);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    VisualEditorService.setFillColor = /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    function (canvas, color) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('fill', color);
        canvas.requestRenderAll();
        return color;
    };
    /**
     * @param {?} canvas
     * @param {?} fontSize
     * @return {?}
     */
    VisualEditorService.setFontSize = /**
     * @param {?} canvas
     * @param {?} fontSize
     * @return {?}
     */
    function (canvas, fontSize) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('fontSize', fontSize);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} lineHeight
     * @return {?}
     */
    VisualEditorService.setLineHeight = /**
     * @param {?} canvas
     * @param {?} lineHeight
     * @return {?}
     */
    function (canvas, lineHeight) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('lineHeight', lineHeight);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} characterSpacing
     * @return {?}
     */
    VisualEditorService.setCharacterSpacing = /**
     * @param {?} canvas
     * @param {?} characterSpacing
     * @return {?}
     */
    function (canvas, characterSpacing) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('charSpacing', characterSpacing);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addImage = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        fabric.Image.fromURL(params.fileUrl, (/**
         * @param {?} img
         * @return {?}
         */
        function (img) {
            if (params.scaling) {
                /** @type {?} */
                var scaleFactor = img.height > img.width
                    ? (canvas.height * params.scaling) / img.height
                    : (canvas.width * params.scaling) / img.width;
                img.set({
                    scaleX: scaleFactor,
                    scaleY: scaleFactor
                });
            }
            canvas.add(img);
            canvas.requestRenderAll();
        }), params.config);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @param {?} url
     * @param {?} callback
     * @return {?}
     */
    VisualEditorService.updateImage = /**
     * @param {?} canvas
     * @param {?} url
     * @param {?} callback
     * @return {?}
     */
    function (canvas, url, callback) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        var width = selectedObject.width, height = selectedObject.height, scaleX = selectedObject.scaleX, scaleY = selectedObject.scaleY;
        selectedObject.setSrc(url, (/**
         * @param {?} img
         * @return {?}
         */
        function (img) {
            /** @type {?} */
            var scaleFactor = _.min([
                (width * scaleX) / img.width,
                (height * scaleY) / img.height
            ]);
            img.set({
                scaleX: scaleFactor,
                scaleY: scaleFactor
            });
            canvas.requestRenderAll();
            callback();
        }), { crossOrigin: 'anonymous' });
    };
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    VisualEditorService.setStrokeColor = /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    function (canvas, color) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('stroke', color);
        canvas.requestRenderAll();
        return color;
    };
    /**
     * @param {?} canvas
     * @param {?} width
     * @return {?}
     */
    VisualEditorService.setStrokeWidth = /**
     * @param {?} canvas
     * @param {?} width
     * @return {?}
     */
    function (canvas, width) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        selectedObject.set('strokeWidth', width);
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.deleteSelection = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        canvas.remove(selectedObject);
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.deleteSelectionGroup = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObjects = canvas.getActiveObject();
        selectedObjects.forEachObject((/**
         * @param {?} o
         * @return {?}
         */
        function (o) { return canvas.remove(o); }));
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.deleteAllSelections = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        canvas.remove.apply(canvas, tslib_1.__spread(canvas.getObjects()));
        VisualEditorService.resetObjectTypeCount;
    };
    /**
     * @return {?}
     */
    VisualEditorService.resetObjectTypeCount = /**
     * @return {?}
     */
    function () {
        VisualEditorService.objectTypeCount = {
            product: 0,
            primary_logo: 0,
            secondary_logo: 0,
            lockup: 0,
            headline_textbox: 0,
            other_textbox: 0,
            graphic_accent: 0,
            photo_accent: 0,
            button: 0,
            banner: 0,
            sticker: 0,
            frame: 0
        };
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addRectangle = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var config = params.config;
        /** @type {?} */
        var rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addBoundingBox = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var config = params.config;
        /** @type {?} */
        var rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
        /** @type {?} */
        var objectType = params.additionalProperties.objectType;
        /** @type {?} */
        var objectId = VisualEditorService.createObjectId(objectType);
        VisualEditorService.extendObjectJson(rectangle, objectId, objectType);
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addLine = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var config = params.config, coords = params.coords;
        /** @type {?} */
        var line = new fabric.Line(coords, config);
        canvas.add(line);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addCircle = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var config = params.config;
        /** @type {?} */
        var circle = new fabric.Circle(config);
        canvas.add(circle);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.addTriangle = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var config = params.config, coords = params.coords;
        /** @type {?} */
        var triangle = new fabric.Polygon(coords, config);
        canvas.add(triangle);
        canvas.discardActiveObject();
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.cloneObject = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        /** @type {?} */
        var clone = new fabric.Rect(selectedObject.toObject());
        clone.set({ left: 0, top: 0 });
        canvas.add(clone);
        VisualEditorService.extendObjectJson(clone, selectedObject.toObject()._oId, selectedObject.toObject().objectType);
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionLeft = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'left');
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionHorizontalCenter = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'horizontal-center');
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionRight = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'right');
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionTop = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'top');
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionVerticalCenter = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'vertical-center');
    };
    /**
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorService.alignSelectionBottom = /**
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        VisualEditorService.alignSelection(canvas, 'bottom');
    };
    /**
     * @param {?} canvas
     * @param {?} direction
     * @return {?}
     */
    VisualEditorService.alignSelection = /**
     * @param {?} canvas
     * @param {?} direction
     * @return {?}
     */
    function (canvas, direction) {
        /** @type {?} */
        var group = canvas.getActiveObject();
        /** @type {?} */
        var groupBoundingRect = group.getBoundingRect(true);
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            /** @type {?} */
            var itemBoundingRect = item.getBoundingRect();
            switch (direction) {
                case 'left': {
                    item.set({
                        left: -groupBoundingRect.width / 2 + (item.left - itemBoundingRect.left)
                    });
                    break;
                }
                case 'horizontal-center': {
                    item.set({
                        left: item.left - itemBoundingRect.left - itemBoundingRect.width / 2
                    });
                    break;
                }
                case 'right': {
                    item.set({
                        left: item.left +
                            (groupBoundingRect.width / 2 - itemBoundingRect.left) -
                            itemBoundingRect.width
                    });
                    break;
                }
                case 'top': {
                    item.set({
                        top: -groupBoundingRect.height / 2 + (item.top - itemBoundingRect.top)
                    });
                    break;
                }
                case 'vertical-center': {
                    item.set({
                        top: item.top - itemBoundingRect.top - itemBoundingRect.height / 2
                    });
                    break;
                }
                case 'bottom': {
                    item.set({
                        top: item.top +
                            (groupBoundingRect.height / 2 - itemBoundingRect.top) -
                            itemBoundingRect.height
                    });
                    break;
                }
            }
        }));
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            group.removeWithUpdate(item).addWithUpdate(item);
        }));
        group.setCoords();
        canvas.requestRenderAll();
    };
    /**
     * @param {?} length
     * @param {?} angle
     * @param {?} pos
     * @return {?}
     */
    VisualEditorService.getShadowOffset = /**
     * @param {?} length
     * @param {?} angle
     * @param {?} pos
     * @return {?}
     */
    function (length, angle, pos) {
        /** @type {?} */
        var x;
        /** @type {?} */
        var y;
        switch (pos) {
            case 'BOTTOM':
                x = 0;
                y = 10;
                break;
            case 'BOTTOM_LEFT':
                x = -6;
                y = 10;
                break;
            case 'BOTTOM_RIGHT':
                x = 6;
                y = 10;
                break;
            case 'TOP':
                x = 0;
                y = -10;
                break;
            case 'TOP_LEFT':
                x = -6;
                y = -10;
                break;
            case 'TOP_RIGHT':
                x = 6;
                y = -10;
                break;
            default:
                x = 0;
                y = 0;
                break;
        }
        return fabric.util.rotatePoint(new fabric.Point(x * length, y * length), new fabric.Point(0, 0), fabric.util.degreesToRadians(360 - angle));
    };
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    VisualEditorService.setShadow = /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    function (canvas, params) {
        var angle = params.angle, length = params.length, blur = params.blur;
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        /** @type {?} */
        var shadowOffset = VisualEditorService.getShadowOffset(length, -angle, 'BOTTOM');
        /** @type {?} */
        var color = selectedObject.shadow && selectedObject.shadow.color
            ? selectedObject.shadow.color
            : 'rgba(0,0,0,0.5)';
        selectedObject.setShadow(tslib_1.__assign({}, selectedObject.shadow, { color: color,
            blur: blur, offsetX: shadowOffset.x, offsetY: shadowOffset.y, affectStroke: false }));
        canvas.requestRenderAll();
    };
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    VisualEditorService.setShadowColor = /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    function (canvas, color) {
        /** @type {?} */
        var selectedObject = canvas.getActiveObject();
        /** @type {?} */
        var alpha = selectedObject.shadow &&
            selectedObject.shadow.color &&
            fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            ? fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            : 0.5;
        /** @type {?} */
        var rgbaColor = fabric.Color.fromHex(color)
            .setAlpha(alpha)
            .toRgba();
        selectedObject.setShadow(tslib_1.__assign({}, selectedObject.shadow, { color: rgbaColor, blur: 0.5 }));
        canvas.requestRenderAll();
        return color;
    };
    /**
     * @param {?} imgUrl
     * @param {?} canvasObj
     * @return {?}
     */
    VisualEditorService.transformCanvas = /**
     * @param {?} imgUrl
     * @param {?} canvasObj
     * @return {?}
     */
    function (imgUrl, canvasObj) {
        /** @type {?} */
        var backgroundPlaceholderObject = {
            src: 'background_placeholder',
            top: 300,
            _key: 'background_placeholder',
            fill: 'rgb(0,0,0)',
            left: 300,
            type: 'image',
            angle: 0,
            cropX: 0,
            cropY: 0,
            flipX: false,
            flipY: false,
            skewX: 0,
            skewY: 0,
            width: canvasObj.backgroundImage.width,
            clipTo: null,
            height: canvasObj.backgroundImage.height,
            scaleX: 1,
            scaleY: 1,
            shadow: null,
            stroke: null,
            filters: [],
            opacity: 1,
            originX: 'center',
            originY: 'center',
            version: '2.3.6',
            visible: true,
            fillRule: 'nonzero',
            paintFirst: 'fill',
            crossOrigin: '',
            strokeWidth: 0,
            strokeLineCap: 'butt',
            strokeLineJoin: 'miter',
            backgroundColor: '',
            strokeDashArray: null,
            transformMatrix: null,
            strokeMiterLimit: 4,
            globalCompositeOperation: 'source-over'
        };
        canvasObj.objects.forEach((/**
         * @param {?} o
         * @return {?}
         */
        function (o) {
            o.stroke = null;
            o.strokeWidth = 0;
            o.type = 'image';
            o.width = o.width * o.scaleX;
            o.height = o.height * o.scaleY;
            o.scaleX = 1;
            o.scaleY = 1;
            o.src = o._key;
            delete o._oId;
            delete o.objectType;
        }));
        canvasObj.objects.unshift(backgroundPlaceholderObject);
        canvasObj.backgroundImage.src = '';
        canvasObj.backgroundImage.crossOrigin = 'anonymous';
        canvasObj._originalImgUrl = imgUrl;
        /** @type {?} */
        var canvasJson = JSON.stringify(canvasObj, null, 2);
        console.log(canvasJson);
        return canvasJson;
    };
    /**
     * @param {?} objectType
     * @return {?}
     */
    VisualEditorService.createObjectId = /**
     * @param {?} objectType
     * @return {?}
     */
    function (objectType) {
        /** @type {?} */
        var id = VisualEditorService.objectTypeCount[objectType];
        VisualEditorService.objectTypeCount[objectType] += 1;
        return id;
    };
    /**
     * @param {?} obj
     * @param {?} id
     * @param {?} objectType
     * @return {?}
     */
    VisualEditorService.extendObjectJson = /**
     * @param {?} obj
     * @param {?} id
     * @param {?} objectType
     * @return {?}
     */
    function (obj, id, objectType) {
        obj.toObject = ((/**
         * @param {?} toObject
         * @return {?}
         */
        function (toObject) {
            return (/**
             * @return {?}
             */
            function () {
                return fabric.util.object.extend(toObject.call(this), {
                    _oId: id,
                    _key: objectType + "_placeholder_" + id,
                    objectType: objectType
                });
            });
        }))(obj.toObject);
    };
    VisualEditorService.objectTypeCount = {
        product: 0,
        primary_logo: 0,
        secondary_logo: 0,
        lockup: 0,
        headline_textbox: 0,
        other_textbox: 0,
        graphic_accent: 0,
        photo_accent: 0,
        button: 0,
        banner: 0,
        sticker: 0,
        frame: 0
    };
    VisualEditorService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */ VisualEditorService.ngInjectableDef = i0.defineInjectable({ factory: function VisualEditorService_Factory() { return new VisualEditorService(); }, token: VisualEditorService, providedIn: "root" });
    return VisualEditorService;
}());
export { VisualEditorService };
if (false) {
    /** @type {?} */
    VisualEditorService.objectTypeCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlzdWFsLWVkaXRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sUUFBUSxDQUFDOzs7OztBQUdoQiwwQkFNQzs7O0lBTEMsNEJBQWM7O0lBQ2QsNkJBQWU7O0lBQ2Ysa0NBQW9COztJQUNwQiw4QkFBZ0I7O0lBQ2hCLDhCQUFnQjs7QUFHbEI7SUFBQTtLQXNoQkM7Ozs7OztJQW5nQlEsNEJBQVE7Ozs7O0lBQWYsVUFBZ0IsTUFBTSxFQUFFLEtBQUs7UUFDM0IsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFTSw2Q0FBeUI7Ozs7SUFBaEMsVUFBaUMsTUFBTTtRQUNyQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMseUNBQXlDO0lBQ3hGLENBQUM7Ozs7OztJQUVNLGdDQUFZOzs7OztJQUFuQixVQUFvQixNQUFNLEVBQUUsTUFBVTtRQUFWLHVCQUFBLEVBQUEsVUFBVTtRQUNwQyxNQUFNO2FBQ0gsVUFBVSxFQUFFO2FBQ1osS0FBSyxDQUFDLE1BQU0sQ0FBQzthQUNiLE9BQU87Ozs7UUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQWhCLENBQWdCLEVBQUMsQ0FBQztJQUNwQyxDQUFDOzs7OztJQUVNLDRDQUF3Qjs7OztJQUEvQixVQUFnQyxNQUFNO1FBQ3BDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyx5Q0FBeUM7SUFDdkYsQ0FBQzs7Ozs7O0lBRU0sK0JBQVc7Ozs7O0lBQWxCLFVBQW1CLE1BQU0sRUFBRSxNQUFVO1FBQVYsdUJBQUEsRUFBQSxVQUFVOztZQUM3QixJQUFJLEdBQUcsTUFBTSxDQUFDLFVBQVUsRUFBRTtRQUNoQyxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxFQUFFO1lBQ3hCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7Ozs7OztJQUVNLDJCQUFPOzs7OztJQUFkLFVBQWUsTUFBTSxFQUFFLE1BQU07UUFDbkIsSUFBQSxrQkFBSSxFQUFFLHNCQUFNOztZQUNkLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQztRQUNoRCxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRU0sZ0NBQVk7Ozs7SUFBbkIsVUFBb0IsTUFBTTs7WUFDbEIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsTUFBTSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUVNLGlDQUFhOzs7O0lBQXBCLFVBQXFCLE1BQU07O1lBQ25CLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFTSwyQkFBTzs7OztJQUFkLFVBQWUsTUFBTTs7WUFDYixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUNoQixZQUFZLEVBQ1osY0FBYyxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUN6RCxDQUFDO1FBQ0YsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFTSw2QkFBUzs7OztJQUFoQixVQUFpQixNQUFNOztZQUNmLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQ2hCLFdBQVcsRUFDWCxjQUFjLENBQUMsU0FBUyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQzVELENBQUM7UUFDRixNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVNLGdDQUFZOzs7O0lBQW5CLFVBQW9CLE1BQU07O1lBQ2xCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNELE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRU0sNkJBQVM7Ozs7SUFBaEIsVUFBaUIsTUFBTTs7WUFDZixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN4QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVNLCtCQUFXOzs7O0lBQWxCLFVBQW1CLE1BQU07O1lBQ2pCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQzFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRU0sOEJBQVU7Ozs7SUFBakIsVUFBa0IsTUFBTTs7WUFDaEIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDekMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRU0saUNBQWE7Ozs7O0lBQXBCLFVBQXFCLE1BQU0sRUFBRSxVQUFVOztZQUMvQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTSxnQ0FBWTs7Ozs7SUFBbkIsVUFBb0IsTUFBTSxFQUFFLEtBQUs7O1lBQ3pCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzFCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Ozs7O0lBRU0sK0JBQVc7Ozs7O0lBQWxCLFVBQW1CLE1BQU0sRUFBRSxRQUFROztZQUMzQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN6QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTSxpQ0FBYTs7Ozs7SUFBcEIsVUFBcUIsTUFBTSxFQUFFLFVBQVU7O1lBQy9CLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQzdDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7OztJQUVNLHVDQUFtQjs7Ozs7SUFBMUIsVUFBMkIsTUFBTSxFQUFFLGdCQUFnQjs7WUFDM0MsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUNwRCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTSw0QkFBUTs7Ozs7SUFBZixVQUFnQixNQUFNLEVBQUUsTUFBTTtRQUM1QixNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FDbEIsTUFBTSxDQUFDLE9BQU87Ozs7UUFDZCxVQUFBLEdBQUc7WUFDRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7O29CQUNaLFdBQVcsR0FDZixHQUFHLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxLQUFLO29CQUNwQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTTtvQkFDL0MsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUs7Z0JBQ2pELEdBQUcsQ0FBQyxHQUFHLENBQUM7b0JBQ04sTUFBTSxFQUFFLFdBQVc7b0JBQ25CLE1BQU0sRUFBRSxXQUFXO2lCQUNwQixDQUFDLENBQUM7YUFDSjtZQUNELE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxHQUNELE1BQU0sQ0FBQyxNQUFNLENBQ2QsQ0FBQztRQUVGLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7Ozs7SUFFTSwrQkFBVzs7Ozs7O0lBQWxCLFVBQW1CLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUTs7WUFDaEMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDdkMsSUFBQSw0QkFBSyxFQUFFLDhCQUFNLEVBQUUsOEJBQU0sRUFBRSw4QkFBTTtRQUNyQyxjQUFjLENBQUMsTUFBTSxDQUNuQixHQUFHOzs7O1FBQ0gsVUFBQSxHQUFHOztnQkFDSyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQztnQkFDeEIsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLEtBQUs7Z0JBQzVCLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxNQUFNO2FBQy9CLENBQUM7WUFDRixHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUNOLE1BQU0sRUFBRSxXQUFXO2dCQUNuQixNQUFNLEVBQUUsV0FBVzthQUNwQixDQUFDLENBQUM7WUFDSCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUMxQixRQUFRLEVBQUUsQ0FBQztRQUNiLENBQUMsR0FDRCxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsQ0FDN0IsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVNLGtDQUFjOzs7OztJQUFyQixVQUFzQixNQUFNLEVBQUUsS0FBSzs7WUFDM0IsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDcEMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDMUIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFFTSxrQ0FBYzs7Ozs7SUFBckIsVUFBc0IsTUFBTSxFQUFFLEtBQUs7O1lBQzNCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRU0sbUNBQWU7Ozs7SUFBdEIsVUFBdUIsTUFBTTs7WUFDckIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVNLHdDQUFvQjs7OztJQUEzQixVQUE0QixNQUFNOztZQUMxQixlQUFlLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUNoRCxlQUFlLENBQUMsYUFBYTs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBaEIsQ0FBZ0IsRUFBQyxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRU0sdUNBQW1COzs7O0lBQTFCLFVBQTJCLE1BQU07UUFDL0IsTUFBTSxDQUFDLE1BQU0sT0FBYixNQUFNLG1CQUFXLE1BQU0sQ0FBQyxVQUFVLEVBQUUsR0FBRTtRQUV0QyxtQkFBbUIsQ0FBQyxvQkFBb0IsQ0FBQztJQUMzQyxDQUFDOzs7O0lBRU0sd0NBQW9COzs7SUFBM0I7UUFDRSxtQkFBbUIsQ0FBQyxlQUFlLEdBQUc7WUFDcEMsT0FBTyxFQUFFLENBQUM7WUFDVixZQUFZLEVBQUUsQ0FBQztZQUNmLGNBQWMsRUFBRSxDQUFDO1lBQ2pCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsZ0JBQWdCLEVBQUUsQ0FBQztZQUNuQixhQUFhLEVBQUUsQ0FBQztZQUNoQixjQUFjLEVBQUUsQ0FBQztZQUNqQixZQUFZLEVBQUUsQ0FBQztZQUNmLE1BQU0sRUFBRSxDQUFDO1lBQ1QsTUFBTSxFQUFFLENBQUM7WUFDVCxPQUFPLEVBQUUsQ0FBQztZQUNWLEtBQUssRUFBRSxDQUFDO1NBQ1QsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVNLGdDQUFZOzs7OztJQUFuQixVQUFvQixNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFBLHNCQUFNOztZQUNSLFNBQVMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEIsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRU0sa0NBQWM7Ozs7O0lBQXJCLFVBQXNCLE1BQU0sRUFBRSxNQUFNO1FBQzFCLElBQUEsc0JBQU07O1lBQ1IsU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDekMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0QixNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzs7WUFFdkIsVUFBVSxHQUFHLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVOztZQUNuRCxRQUFRLEdBQUcsbUJBQW1CLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztRQUMvRCxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7Ozs7OztJQUVNLDJCQUFPOzs7OztJQUFkLFVBQWUsTUFBTSxFQUFFLE1BQU07UUFDbkIsSUFBQSxzQkFBTSxFQUFFLHNCQUFNOztZQUNoQixJQUFJLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUM7UUFDNUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqQixNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFTSw2QkFBUzs7Ozs7SUFBaEIsVUFBaUIsTUFBTSxFQUFFLE1BQU07UUFDckIsSUFBQSxzQkFBTTs7WUFDUixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUN4QyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25CLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUVNLCtCQUFXOzs7OztJQUFsQixVQUFtQixNQUFNLEVBQUUsTUFBTTtRQUN2QixJQUFBLHNCQUFNLEVBQUUsc0JBQU07O1lBQ2hCLFFBQVEsR0FBRyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQztRQUNuRCxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3JCLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRU0sK0JBQVc7Ozs7SUFBbEIsVUFBbUIsTUFBTTs7WUFDakIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7O1lBQ3pDLEtBQUssR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRS9CLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFbEIsbUJBQW1CLENBQUMsZ0JBQWdCLENBQ2xDLEtBQUssRUFDTCxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxFQUM5QixjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsVUFBVSxDQUNyQyxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFTSxzQ0FBa0I7Ozs7SUFBekIsVUFBMEIsTUFBTTtRQUM5QixtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7O0lBRU0sa0RBQThCOzs7O0lBQXJDLFVBQXNDLE1BQU07UUFDMUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Ozs7O0lBRU0sdUNBQW1COzs7O0lBQTFCLFVBQTJCLE1BQU07UUFDL0IsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7OztJQUVNLHFDQUFpQjs7OztJQUF4QixVQUF5QixNQUFNO1FBQzdCLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Ozs7SUFFTSxnREFBNEI7Ozs7SUFBbkMsVUFBb0MsTUFBTTtRQUN4QyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7SUFFTSx3Q0FBb0I7Ozs7SUFBM0IsVUFBNEIsTUFBTTtRQUNoQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7OztJQUVNLGtDQUFjOzs7OztJQUFyQixVQUFzQixNQUFNLEVBQUUsU0FBUzs7WUFDL0IsS0FBSyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7O1lBQ2hDLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDO1FBRXJELEtBQUssQ0FBQyxhQUFhOzs7O1FBQUMsVUFBQSxJQUFJOztnQkFDaEIsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUUvQyxRQUFRLFNBQVMsRUFBRTtnQkFDakIsS0FBSyxNQUFNLENBQUMsQ0FBQztvQkFDWCxJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLElBQUksRUFDRixDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQztxQkFDckUsQ0FBQyxDQUFDO29CQUNILE1BQU07aUJBQ1A7Z0JBQ0QsS0FBSyxtQkFBbUIsQ0FBQyxDQUFDO29CQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxHQUFHLGdCQUFnQixDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLEdBQUcsQ0FBQztxQkFDckUsQ0FBQyxDQUFDO29CQUNILE1BQU07aUJBQ1A7Z0JBQ0QsS0FBSyxPQUFPLENBQUMsQ0FBQztvQkFDWixJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLElBQUksRUFDRixJQUFJLENBQUMsSUFBSTs0QkFDVCxDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDOzRCQUNyRCxnQkFBZ0IsQ0FBQyxLQUFLO3FCQUN6QixDQUFDLENBQUM7b0JBQ0gsTUFBTTtpQkFDUDtnQkFDRCxLQUFLLEtBQUssQ0FBQyxDQUFDO29CQUNWLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQ1AsR0FBRyxFQUNELENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDO3FCQUNwRSxDQUFDLENBQUM7b0JBQ0gsTUFBTTtpQkFDUDtnQkFDRCxLQUFLLGlCQUFpQixDQUFDLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQ1AsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDO3FCQUNuRSxDQUFDLENBQUM7b0JBQ0gsTUFBTTtpQkFDUDtnQkFDRCxLQUFLLFFBQVEsQ0FBQyxDQUFDO29CQUNiLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQ1AsR0FBRyxFQUNELElBQUksQ0FBQyxHQUFHOzRCQUNSLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUM7NEJBQ3JELGdCQUFnQixDQUFDLE1BQU07cUJBQzFCLENBQUMsQ0FBQztvQkFDSCxNQUFNO2lCQUNQO2FBQ0Y7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILEtBQUssQ0FBQyxhQUFhOzs7O1FBQUMsVUFBQSxJQUFJO1lBQ3RCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQyxFQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbEIsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7OztJQUVNLG1DQUFlOzs7Ozs7SUFBdEIsVUFBdUIsTUFBYyxFQUFFLEtBQWEsRUFBRSxHQUFXOztZQUMzRCxDQUFDOztZQUNELENBQUM7UUFFTCxRQUFRLEdBQUcsRUFBRTtZQUNYLEtBQUssUUFBUTtnQkFDWCxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNOLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ1AsTUFBTTtZQUNSLEtBQUssYUFBYTtnQkFDaEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ1AsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNQLE1BQU07WUFDUixLQUFLLEtBQUs7Z0JBQ1IsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQ1IsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO2dCQUNSLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQ1IsTUFBTTtZQUNSO2dCQUNFLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ04sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixNQUFNO1NBQ1Q7UUFFRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUM1QixJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLEVBQ3hDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUMxQyxDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRU0sNkJBQVM7Ozs7O0lBQWhCLFVBQWlCLE1BQU0sRUFBRSxNQUFNO1FBQ3JCLElBQUEsb0JBQUssRUFBRSxzQkFBTSxFQUFFLGtCQUFJOztZQUNyQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTs7WUFDekMsWUFBWSxHQUFHLG1CQUFtQixDQUFDLGVBQWUsQ0FDdEQsTUFBTSxFQUNOLENBQUMsS0FBSyxFQUNOLFFBQVEsQ0FDVDs7WUFFSyxLQUFLLEdBQ1QsY0FBYyxDQUFDLE1BQU0sSUFBSSxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDbEQsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSztZQUM3QixDQUFDLENBQUMsaUJBQWlCO1FBRXZCLGNBQWMsQ0FBQyxTQUFTLHNCQUNuQixjQUFjLENBQUMsTUFBTSxJQUN4QixLQUFLLE9BQUE7WUFDTCxJQUFJLE1BQUEsRUFDSixPQUFPLEVBQUUsWUFBWSxDQUFDLENBQUMsRUFDdkIsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFDLEVBQ3ZCLFlBQVksRUFBRSxLQUFLLElBQ25CLENBQUM7UUFDSCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTSxrQ0FBYzs7Ozs7SUFBckIsVUFBc0IsTUFBTSxFQUFFLEtBQUs7O1lBQzNCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFOztZQUN6QyxLQUFLLEdBQ1QsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLO1lBQzNCLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxFQUFFO1lBQzNELENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUMvRCxDQUFDLENBQUMsR0FBRzs7WUFDSCxTQUFTLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDO2FBQzFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7YUFDZixNQUFNLEVBQUU7UUFDWCxjQUFjLENBQUMsU0FBUyxzQkFDbkIsY0FBYyxDQUFDLE1BQU0sSUFDeEIsS0FBSyxFQUFFLFNBQVMsRUFDaEIsSUFBSSxFQUFFLEdBQUcsSUFDVCxDQUFDO1FBQ0gsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFFMUIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFFTSxtQ0FBZTs7Ozs7SUFBdEIsVUFBdUIsTUFBTSxFQUFFLFNBQVM7O1lBQ2hDLDJCQUEyQixHQUFHO1lBQ2xDLEdBQUcsRUFBRSx3QkFBd0I7WUFDN0IsR0FBRyxFQUFFLEdBQUc7WUFDUixJQUFJLEVBQUUsd0JBQXdCO1lBQzlCLElBQUksRUFBRSxZQUFZO1lBQ2xCLElBQUksRUFBRSxHQUFHO1lBQ1QsSUFBSSxFQUFFLE9BQU87WUFDYixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsS0FBSztZQUNaLEtBQUssRUFBRSxLQUFLO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLEtBQUs7WUFDdEMsTUFBTSxFQUFFLElBQUk7WUFDWixNQUFNLEVBQUUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNO1lBQ3hDLE1BQU0sRUFBRSxDQUFDO1lBQ1QsTUFBTSxFQUFFLENBQUM7WUFDVCxNQUFNLEVBQUUsSUFBSTtZQUNaLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLEVBQUU7WUFDWCxPQUFPLEVBQUUsQ0FBQztZQUNWLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLE9BQU8sRUFBRSxJQUFJO1lBQ2IsUUFBUSxFQUFFLFNBQVM7WUFDbkIsVUFBVSxFQUFFLE1BQU07WUFDbEIsV0FBVyxFQUFFLEVBQUU7WUFDZixXQUFXLEVBQUUsQ0FBQztZQUNkLGFBQWEsRUFBRSxNQUFNO1lBQ3JCLGNBQWMsRUFBRSxPQUFPO1lBQ3ZCLGVBQWUsRUFBRSxFQUFFO1lBQ25CLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLGdCQUFnQixFQUFFLENBQUM7WUFDbkIsd0JBQXdCLEVBQUUsYUFBYTtTQUN4QztRQUVELFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsQ0FBQztZQUN6QixDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztZQUNsQixDQUFDLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQztZQUNqQixDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUM3QixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUMvQixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNiLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ2IsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFBO1lBRWQsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ2QsT0FBTyxDQUFDLENBQUMsVUFBVSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO1FBQ0gsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsMkJBQTJCLENBQUMsQ0FBQztRQUN2RCxTQUFTLENBQUMsZUFBZSxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUM7UUFDbkMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ3BELFNBQVMsQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDOztZQUU3QixVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3hCLE9BQU8sVUFBVSxDQUFDO0lBQ3BCLENBQUM7Ozs7O0lBRU0sa0NBQWM7Ozs7SUFBckIsVUFBc0IsVUFBa0I7O1lBQ2hDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1FBQzFELG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDOzs7Ozs7O0lBRU0sb0NBQWdCOzs7Ozs7SUFBdkIsVUFBd0IsR0FBRyxFQUFFLEVBQUUsRUFBRSxVQUFVO1FBQ3pDLEdBQUcsQ0FBQyxRQUFRLEdBQUc7Ozs7UUFBQyxVQUFTLFFBQVE7WUFDL0I7OztZQUFPO2dCQUNMLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ3BELElBQUksRUFBRSxFQUFFO29CQUNSLElBQUksRUFBSyxVQUFVLHFCQUFnQixFQUFJO29CQUN2QyxVQUFVLFlBQUE7aUJBQ1gsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25CLENBQUM7SUFqaEJNLG1DQUFlLEdBQU87UUFDM0IsT0FBTyxFQUFFLENBQUM7UUFDVixZQUFZLEVBQUUsQ0FBQztRQUNmLGNBQWMsRUFBRSxDQUFDO1FBQ2pCLE1BQU0sRUFBRSxDQUFDO1FBQ1QsZ0JBQWdCLEVBQUUsQ0FBQztRQUNuQixhQUFhLEVBQUUsQ0FBQztRQUNoQixjQUFjLEVBQUUsQ0FBQztRQUNqQixZQUFZLEVBQUUsQ0FBQztRQUNmLE1BQU0sRUFBRSxDQUFDO1FBQ1QsTUFBTSxFQUFFLENBQUM7UUFDVCxPQUFPLEVBQUUsQ0FBQztRQUNWLEtBQUssRUFBRSxDQUFDO0tBQ1QsQ0FBQzs7Z0JBakJILFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs4QkFmRDtDQW1pQkMsQUF0aEJELElBc2hCQztTQW5oQlksbUJBQW1COzs7SUFDOUIsb0NBYUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgJ2ZhYnJpYyc7XG5kZWNsYXJlIGNvbnN0IGZhYnJpYzogYW55O1xuXG5pbnRlcmZhY2UgSW1hZ2VDb25maWcge1xuICB3aWR0aDogbnVtYmVyO1xuICBoZWlnaHQ6IG51bWJlcjtcbiAgc2NhbGVGYWN0b3I6IG51bWJlcjtcbiAgeE9mZnNldDogbnVtYmVyO1xuICB5T2Zmc2V0OiBudW1iZXI7XG59XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIFZpc3VhbEVkaXRvclNlcnZpY2Uge1xuICBzdGF0aWMgb2JqZWN0VHlwZUNvdW50OiB7fSA9IHtcbiAgICBwcm9kdWN0OiAwLFxuICAgIHByaW1hcnlfbG9nbzogMCxcbiAgICBzZWNvbmRhcnlfbG9nbzogMCxcbiAgICBsb2NrdXA6IDAsXG4gICAgaGVhZGxpbmVfdGV4dGJveDogMCxcbiAgICBvdGhlcl90ZXh0Ym94OiAwLFxuICAgIGdyYXBoaWNfYWNjZW50OiAwLFxuICAgIHBob3RvX2FjY2VudDogMCxcbiAgICBidXR0b246IDAsXG4gICAgYmFubmVyOiAwLFxuICAgIHN0aWNrZXI6IDAsXG4gICAgZnJhbWU6IDBcbiAgfTtcblxuICBzdGF0aWMgc2V0Q29sb3IoY2FudmFzLCBjb2xvcikge1xuICAgIGNhbnZhcy5mcmVlRHJhd2luZ0JydXNoLmNvbG9yID0gY29sb3I7XG4gIH1cblxuICBzdGF0aWMgY2xlYXJEcmF3aW5nVHJhbnNwYXJlbnRCZyhjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmNsZWFyRHJhd2luZyhjYW52YXMsIDEpOyAvLyBOT1RFOiBmaXJzdCBvYmplY3QgaXMgdGhlIGltYWdlIGl0c2VsZlxuICB9XG5cbiAgc3RhdGljIGNsZWFyRHJhd2luZyhjYW52YXMsIG9mZnNldCA9IDApIHtcbiAgICBjYW52YXNcbiAgICAgIC5nZXRPYmplY3RzKClcbiAgICAgIC5zbGljZShvZmZzZXQpXG4gICAgICAuZm9yRWFjaChvID0+IGNhbnZhcy5yZW1vdmUobykpO1xuICB9XG5cbiAgc3RhdGljIHVuZG9EcmF3aW5nVHJhbnNwYXJlbnRCZyhjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLnVuZG9EcmF3aW5nKGNhbnZhcywgMSk7IC8vIE5PVEU6IGZpcnN0IG9iamVjdCBpcyB0aGUgaW1hZ2UgaXRzZWxmXG4gIH1cblxuICBzdGF0aWMgdW5kb0RyYXdpbmcoY2FudmFzLCBvZmZzZXQgPSAwKSB7XG4gICAgY29uc3Qgb2JqcyA9IGNhbnZhcy5nZXRPYmplY3RzKCk7XG4gICAgaWYgKG9ianMubGVuZ3RoID4gb2Zmc2V0KSB7XG4gICAgICBjYW52YXMucmVtb3ZlKG9ianNbb2Jqcy5sZW5ndGggLSAxXSk7XG4gICAgfVxuICB9XG5cbiAgc3RhdGljIGFkZFRleHQoY2FudmFzLCBwYXJhbXMpIHtcbiAgICBjb25zdCB7IHRleHQsIGNvbmZpZyB9ID0gcGFyYW1zO1xuICAgIGNvbnN0IHRleHRib3ggPSBuZXcgZmFicmljLlRleHRib3godGV4dCwgY29uZmlnKTtcbiAgICBjYW52YXMuYWRkKHRleHRib3gpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgYnJpbmdGb3J3YXJkKGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNhbnZhcy5icmluZ0ZvcndhcmQoc2VsZWN0ZWRPYmplY3QpO1xuICB9XG5cbiAgc3RhdGljIHNlbmRCYWNrd2FyZHMoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY2FudmFzLnNlbmRCYWNrd2FyZHMoc2VsZWN0ZWRPYmplY3QpO1xuICB9XG5cbiAgc3RhdGljIHNldEJvbGQoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KFxuICAgICAgJ2ZvbnRXZWlnaHQnLFxuICAgICAgc2VsZWN0ZWRPYmplY3QuZm9udFdlaWdodCA9PT0gJ2JvbGQnID8gJ25vcm1hbCcgOiAnYm9sZCdcbiAgICApO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgc2V0SXRhbGljKGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldChcbiAgICAgICdmb250U3R5bGUnLFxuICAgICAgc2VsZWN0ZWRPYmplY3QuZm9udFN0eWxlID09PSAnaXRhbGljJyA/ICdub3JtYWwnIDogJ2l0YWxpYydcbiAgICApO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgc2V0VW5kZXJsaW5lKGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgndW5kZXJsaW5lJywgIXNlbGVjdGVkT2JqZWN0LnVuZGVybGluZSk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnbkxlZnQoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCd0ZXh0QWxpZ24nLCAnbGVmdCcpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25DZW50ZXIoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCd0ZXh0QWxpZ24nLCAnY2VudGVyJyk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnblJpZ2h0KGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgndGV4dEFsaWduJywgJ3JpZ2h0Jyk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRGb250RmFtaWx5KGNhbnZhcywgZm9udEZhbWlseSkge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnZm9udEZhbWlseScsIGZvbnRGYW1pbHkpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgc2V0RmlsbENvbG9yKGNhbnZhcywgY29sb3IpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoJ2ZpbGwnLCBjb2xvcik7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICByZXR1cm4gY29sb3I7XG4gIH1cblxuICBzdGF0aWMgc2V0Rm9udFNpemUoY2FudmFzLCBmb250U2l6ZSkge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnZm9udFNpemUnLCBmb250U2l6ZSk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuICBcbiAgc3RhdGljIHNldExpbmVIZWlnaHQoY2FudmFzLCBsaW5lSGVpZ2h0KSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCdsaW5lSGVpZ2h0JywgbGluZUhlaWdodCk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRDaGFyYWN0ZXJTcGFjaW5nKGNhbnZhcywgY2hhcmFjdGVyU3BhY2luZykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnY2hhclNwYWNpbmcnLCBjaGFyYWN0ZXJTcGFjaW5nKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIGFkZEltYWdlKGNhbnZhcywgcGFyYW1zKSB7XG4gICAgZmFicmljLkltYWdlLmZyb21VUkwoXG4gICAgICBwYXJhbXMuZmlsZVVybCxcbiAgICAgIGltZyA9PiB7XG4gICAgICAgIGlmIChwYXJhbXMuc2NhbGluZykge1xuICAgICAgICAgIGNvbnN0IHNjYWxlRmFjdG9yID1cbiAgICAgICAgICAgIGltZy5oZWlnaHQgPiBpbWcud2lkdGhcbiAgICAgICAgICAgICAgPyAoY2FudmFzLmhlaWdodCAqIHBhcmFtcy5zY2FsaW5nKSAvIGltZy5oZWlnaHRcbiAgICAgICAgICAgICAgOiAoY2FudmFzLndpZHRoICogcGFyYW1zLnNjYWxpbmcpIC8gaW1nLndpZHRoO1xuICAgICAgICAgIGltZy5zZXQoe1xuICAgICAgICAgICAgc2NhbGVYOiBzY2FsZUZhY3RvcixcbiAgICAgICAgICAgIHNjYWxlWTogc2NhbGVGYWN0b3JcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBjYW52YXMuYWRkKGltZyk7XG4gICAgICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gICAgICB9LFxuICAgICAgcGFyYW1zLmNvbmZpZ1xuICAgICk7XG5cbiAgICBjYW52YXMuZGlzY2FyZEFjdGl2ZU9iamVjdCgpO1xuICB9XG5cbiAgc3RhdGljIHVwZGF0ZUltYWdlKGNhbnZhcywgdXJsLCBjYWxsYmFjaykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNvbnN0IHsgd2lkdGgsIGhlaWdodCwgc2NhbGVYLCBzY2FsZVkgfSA9IHNlbGVjdGVkT2JqZWN0O1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldFNyYyhcbiAgICAgIHVybCxcbiAgICAgIGltZyA9PiB7XG4gICAgICAgIGNvbnN0IHNjYWxlRmFjdG9yID0gXy5taW4oW1xuICAgICAgICAgICh3aWR0aCAqIHNjYWxlWCkgLyBpbWcud2lkdGgsXG4gICAgICAgICAgKGhlaWdodCAqIHNjYWxlWSkgLyBpbWcuaGVpZ2h0XG4gICAgICAgIF0pO1xuICAgICAgICBpbWcuc2V0KHtcbiAgICAgICAgICBzY2FsZVg6IHNjYWxlRmFjdG9yLFxuICAgICAgICAgIHNjYWxlWTogc2NhbGVGYWN0b3JcbiAgICAgICAgfSk7XG4gICAgICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gICAgICAgIGNhbGxiYWNrKCk7XG4gICAgICB9LFxuICAgICAgeyBjcm9zc09yaWdpbjogJ2Fub255bW91cycgfVxuICAgICk7XG4gIH1cblxuICBzdGF0aWMgc2V0U3Ryb2tlQ29sb3IoY2FudmFzLCBjb2xvcikge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnc3Ryb2tlJywgY29sb3IpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gICAgcmV0dXJuIGNvbG9yO1xuICB9XG5cbiAgc3RhdGljIHNldFN0cm9rZVdpZHRoKGNhbnZhcywgd2lkdGgpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoJ3N0cm9rZVdpZHRoJywgd2lkdGgpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgZGVsZXRlU2VsZWN0aW9uKGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNhbnZhcy5yZW1vdmUoc2VsZWN0ZWRPYmplY3QpO1xuICB9XG5cbiAgc3RhdGljIGRlbGV0ZVNlbGVjdGlvbkdyb3VwKGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0cyA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdHMuZm9yRWFjaE9iamVjdChvID0+IGNhbnZhcy5yZW1vdmUobykpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgZGVsZXRlQWxsU2VsZWN0aW9ucyhjYW52YXMpIHtcbiAgICBjYW52YXMucmVtb3ZlKC4uLmNhbnZhcy5nZXRPYmplY3RzKCkpO1xuXG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5yZXNldE9iamVjdFR5cGVDb3VudDtcbiAgfVxuXG4gIHN0YXRpYyByZXNldE9iamVjdFR5cGVDb3VudCgpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLm9iamVjdFR5cGVDb3VudCA9IHtcbiAgICAgIHByb2R1Y3Q6IDAsXG4gICAgICBwcmltYXJ5X2xvZ286IDAsXG4gICAgICBzZWNvbmRhcnlfbG9nbzogMCxcbiAgICAgIGxvY2t1cDogMCxcbiAgICAgIGhlYWRsaW5lX3RleHRib3g6IDAsXG4gICAgICBvdGhlcl90ZXh0Ym94OiAwLFxuICAgICAgZ3JhcGhpY19hY2NlbnQ6IDAsXG4gICAgICBwaG90b19hY2NlbnQ6IDAsXG4gICAgICBidXR0b246IDAsXG4gICAgICBiYW5uZXI6IDAsXG4gICAgICBzdGlja2VyOiAwLFxuICAgICAgZnJhbWU6IDBcbiAgICB9O1xuICB9XG5cbiAgc3RhdGljIGFkZFJlY3RhbmdsZShjYW52YXMsIHBhcmFtcykge1xuICAgIGNvbnN0IHsgY29uZmlnIH0gPSBwYXJhbXM7XG4gICAgY29uc3QgcmVjdGFuZ2xlID0gbmV3IGZhYnJpYy5SZWN0KGNvbmZpZyk7XG4gICAgY2FudmFzLmFkZChyZWN0YW5nbGUpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgYWRkQm91bmRpbmdCb3goY2FudmFzLCBwYXJhbXMpIHtcbiAgICBjb25zdCB7IGNvbmZpZyB9ID0gcGFyYW1zO1xuICAgIGNvbnN0IHJlY3RhbmdsZSA9IG5ldyBmYWJyaWMuUmVjdChjb25maWcpO1xuICAgIGNhbnZhcy5hZGQocmVjdGFuZ2xlKTtcbiAgICBjYW52YXMuZGlzY2FyZEFjdGl2ZU9iamVjdCgpO1xuXG4gICAgY29uc3Qgb2JqZWN0VHlwZSA9IHBhcmFtcy5hZGRpdGlvbmFsUHJvcGVydGllcy5vYmplY3RUeXBlO1xuICAgIGNvbnN0IG9iamVjdElkID0gVmlzdWFsRWRpdG9yU2VydmljZS5jcmVhdGVPYmplY3RJZChvYmplY3RUeXBlKTtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmV4dGVuZE9iamVjdEpzb24ocmVjdGFuZ2xlLCBvYmplY3RJZCwgb2JqZWN0VHlwZSk7XG4gIH1cblxuICBzdGF0aWMgYWRkTGluZShjYW52YXMsIHBhcmFtcykge1xuICAgIGNvbnN0IHsgY29uZmlnLCBjb29yZHMgfSA9IHBhcmFtcztcbiAgICBjb25zdCBsaW5lID0gbmV3IGZhYnJpYy5MaW5lKGNvb3JkcywgY29uZmlnKTtcbiAgICBjYW52YXMuYWRkKGxpbmUpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgYWRkQ2lyY2xlKGNhbnZhcywgcGFyYW1zKSB7XG4gICAgY29uc3QgeyBjb25maWcgfSA9IHBhcmFtcztcbiAgICBjb25zdCBjaXJjbGUgPSBuZXcgZmFicmljLkNpcmNsZShjb25maWcpO1xuICAgIGNhbnZhcy5hZGQoY2lyY2xlKTtcbiAgICBjYW52YXMuZGlzY2FyZEFjdGl2ZU9iamVjdCgpO1xuICB9XG5cbiAgc3RhdGljIGFkZFRyaWFuZ2xlKGNhbnZhcywgcGFyYW1zKSB7XG4gICAgY29uc3QgeyBjb25maWcsIGNvb3JkcyB9ID0gcGFyYW1zO1xuICAgIGNvbnN0IHRyaWFuZ2xlID0gbmV3IGZhYnJpYy5Qb2x5Z29uKGNvb3JkcywgY29uZmlnKTtcbiAgICBjYW52YXMuYWRkKHRyaWFuZ2xlKTtcbiAgICBjYW52YXMuZGlzY2FyZEFjdGl2ZU9iamVjdCgpO1xuICB9XG5cbiAgc3RhdGljIGNsb25lT2JqZWN0KGNhbnZhcykge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNvbnN0IGNsb25lID0gbmV3IGZhYnJpYy5SZWN0KHNlbGVjdGVkT2JqZWN0LnRvT2JqZWN0KCkpO1xuICAgIGNsb25lLnNldCh7IGxlZnQ6IDAsIHRvcDogMCB9KTtcblxuICAgIGNhbnZhcy5hZGQoY2xvbmUpO1xuXG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5leHRlbmRPYmplY3RKc29uKFxuICAgICAgY2xvbmUsXG4gICAgICBzZWxlY3RlZE9iamVjdC50b09iamVjdCgpLl9vSWQsXG4gICAgICBzZWxlY3RlZE9iamVjdC50b09iamVjdCgpLm9iamVjdFR5cGVcbiAgICApO1xuICB9XG5cbiAgc3RhdGljIGFsaWduU2VsZWN0aW9uTGVmdChjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uKGNhbnZhcywgJ2xlZnQnKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnblNlbGVjdGlvbkhvcml6b250YWxDZW50ZXIoY2FudmFzKSB7XG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbihjYW52YXMsICdob3Jpem9udGFsLWNlbnRlcicpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduU2VsZWN0aW9uUmlnaHQoY2FudmFzKSB7XG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbihjYW52YXMsICdyaWdodCcpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduU2VsZWN0aW9uVG9wKGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb24oY2FudmFzLCAndG9wJyk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcihjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uKGNhbnZhcywgJ3ZlcnRpY2FsLWNlbnRlcicpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduU2VsZWN0aW9uQm90dG9tKGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb24oY2FudmFzLCAnYm90dG9tJyk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb24oY2FudmFzLCBkaXJlY3Rpb24pIHtcbiAgICBjb25zdCBncm91cCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBjb25zdCBncm91cEJvdW5kaW5nUmVjdCA9IGdyb3VwLmdldEJvdW5kaW5nUmVjdCh0cnVlKTtcblxuICAgIGdyb3VwLmZvckVhY2hPYmplY3QoaXRlbSA9PiB7XG4gICAgICBjb25zdCBpdGVtQm91bmRpbmdSZWN0ID0gaXRlbS5nZXRCb3VuZGluZ1JlY3QoKTtcblxuICAgICAgc3dpdGNoIChkaXJlY3Rpb24pIHtcbiAgICAgICAgY2FzZSAnbGVmdCc6IHtcbiAgICAgICAgICBpdGVtLnNldCh7XG4gICAgICAgICAgICBsZWZ0OlxuICAgICAgICAgICAgICAtZ3JvdXBCb3VuZGluZ1JlY3Qud2lkdGggLyAyICsgKGl0ZW0ubGVmdCAtIGl0ZW1Cb3VuZGluZ1JlY3QubGVmdClcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjYXNlICdob3Jpem9udGFsLWNlbnRlcic6IHtcbiAgICAgICAgICBpdGVtLnNldCh7XG4gICAgICAgICAgICBsZWZ0OiBpdGVtLmxlZnQgLSBpdGVtQm91bmRpbmdSZWN0LmxlZnQgLSBpdGVtQm91bmRpbmdSZWN0LndpZHRoIC8gMlxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ3JpZ2h0Jzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIGxlZnQ6XG4gICAgICAgICAgICAgIGl0ZW0ubGVmdCArXG4gICAgICAgICAgICAgIChncm91cEJvdW5kaW5nUmVjdC53aWR0aCAvIDIgLSBpdGVtQm91bmRpbmdSZWN0LmxlZnQpIC1cbiAgICAgICAgICAgICAgaXRlbUJvdW5kaW5nUmVjdC53aWR0aFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ3RvcCc6IHtcbiAgICAgICAgICBpdGVtLnNldCh7XG4gICAgICAgICAgICB0b3A6XG4gICAgICAgICAgICAgIC1ncm91cEJvdW5kaW5nUmVjdC5oZWlnaHQgLyAyICsgKGl0ZW0udG9wIC0gaXRlbUJvdW5kaW5nUmVjdC50b3ApXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAndmVydGljYWwtY2VudGVyJzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIHRvcDogaXRlbS50b3AgLSBpdGVtQm91bmRpbmdSZWN0LnRvcCAtIGl0ZW1Cb3VuZGluZ1JlY3QuaGVpZ2h0IC8gMlxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ2JvdHRvbSc6IHtcbiAgICAgICAgICBpdGVtLnNldCh7XG4gICAgICAgICAgICB0b3A6XG4gICAgICAgICAgICAgIGl0ZW0udG9wICtcbiAgICAgICAgICAgICAgKGdyb3VwQm91bmRpbmdSZWN0LmhlaWdodCAvIDIgLSBpdGVtQm91bmRpbmdSZWN0LnRvcCkgLVxuICAgICAgICAgICAgICBpdGVtQm91bmRpbmdSZWN0LmhlaWdodFxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICBncm91cC5mb3JFYWNoT2JqZWN0KGl0ZW0gPT4ge1xuICAgICAgZ3JvdXAucmVtb3ZlV2l0aFVwZGF0ZShpdGVtKS5hZGRXaXRoVXBkYXRlKGl0ZW0pO1xuICAgIH0pO1xuICAgIGdyb3VwLnNldENvb3JkcygpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgZ2V0U2hhZG93T2Zmc2V0KGxlbmd0aDogbnVtYmVyLCBhbmdsZTogbnVtYmVyLCBwb3M6IHN0cmluZykge1xuICAgIGxldCB4O1xuICAgIGxldCB5O1xuXG4gICAgc3dpdGNoIChwb3MpIHtcbiAgICAgIGNhc2UgJ0JPVFRPTSc6XG4gICAgICAgIHggPSAwO1xuICAgICAgICB5ID0gMTA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnQk9UVE9NX0xFRlQnOlxuICAgICAgICB4ID0gLTY7XG4gICAgICAgIHkgPSAxMDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdCT1RUT01fUklHSFQnOlxuICAgICAgICB4ID0gNjtcbiAgICAgICAgeSA9IDEwO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1RPUCc6XG4gICAgICAgIHggPSAwO1xuICAgICAgICB5ID0gLTEwO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1RPUF9MRUZUJzpcbiAgICAgICAgeCA9IC02O1xuICAgICAgICB5ID0gLTEwO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ1RPUF9SSUdIVCc6XG4gICAgICAgIHggPSA2O1xuICAgICAgICB5ID0gLTEwO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHggPSAwO1xuICAgICAgICB5ID0gMDtcbiAgICAgICAgYnJlYWs7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhYnJpYy51dGlsLnJvdGF0ZVBvaW50KFxuICAgICAgbmV3IGZhYnJpYy5Qb2ludCh4ICogbGVuZ3RoLCB5ICogbGVuZ3RoKSxcbiAgICAgIG5ldyBmYWJyaWMuUG9pbnQoMCwgMCksXG4gICAgICBmYWJyaWMudXRpbC5kZWdyZWVzVG9SYWRpYW5zKDM2MCAtIGFuZ2xlKVxuICAgICk7XG4gIH1cblxuICBzdGF0aWMgc2V0U2hhZG93KGNhbnZhcywgcGFyYW1zKSB7XG4gICAgY29uc3QgeyBhbmdsZSwgbGVuZ3RoLCBibHVyIH0gPSBwYXJhbXM7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY29uc3Qgc2hhZG93T2Zmc2V0ID0gVmlzdWFsRWRpdG9yU2VydmljZS5nZXRTaGFkb3dPZmZzZXQoXG4gICAgICBsZW5ndGgsXG4gICAgICAtYW5nbGUsXG4gICAgICAnQk9UVE9NJ1xuICAgICk7XG5cbiAgICBjb25zdCBjb2xvciA9XG4gICAgICBzZWxlY3RlZE9iamVjdC5zaGFkb3cgJiYgc2VsZWN0ZWRPYmplY3Quc2hhZG93LmNvbG9yXG4gICAgICAgID8gc2VsZWN0ZWRPYmplY3Quc2hhZG93LmNvbG9yXG4gICAgICAgIDogJ3JnYmEoMCwwLDAsMC41KSc7XG5cbiAgICBzZWxlY3RlZE9iamVjdC5zZXRTaGFkb3coe1xuICAgICAgLi4uc2VsZWN0ZWRPYmplY3Quc2hhZG93LFxuICAgICAgY29sb3IsXG4gICAgICBibHVyLFxuICAgICAgb2Zmc2V0WDogc2hhZG93T2Zmc2V0LngsXG4gICAgICBvZmZzZXRZOiBzaGFkb3dPZmZzZXQueSxcbiAgICAgIGFmZmVjdFN0cm9rZTogZmFsc2VcbiAgICB9KTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIHNldFNoYWRvd0NvbG9yKGNhbnZhcywgY29sb3IpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBjb25zdCBhbHBoYSA9XG4gICAgICBzZWxlY3RlZE9iamVjdC5zaGFkb3cgJiZcbiAgICAgIHNlbGVjdGVkT2JqZWN0LnNoYWRvdy5jb2xvciAmJlxuICAgICAgZmFicmljLkNvbG9yLmZyb21SZ2JhKHNlbGVjdGVkT2JqZWN0LnNoYWRvdy5jb2xvcikuZ2V0QWxwaGEoKVxuICAgICAgICA/IGZhYnJpYy5Db2xvci5mcm9tUmdiYShzZWxlY3RlZE9iamVjdC5zaGFkb3cuY29sb3IpLmdldEFscGhhKClcbiAgICAgICAgOiAwLjU7XG4gICAgY29uc3QgcmdiYUNvbG9yID0gZmFicmljLkNvbG9yLmZyb21IZXgoY29sb3IpXG4gICAgICAuc2V0QWxwaGEoYWxwaGEpXG4gICAgICAudG9SZ2JhKCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0U2hhZG93KHtcbiAgICAgIC4uLnNlbGVjdGVkT2JqZWN0LnNoYWRvdyxcbiAgICAgIGNvbG9yOiByZ2JhQ29sb3IsXG4gICAgICBibHVyOiAwLjVcbiAgICB9KTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuXG4gICAgcmV0dXJuIGNvbG9yO1xuICB9XG5cbiAgc3RhdGljIHRyYW5zZm9ybUNhbnZhcyhpbWdVcmwsIGNhbnZhc09iaik6IGFueSB7XG4gICAgY29uc3QgYmFja2dyb3VuZFBsYWNlaG9sZGVyT2JqZWN0ID0ge1xuICAgICAgc3JjOiAnYmFja2dyb3VuZF9wbGFjZWhvbGRlcicsXG4gICAgICB0b3A6IDMwMCxcbiAgICAgIF9rZXk6ICdiYWNrZ3JvdW5kX3BsYWNlaG9sZGVyJyxcbiAgICAgIGZpbGw6ICdyZ2IoMCwwLDApJyxcbiAgICAgIGxlZnQ6IDMwMCxcbiAgICAgIHR5cGU6ICdpbWFnZScsXG4gICAgICBhbmdsZTogMCxcbiAgICAgIGNyb3BYOiAwLFxuICAgICAgY3JvcFk6IDAsXG4gICAgICBmbGlwWDogZmFsc2UsXG4gICAgICBmbGlwWTogZmFsc2UsXG4gICAgICBza2V3WDogMCxcbiAgICAgIHNrZXdZOiAwLFxuICAgICAgd2lkdGg6IGNhbnZhc09iai5iYWNrZ3JvdW5kSW1hZ2Uud2lkdGgsXG4gICAgICBjbGlwVG86IG51bGwsXG4gICAgICBoZWlnaHQ6IGNhbnZhc09iai5iYWNrZ3JvdW5kSW1hZ2UuaGVpZ2h0LFxuICAgICAgc2NhbGVYOiAxLFxuICAgICAgc2NhbGVZOiAxLFxuICAgICAgc2hhZG93OiBudWxsLFxuICAgICAgc3Ryb2tlOiBudWxsLFxuICAgICAgZmlsdGVyczogW10sXG4gICAgICBvcGFjaXR5OiAxLFxuICAgICAgb3JpZ2luWDogJ2NlbnRlcicsXG4gICAgICBvcmlnaW5ZOiAnY2VudGVyJyxcbiAgICAgIHZlcnNpb246ICcyLjMuNicsXG4gICAgICB2aXNpYmxlOiB0cnVlLFxuICAgICAgZmlsbFJ1bGU6ICdub256ZXJvJyxcbiAgICAgIHBhaW50Rmlyc3Q6ICdmaWxsJyxcbiAgICAgIGNyb3NzT3JpZ2luOiAnJyxcbiAgICAgIHN0cm9rZVdpZHRoOiAwLFxuICAgICAgc3Ryb2tlTGluZUNhcDogJ2J1dHQnLFxuICAgICAgc3Ryb2tlTGluZUpvaW46ICdtaXRlcicsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6ICcnLFxuICAgICAgc3Ryb2tlRGFzaEFycmF5OiBudWxsLFxuICAgICAgdHJhbnNmb3JtTWF0cml4OiBudWxsLFxuICAgICAgc3Ryb2tlTWl0ZXJMaW1pdDogNCxcbiAgICAgIGdsb2JhbENvbXBvc2l0ZU9wZXJhdGlvbjogJ3NvdXJjZS1vdmVyJ1xuICAgIH07XG5cbiAgICBjYW52YXNPYmoub2JqZWN0cy5mb3JFYWNoKG8gPT4ge1xuICAgICAgby5zdHJva2UgPSBudWxsO1xuICAgICAgby5zdHJva2VXaWR0aCA9IDA7XG4gICAgICBvLnR5cGUgPSAnaW1hZ2UnO1xuICAgICAgby53aWR0aCA9IG8ud2lkdGggKiBvLnNjYWxlWDtcbiAgICAgIG8uaGVpZ2h0ID0gby5oZWlnaHQgKiBvLnNjYWxlWTtcbiAgICAgIG8uc2NhbGVYID0gMTtcbiAgICAgIG8uc2NhbGVZID0gMTtcbiAgICAgIG8uc3JjID0gby5fa2V5XG5cbiAgICAgIGRlbGV0ZSBvLl9vSWQ7XG4gICAgICBkZWxldGUgby5vYmplY3RUeXBlO1xuICAgIH0pO1xuICAgIGNhbnZhc09iai5vYmplY3RzLnVuc2hpZnQoYmFja2dyb3VuZFBsYWNlaG9sZGVyT2JqZWN0KTtcbiAgICBjYW52YXNPYmouYmFja2dyb3VuZEltYWdlLnNyYyA9ICcnO1xuICAgIGNhbnZhc09iai5iYWNrZ3JvdW5kSW1hZ2UuY3Jvc3NPcmlnaW4gPSAnYW5vbnltb3VzJztcbiAgICBjYW52YXNPYmouX29yaWdpbmFsSW1nVXJsID0gaW1nVXJsO1xuXG4gICAgY29uc3QgY2FudmFzSnNvbiA9IEpTT04uc3RyaW5naWZ5KGNhbnZhc09iaiwgbnVsbCwgMik7XG4gICAgY29uc29sZS5sb2coY2FudmFzSnNvbik7XG4gICAgcmV0dXJuIGNhbnZhc0pzb247XG4gIH1cblxuICBzdGF0aWMgY3JlYXRlT2JqZWN0SWQob2JqZWN0VHlwZTogc3RyaW5nKSB7XG4gICAgY29uc3QgaWQgPSBWaXN1YWxFZGl0b3JTZXJ2aWNlLm9iamVjdFR5cGVDb3VudFtvYmplY3RUeXBlXTtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLm9iamVjdFR5cGVDb3VudFtvYmplY3RUeXBlXSArPSAxO1xuICAgIHJldHVybiBpZDtcbiAgfVxuXG4gIHN0YXRpYyBleHRlbmRPYmplY3RKc29uKG9iaiwgaWQsIG9iamVjdFR5cGUpIHtcbiAgICBvYmoudG9PYmplY3QgPSAoZnVuY3Rpb24odG9PYmplY3QpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIGZhYnJpYy51dGlsLm9iamVjdC5leHRlbmQodG9PYmplY3QuY2FsbCh0aGlzKSwge1xuICAgICAgICAgIF9vSWQ6IGlkLFxuICAgICAgICAgIF9rZXk6IGAke29iamVjdFR5cGV9X3BsYWNlaG9sZGVyXyR7aWR9YCxcbiAgICAgICAgICBvYmplY3RUeXBlXG4gICAgICAgIH0pO1xuICAgICAgfTtcbiAgICB9KShvYmoudG9PYmplY3QpO1xuICB9XG59XG4iXX0=