/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, HostListener } from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
import { MatDialog } from '@angular/material';
import { ActionTypes, ObjectTypes } from './configs/action-types';
import { VisualEditorService } from './services/visual-editor.service';
var VisualEditorComponent = /** @class */ (function () {
    function VisualEditorComponent(dialog) {
        this.dialog = dialog;
        this.customFonts = [];
        this.outgoingEventTriggered = new EventEmitter();
        this.outgoingSelectionEventTriggered = new EventEmitter();
        this.ActionTypes = ActionTypes;
        this.VisualEditorService = VisualEditorService;
        this.checkersImageUrl = '@app/../assets/images/checkers_bg2.png';
        this.imageConfig = {
            width: 0,
            height: 0,
            scaleFactor: 1,
            xOffset: 0,
            yOffset: 0
        };
        this.tools = [];
        this.minZoom = 1;
    }
    // Start of ngOnInit function
    // Start of ngOnInit function
    /**
     * @return {?}
     */
    VisualEditorComponent.prototype.ngOnInit = 
    // Start of ngOnInit function
    /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.addCustomFonts();
        this.loadCustomFonts();
        this.tools = this.visualEditorConfig.tools;
        this.isFirstToolSelected = this.visualEditorConfig.isFirstToolSelected;
        this.hasCheckersBg = this.visualEditorConfig.hasCheckersBg;
        this.hasHiddenSelectionControls = this.visualEditorConfig.hasHiddenSelectionControls;
        this.hasZoom = this.visualEditorConfig.hasZoom;
        this.hasPan = this.visualEditorConfig.hasPan;
        this.isWideCanvas = this.visualEditorConfig.isWideCanvas;
        this.isShowFrame = this.visualEditorConfig.isShowFrame;
        this.resizeCanvasToBackgroundImage = this.visualEditorConfig.resizeCanvasToBackgroundImage;
        this.isCenterBackgroundImage = this.visualEditorConfig.isCenterBackgroundImage;
        this.toolsFlex = this.visualEditorConfig.toolsFlex;
        this.actionsFlex = this.visualEditorConfig.actionsFlex;
        // Keyboard Shortcuts
        this.hasShortcutsExpansionPanel = this.visualEditorConfig.hasShortcutsExpansionPanel;
        this.hasCloneShortcut = this.visualEditorConfig.hasCloneShortcut;
        this.hasRemoveShortcut = this.visualEditorConfig.hasRemoveShortcut;
        this.hasRemoveAllShortcut = this.visualEditorConfig.hasRemoveAllShortcut;
        this.hasSendBackwardsShortcut = this.visualEditorConfig.hasSendBackwardsShortcut;
        this.hasBringForwardShortcut = this.visualEditorConfig.hasBringForwardShortcut;
        // FontAwesome Icons
        this.faClone = this.visualEditorConfig.faClone;
        this.faTrash = this.visualEditorConfig.faTrash;
        this.faTrashAlt = this.visualEditorConfig.faTrashAlt;
        this.faArrowUp = this.visualEditorConfig.faArrowUp;
        this.faArrowDown = this.visualEditorConfig.faArrowDown;
        this.fileService = this.visualEditorConfig.fileService;
        this.s3Service = this.visualEditorConfig.s3Service;
        if (!this.isWideCanvas) {
            this.editedImageCanvas = new fabric.Canvas('editedImageCanvas', {
                isDrawingMode: true
            });
        }
        else {
            this.editedImageCanvas = new fabric.Canvas('wideCanvas', {
                isDrawingMode: true
            });
        }
        this.loadCanvas();
        if (this.hasHiddenSelectionControls) {
            this.editedImageCanvas.on('selection:created', (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                _this.selectHiddenControls(e.selected);
            }));
            this.editedImageCanvas.on('selection:updated', (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                if (_this.editedImageCanvas.getActiveObject().isType('activeSelection')) {
                    _this.selectHiddenControls(_this.editedImageCanvas.getActiveObject().getObjects());
                }
                else {
                    _this.selectHiddenControls(e.selected);
                }
            }));
            this.editedImageCanvas.on('selection:cleared', (/**
             * @param {?} e
             * @return {?}
             */
            function (e) {
                _this.activeTool = _this.tools[0]; // Code changed to render the 'Remove All' and 'Submit' buttons all the time
            }));
        }
        if (this.isFirstToolSelected) {
            this.activeTool = this.tools[0];
            this.activeTool.canvasConfigs.forEach((/**
             * @param {?} confFunc
             * @return {?}
             */
            function (confFunc) {
                _this.editedImageCanvas = confFunc(_this.editedImageCanvas);
            }));
        }
        else {
            this.activeTool = {};
        }
        if (this.hasZoom) {
            this.addZoomControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addZoomControls(this.originalImageCanvas);
            }
        }
        if (this.hasPan) {
            this.addPanControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addPanControls(this.originalImageCanvas);
            }
        }
    };
    // End of ngOnInit function
    // End of ngOnInit function
    /**
     * @param {?} changes
     * @return {?}
     */
    VisualEditorComponent.prototype.ngOnChanges = 
    // End of ngOnInit function
    /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!changes.visualJson.isFirstChange()) {
            this.loadCanvas();
        }
    };
    /**
     * @return {?}
     */
    VisualEditorComponent.prototype.loadCanvas = /**
     * @return {?}
     */
    function () {
        this.VisualEditorService.resetObjectTypeCount();
        if (this.visualJson) {
            this.loadJson(this.visualJson, this.editedImageCanvas);
            this.editedImageCanvas.requestRenderAll();
        }
        else {
            this.setScaledImageToCanvas(this.editedImageCanvas, this.editedImageUrl, true);
            this.editedImageCanvas.requestRenderAll();
            if (this.originalImageUrl) {
                this.originalImageCanvas = new fabric.StaticCanvas('originalImageCanvas');
                this.setScaledImageToCanvas(this.originalImageCanvas, this.originalImageUrl);
                this.originalImageCanvas.requestRenderAll();
            }
        }
    };
    /**
     * @private
     * @return {?}
     */
    VisualEditorComponent.prototype.addCustomFonts = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        _.each(this.visualEditorConfig.tools, (/**
         * @param {?} t
         * @return {?}
         */
        function (t) {
            _.each(t.sections, (/**
             * @param {?} s
             * @return {?}
             */
            function (s) {
                _.each(s.actions, (/**
                 * @param {?} a
                 * @return {?}
                 */
                function (a) {
                    if (a.label === 'FONT FAMILY') {
                        a.dropdownOption = _.sortBy(_.union(a.dropdownOption, _.map(_this.customFonts, (/**
                         * @param {?} f
                         * @return {?}
                         */
                        function (f) { return f.fontFamily; }))));
                    }
                }));
            }));
        }));
    };
    /**
     * @private
     * @return {?}
     */
    VisualEditorComponent.prototype.loadCustomFonts = /**
     * @private
     * @return {?}
     */
    function () {
        // FIXME: IE support
        /** @type {?} */
        var customFontFaces = _.map(this.customFonts, (/**
         * @param {?} f
         * @return {?}
         */
        function (f) { return new FontFace(f.fontFamily, "url(" + f.fontFileUrl + ")"); }));
        _.each(customFontFaces, (/**
         * @param {?} cff
         * @return {?}
         */
        function (cff) {
            // NOTE: need to specify a font size for check() function
            if (!((/** @type {?} */ (document))).fonts.check("12px " + cff.family)) {
                cff
                    .load()
                    .then((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    ((/** @type {?} */ (document))).fonts.add(res);
                }))
                    .catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) {
                    // FIXME: Error display
                }));
            }
        }));
    };
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorComponent.prototype.addZoomControls = /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        var _this = this;
        canvas.on('mouse:wheel', (/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) {
            /** @type {?} */
            var delta = opt.e.deltaY;
            /** @type {?} */
            var zoom = canvas.getZoom();
            zoom = zoom - delta / _.min([canvas.getWidth(), canvas.getHeight()]);
            if (zoom > 20) {
                zoom = 20;
            }
            if (zoom < _this.minZoom) {
                zoom = _this.minZoom;
            }
            canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
            opt.e.preventDefault();
            opt.e.stopPropagation();
        }));
    };
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorComponent.prototype.addPanControls = /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        var _this = this;
        canvas.on('mouse:down', (/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) {
            /** @type {?} */
            var evt = opt.e;
            if (evt.altKey === true) {
                _this.isDragging = true;
                canvas.selection = false;
                _this.lastPosX = evt.clientX;
                _this.lastPosY = evt.clientY;
            }
        }));
        canvas.on('mouse:move', (/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) {
            if (_this.isDragging) {
                /** @type {?} */
                var e = opt.e;
                canvas.viewportTransform[4] += e.clientX - _this.lastPosX;
                canvas.viewportTransform[5] += e.clientY - _this.lastPosY;
                canvas.forEachObject((/**
                 * @param {?} o
                 * @return {?}
                 */
                function (o) { return o.setCoords(); }));
                canvas.requestRenderAll();
                _this.lastPosX = e.clientX;
                _this.lastPosY = e.clientY;
            }
        }));
        canvas.on('mouse:up', (/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) {
            _this.isDragging = false;
            canvas.selection = true;
        }));
    };
    // Start of loadJson function
    // Start of loadJson function
    /**
     * @private
     * @param {?} inputJson
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorComponent.prototype.loadJson = 
    // Start of loadJson function
    /**
     * @private
     * @param {?} inputJson
     * @param {?} canvas
     * @return {?}
     */
    function (inputJson, canvas) {
        var _this = this;
        /** @type {?} */
        var img = new Image();
        img.onload = (/**
         * @return {?}
         */
        function () {
            // Extracting the images width and height and adding it into the _canvasDimensions property in the inputJson
            if (_this.resizeCanvasToBackgroundImage) {
                inputJson._canvasDimensions = { width: img.width, height: img.height };
                inputJson.backgroundImage.width = inputJson._canvasDimensions.width;
                inputJson.backgroundImage.height = inputJson._canvasDimensions.height;
            }
            canvas.loadFromJSON(inputJson, (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var scaleFactor = _.min([
                    canvas.getWidth() / inputJson._canvasDimensions.width,
                    canvas.getHeight() / inputJson._canvasDimensions.height
                ]);
                _this.minZoom = scaleFactor;
                if (!_this.isWideCanvas) {
                    canvas.setWidth(inputJson._canvasDimensions.width * scaleFactor);
                    canvas.setHeight(inputJson._canvasDimensions.height * scaleFactor);
                    canvas.setZoom(_this.minZoom);
                    /** @type {?} */
                    var isWidthSmaller = inputJson._canvasDimensions.width / canvas.backgroundImage.width >
                        inputJson._canvasDimensions.height / canvas.backgroundImage.height;
                    if (isWidthSmaller) {
                        canvas.backgroundImage.scaleToWidth(inputJson._canvasDimensions.width);
                    }
                    else {
                        canvas.backgroundImage.scaleToHeight(inputJson._canvasDimensions.height);
                    }
                }
                else {
                    /** @type {?} */
                    var padding = 5;
                    /** @type {?} */
                    var border = 0;
                    /** @type {?} */
                    var newWidth = _this.wideCanvas.nativeElement.offsetWidth - 2 * (padding + border);
                    /** @type {?} */
                    var newHeight = _this.wideCanvas.nativeElement.offsetHeight - 2 * (padding + border);
                    canvas.setWidth(newWidth);
                    canvas.setHeight(newHeight);
                    canvas.setZoom(0.8 *
                        _.min([
                            canvas.getWidth() / inputJson._canvasDimensions.width,
                            canvas.getHeight() / inputJson._canvasDimensions.height
                        ]));
                    canvas.viewportTransform[4] =
                        (newWidth - inputJson._canvasDimensions.width * canvas.getZoom()) /
                            2;
                    canvas.viewportTransform[5] =
                        (newHeight -
                            inputJson._canvasDimensions.height * canvas.getZoom()) /
                            2;
                    canvas.forEachObject((/**
                     * @param {?} o
                     * @return {?}
                     */
                    function (o) { return o.setCoords(); }));
                }
                if (_this.isCenterBackgroundImage) {
                    canvas.viewportCenterObject(canvas.backgroundImage); // Centers the image on the canvas
                }
                if (_this.isShowFrame) {
                    /** @type {?} */
                    var strokeWidth = 3;
                    /** @type {?} */
                    var rectangle = new fabric.Rect({
                        left: -strokeWidth,
                        top: -strokeWidth,
                        height: inputJson._canvasDimensions.height + strokeWidth,
                        width: inputJson._canvasDimensions.width + strokeWidth,
                        stroke: 'rgb(0,255,0)',
                        strokeWidth: strokeWidth,
                        strokeDashArray: [5 * strokeWidth, 5 * strokeWidth],
                        fill: 'rgba(0,0,0,0)',
                        selectable: false,
                        evented: false
                    });
                    rectangle._key = 'frame_rectangle';
                    canvas.add(rectangle);
                    rectangle.bringToFront();
                }
                canvas.renderAll();
            }));
        });
        img.src = inputJson.backgroundImage.src;
    };
    // End of loadJson function
    // End of loadJson function
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    VisualEditorComponent.prototype.resetCanvas = 
    // End of loadJson function
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    function (canvas) {
        canvas.getObjects().forEach((/**
         * @param {?} o
         * @return {?}
         */
        function (o) { return canvas.remove(o); }));
    };
    /**
     * @private
     * @param {?} canvas
     * @param {?} imageUrl
     * @param {?=} isEditedImage
     * @return {?}
     */
    VisualEditorComponent.prototype.setScaledImageToCanvas = /**
     * @private
     * @param {?} canvas
     * @param {?} imageUrl
     * @param {?=} isEditedImage
     * @return {?}
     */
    function (canvas, imageUrl, isEditedImage) {
        var _this = this;
        if (isEditedImage === void 0) { isEditedImage = false; }
        fabric.Image.fromURL(imageUrl, (/**
         * @param {?} img
         * @return {?}
         */
        function (img) {
            /** @type {?} */
            var isWidthSmaller = canvas.getWidth() / img.width > canvas.getHeight() / img.height;
            if (isWidthSmaller) {
                img.scaleToHeight(canvas.getHeight());
            }
            else {
                img.scaleToWidth(canvas.getWidth());
            }
            var width = img.width, height = img.height;
            /** @type {?} */
            var left = isWidthSmaller
                ? (canvas.getWidth() - width * img.scaleX) / 2.0
                : 0;
            /** @type {?} */
            var top = isWidthSmaller
                ? 0
                : (canvas.getHeight() - height * img.scaleX) / 2.0;
            img.set({
                left: left,
                top: top
            });
            if (isEditedImage) {
                _this.imageConfig = {
                    width: width,
                    height: height,
                    scaleFactor: img.scaleX,
                    xOffset: left,
                    yOffset: top
                };
            }
            if (_this.hasCheckersBg) {
                canvas.add(img);
                canvas.renderAll();
                fabric.Image.fromURL(_this.checkersImageUrl, (/**
                 * @param {?} checkerImg
                 * @return {?}
                 */
                function (checkerImg) {
                    checkerImg.scaleToWidth(canvas.getWidth());
                    canvas.setBackgroundImage(checkerImg, canvas.renderAll.bind(canvas));
                }));
            }
            else {
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            }
        }));
    };
    /**
     * @private
     * @param {?} selectedObjects
     * @return {?}
     */
    VisualEditorComponent.prototype.selectHiddenControls = /**
     * @private
     * @param {?} selectedObjects
     * @return {?}
     */
    function (selectedObjects) {
        var _this = this;
        if (selectedObjects.length === 1) {
            /** @type {?} */
            var isShape_1 = selectedObjects[0].isType(ObjectTypes.RECT) ||
                selectedObjects[0].isType(ObjectTypes.CIRCLE) ||
                selectedObjects[0].isType(ObjectTypes.POLYGON);
            /** @type {?} */
            var isImage_1 = selectedObjects[0].isType(ObjectTypes.IMAGE);
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            function (t) {
                if (t.isHidden) {
                    if (isShape_1) {
                        if (t.objectType === ObjectTypes.SHAPE) {
                            _this.onSelectTool(t);
                        }
                    }
                    else if (isImage_1) {
                        if (selectedObjects[0]._key &&
                            selectedObjects[0]._key.includes(ObjectTypes.PRODUCT)) {
                            if (t.objectType === ObjectTypes.PRODUCT) {
                                _this.onSelectTool(t);
                            }
                        }
                        else {
                            // Non product image
                            if (selectedObjects[0].isType(t.objectType)) {
                                _this.onSelectTool(t);
                            }
                        }
                    }
                    else {
                        // Non shape and non image object
                        if (selectedObjects[0].isType(t.objectType)) {
                            _this.onSelectTool(t);
                        }
                    }
                }
            }));
        }
        else if (selectedObjects.length > 1) {
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            function (t) {
                if (t.isHidden && t.objectType === ObjectTypes.GROUP) {
                    _this.onSelectTool(t);
                }
            }));
        }
    };
    /**
     * @param {?} tool
     * @return {?}
     */
    VisualEditorComponent.prototype.onSelectTool = /**
     * @param {?} tool
     * @return {?}
     */
    function (tool) {
        var _this = this;
        if (tool.type === ActionTypes.UPLOAD) {
            this.fileUpload.click();
        }
        else {
            if (!_.isUndefined(tool.onSelect)) {
                tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                this.activeTool = {};
            }
            if (!this.activeTool || this.activeTool.name !== tool.name) {
                this.activeTool = tool;
                if (tool.canvasConfigs) {
                    tool.canvasConfigs.forEach((/**
                     * @param {?} confFunc
                     * @return {?}
                     */
                    function (confFunc) {
                        _this.editedImageCanvas = confFunc(_this.editedImageCanvas);
                    }));
                }
            }
        }
    };
    // Start of private onToolAction(action) function
    // Start of private onToolAction(action) function
    /**
     * @param {?} action
     * @return {?}
     */
    VisualEditorComponent.prototype.onToolAction = 
    // Start of private onToolAction(action) function
    /**
     * @param {?} action
     * @return {?}
     */
    function (action) {
        var _this = this;
        switch (action.type) {
            case ActionTypes.BUTTON: {
                /** @type {?} */
                var res = void 0;
                if (!_.isUndefined(action.onSelectParams)) {
                    res = action.action(this.editedImageCanvas, action.onSelectParams);
                }
                else {
                    res = action.action(this.editedImageCanvas);
                }
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                if (!_.isUndefined(res) && !_.isUndefined(action.onActionReturn)) {
                    action.onActionReturn(action, res);
                }
                break;
            }
            case ActionTypes.TOGGLE: {
                action.isOn = !action.isOn;
                action.action(this.editedImageCanvas, action.isOn ? action.onValue : action.offValue);
                break;
            }
            case ActionTypes.DROPDOWN: {
                action.action(this.editedImageCanvas, action.dropdownSelectedOption);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.RELATED_INPUT: {
                /** @type {?} */
                var relatedValues_1 = {};
                this.activeTool.sections.forEach((/**
                 * @param {?} s
                 * @return {?}
                 */
                function (s) {
                    s.actions.forEach((/**
                     * @param {?} a
                     * @return {?}
                     */
                    function (a) {
                        if (a.type === ActionTypes.RELATED_INPUT &&
                            a.groupName === action.groupName &&
                            a.label !== action.label) {
                            relatedValues_1[a.keyName] = a.inputValue;
                        }
                    }));
                }));
                relatedValues_1[action.keyName] = action.inputValue;
                action.action(this.editedImageCanvas, relatedValues_1);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.DIALOG: {
                if (action.dialog) {
                    /** @type {?} */
                    var dialogRef = this.dialog.open(action.dialog.component, {
                        autoFocus: false,
                        data: action.dialog.data,
                        minWidth: '300px',
                        panelClass: 'app-dialog'
                    });
                    dialogRef.afterClosed().subscribe((/**
                     * @param {?} resDialog
                     * @return {?}
                     */
                    function (resDialog) {
                        if (!_.isUndefined(resDialog)) {
                            /** @type {?} */
                            var resAction = action.dialog.onClose(_this.editedImageCanvas, resDialog);
                            if (!_.isUndefined(resAction) &&
                                !_.isUndefined(action.onActionReturn)) {
                                action.onActionReturn(action, resAction);
                            }
                        }
                    }));
                }
                break;
            }
            case ActionTypes.OUTGOING_EVENT_TRIGGER: {
                this.outgoingEventTriggered.emit({
                    canvas: this.editedImageCanvas,
                    transformedCanvas: action.label === 'Submit'
                        ? action.action(this.visualJson.backgroundImage.src, this.editedImageCanvas.toJSON(['_canvasDimensions']))
                        : ''
                });
                break;
            }
            default: {
                action.action(this.editedImageCanvas);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
        }
    };
    // End of private onToolAction(action) function
    // End of private onToolAction(action) function
    /**
     * @param {?} imgUrl
     * @return {?}
     */
    VisualEditorComponent.prototype.onUpdate = 
    // End of private onToolAction(action) function
    /**
     * @param {?} imgUrl
     * @return {?}
     */
    function (imgUrl) {
        this.resetCanvas(this.editedImageCanvas);
        this.setScaledImageToCanvas(this.editedImageCanvas, imgUrl, true);
    };
    /**
     * @return {?}
     */
    VisualEditorComponent.prototype.getCanvasAndConfig = /**
     * @return {?}
     */
    function () {
        return {
            canvas: this.editedImageCanvas,
            imageConfig: this.imageConfig
        };
    };
    /**
     * @param {?} res
     * @return {?}
     */
    VisualEditorComponent.prototype.onFileSelect = /**
     * @param {?} res
     * @return {?}
     */
    function (res) {
        var _this = this;
        var files = res.files, tool = res.caller;
        if (!_.isUndefined(tool)) {
            if (!_.isUndefined(tool.onSelect) && files.length > 0) {
                /** @type {?} */
                var file_1 = files[0];
                /** @type {?} */
                var filename = _.replace(file_1.name, /[^A-Z0-9.]+/gi, '_');
                this.fileService
                    .getUploadUrl({
                    filename: filename,
                    contentType: file_1.type,
                    expiresInSecs: 3600
                })
                    .subscribe((/**
                 * @param {?} respUploadUrl
                 * @return {?}
                 */
                function (respUploadUrl) {
                    _this.s3Service
                        .upload(respUploadUrl.url, file_1, file_1.type)
                        .subscribe((/**
                     * @param {?} respUpload
                     * @return {?}
                     */
                    function (respUpload) {
                        /** @type {?} */
                        var fileUrl = respUploadUrl.url.split('?')[0];
                        tool.onSelectParams.fileUrl = fileUrl;
                        tool.onSelect(_this.editedImageCanvas, tool.onSelectParams);
                        _this.activeTool = {};
                    }));
                }));
            }
        }
    };
    // Keyboard Shortcuts
    // Keyboard Shortcuts
    /**
     * @param {?} event
     * @return {?}
     */
    VisualEditorComponent.prototype.canvasKeyboardEvent = 
    // Keyboard Shortcuts
    /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.hasCloneShortcut &&
            ((event.ctrlKey || event.metaKey) &&
                event.shiftKey &&
                event.keyCode === 86)) {
            this.VisualEditorService.cloneObject(this.editedImageCanvas);
        }
        if (this.hasRemoveShortcut && (!event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteSelection(this.editedImageCanvas);
        }
        if (this.hasRemoveAllShortcut && (event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteAllSelections(this.editedImageCanvas);
        }
        if (this.hasSendBackwardsShortcut &&
            (event.shiftKey && event.keyCode === 40)) {
            this.VisualEditorService.sendBackwards(this.editedImageCanvas);
        }
        if (this.hasBringForwardShortcut &&
            (event.shiftKey && event.keyCode === 38)) {
            this.VisualEditorService.bringForward(this.editedImageCanvas);
        }
    };
    VisualEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-visual-editor',
                    template: "<lib-file-upload #fileUpload\n[accept]=\"'image/png, image/jpg, image/gif'\"\n[multiple]=\"false\"\n(selectedFilesWithCaller)=\"onFileSelect($event)\"></lib-file-upload>\n\n<!-- Template starts from here -->\n<div fxFill fxLayout=\"row\" fxLayoutGap='2%' fxLayoutAlign=\"space-between stretch\" (keydown)=\"canvasKeyboardEvent($event)\">\n\n  <!-- Start of LHS Container -->\n  <div [fxFlex]=\"toolsFlex\" class=\"side-panel left\" fxLayout=\"column\" fxLayoutAlign=\"center stretch\" fxLayoutGap='7%'>\n      <div *ngFor=\"let tool of tools\"  fxLayoutAlign=\"center\"> \n          \n          <button *ngIf=\"!tool.isHidden && tool.type !== ActionTypes.UPLOAD\"\n               [ngStyle]='tool.style'\n                mat-flat-button\n                color=\"primary\"\n                class='button-width-lhs button-sharp button-font-size'\n                (click)=\"onSelectTool(tool)\">\n            <img *ngIf='tool.iconImgUrl'class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n          <button *ngIf=\"!tool.isHidden && tool.type === ActionTypes.UPLOAD\"\n                mat-flat-button\n                fxLayoutAlign=\"center\"\n                color=\"primary\"\n                class=\"button-width-lhs button-sharp button-font-size\"\n                (click)=\"fileUpload.click(tool)\">\n            <img *ngIf='tool.iconImgUrl' class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n      </div> \n    </div> \n  <!-- /End LHS Container -->\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!originalImageUrl || isWideCanvas\">\n    <canvas id=\"originalImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"isWideCanvas\">\n    <canvas id=\"editedImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div class=\"wideCanvasContainer\" #wideCanvas fxFlex=\"1 0 auto\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!isWideCanvas\">\n    <canvas id=\"wideCanvas\">\n    </canvas>\n  </div>\n\n  <!-- Start of RHS Container -->\n  <div [fxFlex]=\"actionsFlex\" class=\"side-panel right\" fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap='5%'>\n      <div fxLayout=\"row\" fxLayoutAlign=\"center stretch\">\n          <mat-expansion-panel *ngIf='hasShortcutsExpansionPanel' class='expansion-panel-spacing-bottom'>\n              <mat-expansion-panel-header>\n                <mat-panel-description class='expansion-panel-header-font-size'>Keyboard shortcuts</mat-panel-description>\n              </mat-expansion-panel-header>\n                    <mat-list role=\"list\">\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasCloneShortcut'><fa-icon [icon]='faClone' class='action-icon-spacing'></fa-icon>Clone - Ctrl/Cmd + Shift + V</mat-list-item>\n                      <mat-divider *ngIf='hasCloneShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveShortcut'><fa-icon [icon]='faTrashAlt' class='action-icon-spacing'></fa-icon>Remove - Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveAllShortcut'><fa-icon [icon]='faTrash' class='action-icon-spacing'></fa-icon>Remove All - Shift + Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveAllShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasBringForwardShortcut'><fa-icon [icon]='faArrowUp' class='action-icon-spacing'></fa-icon>Bring Forward - Shift + Up Arrow</mat-list-item>\n                      <mat-divider *ngIf='hasBringForwardShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasSendBackwardsShortcut'><fa-icon [icon]='faArrowDown' class='action-icon-spacing'></fa-icon>Send Backwards - Shift + Down Arrow</mat-list-item>\n                    </mat-list>\n            </mat-expansion-panel>\n      </div>\n\n      <div *ngFor=\"let section of activeTool.sections\" [fxFlex]=\"section.flex\" fxLayout=\"row\" fxLayoutAlign=\"center stretch\" class='action-section' [class.end-section]='section.isEndSection' fxLayoutGap='2%'>\n        \n        <div *ngFor=\"let action of section.actions\" [fxFlex]=\"action.flex\"> \n\n          <div [ngSwitch]=\"action.type\">\n\n            <div *ngSwitchCase=\"ActionTypes.BUTTON\">\n              <button fxFill mat-stroked-button\n                      [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                      matTooltipPosition='above'\n                      color=\"primary\"\n                      class='button-sharp button-font-size'\n                      [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                      (click)=\"onToolAction(action)\">\n                      <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                      <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                      <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                      <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n              </button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.OUTGOING_EVENT_TRIGGER\">\n              <button fxFill mat-flat-button\n              color=\"primary\"\n              class='button-sharp button-font-size'\n              (click)=\"onToolAction(action)\">{{ action.label }}</button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.DIALOG\">\n                <button fxFill mat-stroked-button\n                        [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                        matTooltipPosition='above'\n                        color=\"primary\"\n                        class=\"button-sharp button-font-size\"\n                        [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                        (click)=\"onToolAction(action)\">\n                  <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                  <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                  <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                  <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n                </button>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.DROPDOWN\">\n                <mat-form-field fxFill>\n                  <mat-label>{{ action.label }}</mat-label>\n                  <mat-select [(value)]=\"action.dropdownSelectedOption\" (selectionChange)=\"onToolAction(action)\">\n                    <mat-option *ngFor=\"let value of action.dropdownOption\" [value]=\"value\">\n                      {{ value }}\n                    </mat-option>\n                  </mat-select>\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.RELATED_INPUT\">\n                <mat-form-field floatLabel=\"never\" class=\"editor-input-field\">\n                  <input matInput\n                         [(ngModel)]=\"action.inputValue\"\n                         (change)=\"onToolAction(action)\"\n                         [type]=\"action.inputType\">\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.TOGGLE\">\n                <div>\n                  {{ action.isOn ? action.onLabel : action.offLabel}}\n                </div>\n                <mat-slide-toggle (change)=\"onToolAction(action)\" class=\"visual-editor-toggle\"></mat-slide-toggle>\n              </div>\n  \n              <div *ngSwitchDefault>\n                <button fxFill mat-button\n                        color=\"primary\"\n                        class=\"primary-button\"\n                        (click)=\"onToolAction(action)\">\n                  <i [class]=\"action.iconClass + ' tool-icon'\"></i>\n                </button>\n              </div>\n            \n          </div> <!-- End of [ngSwitch]=\"action.type\" div -->\n          \n        </div> <!-- End of *ngFor=\"let action of section.actions\" div -->\n      </div> <!-- End of *ngFor=\"let section of activeTool.sections\" div-->\n  </div>\n  <!-- /End of RHS Container  -->\n</div> \n\n\n",
                    styles: ["div.canvas-invisible{display:none!important}.button-sharp{border-radius:0}.button-font-size{font-size:.8vw}.button-width-lhs{width:8vw}.button-width-rhs{width:6vw}.action-icon{max-width:40px;max-height:40px}.action-icon-spacing{margin-right:3px}.canvas{border:1px solid #d5d5d5}.wideCanvasContainer{padding:5px;background-image:url(/assets/images/checkers_bg2.png)}.expansion-panel-header-font-size{font-size:.9vw}.expansion-panel-spacing-bottom{margin-bottom:15vh}.list-item-font-size{font-size:.7vw}.side-panel{padding:0}.side-panel.left{border-right:1px solid #eee}.side-panel.right{border-left:1px solid #eee}"]
                }] }
    ];
    /** @nocollapse */
    VisualEditorComponent.ctorParameters = function () { return [
        { type: MatDialog }
    ]; };
    VisualEditorComponent.propDecorators = {
        originalImageUrl: [{ type: Input }],
        editedImageUrl: [{ type: Input }],
        visualJson: [{ type: Input }],
        visualEditorConfig: [{ type: Input }],
        customFonts: [{ type: Input }],
        outgoingEventTriggered: [{ type: Output }],
        outgoingSelectionEventTriggered: [{ type: Output }],
        wideCanvas: [{ type: ViewChild, args: ['wideCanvas',] }],
        canvasKeyboardEvent: [{ type: HostListener, args: ['window:keydown', ['$event'],] }]
    };
    return VisualEditorComponent;
}());
export { VisualEditorComponent };
if (false) {
    /** @type {?} */
    VisualEditorComponent.prototype.originalImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.editedImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.visualJson;
    /** @type {?} */
    VisualEditorComponent.prototype.visualEditorConfig;
    /** @type {?} */
    VisualEditorComponent.prototype.customFonts;
    /** @type {?} */
    VisualEditorComponent.prototype.outgoingEventTriggered;
    /** @type {?} */
    VisualEditorComponent.prototype.outgoingSelectionEventTriggered;
    /** @type {?} */
    VisualEditorComponent.prototype.wideCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.ActionTypes;
    /** @type {?} */
    VisualEditorComponent.prototype.VisualEditorService;
    /** @type {?} */
    VisualEditorComponent.prototype.checkersImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.originalImageCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.editedImageCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.imageConfig;
    /** @type {?} */
    VisualEditorComponent.prototype.tools;
    /** @type {?} */
    VisualEditorComponent.prototype.activeTool;
    /** @type {?} */
    VisualEditorComponent.prototype.objectTypeCount;
    /** @type {?} */
    VisualEditorComponent.prototype.isFirstToolSelected;
    /** @type {?} */
    VisualEditorComponent.prototype.hasCheckersBg;
    /** @type {?} */
    VisualEditorComponent.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    VisualEditorComponent.prototype.hasZoom;
    /** @type {?} */
    VisualEditorComponent.prototype.hasPan;
    /** @type {?} */
    VisualEditorComponent.prototype.isWideCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.isShowFrame;
    /** @type {?} */
    VisualEditorComponent.prototype.resizeCanvasToBackgroundImage;
    /** @type {?} */
    VisualEditorComponent.prototype.isCenterBackgroundImage;
    /** @type {?} */
    VisualEditorComponent.prototype.toolsFlex;
    /** @type {?} */
    VisualEditorComponent.prototype.actionsFlex;
    /** @type {?} */
    VisualEditorComponent.prototype.selection;
    /** @type {?} */
    VisualEditorComponent.prototype.fileUpload;
    /** @type {?} */
    VisualEditorComponent.prototype.fileService;
    /** @type {?} */
    VisualEditorComponent.prototype.s3Service;
    /** @type {?} */
    VisualEditorComponent.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    VisualEditorComponent.prototype.hasCloneShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasRemoveShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasBringForwardShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.faClone;
    /** @type {?} */
    VisualEditorComponent.prototype.faTrash;
    /** @type {?} */
    VisualEditorComponent.prototype.faTrashAlt;
    /** @type {?} */
    VisualEditorComponent.prototype.faArrowUp;
    /** @type {?} */
    VisualEditorComponent.prototype.faArrowDown;
    /** @type {?} */
    VisualEditorComponent.prototype.isDragging;
    /** @type {?} */
    VisualEditorComponent.prototype.lastPosY;
    /** @type {?} */
    VisualEditorComponent.prototype.lastPosX;
    /** @type {?} */
    VisualEditorComponent.prototype.minZoom;
    /**
     * @type {?}
     * @private
     */
    VisualEditorComponent.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlzdWFsLWVkaXRvci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL3Zpc3VhbC1lZGl0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUdULFlBQVksRUFDWixLQUFLLEVBQ0wsTUFBTSxFQUNOLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUViLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sUUFBUSxDQUFDO0FBR2hCLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUU5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXZFO0lBcUVFLCtCQUFvQixNQUFpQjtRQUFqQixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBM0Q1QixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNoQiwyQkFBc0IsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBQ3BELG9DQUErQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFJdkUsZ0JBQVcsR0FBRyxXQUFXLENBQUM7UUFDMUIsd0JBQW1CLEdBQUcsbUJBQW1CLENBQUM7UUFFMUMscUJBQWdCLEdBQUcsd0NBQXdDLENBQUM7UUFHNUQsZ0JBQVcsR0FBRztZQUNaLEtBQUssRUFBRSxDQUFDO1lBQ1IsTUFBTSxFQUFFLENBQUM7WUFDVCxXQUFXLEVBQUUsQ0FBQztZQUNkLE9BQU8sRUFBRSxDQUFDO1lBQ1YsT0FBTyxFQUFFLENBQUM7U0FDWCxDQUFDO1FBRUYsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQXFDWCxZQUFPLEdBQUcsQ0FBQyxDQUFDO0lBRTRCLENBQUM7SUFFekMsNkJBQTZCOzs7OztJQUM3Qix3Q0FBUTs7Ozs7SUFBUjtRQUFBLGlCQXlGQztRQXhGQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXZCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQztRQUMzQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG1CQUFtQixDQUFDO1FBQ3ZFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsQ0FBQztRQUMzRCxJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDO1FBQ3JGLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7UUFDN0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDO1FBQ3pELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztRQUN2RCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDZCQUE2QixDQUFDO1FBQzNGLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUM7UUFDL0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztRQUV2RCxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQywwQkFBMEIsQ0FBQztRQUNyRixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDO1FBQ2pFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUM7UUFDbkUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxvQkFBb0IsQ0FBQztRQUN6RSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHdCQUF3QixDQUFDO1FBQ2pGLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsdUJBQXVCLENBQUM7UUFFL0Usb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztRQUMvQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7UUFDL0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQztRQUNuRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUM7UUFFdkQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQztRQUVuRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN0QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFO2dCQUM5RCxhQUFhLEVBQUUsSUFBSTthQUNwQixDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUU7Z0JBQ3ZELGFBQWEsRUFBRSxJQUFJO2FBQ3BCLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBRWxCLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsbUJBQW1COzs7O1lBQUUsVUFBQSxDQUFDO2dCQUM5QyxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3hDLENBQUMsRUFBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUI7Ozs7WUFBRSxVQUFBLENBQUM7Z0JBQzlDLElBQ0UsS0FBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxFQUNsRTtvQkFDQSxLQUFJLENBQUMsb0JBQW9CLENBQ3ZCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FDdEQsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QztZQUNILENBQUMsRUFBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUI7Ozs7WUFBRSxVQUFBLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLDRFQUE0RTtZQUMvRyxDQUFDLEVBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLFFBQVE7Z0JBQzVDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDNUQsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7U0FDdEI7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM3QyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzthQUNoRDtTQUNGO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUM1QyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzthQUMvQztTQUNGO0lBQ0gsQ0FBQztJQUNELDJCQUEyQjs7Ozs7O0lBRTNCLDJDQUFXOzs7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUNuQjtJQUNILENBQUM7Ozs7SUFFTSwwQ0FBVTs7O0lBQWpCO1FBQ0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDaEQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztTQUMzQzthQUFNO1lBQ0wsSUFBSSxDQUFDLHNCQUFzQixDQUN6QixJQUFJLENBQUMsaUJBQWlCLEVBQ3RCLElBQUksQ0FBQyxjQUFjLEVBQ25CLElBQUksQ0FDTCxDQUFDO1lBQ0YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDMUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLE1BQU0sQ0FBQyxZQUFZLENBQ2hELHFCQUFxQixDQUN0QixDQUFDO2dCQUNGLElBQUksQ0FBQyxzQkFBc0IsQ0FDekIsSUFBSSxDQUFDLG1CQUFtQixFQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQ3RCLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLENBQUM7YUFDN0M7U0FDRjtJQUNILENBQUM7Ozs7O0lBRU8sOENBQWM7Ozs7SUFBdEI7UUFBQSxpQkFlQztRQWRDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUs7Ozs7UUFBRSxVQUFBLENBQUM7WUFDckMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUTs7OztZQUFFLFVBQUEsQ0FBQztnQkFDbEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTzs7OztnQkFBRSxVQUFBLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxhQUFhLEVBQUU7d0JBQzdCLENBQUMsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FDekIsQ0FBQyxDQUFDLEtBQUssQ0FDTCxDQUFDLENBQUMsY0FBYyxFQUNoQixDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxXQUFXOzs7O3dCQUFFLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFVBQVUsRUFBWixDQUFZLEVBQUMsQ0FDM0MsQ0FDRixDQUFDO3FCQUNIO2dCQUNILENBQUMsRUFBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU8sK0NBQWU7Ozs7SUFBdkI7OztZQUVRLGVBQWUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUMzQixJQUFJLENBQUMsV0FBVzs7OztRQUNoQixVQUFBLENBQUMsSUFBSSxPQUFBLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsU0FBTyxDQUFDLENBQUMsV0FBVyxNQUFHLENBQUMsRUFBbkQsQ0FBbUQsRUFDekQ7UUFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWU7Ozs7UUFBRSxVQUFBLEdBQUc7WUFDekIseURBQXlEO1lBQ3pELElBQUksQ0FBQyxDQUFDLG1CQUFBLFFBQVEsRUFBTyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxVQUFRLEdBQUcsQ0FBQyxNQUFRLENBQUMsRUFBRTtnQkFDeEQsR0FBRztxQkFDQSxJQUFJLEVBQUU7cUJBQ04sSUFBSTs7OztnQkFBQyxVQUFBLEdBQUc7b0JBQ1AsQ0FBQyxtQkFBQSxRQUFRLEVBQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsRUFBQztxQkFDRCxLQUFLOzs7O2dCQUFDLFVBQUEsS0FBSztvQkFDVix1QkFBdUI7Z0JBQ3pCLENBQUMsRUFBQyxDQUFDO2FBQ047UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLCtDQUFlOzs7OztJQUF2QixVQUF3QixNQUFNO1FBQTlCLGlCQWtCQztRQWpCQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWE7Ozs7UUFBRSxVQUFBLEdBQUc7O2dCQUNwQixLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNOztnQkFDdEIsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFFM0IsSUFBSSxHQUFHLElBQUksR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksSUFBSSxHQUFHLEVBQUUsRUFBRTtnQkFDYixJQUFJLEdBQUcsRUFBRSxDQUFDO2FBQ1g7WUFDRCxJQUFJLElBQUksR0FBRyxLQUFJLENBQUMsT0FBTyxFQUFFO2dCQUN2QixJQUFJLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQzthQUNyQjtZQUVELE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFakUsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixHQUFHLENBQUMsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sOENBQWM7Ozs7O0lBQXRCLFVBQXVCLE1BQU07UUFBN0IsaUJBeUJDO1FBeEJDLE1BQU0sQ0FBQyxFQUFFLENBQUMsWUFBWTs7OztRQUFFLFVBQUEsR0FBRzs7Z0JBQ25CLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQztZQUNqQixJQUFJLEdBQUcsQ0FBQyxNQUFNLEtBQUssSUFBSSxFQUFFO2dCQUN2QixLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztnQkFDdkIsTUFBTSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQztnQkFDNUIsS0FBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO2FBQzdCO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsRUFBRSxDQUFDLFlBQVk7Ozs7UUFBRSxVQUFBLEdBQUc7WUFDekIsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFOztvQkFDYixDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ2YsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztnQkFDekQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQztnQkFDekQsTUFBTSxDQUFDLGFBQWE7Ozs7Z0JBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQWIsQ0FBYSxFQUFDLENBQUM7Z0JBQ3pDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2dCQUMxQixLQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUM7Z0JBQzFCLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQzthQUMzQjtRQUNILENBQUMsRUFBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLEVBQUUsQ0FBQyxVQUFVOzs7O1FBQUUsVUFBQSxHQUFHO1lBQ3ZCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1lBQ3hCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDZCQUE2Qjs7Ozs7Ozs7SUFDckIsd0NBQVE7Ozs7Ozs7O0lBQWhCLFVBQWlCLFNBQVMsRUFBRSxNQUFNO1FBQWxDLGlCQXVGQzs7WUF0Rk8sR0FBRyxHQUFHLElBQUksS0FBSyxFQUFFO1FBQ3ZCLEdBQUcsQ0FBQyxNQUFNOzs7UUFBRztZQUNYLDRHQUE0RztZQUM1RyxJQUFJLEtBQUksQ0FBQyw2QkFBNkIsRUFBRTtnQkFDdEMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDdkUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQztnQkFDcEUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQzthQUN2RTtZQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUzs7O1lBQUU7O29CQUN2QixXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQztvQkFDeEIsTUFBTSxDQUFDLFFBQVEsRUFBRSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLO29CQUNyRCxNQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU07aUJBQ3hELENBQUM7Z0JBQ0YsS0FBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7Z0JBRTNCLElBQUksQ0FBQyxLQUFJLENBQUMsWUFBWSxFQUFFO29CQUN0QixNQUFNLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLENBQUM7b0JBQ2pFLE1BQU0sQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQztvQkFDbkUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7O3dCQUV2QixjQUFjLEdBQ2xCLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxLQUFLO3dCQUNoRSxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBTTtvQkFDcEUsSUFBSSxjQUFjLEVBQUU7d0JBQ2xCLE1BQU0sQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUNqQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUNsQyxDQUFDO3FCQUNIO3lCQUFNO3dCQUNMLE1BQU0sQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUNsQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUNuQyxDQUFDO3FCQUNIO2lCQUNGO3FCQUFNOzt3QkFDQyxPQUFPLEdBQUcsQ0FBQzs7d0JBQ1gsTUFBTSxHQUFHLENBQUM7O3dCQUNWLFFBQVEsR0FDWixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQzs7d0JBQzlELFNBQVMsR0FDYixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztvQkFDckUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDMUIsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDNUIsTUFBTSxDQUFDLE9BQU8sQ0FDWixHQUFHO3dCQUNELENBQUMsQ0FBQyxHQUFHLENBQUM7NEJBQ0osTUFBTSxDQUFDLFFBQVEsRUFBRSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLOzRCQUNyRCxNQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU07eUJBQ3hELENBQUMsQ0FDTCxDQUFDO29CQUNGLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDOzRCQUNqRSxDQUFDLENBQUM7b0JBQ0osTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzt3QkFDekIsQ0FBQyxTQUFTOzRCQUNSLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDOzRCQUN4RCxDQUFDLENBQUM7b0JBQ0osTUFBTSxDQUFDLGFBQWE7Ozs7b0JBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQWIsQ0FBYSxFQUFDLENBQUM7aUJBQzFDO2dCQUVELElBQUksS0FBSSxDQUFDLHVCQUF1QixFQUFFO29CQUNoQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsa0NBQWtDO2lCQUN4RjtnQkFFRCxJQUFJLEtBQUksQ0FBQyxXQUFXLEVBQUU7O3dCQUNkLFdBQVcsR0FBRyxDQUFDOzt3QkFDZixTQUFTLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQyxJQUFJLEVBQUUsQ0FBQyxXQUFXO3dCQUNsQixHQUFHLEVBQUUsQ0FBQyxXQUFXO3dCQUNqQixNQUFNLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxXQUFXO3dCQUN4RCxLQUFLLEVBQUUsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxXQUFXO3dCQUN0RCxNQUFNLEVBQUUsY0FBYzt3QkFDdEIsV0FBVyxhQUFBO3dCQUNYLGVBQWUsRUFBRSxDQUFDLENBQUMsR0FBRyxXQUFXLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQzt3QkFDbkQsSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsS0FBSztxQkFDZixDQUFDO29CQUNGLFNBQVMsQ0FBQyxJQUFJLEdBQUcsaUJBQWlCLENBQUM7b0JBQ25DLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQ3RCLFNBQVMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDMUI7Z0JBRUQsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3JCLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFBLENBQUM7UUFDRixHQUFHLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDO0lBQzFDLENBQUM7SUFDRCwyQkFBMkI7Ozs7Ozs7SUFFbkIsMkNBQVc7Ozs7Ozs7SUFBbkIsVUFBb0IsTUFBTTtRQUN4QixNQUFNLENBQUMsVUFBVSxFQUFFLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBaEIsQ0FBZ0IsRUFBQyxDQUFDO0lBQ3JELENBQUM7Ozs7Ozs7O0lBRU8sc0RBQXNCOzs7Ozs7O0lBQTlCLFVBQStCLE1BQU0sRUFBRSxRQUFRLEVBQUUsYUFBcUI7UUFBdEUsaUJBNkNDO1FBN0NnRCw4QkFBQSxFQUFBLHFCQUFxQjtRQUNwRSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFROzs7O1FBQUUsVUFBQSxHQUFHOztnQkFDMUIsY0FBYyxHQUNsQixNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsU0FBUyxFQUFFLEdBQUcsR0FBRyxDQUFDLE1BQU07WUFDakUsSUFBSSxjQUFjLEVBQUU7Z0JBQ2xCLEdBQUcsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0wsR0FBRyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQzthQUNyQztZQUVPLElBQUEsaUJBQUssRUFBRSxtQkFBTTs7Z0JBQ2YsSUFBSSxHQUFHLGNBQWM7Z0JBQ3pCLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUc7Z0JBQ2hELENBQUMsQ0FBQyxDQUFDOztnQkFDQyxHQUFHLEdBQUcsY0FBYztnQkFDeEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxHQUFHLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsR0FBRztZQUVwRCxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUNOLElBQUksTUFBQTtnQkFDSixHQUFHLEtBQUE7YUFDSixDQUFDLENBQUM7WUFFSCxJQUFJLGFBQWEsRUFBRTtnQkFDakIsS0FBSSxDQUFDLFdBQVcsR0FBRztvQkFDakIsS0FBSyxPQUFBO29CQUNMLE1BQU0sUUFBQTtvQkFDTixXQUFXLEVBQUUsR0FBRyxDQUFDLE1BQU07b0JBQ3ZCLE9BQU8sRUFBRSxJQUFJO29CQUNiLE9BQU8sRUFBRSxHQUFHO2lCQUNiLENBQUM7YUFDSDtZQUVELElBQUksS0FBSSxDQUFDLGFBQWEsRUFBRTtnQkFDdEIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEIsTUFBTSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUVuQixNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsZ0JBQWdCOzs7O2dCQUFFLFVBQUEsVUFBVTtvQkFDcEQsVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztvQkFDM0MsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN2RSxDQUFDLEVBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzthQUMvRDtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sb0RBQW9COzs7OztJQUE1QixVQUE2QixlQUFlO1FBQTVDLGlCQTBDQztRQXpDQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOztnQkFDMUIsU0FBTyxHQUNYLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQkFDM0MsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO2dCQUM3QyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7O2dCQUMxQyxTQUFPLEdBQUcsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTzs7OztZQUFDLFVBQUEsQ0FBQztnQkFDbEIsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFO29CQUNkLElBQUksU0FBTyxFQUFFO3dCQUNYLElBQUksQ0FBQyxDQUFDLFVBQVUsS0FBSyxXQUFXLENBQUMsS0FBSyxFQUFFOzRCQUN0QyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUN0QjtxQkFDRjt5QkFBTSxJQUFJLFNBQU8sRUFBRTt3QkFDbEIsSUFDRSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs0QkFDdkIsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUNyRDs0QkFDQSxJQUFJLENBQUMsQ0FBQyxVQUFVLEtBQUssV0FBVyxDQUFDLE9BQU8sRUFBRTtnQ0FDeEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDdEI7eUJBQ0Y7NkJBQU07NEJBQ0wsb0JBQW9COzRCQUNwQixJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dDQUMzQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUN0Qjt5QkFDRjtxQkFDRjt5QkFBTTt3QkFDTCxpQ0FBaUM7d0JBQ2pDLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUU7NEJBQzNDLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3RCO3FCQUNGO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNLElBQUksZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxDQUFDO2dCQUNsQixJQUFJLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLFVBQVUsS0FBSyxXQUFXLENBQUMsS0FBSyxFQUFFO29CQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN0QjtZQUNILENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQUVNLDRDQUFZOzs7O0lBQW5CLFVBQW9CLElBQUk7UUFBeEIsaUJBaUJDO1FBaEJDLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsTUFBTSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDekI7YUFBTTtZQUNMLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQzthQUN0QjtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQzFELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dCQUN2QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztvQkFBQyxVQUFBLFFBQVE7d0JBQ2pDLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7b0JBQzVELENBQUMsRUFBQyxDQUFDO2lCQUNKO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxpREFBaUQ7Ozs7OztJQUNqRCw0Q0FBWTs7Ozs7O0lBQVosVUFBYSxNQUFNO1FBQW5CLGlCQTJGQztRQTFGQyxRQUFRLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDbkIsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7O29CQUNuQixHQUFHLFNBQUE7Z0JBRVAsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFO29CQUN6QyxHQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lCQUNwRTtxQkFBTTtvQkFDTCxHQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztpQkFDN0M7Z0JBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQUU7b0JBQ2hFLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUNwQztnQkFDRCxNQUFNO2FBQ1A7WUFDRCxLQUFLLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdkIsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxNQUFNLENBQ1gsSUFBSSxDQUFDLGlCQUFpQixFQUN0QixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUMvQyxDQUFDO2dCQUNGLE1BQU07YUFDUDtZQUNELEtBQUssV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxNQUFNLENBQUMsc0JBQXNCLENBQUMsQ0FBQztnQkFDckUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQzlELE1BQU07YUFDUDtZQUNELEtBQUssV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDOztvQkFDeEIsZUFBYSxHQUFHLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQSxDQUFDO29CQUNoQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU87Ozs7b0JBQUMsVUFBQSxDQUFDO3dCQUNqQixJQUNFLENBQUMsQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLGFBQWE7NEJBQ3BDLENBQUMsQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLFNBQVM7NEJBQ2hDLENBQUMsQ0FBQyxLQUFLLEtBQUssTUFBTSxDQUFDLEtBQUssRUFDeEI7NEJBQ0EsZUFBYSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDO3lCQUN6QztvQkFDSCxDQUFDLEVBQUMsQ0FBQztnQkFDTCxDQUFDLEVBQUMsQ0FBQztnQkFDSCxlQUFhLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7Z0JBQ2xELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGVBQWEsQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDOUQsTUFBTTthQUNQO1lBQ0QsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTs7d0JBQ1gsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFO3dCQUMxRCxTQUFTLEVBQUUsS0FBSzt3QkFDaEIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSTt3QkFDeEIsUUFBUSxFQUFFLE9BQU87d0JBQ2pCLFVBQVUsRUFBRSxZQUFZO3FCQUN6QixDQUFDO29CQUNGLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTOzs7O29CQUFDLFVBQUEsU0FBUzt3QkFDekMsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLEVBQUU7O2dDQUN2QixTQUFTLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQ3JDLEtBQUksQ0FBQyxpQkFBaUIsRUFDdEIsU0FBUyxDQUNWOzRCQUNELElBQ0UsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQztnQ0FDekIsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFDckM7Z0NBQ0EsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsU0FBUyxDQUFDLENBQUM7NkJBQzFDO3lCQUNGO29CQUNILENBQUMsRUFBQyxDQUFDO2lCQUNKO2dCQUNELE1BQU07YUFDUDtZQUNELEtBQUssV0FBVyxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3ZDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7b0JBQy9CLE1BQU0sRUFBRSxJQUFJLENBQUMsaUJBQWlCO29CQUM5QixpQkFBaUIsRUFDZixNQUFNLENBQUMsS0FBSyxLQUFLLFFBQVE7d0JBQ3ZCLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUNYLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLEdBQUcsRUFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FDckQ7d0JBQ0gsQ0FBQyxDQUFDLEVBQUU7aUJBQ1QsQ0FBQyxDQUFDO2dCQUNILE1BQU07YUFDUDtZQUNELE9BQU8sQ0FBQyxDQUFDO2dCQUNQLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM5RCxNQUFNO2FBQ1A7U0FDRjtJQUNILENBQUM7SUFDRCwrQ0FBK0M7Ozs7OztJQUV4Qyx3Q0FBUTs7Ozs7O0lBQWYsVUFBZ0IsTUFBTTtRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3BFLENBQUM7Ozs7SUFFTSxrREFBa0I7OztJQUF6QjtRQUNFLE9BQU87WUFDTCxNQUFNLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtZQUM5QixXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVc7U0FDOUIsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsNENBQVk7Ozs7SUFBWixVQUFhLEdBQUc7UUFBaEIsaUJBeUJDO1FBeEJTLElBQUEsaUJBQUssRUFBRSxpQkFBWTtRQUMzQixJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN4QixJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7O29CQUMvQyxNQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7b0JBQ2YsUUFBUSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBSSxDQUFDLElBQUksRUFBRSxlQUFlLEVBQUUsR0FBRyxDQUFDO2dCQUUzRCxJQUFJLENBQUMsV0FBVztxQkFDYixZQUFZLENBQUM7b0JBQ1osUUFBUSxVQUFBO29CQUNSLFdBQVcsRUFBRSxNQUFJLENBQUMsSUFBSTtvQkFDdEIsYUFBYSxFQUFFLElBQUk7aUJBQ3BCLENBQUM7cUJBQ0QsU0FBUzs7OztnQkFBQyxVQUFBLGFBQWE7b0JBQ3RCLEtBQUksQ0FBQyxTQUFTO3lCQUNYLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLE1BQUksRUFBRSxNQUFJLENBQUMsSUFBSSxDQUFDO3lCQUMxQyxTQUFTOzs7O29CQUFDLFVBQUEsVUFBVTs7NEJBQ2IsT0FBTyxHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDL0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO3dCQUN0QyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBQzNELEtBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO29CQUN2QixDQUFDLEVBQUMsQ0FBQztnQkFDUCxDQUFDLEVBQUMsQ0FBQzthQUNOO1NBQ0Y7SUFDSCxDQUFDO0lBRUQscUJBQXFCOzs7Ozs7SUFFckIsbURBQW1COzs7Ozs7SUFEbkIsVUFDb0IsS0FBb0I7UUFDdEMsSUFDRSxJQUFJLENBQUMsZ0JBQWdCO1lBQ3JCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUM7Z0JBQy9CLEtBQUssQ0FBQyxRQUFRO2dCQUNkLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxDQUFDLEVBQ3ZCO1lBQ0EsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUM5RDtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDdEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNsRTtRQUVELElBQUksSUFBSSxDQUFDLG9CQUFvQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3hFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN0RTtRQUVELElBQ0UsSUFBSSxDQUFDLHdCQUF3QjtZQUM3QixDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsRUFDeEM7WUFDQSxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ2hFO1FBRUQsSUFDRSxJQUFJLENBQUMsdUJBQXVCO1lBQzVCLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLEVBQUUsQ0FBQyxFQUN4QztZQUNBLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDL0Q7SUFDSCxDQUFDOztnQkE1b0JGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3Qix5MVJBQTZDOztpQkFFOUM7Ozs7Z0JBVFEsU0FBUzs7O21DQVdmLEtBQUs7aUNBQ0wsS0FBSzs2QkFDTCxLQUFLO3FDQUNMLEtBQUs7OEJBQ0wsS0FBSzt5Q0FDTCxNQUFNO2tEQUNOLE1BQU07NkJBRU4sU0FBUyxTQUFDLFlBQVk7c0NBOGxCdEIsWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDOztJQWlDNUMsNEJBQUM7Q0FBQSxBQTdvQkQsSUE2b0JDO1NBeG9CWSxxQkFBcUI7OztJQUNoQyxpREFBa0M7O0lBQ2xDLCtDQUFnQzs7SUFDaEMsMkNBQXlCOztJQUN6QixtREFBaUM7O0lBQ2pDLDRDQUEwQjs7SUFDMUIsdURBQThEOztJQUM5RCxnRUFBdUU7O0lBRXZFLDJDQUFnRDs7SUFFaEQsNENBQTBCOztJQUMxQixvREFBMEM7O0lBRTFDLGlEQUE0RDs7SUFDNUQsb0RBQXlCOztJQUN6QixrREFBdUI7O0lBQ3ZCLDRDQU1FOztJQUVGLHNDQUFXOztJQUNYLDJDQUFnQjs7SUFDaEIsZ0RBQXFCOztJQUNyQixvREFBNkI7O0lBQzdCLDhDQUF1Qjs7SUFDdkIsMkRBQW9DOztJQUNwQyx3Q0FBaUI7O0lBQ2pCLHVDQUFnQjs7SUFDaEIsNkNBQXNCOztJQUN0Qiw0Q0FBcUI7O0lBQ3JCLDhEQUF1Qzs7SUFDdkMsd0RBQWlDOztJQUNqQywwQ0FBZTs7SUFDZiw0Q0FBaUI7O0lBQ2pCLDBDQUFlOztJQUNmLDJDQUFnQjs7SUFDaEIsNENBQWlCOztJQUNqQiwwQ0FBZTs7SUFHZiwyREFBb0M7O0lBQ3BDLGlEQUEwQjs7SUFDMUIsa0RBQTJCOztJQUMzQixxREFBOEI7O0lBQzlCLHlEQUFrQzs7SUFDbEMsd0RBQWlDOztJQUdqQyx3Q0FBUTs7SUFDUix3Q0FBUTs7SUFDUiwyQ0FBVzs7SUFDWCwwQ0FBVTs7SUFDViw0Q0FBWTs7SUFFWiwyQ0FBb0I7O0lBQ3BCLHlDQUFpQjs7SUFDakIseUNBQWlCOztJQUNqQix3Q0FBWTs7Ozs7SUFFQSx1Q0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgT25DaGFuZ2VzLFxuICBFdmVudEVtaXR0ZXIsXG4gIElucHV0LFxuICBPdXRwdXQsXG4gIFZpZXdDaGlsZCxcbiAgRWxlbWVudFJlZixcbiAgSG9zdExpc3RlbmVyLFxuICBTaW1wbGVDaGFuZ2VzXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0ICdmYWJyaWMnO1xuZGVjbGFyZSBjb25zdCBmYWJyaWM6IGFueTtcbmRlY2xhcmUgbGV0IEZvbnRGYWNlOiBhbnk7XG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vY29uZmlncy9hY3Rpb24tdHlwZXMnO1xuaW1wb3J0IHsgVmlzdWFsRWRpdG9yU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLXZpc3VhbC1lZGl0b3InLFxuICB0ZW1wbGF0ZVVybDogJy4vdmlzdWFsLWVkaXRvci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3Zpc3VhbC1lZGl0b3IuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBWaXN1YWxFZGl0b3JDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIG9yaWdpbmFsSW1hZ2VVcmw6IHN0cmluZztcbiAgQElucHV0KCkgZWRpdGVkSW1hZ2VVcmw6IHN0cmluZztcbiAgQElucHV0KCkgdmlzdWFsSnNvbjogYW55O1xuICBASW5wdXQoKSB2aXN1YWxFZGl0b3JDb25maWc6IGFueTtcbiAgQElucHV0KCkgY3VzdG9tRm9udHMgPSBbXTtcbiAgQE91dHB1dCgpIG91dGdvaW5nRXZlbnRUcmlnZ2VyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcbiAgQE91dHB1dCgpIG91dGdvaW5nU2VsZWN0aW9uRXZlbnRUcmlnZ2VyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcblxuICBAVmlld0NoaWxkKCd3aWRlQ2FudmFzJykgd2lkZUNhbnZhczogRWxlbWVudFJlZjtcblxuICBBY3Rpb25UeXBlcyA9IEFjdGlvblR5cGVzO1xuICBWaXN1YWxFZGl0b3JTZXJ2aWNlID0gVmlzdWFsRWRpdG9yU2VydmljZTtcblxuICBjaGVja2Vyc0ltYWdlVXJsID0gJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9jaGVja2Vyc19iZzIucG5nJztcbiAgb3JpZ2luYWxJbWFnZUNhbnZhczogYW55O1xuICBlZGl0ZWRJbWFnZUNhbnZhczogYW55O1xuICBpbWFnZUNvbmZpZyA9IHtcbiAgICB3aWR0aDogMCxcbiAgICBoZWlnaHQ6IDAsXG4gICAgc2NhbGVGYWN0b3I6IDEsXG4gICAgeE9mZnNldDogMCxcbiAgICB5T2Zmc2V0OiAwXG4gIH07XG5cbiAgdG9vbHMgPSBbXTtcbiAgYWN0aXZlVG9vbDogYW55O1xuICBvYmplY3RUeXBlQ291bnQ6IGFueTtcbiAgaXNGaXJzdFRvb2xTZWxlY3RlZDogYm9vbGVhbjtcbiAgaGFzQ2hlY2tlcnNCZzogYm9vbGVhbjtcbiAgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHM6IGJvb2xlYW47XG4gIGhhc1pvb206IGJvb2xlYW47XG4gIGhhc1BhbjogYm9vbGVhbjtcbiAgaXNXaWRlQ2FudmFzOiBib29sZWFuO1xuICBpc1Nob3dGcmFtZTogYm9vbGVhbjtcbiAgcmVzaXplQ2FudmFzVG9CYWNrZ3JvdW5kSW1hZ2U6IGJvb2xlYW47XG4gIGlzQ2VudGVyQmFja2dyb3VuZEltYWdlOiBib29sZWFuO1xuICB0b29sc0ZsZXg6IGFueTtcbiAgYWN0aW9uc0ZsZXg6IGFueTtcbiAgc2VsZWN0aW9uOiBhbnk7XG4gIGZpbGVVcGxvYWQ6IGFueTtcbiAgZmlsZVNlcnZpY2U6IGFueTtcbiAgczNTZXJ2aWNlOiBhbnk7XG5cbiAgLy8gS2V5Ym9hcmQgU2hvcnRjdXRzXG4gIGhhc1Nob3J0Y3V0c0V4cGFuc2lvblBhbmVsOiBib29sZWFuO1xuICBoYXNDbG9uZVNob3J0Y3V0OiBib29sZWFuO1xuICBoYXNSZW1vdmVTaG9ydGN1dDogYm9vbGVhbjtcbiAgaGFzUmVtb3ZlQWxsU2hvcnRjdXQ6IGJvb2xlYW47XG4gIGhhc1NlbmRCYWNrd2FyZHNTaG9ydGN1dDogYm9vbGVhbjtcbiAgaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQ6IGJvb2xlYW47XG5cbiAgLy8gRm9udEF3ZXNvbWUgSWNvbnNcbiAgZmFDbG9uZTtcbiAgZmFUcmFzaDtcbiAgZmFUcmFzaEFsdDtcbiAgZmFBcnJvd1VwO1xuICBmYUFycm93RG93bjtcblxuICBpc0RyYWdnaW5nOiBib29sZWFuO1xuICBsYXN0UG9zWTogbnVtYmVyO1xuICBsYXN0UG9zWDogbnVtYmVyO1xuICBtaW5ab29tID0gMTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRpYWxvZzogTWF0RGlhbG9nKSB7fVxuXG4gIC8vIFN0YXJ0IG9mIG5nT25Jbml0IGZ1bmN0aW9uXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuYWRkQ3VzdG9tRm9udHMoKTtcbiAgICB0aGlzLmxvYWRDdXN0b21Gb250cygpO1xuXG4gICAgdGhpcy50b29scyA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnRvb2xzO1xuICAgIHRoaXMuaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmlzRmlyc3RUb29sU2VsZWN0ZWQ7XG4gICAgdGhpcy5oYXNDaGVja2Vyc0JnID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzQ2hlY2tlcnNCZztcbiAgICB0aGlzLmhhc0hpZGRlblNlbGVjdGlvbkNvbnRyb2xzID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHM7XG4gICAgdGhpcy5oYXNab29tID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzWm9vbTtcbiAgICB0aGlzLmhhc1BhbiA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmhhc1BhbjtcbiAgICB0aGlzLmlzV2lkZUNhbnZhcyA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmlzV2lkZUNhbnZhcztcbiAgICB0aGlzLmlzU2hvd0ZyYW1lID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaXNTaG93RnJhbWU7XG4gICAgdGhpcy5yZXNpemVDYW52YXNUb0JhY2tncm91bmRJbWFnZSA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnJlc2l6ZUNhbnZhc1RvQmFja2dyb3VuZEltYWdlO1xuICAgIHRoaXMuaXNDZW50ZXJCYWNrZ3JvdW5kSW1hZ2UgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5pc0NlbnRlckJhY2tncm91bmRJbWFnZTtcbiAgICB0aGlzLnRvb2xzRmxleCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnRvb2xzRmxleDtcbiAgICB0aGlzLmFjdGlvbnNGbGV4ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuYWN0aW9uc0ZsZXg7XG5cbiAgICAvLyBLZXlib2FyZCBTaG9ydGN1dHNcbiAgICB0aGlzLmhhc1Nob3J0Y3V0c0V4cGFuc2lvblBhbmVsID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzU2hvcnRjdXRzRXhwYW5zaW9uUGFuZWw7XG4gICAgdGhpcy5oYXNDbG9uZVNob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzQ2xvbmVTaG9ydGN1dDtcbiAgICB0aGlzLmhhc1JlbW92ZVNob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzUmVtb3ZlU2hvcnRjdXQ7XG4gICAgdGhpcy5oYXNSZW1vdmVBbGxTaG9ydGN1dCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmhhc1JlbW92ZUFsbFNob3J0Y3V0O1xuICAgIHRoaXMuaGFzU2VuZEJhY2t3YXJkc1Nob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzU2VuZEJhY2t3YXJkc1Nob3J0Y3V0O1xuICAgIHRoaXMuaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5oYXNCcmluZ0ZvcndhcmRTaG9ydGN1dDtcblxuICAgIC8vIEZvbnRBd2Vzb21lIEljb25zXG4gICAgdGhpcy5mYUNsb25lID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuZmFDbG9uZTtcbiAgICB0aGlzLmZhVHJhc2ggPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5mYVRyYXNoO1xuICAgIHRoaXMuZmFUcmFzaEFsdCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZhVHJhc2hBbHQ7XG4gICAgdGhpcy5mYUFycm93VXAgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5mYUFycm93VXA7XG4gICAgdGhpcy5mYUFycm93RG93biA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZhQXJyb3dEb3duO1xuXG4gICAgdGhpcy5maWxlU2VydmljZSA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZpbGVTZXJ2aWNlO1xuICAgIHRoaXMuczNTZXJ2aWNlID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuczNTZXJ2aWNlO1xuXG4gICAgaWYgKCF0aGlzLmlzV2lkZUNhbnZhcykge1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyA9IG5ldyBmYWJyaWMuQ2FudmFzKCdlZGl0ZWRJbWFnZUNhbnZhcycsIHtcbiAgICAgICAgaXNEcmF3aW5nTW9kZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMgPSBuZXcgZmFicmljLkNhbnZhcygnd2lkZUNhbnZhcycsIHtcbiAgICAgICAgaXNEcmF3aW5nTW9kZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgdGhpcy5sb2FkQ2FudmFzKCk7XG5cbiAgICBpZiAodGhpcy5oYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scykge1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOmNyZWF0ZWQnLCBlID0+IHtcbiAgICAgICAgdGhpcy5zZWxlY3RIaWRkZW5Db250cm9scyhlLnNlbGVjdGVkKTtcbiAgICAgIH0pO1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOnVwZGF0ZWQnLCBlID0+IHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCkuaXNUeXBlKCdhY3RpdmVTZWxlY3Rpb24nKVxuICAgICAgICApIHtcbiAgICAgICAgICB0aGlzLnNlbGVjdEhpZGRlbkNvbnRyb2xzKFxuICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKS5nZXRPYmplY3RzKClcbiAgICAgICAgICApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2VsZWN0SGlkZGVuQ29udHJvbHMoZS5zZWxlY3RlZCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOmNsZWFyZWQnLCBlID0+IHtcbiAgICAgICAgdGhpcy5hY3RpdmVUb29sID0gdGhpcy50b29sc1swXTsgLy8gQ29kZSBjaGFuZ2VkIHRvIHJlbmRlciB0aGUgJ1JlbW92ZSBBbGwnIGFuZCAnU3VibWl0JyBidXR0b25zIGFsbCB0aGUgdGltZVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaXNGaXJzdFRvb2xTZWxlY3RlZCkge1xuICAgICAgdGhpcy5hY3RpdmVUb29sID0gdGhpcy50b29sc1swXTtcbiAgICAgIHRoaXMuYWN0aXZlVG9vbC5jYW52YXNDb25maWdzLmZvckVhY2goY29uZkZ1bmMgPT4ge1xuICAgICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzID0gY29uZkZ1bmModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5hY3RpdmVUb29sID0ge307XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzWm9vbSkge1xuICAgICAgdGhpcy5hZGRab29tQ29udHJvbHModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICBpZiAodGhpcy5vcmlnaW5hbEltYWdlVXJsKSB7XG4gICAgICAgIHRoaXMuYWRkWm9vbUNvbnRyb2xzKHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzUGFuKSB7XG4gICAgICB0aGlzLmFkZFBhbkNvbnRyb2xzKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgaWYgKHRoaXMub3JpZ2luYWxJbWFnZVVybCkge1xuICAgICAgICB0aGlzLmFkZFBhbkNvbnRyb2xzKHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcyk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC8vIEVuZCBvZiBuZ09uSW5pdCBmdW5jdGlvblxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICBpZiAoIWNoYW5nZXMudmlzdWFsSnNvbi5pc0ZpcnN0Q2hhbmdlKCkpIHtcbiAgICAgIHRoaXMubG9hZENhbnZhcygpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBsb2FkQ2FudmFzKCkge1xuICAgIHRoaXMuVmlzdWFsRWRpdG9yU2VydmljZS5yZXNldE9iamVjdFR5cGVDb3VudCgpO1xuICAgIGlmICh0aGlzLnZpc3VhbEpzb24pIHtcbiAgICAgIHRoaXMubG9hZEpzb24odGhpcy52aXN1YWxKc29uLCB0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFNjYWxlZEltYWdlVG9DYW52YXMoXG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsXG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VVcmwsXG4gICAgICAgIHRydWVcbiAgICAgICk7XG4gICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgIGlmICh0aGlzLm9yaWdpbmFsSW1hZ2VVcmwpIHtcbiAgICAgICAgdGhpcy5vcmlnaW5hbEltYWdlQ2FudmFzID0gbmV3IGZhYnJpYy5TdGF0aWNDYW52YXMoXG4gICAgICAgICAgJ29yaWdpbmFsSW1hZ2VDYW52YXMnXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyhcbiAgICAgICAgICB0aGlzLm9yaWdpbmFsSW1hZ2VDYW52YXMsXG4gICAgICAgICAgdGhpcy5vcmlnaW5hbEltYWdlVXJsXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGRDdXN0b21Gb250cygpIHtcbiAgICBfLmVhY2godGhpcy52aXN1YWxFZGl0b3JDb25maWcudG9vbHMsIHQgPT4ge1xuICAgICAgXy5lYWNoKHQuc2VjdGlvbnMsIHMgPT4ge1xuICAgICAgICBfLmVhY2gocy5hY3Rpb25zLCBhID0+IHtcbiAgICAgICAgICBpZiAoYS5sYWJlbCA9PT0gJ0ZPTlQgRkFNSUxZJykge1xuICAgICAgICAgICAgYS5kcm9wZG93bk9wdGlvbiA9IF8uc29ydEJ5KFxuICAgICAgICAgICAgICBfLnVuaW9uKFxuICAgICAgICAgICAgICAgIGEuZHJvcGRvd25PcHRpb24sXG4gICAgICAgICAgICAgICAgXy5tYXAodGhpcy5jdXN0b21Gb250cywgZiA9PiBmLmZvbnRGYW1pbHkpXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBsb2FkQ3VzdG9tRm9udHMoKSB7XG4gICAgLy8gRklYTUU6IElFIHN1cHBvcnRcbiAgICBjb25zdCBjdXN0b21Gb250RmFjZXMgPSBfLm1hcChcbiAgICAgIHRoaXMuY3VzdG9tRm9udHMsXG4gICAgICBmID0+IG5ldyBGb250RmFjZShmLmZvbnRGYW1pbHksIGB1cmwoJHtmLmZvbnRGaWxlVXJsfSlgKVxuICAgICk7XG4gICAgXy5lYWNoKGN1c3RvbUZvbnRGYWNlcywgY2ZmID0+IHtcbiAgICAgIC8vIE5PVEU6IG5lZWQgdG8gc3BlY2lmeSBhIGZvbnQgc2l6ZSBmb3IgY2hlY2soKSBmdW5jdGlvblxuICAgICAgaWYgKCEoZG9jdW1lbnQgYXMgYW55KS5mb250cy5jaGVjayhgMTJweCAke2NmZi5mYW1pbHl9YCkpIHtcbiAgICAgICAgY2ZmXG4gICAgICAgICAgLmxvYWQoKVxuICAgICAgICAgIC50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICAoZG9jdW1lbnQgYXMgYW55KS5mb250cy5hZGQocmVzKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAvLyBGSVhNRTogRXJyb3IgZGlzcGxheVxuICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRab29tQ29udHJvbHMoY2FudmFzKSB7XG4gICAgY2FudmFzLm9uKCdtb3VzZTp3aGVlbCcsIG9wdCA9PiB7XG4gICAgICBjb25zdCBkZWx0YSA9IG9wdC5lLmRlbHRhWTtcbiAgICAgIGxldCB6b29tID0gY2FudmFzLmdldFpvb20oKTtcblxuICAgICAgem9vbSA9IHpvb20gLSBkZWx0YSAvIF8ubWluKFtjYW52YXMuZ2V0V2lkdGgoKSwgY2FudmFzLmdldEhlaWdodCgpXSk7XG4gICAgICBpZiAoem9vbSA+IDIwKSB7XG4gICAgICAgIHpvb20gPSAyMDtcbiAgICAgIH1cbiAgICAgIGlmICh6b29tIDwgdGhpcy5taW5ab29tKSB7XG4gICAgICAgIHpvb20gPSB0aGlzLm1pblpvb207XG4gICAgICB9XG5cbiAgICAgIGNhbnZhcy56b29tVG9Qb2ludCh7IHg6IG9wdC5lLm9mZnNldFgsIHk6IG9wdC5lLm9mZnNldFkgfSwgem9vbSk7XG5cbiAgICAgIG9wdC5lLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBvcHQuZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYWRkUGFuQ29udHJvbHMoY2FudmFzKSB7XG4gICAgY2FudmFzLm9uKCdtb3VzZTpkb3duJywgb3B0ID0+IHtcbiAgICAgIGNvbnN0IGV2dCA9IG9wdC5lO1xuICAgICAgaWYgKGV2dC5hbHRLZXkgPT09IHRydWUpIHtcbiAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gdHJ1ZTtcbiAgICAgICAgY2FudmFzLnNlbGVjdGlvbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLmxhc3RQb3NYID0gZXZ0LmNsaWVudFg7XG4gICAgICAgIHRoaXMubGFzdFBvc1kgPSBldnQuY2xpZW50WTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjYW52YXMub24oJ21vdXNlOm1vdmUnLCBvcHQgPT4ge1xuICAgICAgaWYgKHRoaXMuaXNEcmFnZ2luZykge1xuICAgICAgICBjb25zdCBlID0gb3B0LmU7XG4gICAgICAgIGNhbnZhcy52aWV3cG9ydFRyYW5zZm9ybVs0XSArPSBlLmNsaWVudFggLSB0aGlzLmxhc3RQb3NYO1xuICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNV0gKz0gZS5jbGllbnRZIC0gdGhpcy5sYXN0UG9zWTtcbiAgICAgICAgY2FudmFzLmZvckVhY2hPYmplY3QobyA9PiBvLnNldENvb3JkcygpKTtcbiAgICAgICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgICAgdGhpcy5sYXN0UG9zWCA9IGUuY2xpZW50WDtcbiAgICAgICAgdGhpcy5sYXN0UG9zWSA9IGUuY2xpZW50WTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjYW52YXMub24oJ21vdXNlOnVwJywgb3B0ID0+IHtcbiAgICAgIHRoaXMuaXNEcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgY2FudmFzLnNlbGVjdGlvbiA9IHRydWU7XG4gICAgfSk7XG4gIH1cblxuICAvLyBTdGFydCBvZiBsb2FkSnNvbiBmdW5jdGlvblxuICBwcml2YXRlIGxvYWRKc29uKGlucHV0SnNvbiwgY2FudmFzKSB7XG4gICAgY29uc3QgaW1nID0gbmV3IEltYWdlKCk7XG4gICAgaW1nLm9ubG9hZCA9ICgpID0+IHtcbiAgICAgIC8vIEV4dHJhY3RpbmcgdGhlIGltYWdlcyB3aWR0aCBhbmQgaGVpZ2h0IGFuZCBhZGRpbmcgaXQgaW50byB0aGUgX2NhbnZhc0RpbWVuc2lvbnMgcHJvcGVydHkgaW4gdGhlIGlucHV0SnNvblxuICAgICAgaWYgKHRoaXMucmVzaXplQ2FudmFzVG9CYWNrZ3JvdW5kSW1hZ2UpIHtcbiAgICAgICAgaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zID0geyB3aWR0aDogaW1nLndpZHRoLCBoZWlnaHQ6IGltZy5oZWlnaHQgfTtcbiAgICAgICAgaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS53aWR0aCA9IGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aDtcbiAgICAgICAgaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS5oZWlnaHQgPSBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0O1xuICAgICAgfVxuXG4gICAgICBjYW52YXMubG9hZEZyb21KU09OKGlucHV0SnNvbiwgKCkgPT4ge1xuICAgICAgICBjb25zdCBzY2FsZUZhY3RvciA9IF8ubWluKFtcbiAgICAgICAgICBjYW52YXMuZ2V0V2lkdGgoKSAvIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCxcbiAgICAgICAgICBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgIF0pO1xuICAgICAgICB0aGlzLm1pblpvb20gPSBzY2FsZUZhY3RvcjtcblxuICAgICAgICBpZiAoIXRoaXMuaXNXaWRlQ2FudmFzKSB7XG4gICAgICAgICAgY2FudmFzLnNldFdpZHRoKGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCAqIHNjYWxlRmFjdG9yKTtcbiAgICAgICAgICBjYW52YXMuc2V0SGVpZ2h0KGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy5oZWlnaHQgKiBzY2FsZUZhY3Rvcik7XG4gICAgICAgICAgY2FudmFzLnNldFpvb20odGhpcy5taW5ab29tKTtcblxuICAgICAgICAgIGNvbnN0IGlzV2lkdGhTbWFsbGVyID1cbiAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCAvIGNhbnZhcy5iYWNrZ3JvdW5kSW1hZ2Uud2lkdGggPlxuICAgICAgICAgICAgaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLmhlaWdodCAvIGNhbnZhcy5iYWNrZ3JvdW5kSW1hZ2UuaGVpZ2h0O1xuICAgICAgICAgIGlmIChpc1dpZHRoU21hbGxlcikge1xuICAgICAgICAgICAgY2FudmFzLmJhY2tncm91bmRJbWFnZS5zY2FsZVRvV2lkdGgoXG4gICAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FudmFzLmJhY2tncm91bmRJbWFnZS5zY2FsZVRvSGVpZ2h0KFxuICAgICAgICAgICAgICBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgICAgICApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zdCBwYWRkaW5nID0gNTtcbiAgICAgICAgICBjb25zdCBib3JkZXIgPSAwO1xuICAgICAgICAgIGNvbnN0IG5ld1dpZHRoID1cbiAgICAgICAgICAgIHRoaXMud2lkZUNhbnZhcy5uYXRpdmVFbGVtZW50Lm9mZnNldFdpZHRoIC0gMiAqIChwYWRkaW5nICsgYm9yZGVyKTtcbiAgICAgICAgICBjb25zdCBuZXdIZWlnaHQgPVxuICAgICAgICAgICAgdGhpcy53aWRlQ2FudmFzLm5hdGl2ZUVsZW1lbnQub2Zmc2V0SGVpZ2h0IC0gMiAqIChwYWRkaW5nICsgYm9yZGVyKTtcbiAgICAgICAgICBjYW52YXMuc2V0V2lkdGgobmV3V2lkdGgpO1xuICAgICAgICAgIGNhbnZhcy5zZXRIZWlnaHQobmV3SGVpZ2h0KTtcbiAgICAgICAgICBjYW52YXMuc2V0Wm9vbShcbiAgICAgICAgICAgIDAuOCAqXG4gICAgICAgICAgICAgIF8ubWluKFtcbiAgICAgICAgICAgICAgICBjYW52YXMuZ2V0V2lkdGgoKSAvIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCxcbiAgICAgICAgICAgICAgICBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgKTtcbiAgICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNF0gPVxuICAgICAgICAgICAgKG5ld1dpZHRoIC0gaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLndpZHRoICogY2FudmFzLmdldFpvb20oKSkgL1xuICAgICAgICAgICAgMjtcbiAgICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNV0gPVxuICAgICAgICAgICAgKG5ld0hlaWdodCAtXG4gICAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy5oZWlnaHQgKiBjYW52YXMuZ2V0Wm9vbSgpKSAvXG4gICAgICAgICAgICAyO1xuICAgICAgICAgIGNhbnZhcy5mb3JFYWNoT2JqZWN0KG8gPT4gby5zZXRDb29yZHMoKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0NlbnRlckJhY2tncm91bmRJbWFnZSkge1xuICAgICAgICAgIGNhbnZhcy52aWV3cG9ydENlbnRlck9iamVjdChjYW52YXMuYmFja2dyb3VuZEltYWdlKTsgLy8gQ2VudGVycyB0aGUgaW1hZ2Ugb24gdGhlIGNhbnZhc1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNTaG93RnJhbWUpIHtcbiAgICAgICAgICBjb25zdCBzdHJva2VXaWR0aCA9IDM7XG4gICAgICAgICAgY29uc3QgcmVjdGFuZ2xlID0gbmV3IGZhYnJpYy5SZWN0KHtcbiAgICAgICAgICAgIGxlZnQ6IC1zdHJva2VXaWR0aCxcbiAgICAgICAgICAgIHRvcDogLXN0cm9rZVdpZHRoLFxuICAgICAgICAgICAgaGVpZ2h0OiBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0ICsgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICB3aWR0aDogaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLndpZHRoICsgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICBzdHJva2U6ICdyZ2IoMCwyNTUsMCknLFxuICAgICAgICAgICAgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICBzdHJva2VEYXNoQXJyYXk6IFs1ICogc3Ryb2tlV2lkdGgsIDUgKiBzdHJva2VXaWR0aF0sXG4gICAgICAgICAgICBmaWxsOiAncmdiYSgwLDAsMCwwKScsXG4gICAgICAgICAgICBzZWxlY3RhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIGV2ZW50ZWQ6IGZhbHNlXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmVjdGFuZ2xlLl9rZXkgPSAnZnJhbWVfcmVjdGFuZ2xlJztcbiAgICAgICAgICBjYW52YXMuYWRkKHJlY3RhbmdsZSk7XG4gICAgICAgICAgcmVjdGFuZ2xlLmJyaW5nVG9Gcm9udCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FudmFzLnJlbmRlckFsbCgpO1xuICAgICAgfSk7XG4gICAgfTtcbiAgICBpbWcuc3JjID0gaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS5zcmM7XG4gIH1cbiAgLy8gRW5kIG9mIGxvYWRKc29uIGZ1bmN0aW9uXG5cbiAgcHJpdmF0ZSByZXNldENhbnZhcyhjYW52YXMpIHtcbiAgICBjYW52YXMuZ2V0T2JqZWN0cygpLmZvckVhY2gobyA9PiBjYW52YXMucmVtb3ZlKG8pKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyhjYW52YXMsIGltYWdlVXJsLCBpc0VkaXRlZEltYWdlID0gZmFsc2UpIHtcbiAgICBmYWJyaWMuSW1hZ2UuZnJvbVVSTChpbWFnZVVybCwgaW1nID0+IHtcbiAgICAgIGNvbnN0IGlzV2lkdGhTbWFsbGVyID1cbiAgICAgICAgY2FudmFzLmdldFdpZHRoKCkgLyBpbWcud2lkdGggPiBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbWcuaGVpZ2h0O1xuICAgICAgaWYgKGlzV2lkdGhTbWFsbGVyKSB7XG4gICAgICAgIGltZy5zY2FsZVRvSGVpZ2h0KGNhbnZhcy5nZXRIZWlnaHQoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbWcuc2NhbGVUb1dpZHRoKGNhbnZhcy5nZXRXaWR0aCgpKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgeyB3aWR0aCwgaGVpZ2h0IH0gPSBpbWc7XG4gICAgICBjb25zdCBsZWZ0ID0gaXNXaWR0aFNtYWxsZXJcbiAgICAgICAgPyAoY2FudmFzLmdldFdpZHRoKCkgLSB3aWR0aCAqIGltZy5zY2FsZVgpIC8gMi4wXG4gICAgICAgIDogMDtcbiAgICAgIGNvbnN0IHRvcCA9IGlzV2lkdGhTbWFsbGVyXG4gICAgICAgID8gMFxuICAgICAgICA6IChjYW52YXMuZ2V0SGVpZ2h0KCkgLSBoZWlnaHQgKiBpbWcuc2NhbGVYKSAvIDIuMDtcblxuICAgICAgaW1nLnNldCh7XG4gICAgICAgIGxlZnQsXG4gICAgICAgIHRvcFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChpc0VkaXRlZEltYWdlKSB7XG4gICAgICAgIHRoaXMuaW1hZ2VDb25maWcgPSB7XG4gICAgICAgICAgd2lkdGgsXG4gICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgIHNjYWxlRmFjdG9yOiBpbWcuc2NhbGVYLFxuICAgICAgICAgIHhPZmZzZXQ6IGxlZnQsXG4gICAgICAgICAgeU9mZnNldDogdG9wXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmhhc0NoZWNrZXJzQmcpIHtcbiAgICAgICAgY2FudmFzLmFkZChpbWcpO1xuICAgICAgICBjYW52YXMucmVuZGVyQWxsKCk7XG5cbiAgICAgICAgZmFicmljLkltYWdlLmZyb21VUkwodGhpcy5jaGVja2Vyc0ltYWdlVXJsLCBjaGVja2VySW1nID0+IHtcbiAgICAgICAgICBjaGVja2VySW1nLnNjYWxlVG9XaWR0aChjYW52YXMuZ2V0V2lkdGgoKSk7XG4gICAgICAgICAgY2FudmFzLnNldEJhY2tncm91bmRJbWFnZShjaGVja2VySW1nLCBjYW52YXMucmVuZGVyQWxsLmJpbmQoY2FudmFzKSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY2FudmFzLnNldEJhY2tncm91bmRJbWFnZShpbWcsIGNhbnZhcy5yZW5kZXJBbGwuYmluZChjYW52YXMpKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgc2VsZWN0SGlkZGVuQ29udHJvbHMoc2VsZWN0ZWRPYmplY3RzKSB7XG4gICAgaWYgKHNlbGVjdGVkT2JqZWN0cy5sZW5ndGggPT09IDEpIHtcbiAgICAgIGNvbnN0IGlzU2hhcGUgPVxuICAgICAgICBzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKE9iamVjdFR5cGVzLlJFQ1QpIHx8XG4gICAgICAgIHNlbGVjdGVkT2JqZWN0c1swXS5pc1R5cGUoT2JqZWN0VHlwZXMuQ0lSQ0xFKSB8fFxuICAgICAgICBzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKE9iamVjdFR5cGVzLlBPTFlHT04pO1xuICAgICAgY29uc3QgaXNJbWFnZSA9IHNlbGVjdGVkT2JqZWN0c1swXS5pc1R5cGUoT2JqZWN0VHlwZXMuSU1BR0UpO1xuICAgICAgdGhpcy50b29scy5mb3JFYWNoKHQgPT4ge1xuICAgICAgICBpZiAodC5pc0hpZGRlbikge1xuICAgICAgICAgIGlmIChpc1NoYXBlKSB7XG4gICAgICAgICAgICBpZiAodC5vYmplY3RUeXBlID09PSBPYmplY3RUeXBlcy5TSEFQRSkge1xuICAgICAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2UgaWYgKGlzSW1hZ2UpIHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgc2VsZWN0ZWRPYmplY3RzWzBdLl9rZXkgJiZcbiAgICAgICAgICAgICAgc2VsZWN0ZWRPYmplY3RzWzBdLl9rZXkuaW5jbHVkZXMoT2JqZWN0VHlwZXMuUFJPRFVDVClcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICBpZiAodC5vYmplY3RUeXBlID09PSBPYmplY3RUeXBlcy5QUk9EVUNUKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vblNlbGVjdFRvb2wodCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIC8vIE5vbiBwcm9kdWN0IGltYWdlXG4gICAgICAgICAgICAgIGlmIChzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKHQub2JqZWN0VHlwZSkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyBOb24gc2hhcGUgYW5kIG5vbiBpbWFnZSBvYmplY3RcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKHQub2JqZWN0VHlwZSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5vblNlbGVjdFRvb2wodCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKHNlbGVjdGVkT2JqZWN0cy5sZW5ndGggPiAxKSB7XG4gICAgICB0aGlzLnRvb2xzLmZvckVhY2godCA9PiB7XG4gICAgICAgIGlmICh0LmlzSGlkZGVuICYmIHQub2JqZWN0VHlwZSA9PT0gT2JqZWN0VHlwZXMuR1JPVVApIHtcbiAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uU2VsZWN0VG9vbCh0b29sKSB7XG4gICAgaWYgKHRvb2wudHlwZSA9PT0gQWN0aW9uVHlwZXMuVVBMT0FEKSB7XG4gICAgICB0aGlzLmZpbGVVcGxvYWQuY2xpY2soKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKCFfLmlzVW5kZWZpbmVkKHRvb2wub25TZWxlY3QpKSB7XG4gICAgICAgIHRvb2wub25TZWxlY3QodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgdG9vbC5vblNlbGVjdFBhcmFtcyk7XG4gICAgICAgIHRoaXMuYWN0aXZlVG9vbCA9IHt9O1xuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLmFjdGl2ZVRvb2wgfHwgdGhpcy5hY3RpdmVUb29sLm5hbWUgIT09IHRvb2wubmFtZSkge1xuICAgICAgICB0aGlzLmFjdGl2ZVRvb2wgPSB0b29sO1xuICAgICAgICBpZiAodG9vbC5jYW52YXNDb25maWdzKSB7XG4gICAgICAgICAgdG9vbC5jYW52YXNDb25maWdzLmZvckVhY2goY29uZkZ1bmMgPT4ge1xuICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyA9IGNvbmZGdW5jKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gU3RhcnQgb2YgcHJpdmF0ZSBvblRvb2xBY3Rpb24oYWN0aW9uKSBmdW5jdGlvblxuICBvblRvb2xBY3Rpb24oYWN0aW9uKSB7XG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5CVVRUT046IHtcbiAgICAgICAgbGV0IHJlcztcblxuICAgICAgICBpZiAoIV8uaXNVbmRlZmluZWQoYWN0aW9uLm9uU2VsZWN0UGFyYW1zKSkge1xuICAgICAgICAgIHJlcyA9IGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgYWN0aW9uLm9uU2VsZWN0UGFyYW1zKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXMgPSBhY3Rpb24uYWN0aW9uKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGlmICghXy5pc1VuZGVmaW5lZChyZXMpICYmICFfLmlzVW5kZWZpbmVkKGFjdGlvbi5vbkFjdGlvblJldHVybikpIHtcbiAgICAgICAgICBhY3Rpb24ub25BY3Rpb25SZXR1cm4oYWN0aW9uLCByZXMpO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5UT0dHTEU6IHtcbiAgICAgICAgYWN0aW9uLmlzT24gPSAhYWN0aW9uLmlzT247XG4gICAgICAgIGFjdGlvbi5hY3Rpb24oXG4gICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyxcbiAgICAgICAgICBhY3Rpb24uaXNPbiA/IGFjdGlvbi5vblZhbHVlIDogYWN0aW9uLm9mZlZhbHVlXG4gICAgICAgICk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5EUk9QRE9XTjoge1xuICAgICAgICBhY3Rpb24uYWN0aW9uKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsIGFjdGlvbi5kcm9wZG93blNlbGVjdGVkT3B0aW9uKTtcbiAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5yZW5kZXJBbGwuYmluZCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgICBjYXNlIEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQ6IHtcbiAgICAgICAgY29uc3QgcmVsYXRlZFZhbHVlcyA9IHt9O1xuICAgICAgICB0aGlzLmFjdGl2ZVRvb2wuc2VjdGlvbnMuZm9yRWFjaChzID0+IHtcbiAgICAgICAgICBzLmFjdGlvbnMuZm9yRWFjaChhID0+IHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgYS50eXBlID09PSBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVUICYmXG4gICAgICAgICAgICAgIGEuZ3JvdXBOYW1lID09PSBhY3Rpb24uZ3JvdXBOYW1lICYmXG4gICAgICAgICAgICAgIGEubGFiZWwgIT09IGFjdGlvbi5sYWJlbFxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgIHJlbGF0ZWRWYWx1ZXNbYS5rZXlOYW1lXSA9IGEuaW5wdXRWYWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJlbGF0ZWRWYWx1ZXNbYWN0aW9uLmtleU5hbWVdID0gYWN0aW9uLmlucHV0VmFsdWU7XG4gICAgICAgIGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgcmVsYXRlZFZhbHVlcyk7XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5ESUFMT0c6IHtcbiAgICAgICAgaWYgKGFjdGlvbi5kaWFsb2cpIHtcbiAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKGFjdGlvbi5kaWFsb2cuY29tcG9uZW50LCB7XG4gICAgICAgICAgICBhdXRvRm9jdXM6IGZhbHNlLFxuICAgICAgICAgICAgZGF0YTogYWN0aW9uLmRpYWxvZy5kYXRhLFxuICAgICAgICAgICAgbWluV2lkdGg6ICczMDBweCcsXG4gICAgICAgICAgICBwYW5lbENsYXNzOiAnYXBwLWRpYWxvZydcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzRGlhbG9nID0+IHtcbiAgICAgICAgICAgIGlmICghXy5pc1VuZGVmaW5lZChyZXNEaWFsb2cpKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHJlc0FjdGlvbiA9IGFjdGlvbi5kaWFsb2cub25DbG9zZShcbiAgICAgICAgICAgICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLFxuICAgICAgICAgICAgICAgIHJlc0RpYWxvZ1xuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgIV8uaXNVbmRlZmluZWQocmVzQWN0aW9uKSAmJlxuICAgICAgICAgICAgICAgICFfLmlzVW5kZWZpbmVkKGFjdGlvbi5vbkFjdGlvblJldHVybilcbiAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9uLm9uQWN0aW9uUmV0dXJuKGFjdGlvbiwgcmVzQWN0aW9uKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSOiB7XG4gICAgICAgIHRoaXMub3V0Z29pbmdFdmVudFRyaWdnZXJlZC5lbWl0KHtcbiAgICAgICAgICBjYW52YXM6IHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsXG4gICAgICAgICAgdHJhbnNmb3JtZWRDYW52YXM6XG4gICAgICAgICAgICBhY3Rpb24ubGFiZWwgPT09ICdTdWJtaXQnXG4gICAgICAgICAgICAgID8gYWN0aW9uLmFjdGlvbihcbiAgICAgICAgICAgICAgICAgIHRoaXMudmlzdWFsSnNvbi5iYWNrZ3JvdW5kSW1hZ2Uuc3JjLFxuICAgICAgICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy50b0pTT04oWydfY2FudmFzRGltZW5zaW9ucyddKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgOiAnJ1xuICAgICAgICB9KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgICBkZWZhdWx0OiB7XG4gICAgICAgIGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAvLyBFbmQgb2YgcHJpdmF0ZSBvblRvb2xBY3Rpb24oYWN0aW9uKSBmdW5jdGlvblxuXG4gIHB1YmxpYyBvblVwZGF0ZShpbWdVcmwpIHtcbiAgICB0aGlzLnJlc2V0Q2FudmFzKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgIHRoaXMuc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyh0aGlzLmVkaXRlZEltYWdlQ2FudmFzLCBpbWdVcmwsIHRydWUpO1xuICB9XG5cbiAgcHVibGljIGdldENhbnZhc0FuZENvbmZpZygpIHtcbiAgICByZXR1cm4ge1xuICAgICAgY2FudmFzOiB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLFxuICAgICAgaW1hZ2VDb25maWc6IHRoaXMuaW1hZ2VDb25maWdcbiAgICB9O1xuICB9XG5cbiAgb25GaWxlU2VsZWN0KHJlcykge1xuICAgIGNvbnN0IHsgZmlsZXMsIGNhbGxlcjogdG9vbCB9ID0gcmVzO1xuICAgIGlmICghXy5pc1VuZGVmaW5lZCh0b29sKSkge1xuICAgICAgaWYgKCFfLmlzVW5kZWZpbmVkKHRvb2wub25TZWxlY3QpICYmIGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgY29uc3QgZmlsZSA9IGZpbGVzWzBdO1xuICAgICAgICBjb25zdCBmaWxlbmFtZSA9IF8ucmVwbGFjZShmaWxlLm5hbWUsIC9bXkEtWjAtOS5dKy9naSwgJ18nKTtcblxuICAgICAgICB0aGlzLmZpbGVTZXJ2aWNlXG4gICAgICAgICAgLmdldFVwbG9hZFVybCh7XG4gICAgICAgICAgICBmaWxlbmFtZSxcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBmaWxlLnR5cGUsXG4gICAgICAgICAgICBleHBpcmVzSW5TZWNzOiAzNjAwXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BVcGxvYWRVcmwgPT4ge1xuICAgICAgICAgICAgdGhpcy5zM1NlcnZpY2VcbiAgICAgICAgICAgICAgLnVwbG9hZChyZXNwVXBsb2FkVXJsLnVybCwgZmlsZSwgZmlsZS50eXBlKVxuICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BVcGxvYWQgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGZpbGVVcmwgPSByZXNwVXBsb2FkVXJsLnVybC5zcGxpdCgnPycpWzBdO1xuICAgICAgICAgICAgICAgIHRvb2wub25TZWxlY3RQYXJhbXMuZmlsZVVybCA9IGZpbGVVcmw7XG4gICAgICAgICAgICAgICAgdG9vbC5vblNlbGVjdCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzLCB0b29sLm9uU2VsZWN0UGFyYW1zKTtcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZVRvb2wgPSB7fTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gS2V5Ym9hcmQgU2hvcnRjdXRzXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzprZXlkb3duJywgWyckZXZlbnQnXSlcbiAgY2FudmFzS2V5Ym9hcmRFdmVudChldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGlmIChcbiAgICAgIHRoaXMuaGFzQ2xvbmVTaG9ydGN1dCAmJlxuICAgICAgKChldmVudC5jdHJsS2V5IHx8IGV2ZW50Lm1ldGFLZXkpICYmXG4gICAgICAgIGV2ZW50LnNoaWZ0S2V5ICYmXG4gICAgICAgIGV2ZW50LmtleUNvZGUgPT09IDg2KVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLmNsb25lT2JqZWN0KHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmhhc1JlbW92ZVNob3J0Y3V0ICYmICghZXZlbnQuc2hpZnRLZXkgJiYgZXZlbnQua2V5Q29kZSA9PT0gOCkpIHtcbiAgICAgIHRoaXMuVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzUmVtb3ZlQWxsU2hvcnRjdXQgJiYgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDgpKSB7XG4gICAgICB0aGlzLlZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICB0aGlzLmhhc1NlbmRCYWNrd2FyZHNTaG9ydGN1dCAmJlxuICAgICAgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDQwKVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgfVxuXG4gICAgaWYgKFxuICAgICAgdGhpcy5oYXNCcmluZ0ZvcndhcmRTaG9ydGN1dCAmJlxuICAgICAgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDM4KVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==