/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
var ColorPickerDialogComponent = /** @class */ (function () {
    function ColorPickerDialogComponent(data, colorPickerDialog) {
        this.data = data;
        this.colorPickerDialog = colorPickerDialog;
        this.addAll = false;
        this.color = '#ffffff';
        this.titleText = 'ADD COLOR';
        this.buttonText = 'Add';
    }
    /**
     * @return {?}
     */
    ColorPickerDialogComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.data.color) {
            this.color = this.data.color;
        }
        if (this.data.titleText) {
            this.titleText = this.data.titleText;
        }
        if (this.data.buttonText) {
            this.buttonText = this.data.buttonText;
        }
        if (this.data.addAll) {
            this.addAll = this.data.addAll;
        }
    };
    /**
     * @param {?} addAll
     * @return {?}
     */
    ColorPickerDialogComponent.prototype.onAdd = /**
     * @param {?} addAll
     * @return {?}
     */
    function (addAll) {
        this.colorPickerDialog.close({
            addAll: addAll,
            colorHex: this.color
        });
    };
    /**
     * @return {?}
     */
    ColorPickerDialogComponent.prototype.onCancel = /**
     * @return {?}
     */
    function () {
        this.colorPickerDialog.close();
    };
    ColorPickerDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-color-picker-dialog',
                    template: "<div>\n    <div mat-dialog-title>\n      <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n        <div class=\"subtitle\">{{titleText}}</div>\n        <i class=\"fas fa-times fa-xs clickable\" (click)=\"onCancel()\"></i>\n      </div>\n      <mat-divider></mat-divider>\n    </div>\n    <div mat-dialog-content class=\"dialog-content\">\n      <div fxLayout=\"row\" fxLayoutAlign=\"center\">\n        <span [cpToggle]=\"true\"\n              [cpDialogDisplay]=\"'inline'\"\n              [cpOutputFormat]=\"'hex'\"\n              [(colorPicker)]=\"color\"></span>\n      </div>\n    </div>\n    <mat-dialog-actions>\n      <div fxFill fxLayout=\"row\"\n           fxLayoutAlign=\"end center\"\n           fxLayoutGap=\"5px\">\n        <button *ngIf=\"addAll\"\n                mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                [disableRipple]=\"true\"\n                (click)=\"onAdd(true)\">Add to all</button>\n        <button mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                (click)=\"onAdd(false)\">Add</button>\n      </div>\n    </mat-dialog-actions>\n  </div>\n  \n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ColorPickerDialogComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: MatDialogRef }
    ]; };
    return ColorPickerDialogComponent;
}());
export { ColorPickerDialogComponent };
if (false) {
    /** @type {?} */
    ColorPickerDialogComponent.prototype.addAll;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.color;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.titleText;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.buttonText;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    ColorPickerDialogComponent.prototype.colorPickerDialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sb3ItcGlja2VyLWRpYWxvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29sb3ItcGlja2VyLWRpYWxvZy9jb2xvci1waWNrZXItZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVsRTtJQVdFLG9DQUNrQyxJQUFJLEVBQzVCLGlCQUEyRDtRQURuQyxTQUFJLEdBQUosSUFBSSxDQUFBO1FBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBMEM7UUFQckUsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLFVBQUssR0FBRyxTQUFTLENBQUM7UUFDbEIsY0FBUyxHQUFHLFdBQVcsQ0FBQztRQUN4QixlQUFVLEdBQUcsS0FBSyxDQUFDO0lBS2hCLENBQUM7Ozs7SUFFSiw2Q0FBUTs7O0lBQVI7UUFDRSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDOUI7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDdEM7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDeEM7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDaEM7SUFDSCxDQUFDOzs7OztJQUVELDBDQUFLOzs7O0lBQUwsVUFBTSxNQUFNO1FBQ1YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQztZQUMzQixNQUFNLFFBQUE7WUFDTixRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDckIsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVELDZDQUFROzs7SUFBUjtRQUNFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNqQyxDQUFDOztnQkF4Q0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLHd0Q0FBbUQ7O2lCQUVwRDs7OztnREFRSSxNQUFNLFNBQUMsZUFBZTtnQkFkRCxZQUFZOztJQTJDdEMsaUNBQUM7Q0FBQSxBQXpDRCxJQXlDQztTQXBDWSwwQkFBMEI7OztJQUNyQyw0Q0FBZTs7SUFDZiwyQ0FBa0I7O0lBQ2xCLCtDQUF3Qjs7SUFDeEIsZ0RBQW1COztJQUdqQiwwQ0FBb0M7Ozs7O0lBQ3BDLHVEQUFtRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1BVF9ESUFMT0dfREFUQSwgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtY29sb3ItcGlja2VyLWRpYWxvZycsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb2xvci1waWNrZXItZGlhbG9nLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY29sb3ItcGlja2VyLWRpYWxvZy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgYWRkQWxsID0gZmFsc2U7XG4gIGNvbG9yID0gJyNmZmZmZmYnO1xuICB0aXRsZVRleHQgPSAnQUREIENPTE9SJztcbiAgYnV0dG9uVGV4dCA9ICdBZGQnO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YSxcbiAgICBwcml2YXRlIGNvbG9yUGlja2VyRGlhbG9nOiBNYXREaWFsb2dSZWY8Q29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQ+XG4gICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5kYXRhLmNvbG9yKSB7XG4gICAgICB0aGlzLmNvbG9yID0gdGhpcy5kYXRhLmNvbG9yO1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLnRpdGxlVGV4dCkge1xuICAgICAgdGhpcy50aXRsZVRleHQgPSB0aGlzLmRhdGEudGl0bGVUZXh0O1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLmJ1dHRvblRleHQpIHtcbiAgICAgIHRoaXMuYnV0dG9uVGV4dCA9IHRoaXMuZGF0YS5idXR0b25UZXh0O1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLmFkZEFsbCkge1xuICAgICAgdGhpcy5hZGRBbGwgPSB0aGlzLmRhdGEuYWRkQWxsO1xuICAgIH1cbiAgfVxuXG4gIG9uQWRkKGFkZEFsbCk6IHZvaWQge1xuICAgIHRoaXMuY29sb3JQaWNrZXJEaWFsb2cuY2xvc2Uoe1xuICAgICAgYWRkQWxsLFxuICAgICAgY29sb3JIZXg6IHRoaXMuY29sb3JcbiAgICB9KTtcbiAgfVxuXG4gIG9uQ2FuY2VsKCk6IHZvaWQge1xuICAgIHRoaXMuY29sb3JQaWNrZXJEaWFsb2cuY2xvc2UoKTtcbiAgfVxufVxuIl19