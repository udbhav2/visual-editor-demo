/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent() {
        this.multiple = false;
        this.selectedFiles = new EventEmitter();
        this.selectedFilesWithCaller = new EventEmitter();
    }
    /**
     * @param {?=} caller
     * @return {?}
     */
    FileUploadComponent.prototype.click = /**
     * @param {?=} caller
     * @return {?}
     */
    function (caller) {
        if (caller) {
            this.caller = caller;
        }
        this.fileInput.nativeElement.click();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    FileUploadComponent.prototype.selected = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var files = event.target.files;
        if (this.caller) {
            this.selectedFilesWithCaller.emit({ files: files, caller: this.caller });
        }
        else {
            this.selectedFiles.emit(files);
        }
    };
    FileUploadComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-file-upload',
                    template: "<input #fileInput\n       accept=\"{{accept}}\"\n       (change)=\"selected($event)\"\n       hidden\n       [multiple]=\"multiple\"\n       type=\"file\">\n",
                    styles: [""]
                }] }
    ];
    FileUploadComponent.propDecorators = {
        accept: [{ type: Input }],
        multiple: [{ type: Input }],
        selectedFiles: [{ type: Output }],
        selectedFilesWithCaller: [{ type: Output }],
        fileInput: [{ type: ViewChild, args: ['fileInput',] }]
    };
    return FileUploadComponent;
}());
export { FileUploadComponent };
if (false) {
    /** @type {?} */
    FileUploadComponent.prototype.accept;
    /** @type {?} */
    FileUploadComponent.prototype.multiple;
    /** @type {?} */
    FileUploadComponent.prototype.selectedFiles;
    /** @type {?} */
    FileUploadComponent.prototype.selectedFilesWithCaller;
    /** @type {?} */
    FileUploadComponent.prototype.fileInput;
    /** @type {?} */
    FileUploadComponent.prototype.caller;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11cGxvYWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTlGO0lBQUE7UUFPVyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUMzQyw0QkFBdUIsR0FBRyxJQUFJLFlBQVksRUFBZ0MsQ0FBQztJQXFCdkYsQ0FBQzs7Ozs7SUFmQyxtQ0FBSzs7OztJQUFMLFVBQU0sTUFBTztRQUNYLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdEI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELHNDQUFROzs7O0lBQVIsVUFBUyxLQUFLOztZQUNOLEtBQUssR0FBVyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7UUFDeEMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxFQUFDLEtBQUssT0FBQSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFDLENBQUMsQ0FBQztTQUNqRTthQUFNO1lBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEM7SUFDSCxDQUFDOztnQkE3QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLHlLQUEyQzs7aUJBRTVDOzs7eUJBRUUsS0FBSzsyQkFDTCxLQUFLO2dDQUNMLE1BQU07MENBQ04sTUFBTTs0QkFFTixTQUFTLFNBQUMsV0FBVzs7SUFtQnhCLDBCQUFDO0NBQUEsQUE5QkQsSUE4QkM7U0F6QlksbUJBQW1COzs7SUFDOUIscUNBQXdCOztJQUN4Qix1Q0FBMEI7O0lBQzFCLDRDQUFxRDs7SUFDckQsc0RBQXFGOztJQUVyRix3Q0FBOEM7O0lBRTlDLHFDQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItZmlsZS11cGxvYWQnLFxuICB0ZW1wbGF0ZVVybDogJy4vZmlsZS11cGxvYWQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9maWxlLXVwbG9hZC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEZpbGVVcGxvYWRDb21wb25lbnQge1xuICBASW5wdXQoKSBhY2NlcHQ6IHN0cmluZztcbiAgQElucHV0KCkgbXVsdGlwbGUgPSBmYWxzZTtcbiAgQE91dHB1dCgpIHNlbGVjdGVkRmlsZXMgPSBuZXcgRXZlbnRFbWl0dGVyPEZpbGVbXT4oKTtcbiAgQE91dHB1dCgpIHNlbGVjdGVkRmlsZXNXaXRoQ2FsbGVyID0gbmV3IEV2ZW50RW1pdHRlcjx7ZmlsZXM6IEZpbGVbXSwgY2FsbGVyOiBhbnl9PigpO1xuXG4gIEBWaWV3Q2hpbGQoJ2ZpbGVJbnB1dCcpIGZpbGVJbnB1dDogRWxlbWVudFJlZjtcblxuICBjYWxsZXI6IGFueTtcblxuICBjbGljayhjYWxsZXI/KSB7XG4gICAgaWYgKGNhbGxlcikge1xuICAgICAgdGhpcy5jYWxsZXIgPSBjYWxsZXI7XG4gICAgfVxuICAgIHRoaXMuZmlsZUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcbiAgfVxuXG4gIHNlbGVjdGVkKGV2ZW50KSB7XG4gICAgY29uc3QgZmlsZXM6IEZpbGVbXSA9IGV2ZW50LnRhcmdldC5maWxlcztcbiAgICBpZiAodGhpcy5jYWxsZXIpIHtcbiAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlc1dpdGhDYWxsZXIuZW1pdCh7ZmlsZXMsIGNhbGxlcjogdGhpcy5jYWxsZXJ9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zZWxlY3RlZEZpbGVzLmVtaXQoZmlsZXMpO1xuICAgIH1cbiAgfVxufVxuIl19