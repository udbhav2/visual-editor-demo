/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var ActionTypes = {
    BUTTON: 0,
    TOGGLE: 1,
    DROPDOWN: 2,
    DIALOG: 3,
    UPLOAD: 4,
    LABEL: 5,
    RELATED_INPUT: 6,
    OUTGOING_EVENT_TRIGGER: 7,
};
export { ActionTypes };
ActionTypes[ActionTypes.BUTTON] = 'BUTTON';
ActionTypes[ActionTypes.TOGGLE] = 'TOGGLE';
ActionTypes[ActionTypes.DROPDOWN] = 'DROPDOWN';
ActionTypes[ActionTypes.DIALOG] = 'DIALOG';
ActionTypes[ActionTypes.UPLOAD] = 'UPLOAD';
ActionTypes[ActionTypes.LABEL] = 'LABEL';
ActionTypes[ActionTypes.RELATED_INPUT] = 'RELATED_INPUT';
ActionTypes[ActionTypes.OUTGOING_EVENT_TRIGGER] = 'OUTGOING_EVENT_TRIGGER';
/** @enum {string} */
var ObjectTypes = {
    TEXBOX: 'textbox',
    IMAGE: 'image',
    SHAPE: 'shape',
    RECT: 'rect',
    CIRCLE: 'circle',
    LINE: 'line',
    POLYGON: 'polygon',
    GROUP: 'group',
    PRODUCT: 'product',
};
export { ObjectTypes };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aW9uLXR5cGVzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWdzL2FjdGlvbi10eXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDRSxTQUFNO0lBQ04sU0FBTTtJQUNOLFdBQVE7SUFDUixTQUFNO0lBQ04sU0FBTTtJQUNOLFFBQUs7SUFDTCxnQkFBYTtJQUNiLHlCQUFzQjs7Ozs7Ozs7Ozs7OztJQUl0QixRQUFTLFNBQVM7SUFDbEIsT0FBUSxPQUFPO0lBQ2YsT0FBUSxPQUFPO0lBQ2YsTUFBTyxNQUFNO0lBQ2IsUUFBUyxRQUFRO0lBQ2pCLE1BQU8sTUFBTTtJQUNiLFNBQVUsU0FBUztJQUNuQixPQUFRLE9BQU87SUFDZixTQUFVLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBBY3Rpb25UeXBlcyAge1xuICBCVVRUT04sXG4gIFRPR0dMRSxcbiAgRFJPUERPV04sXG4gIERJQUxPRyxcbiAgVVBMT0FELFxuICBMQUJFTCxcbiAgUkVMQVRFRF9JTlBVVCxcbiAgT1VUR09JTkdfRVZFTlRfVFJJR0dFUlxufVxuXG5leHBvcnQgZW51bSBPYmplY3RUeXBlcyAge1xuICBURVhCT1ggPSAndGV4dGJveCcsXG4gIElNQUdFID0gJ2ltYWdlJyxcbiAgU0hBUEUgPSAnc2hhcGUnLFxuICBSRUNUID0gJ3JlY3QnLFxuICBDSVJDTEUgPSAnY2lyY2xlJyxcbiAgTElORSA9ICdsaW5lJyxcbiAgUE9MWUdPTiA9ICdwb2x5Z29uJyxcbiAgR1JPVVAgPSAnZ3JvdXAnLFxuICBQUk9EVUNUID0gJ3Byb2R1Y3QnLFxufVxuIl19