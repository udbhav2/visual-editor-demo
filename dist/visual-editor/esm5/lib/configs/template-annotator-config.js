/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { VisualEditorService } from '../services/visual-editor.service';
import { faClone, faTrashAlt, faTrash, faArrowUp, faArrowDown, faAlignLeft, faAlignCenter, faAlignRight } from '@fortawesome/free-solid-svg-icons';
import { ActionTypes, ObjectTypes } from './action-types';
var TemplateAnnotatorConfig = /** @class */ (function () {
    function TemplateAnnotatorConfig() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = false;
        this.isWideCanvas = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasPan = false;
        this.resizeCanvasToBackgroundImage = true;
        this.isCenterBackgroundImage = true;
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = true;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasSendBackwardsShortcut = true;
        this.hasBringForwardShortcut = true;
        // FontAwesome Icons
        this.faClone = faClone;
        this.faTrash = faTrash;
        this.faTrashAlt = faTrashAlt;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.faAlignLeft = faAlignLeft;
        this.faAlignCenter = faAlignCenter;
        this.faAlignRight = faAlignRight;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Product',
                isHidden: false,
                iconDisplayText: 'Product',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#3cb44b', fill: '#3cb44b' + '88' }),
                    additionalProperties: {
                        objectType: 'product'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#3cb44b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Primary Logo',
                isHidden: false,
                iconDisplayText: 'Primary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6194b', fill: '#e6194b' + '88' }),
                    additionalProperties: {
                        objectType: 'primary_logo'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6194b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Secondary Logo',
                isHidden: false,
                iconDisplayText: 'Secondary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f032e6', fill: '#f032e6' + '88' }),
                    additionalProperties: {
                        objectType: 'secondary_logo'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f032e6' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Lockup',
                isHidden: false,
                iconDisplayText: 'Lockup',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#bcf60c', fill: '#bcf60c' + '88' }),
                    additionalProperties: {
                        objectType: 'lockup'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#bcf60c', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Headline Textbox',
                isHidden: false,
                iconDisplayText: 'Headline Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#4363d8', fill: '#4363d8' + '88' }),
                    additionalProperties: {
                        objectType: 'headline_textbox'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#4363d8' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Other Textbox',
                isHidden: false,
                iconDisplayText: 'Other Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f58231', fill: '#f58231' + '88' }),
                    additionalProperties: {
                        objectType: 'other_textbox'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f58231' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Graphic Accent',
                isHidden: false,
                iconDisplayText: 'Graphic Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#000075', fill: '#000075' + '88' }),
                    additionalProperties: {
                        objectType: 'graphic_accent'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#000075' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Photo Accent',
                isHidden: false,
                iconDisplayText: 'Photo Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#46f0f0', fill: '#46f0f0' + '88' }),
                    additionalProperties: {
                        objectType: 'photo_accent'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#46f0f0' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Button',
                isHidden: false,
                iconDisplayText: 'Button',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#008080', fill: '#008080' + '88' }),
                    additionalProperties: {
                        objectType: 'button'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#008080' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Banner',
                isHidden: false,
                iconDisplayText: 'Banner',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#ffe119', fill: '#ffe119' + '88' }),
                    additionalProperties: {
                        objectType: 'banner'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#ffe119', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Sticker',
                isHidden: false,
                iconDisplayText: 'Sticker',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#fabebe', fill: '#fabebe' + '88' }),
                    additionalProperties: {
                        objectType: 'sticker'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#fabebe' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Frame',
                isHidden: false,
                iconDisplayText: 'Frame',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: tslib_1.__assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6beff', fill: '#e6beff' + '88' }),
                    additionalProperties: {
                        objectType: 'frame'
                    }
                },
                flex: '33%',
                style: tslib_1.__assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6beff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Clone',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faClone,
                                action: VisualEditorService.cloneObject,
                                flex: '33%'
                            },
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
    TemplateAnnotatorConfig.addBoundingBoxConfigParams = {
        config: {
            left: 60,
            top: 60,
            height: 100,
            width: 100,
            strokeWidth: 2,
            stroke: 'black',
            originX: 'center',
            originY: 'center'
        }
    };
    TemplateAnnotatorConfig.buttonStyles = { 'font-weight': 'bold' };
    return TemplateAnnotatorConfig;
}());
export { TemplateAnnotatorConfig };
if (false) {
    /** @type {?} */
    TemplateAnnotatorConfig.addBoundingBoxConfigParams;
    /** @type {?} */
    TemplateAnnotatorConfig.buttonStyles;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.tools;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasZoom;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isWideCanvas;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.toolsFlex;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.actionsFlex;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasPan;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.resizeCanvasToBackgroundImage;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isCenterBackgroundImage;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasCloneShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasRemoveShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasBringForwardShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faClone;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faTrash;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faTrashAlt;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faArrowUp;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faArrowDown;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignLeft;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignCenter;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignRight;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGUtYW5ub3RhdG9yLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3Zpc3VhbC1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvY29uZmlncy90ZW1wbGF0ZS1hbm5vdGF0b3ItY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBRUEsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDeEUsT0FBTyxFQUNMLE9BQU8sRUFDUCxVQUFVLEVBQ1YsT0FBTyxFQUNQLFNBQVMsRUFDVCxXQUFXLEVBQ1gsV0FBVyxFQUNYLGFBQWEsRUFDYixZQUFZLEVBQ2IsTUFBTSxtQ0FBbUMsQ0FBQztBQUMzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTFEO0lBNENFO1FBM0JPLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQiwrQkFBMEIsR0FBRyxJQUFJLENBQUM7UUFDbEMsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBQ3hCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixrQ0FBNkIsR0FBRyxJQUFJLENBQUM7UUFDckMsNEJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLCtCQUEwQixHQUFHLElBQUksQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1Qiw2QkFBd0IsR0FBRyxJQUFJLENBQUM7UUFDaEMsNEJBQXVCLEdBQUcsSUFBSSxDQUFDOztRQUcvQixZQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ2xCLFlBQU8sR0FBRyxPQUFPLENBQUM7UUFDbEIsZUFBVSxHQUFHLFVBQVUsQ0FBQztRQUN4QixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsV0FBVyxDQUFDO1FBQzFCLGdCQUFXLEdBQUcsV0FBVyxDQUFDO1FBQzFCLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBQzlCLGlCQUFZLEdBQUcsWUFBWSxDQUFDO1FBR2pDLElBQUksQ0FBQyxLQUFLLEdBQUc7WUFDWDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxhQUFhLEVBQUU7Ozs7O29CQUNiLFVBQUEsTUFBTTt3QkFDSixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsU0FBUztnQkFDZixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsU0FBUztnQkFDMUIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLHVCQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsU0FBUztxQkFDdEI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyx1QkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxjQUFjO2dCQUMvQixRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxjQUFjO3FCQUMzQjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLHVCQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsZ0JBQWdCO2dCQUNqQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxnQkFBZ0I7cUJBQzdCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssdUJBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxRQUFRO2dCQUN6QixRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxRQUFRO3FCQUNyQjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLHVCQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxFQUM3QixhQUFhLEVBQUUsbUJBQW1CLEdBQ25DO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxrQkFBa0I7Z0JBQ3hCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxrQkFBa0I7Z0JBQ25DLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dCQUM1QyxjQUFjLEVBQUU7b0JBQ2QsTUFBTSx1QkFDRCx1QkFBdUIsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLElBQzVELE1BQU0sRUFBRSxTQUFTLEVBQ2pCLElBQUksRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUN2QjtvQkFDRCxvQkFBb0IsRUFBRTt3QkFDcEIsVUFBVSxFQUFFLGtCQUFrQjtxQkFDL0I7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyx1QkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGVBQWU7Z0JBQ3JCLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxlQUFlO2dCQUNoQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxlQUFlO3FCQUM1QjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLHVCQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsZ0JBQWdCO2dCQUNqQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxnQkFBZ0I7cUJBQzdCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssdUJBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxjQUFjO2dCQUNwQixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsY0FBYztnQkFDL0IsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLHVCQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsY0FBYztxQkFDM0I7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyx1QkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLFFBQVE7Z0JBQ3pCLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dCQUM1QyxjQUFjLEVBQUU7b0JBQ2QsTUFBTSx1QkFDRCx1QkFBdUIsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLElBQzVELE1BQU0sRUFBRSxTQUFTLEVBQ2pCLElBQUksRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUN2QjtvQkFDRCxvQkFBb0IsRUFBRTt3QkFDcEIsVUFBVSxFQUFFLFFBQVE7cUJBQ3JCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssdUJBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxRQUFRO2dCQUN6QixRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxRQUFRO3FCQUNyQjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLHVCQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxFQUM3QixhQUFhLEVBQUUsbUJBQW1CLEdBQ25DO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxTQUFTO2dCQUNmLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxTQUFTO2dCQUMxQixRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sdUJBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxTQUFTO3FCQUN0QjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLHVCQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsT0FBTztnQkFDYixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsT0FBTztnQkFDeEIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLHVCQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsT0FBTztxQkFDcEI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyx1QkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7OztvQkFDYixVQUFBLE1BQU07d0JBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsU0FBUztnQ0FDcEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsT0FBTztnQ0FDZCxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFFBQVEsRUFBRSxLQUFLO2dDQUNmLFNBQVMsRUFBRSxPQUFPO2dDQUNsQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsV0FBVztnQ0FDdkMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixRQUFRLEVBQUUsS0FBSztnQ0FDZixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsVUFBQSxNQUFNO3dCQUNKLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDO2lCQUNGO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxrQkFBa0I7Z0NBQzlDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUseUJBQXlCO2dDQUNoQyxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsOEJBQThCO2dDQUMxRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFlBQVk7Z0NBQ3ZCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFlBQVksRUFBRSxvQkFBb0I7Z0NBQ2xDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUI7Z0NBQzdDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsdUJBQXVCO2dDQUM5QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFlBQVksRUFBRSx1QkFBdUI7Z0NBQ3JDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw0QkFBNEI7Z0NBQ3hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixZQUFZLEVBQUUsdUJBQXVCO2dDQUNyQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsU0FBUztnQ0FDcEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsV0FBVztnQ0FDdEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUF2dEJlLGtEQUEwQixHQUFHO1FBQzNDLE1BQU0sRUFBRTtZQUNOLElBQUksRUFBRSxFQUFFO1lBQ1IsR0FBRyxFQUFFLEVBQUU7WUFDUCxNQUFNLEVBQUUsR0FBRztZQUNYLEtBQUssRUFBRSxHQUFHO1lBQ1YsV0FBVyxFQUFFLENBQUM7WUFDZCxNQUFNLEVBQUUsT0FBTztZQUNmLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLE9BQU8sRUFBRSxRQUFRO1NBQ2xCO0tBQ0YsQ0FBQztJQUVjLG9DQUFZLEdBQUcsRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLENBQUM7SUEyc0IzRCw4QkFBQztDQUFBLEFBenRCRCxJQXl0QkM7U0F6dEJZLHVCQUF1Qjs7O0lBQ2xDLG1EQVdFOztJQUVGLHFDQUF5RDs7SUFFekQsd0NBQW9COztJQUNwQixzREFBa0M7O0lBQ2xDLGdEQUE0Qjs7SUFDNUIsNkRBQXlDOztJQUN6QywwQ0FBdUI7O0lBQ3ZCLCtDQUEyQjs7SUFDM0IsNENBQTZCOztJQUM3Qiw4Q0FBK0I7O0lBQy9CLHlDQUFzQjs7SUFDdEIsZ0VBQTRDOztJQUM1QywwREFBc0M7O0lBQ3RDLDZEQUF5Qzs7SUFDekMsbURBQStCOztJQUMvQixvREFBZ0M7O0lBQ2hDLHVEQUFtQzs7SUFDbkMsMkRBQXVDOztJQUN2QywwREFBc0M7O0lBR3RDLDBDQUF5Qjs7SUFDekIsMENBQXlCOztJQUN6Qiw2Q0FBK0I7O0lBQy9CLDRDQUE2Qjs7SUFDN0IsOENBQWlDOztJQUNqQyw4Q0FBaUM7O0lBQ2pDLGdEQUFxQzs7SUFDckMsK0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuXG5pbXBvcnQgeyBWaXN1YWxFZGl0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcbmltcG9ydCB7XG4gIGZhQ2xvbmUsXG4gIGZhVHJhc2hBbHQsXG4gIGZhVHJhc2gsXG4gIGZhQXJyb3dVcCxcbiAgZmFBcnJvd0Rvd24sXG4gIGZhQWxpZ25MZWZ0LFxuICBmYUFsaWduQ2VudGVyLFxuICBmYUFsaWduUmlnaHRcbn0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtc29saWQtc3ZnLWljb25zJztcbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vYWN0aW9uLXR5cGVzJztcblxuZXhwb3J0IGNsYXNzIFRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnIHtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zID0ge1xuICAgIGNvbmZpZzoge1xuICAgICAgbGVmdDogNjAsXG4gICAgICB0b3A6IDYwLFxuICAgICAgaGVpZ2h0OiAxMDAsXG4gICAgICB3aWR0aDogMTAwLFxuICAgICAgc3Ryb2tlV2lkdGg6IDIsXG4gICAgICBzdHJva2U6ICdibGFjaycsXG4gICAgICBvcmlnaW5YOiAnY2VudGVyJyxcbiAgICAgIG9yaWdpblk6ICdjZW50ZXInXG4gICAgfVxuICB9O1xuXG4gIHN0YXRpYyByZWFkb25seSBidXR0b25TdHlsZXMgPSB7ICdmb250LXdlaWdodCc6ICdib2xkJyB9O1xuXG4gIHB1YmxpYyB0b29sczogYW55W107XG4gIHB1YmxpYyBpc0ZpcnN0VG9vbFNlbGVjdGVkID0gdHJ1ZTtcbiAgcHVibGljIGhhc0NoZWNrZXJzQmcgPSB0cnVlO1xuICBwdWJsaWMgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHMgPSB0cnVlO1xuICBwdWJsaWMgaGFzWm9vbSA9IGZhbHNlO1xuICBwdWJsaWMgaXNXaWRlQ2FudmFzID0gdHJ1ZTtcbiAgcHVibGljIHRvb2xzRmxleCA9ICcwIDEgMTMlJztcbiAgcHVibGljIGFjdGlvbnNGbGV4ID0gJzAgMSAyMCUnO1xuICBwdWJsaWMgaGFzUGFuID0gZmFsc2U7XG4gIHB1YmxpYyByZXNpemVDYW52YXNUb0JhY2tncm91bmRJbWFnZSA9IHRydWU7XG4gIHB1YmxpYyBpc0NlbnRlckJhY2tncm91bmRJbWFnZSA9IHRydWU7XG4gIHB1YmxpYyBoYXNTaG9ydGN1dHNFeHBhbnNpb25QYW5lbCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDbG9uZVNob3J0Y3V0ID0gdHJ1ZTtcbiAgcHVibGljIGhhc1JlbW92ZVNob3J0Y3V0ID0gdHJ1ZTtcbiAgcHVibGljIGhhc1JlbW92ZUFsbFNob3J0Y3V0ID0gdHJ1ZTtcbiAgcHVibGljIGhhc1NlbmRCYWNrd2FyZHNTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNCcmluZ0ZvcndhcmRTaG9ydGN1dCA9IHRydWU7XG5cbiAgLy8gRm9udEF3ZXNvbWUgSWNvbnNcbiAgcHVibGljIGZhQ2xvbmUgPSBmYUNsb25lO1xuICBwdWJsaWMgZmFUcmFzaCA9IGZhVHJhc2g7XG4gIHB1YmxpYyBmYVRyYXNoQWx0ID0gZmFUcmFzaEFsdDtcbiAgcHVibGljIGZhQXJyb3dVcCA9IGZhQXJyb3dVcDtcbiAgcHVibGljIGZhQXJyb3dEb3duID0gZmFBcnJvd0Rvd247XG4gIHB1YmxpYyBmYUFsaWduTGVmdCA9IGZhQWxpZ25MZWZ0O1xuICBwdWJsaWMgZmFBbGlnbkNlbnRlciA9IGZhQWxpZ25DZW50ZXI7XG4gIHB1YmxpYyBmYUFsaWduUmlnaHQgPSBmYUFsaWduUmlnaHQ7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy50b29scyA9IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbmZpZycsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdQcm9kdWN0JyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdQcm9kdWN0JyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjM2NiNDRiJyxcbiAgICAgICAgICAgIGZpbGw6ICcjM2NiNDRiJyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAncHJvZHVjdCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjM2NiNDRiJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdQcmltYXJ5IExvZ28nLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ1ByaW1hcnkgTG9nbycsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnI2U2MTk0YicsXG4gICAgICAgICAgICBmaWxsOiAnI2U2MTk0YicgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ3ByaW1hcnlfbG9nbydcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjZTYxOTRiJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdTZWNvbmRhcnkgTG9nbycsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnU2Vjb25kYXJ5IExvZ28nLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyNmMDMyZTYnLFxuICAgICAgICAgICAgZmlsbDogJyNmMDMyZTYnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdzZWNvbmRhcnlfbG9nbydcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjZjAzMmU2J1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdMb2NrdXAnLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0xvY2t1cCcsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnI2JjZjYwYycsXG4gICAgICAgICAgICBmaWxsOiAnI2JjZjYwYycgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ2xvY2t1cCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjYmNmNjBjJyxcbiAgICAgICAgICAndGV4dC1zaGFkb3cnOiAnMHB4IDAuN3B4ICNmZmZmZmYnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0hlYWRsaW5lIFRleHRib3gnLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0hlYWRsaW5lIFRleHRib3gnLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyM0MzYzZDgnLFxuICAgICAgICAgICAgZmlsbDogJyM0MzYzZDgnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdoZWFkbGluZV90ZXh0Ym94J1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyM0MzYzZDgnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ090aGVyIFRleHRib3gnLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ090aGVyIFRleHRib3gnLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyNmNTgyMzEnLFxuICAgICAgICAgICAgZmlsbDogJyNmNTgyMzEnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdvdGhlcl90ZXh0Ym94J1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyNmNTgyMzEnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0dyYXBoaWMgQWNjZW50JyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdHcmFwaGljIEFjY2VudCcsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnIzAwMDA3NScsXG4gICAgICAgICAgICBmaWxsOiAnIzAwMDA3NScgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ2dyYXBoaWNfYWNjZW50J1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyMwMDAwNzUnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1Bob3RvIEFjY2VudCcsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnUGhvdG8gQWNjZW50JyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjNDZmMGYwJyxcbiAgICAgICAgICAgIGZpbGw6ICcjNDZmMGYwJyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAncGhvdG9fYWNjZW50J1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyM0NmYwZjAnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0J1dHRvbicsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnQnV0dG9uJyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjMDA4MDgwJyxcbiAgICAgICAgICAgIGZpbGw6ICcjMDA4MDgwJyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnYnV0dG9uJ1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyMwMDgwODAnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0Jhbm5lcicsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnQmFubmVyJyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjZmZlMTE5JyxcbiAgICAgICAgICAgIGZpbGw6ICcjZmZlMTE5JyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnYmFubmVyJ1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyNmZmUxMTknLFxuICAgICAgICAgICd0ZXh0LXNoYWRvdyc6ICcwcHggMC43cHggI2ZmZmZmZidcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU3RpY2tlcicsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnU3RpY2tlcicsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnI2ZhYmViZScsXG4gICAgICAgICAgICBmaWxsOiAnI2ZhYmViZScgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ3N0aWNrZXInXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnI2ZhYmViZSdcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnRnJhbWUnLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0ZyYW1lJyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjZTZiZWZmJyxcbiAgICAgICAgICAgIGZpbGw6ICcjZTZiZWZmJyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnZnJhbWUnXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnI2U2YmVmZidcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBzaGFwZScsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5TSEFQRSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0JyaW5nIEZyb250JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93VXAsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZW5kIEJhY2snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ2xvbmUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ1JIUycsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUNsb25lLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5jbG9uZU9iamVjdCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ1JIUycsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVRyYXNoQWx0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgZ3JvdXAnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuR1JPVVAsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBMZWZ0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFsaWduTGVmdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25MZWZ0LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBIb3Jpem9udGFsIENlbnRlcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Ib3Jpem9udGFsQ2VudGVyLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBSaWdodCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnblJpZ2h0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblJpZ2h0LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsaWduIFRvcCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25NYXRlcmlhbDogJ3ZlcnRpY2FsX2FsaWduX3RvcCcsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uVG9wLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBWZXJ0aWNhbCBDZW50ZXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl9jZW50ZXInLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblZlcnRpY2FsQ2VudGVyLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBCb3R0b20nLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl9ib3R0b20nLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkJvdHRvbSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCcmluZyBGcm9udCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NlbmQgQmFjaycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb25Hcm91cCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH1cbiAgICBdO1xuICB9XG59XG4iXX0=