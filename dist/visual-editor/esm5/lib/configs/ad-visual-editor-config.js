/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';
var AdVisualEditorConfig = /** @class */ (function () {
    function AdVisualEditorConfig() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add shape',
                iconImgUrl: '@app/../assets/images/add-shape-icon.png',
                iconDisplayText: 'ADD SHAPE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ADD RECTANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-rectangle-icon.png',
                                action: VisualEditorService.addRectangle,
                                onSelectParams: AdVisualEditorConfig.addRectangleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Rectangle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD LINE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-line-icon.png',
                                action: VisualEditorService.addLine,
                                onSelectParams: AdVisualEditorConfig.addLineConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Line',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD ELLIPSE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-ellipse-icon.png',
                                action: VisualEditorService.addCircle,
                                onSelectParams: AdVisualEditorConfig.addCircleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Ellipse',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD TRIANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-triangle-icon.png',
                                action: VisualEditorService.addTriangle,
                                onSelectParams: AdVisualEditorConfig.addTriangleConfigParams,
                                flex: '33%'
                            },
                            {
                                label: 'Triangle',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-fill-drip',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultLineStrokeWidth,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: AdVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: AdVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'CHANGE PRODUCT',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                iconClass: 'fas fa-retweet',
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Change product',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
    AdVisualEditorConfig.defaultFontFamily = 'Times New Roman';
    AdVisualEditorConfig.availableFontFamilies = [
        AdVisualEditorConfig.defaultFontFamily,
        'Georgia',
        'Helvetica',
        'Comic Sans MS',
        'Impact',
        'Courier New'
    ];
    AdVisualEditorConfig.defaultFontSize = 34;
    AdVisualEditorConfig.availableFontSizes = _.range(8, 50 + 1);
    AdVisualEditorConfig.addTextConfigParams = {
        text: '<text>',
        config: {
            left: 10,
            top: 10,
            fontFamily: AdVisualEditorConfig.defaultFontFamily,
            fontSize: AdVisualEditorConfig.defaultFontSize,
            cornerColor: '#5c59f0',
            cornerSize: 8,
            transparentCorners: false
        }
    };
    AdVisualEditorConfig.defaultStrokeWidth = 0;
    AdVisualEditorConfig.defaultLineStrokeWidth = 5;
    AdVisualEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
    AdVisualEditorConfig.addImageConfigParams = {
        fileUrl: '',
        scaling: 0.35,
        config: { left: 10, top: 10, stroke: '#000000', strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
    };
    AdVisualEditorConfig.addRectangleConfigParams = {
        config: { left: 10, top: 10, height: 100, width: 100, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
    };
    AdVisualEditorConfig.addLineConfigParams = {
        config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultLineStrokeWidth, stroke: 'black' },
        coords: [0, 0, 100, 100]
    };
    AdVisualEditorConfig.addCircleConfigParams = {
        config: { left: 10, top: 10, radius: 50, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
    };
    AdVisualEditorConfig.addTriangleConfigParams = {
        config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' },
        coords: [{ x: 50, y: 0 }, { x: 0, y: 86 }, { x: 100, y: 86 }]
    };
    return AdVisualEditorConfig;
}());
export { AdVisualEditorConfig };
if (false) {
    /** @type {?} */
    AdVisualEditorConfig.defaultFontFamily;
    /** @type {?} */
    AdVisualEditorConfig.availableFontFamilies;
    /** @type {?} */
    AdVisualEditorConfig.defaultFontSize;
    /** @type {?} */
    AdVisualEditorConfig.availableFontSizes;
    /** @type {?} */
    AdVisualEditorConfig.addTextConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    AdVisualEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    AdVisualEditorConfig.availableStrokeWidths;
    /** @type {?} */
    AdVisualEditorConfig.addImageConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addRectangleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addLineConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addCircleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addTriangleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.prototype.tools;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    AdVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    AdVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWQtdmlzdWFsLWVkaXRvci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbmZpZ3MvYWQtdmlzdWFsLWVkaXRvci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUQ7SUEwREU7UUFWTyx3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsK0JBQTBCLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2QsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUN0QixnQkFBVyxHQUFHLFNBQVMsQ0FBQztRQUc3QixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1g7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsYUFBYSxFQUFFOzs7O29CQUFDLFVBQUMsTUFBTTt3QkFDckIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQzthQUNIO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELGVBQWUsRUFBRSxXQUFXO2dCQUM1QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsZUFBZTtnQ0FDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsZ0RBQWdEO2dDQUN6RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsY0FBYyxFQUFFLG9CQUFvQixDQUFDLHdCQUF3QjtnQ0FDN0QsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxVQUFVO2dDQUNqQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwyQ0FBMkM7Z0NBQ3BELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxPQUFPO2dDQUNuQyxjQUFjLEVBQUUsb0JBQW9CLENBQUMsbUJBQW1CO2dDQUN4RCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsOENBQThDO2dDQUN2RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsY0FBYyxFQUFFLG9CQUFvQixDQUFDLHFCQUFxQjtnQ0FDMUQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxTQUFTO2dDQUNoQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwrQ0FBK0M7Z0NBQ3hELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxXQUFXO2dDQUN2QyxjQUFjLEVBQUUsb0JBQW9CLENBQUMsdUJBQXVCO2dDQUM1RCxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsVUFBVTtnQ0FDakIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFOztvQ0FFTixJQUFJLEVBQUUsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBQztvQ0FDbkUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLFlBQVk7aUNBQzFDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLFVBQUMsTUFBTSxFQUFFLEdBQUc7b0NBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBQyxlQUFlLEVBQUUsZUFBYSxHQUFLLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDLENBQUM7Z0NBQ3BGLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDO2dDQUN4RSxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsT0FBTztnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsZUFBZTtnQ0FDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsT0FBTyxFQUFFLFFBQVE7Z0NBQ2pCLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUU7O29DQUVOLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDO29DQUNuRSxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsVUFBQyxNQUFNLEVBQUUsR0FBRztvQ0FDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFDLGVBQWUsRUFBRSxlQUFhLEdBQUssRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUMsQ0FBQztnQ0FDcEYsQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRSxFQUFDLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUM7Z0NBQ3hFLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLGlDQUFpQztvQkFDakMsY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGtDQUFrQztvQkFDbEMsa0VBQWtFO29CQUNsRSx1RUFBdUU7b0JBQ3ZFLGdDQUFnQztvQkFDaEMsa0RBQWtEO29CQUNsRCxrQkFBa0I7b0JBQ2xCLE1BQU07b0JBQ04sTUFBTTtvQkFDTixjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsZ0NBQWdDO29CQUNoQywrQkFBK0I7b0JBQy9CLGdCQUFnQjtvQkFDaEIsK0NBQStDO29CQUMvQyw2RUFBNkU7b0JBQzdFLG9EQUFvRDtvQkFDcEQsU0FBUztvQkFDVCx5Q0FBeUM7b0JBQ3pDLHdDQUF3QztvQkFDeEMsMkZBQTJGO29CQUMzRixTQUFTO29CQUNULGdGQUFnRjtvQkFDaEYsa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxXQUFXO2dCQUNqQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLElBQUk7Z0JBQzVCLGFBQWEsRUFBRTs7OztvQkFBQyxVQUFDLE1BQU07d0JBQ3JCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsb0JBQW9CLENBQUMscUJBQXFCO2dDQUMxRCxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxzQkFBc0I7Z0NBQ25FLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQ0FDMUMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUU7O29DQUVOLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDO29DQUNuRSxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsVUFBQyxNQUFNLEVBQUUsR0FBRztvQ0FDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFDLGVBQWUsRUFBRSxlQUFhLEdBQUssRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUMsQ0FBQztnQ0FDcEYsQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRSxFQUFDLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUM7Z0NBQ3hFLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsVUFBVSxFQUFFLHlDQUF5QztnQkFDckQsZUFBZSxFQUFFLFVBQVU7Z0JBQzNCLGFBQWEsRUFBRTs7OztvQkFBQyxVQUFDLE1BQU07d0JBQ3JCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0JBQ3JDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxtQkFBbUI7YUFDekQ7WUFDRDtnQkFDRSxJQUFJLEVBQUUsV0FBVztnQkFDakIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dCQUM5QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsb0JBQW9CLENBQUMscUJBQXFCO2dDQUMxRCxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxpQkFBaUI7Z0NBQzlELFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFVBQVU7NkJBQ2pCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2lDQUMxQztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxVQUFDLE1BQU0sRUFBRSxHQUFHO29DQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUMsZUFBZSxFQUFFLGVBQWEsR0FBSyxFQUFDLENBQUM7Z0NBQzNELENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUM7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsb0JBQW9CLENBQUMsa0JBQWtCO2dDQUN2RCxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxlQUFlO2dDQUM1RCxTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxtQkFBbUI7Z0NBQzlCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLHFCQUFxQjtnQ0FDaEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsb0JBQW9CO2dDQUMvQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsVUFBVTtnQ0FDdEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxlQUFlO2dDQUMxQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELGVBQWUsRUFBRSxXQUFXO2dCQUM1QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQkFDeEIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLFFBQVE7Z0JBQ3RDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxvQkFBb0I7YUFDMUQ7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsT0FBTztnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsZUFBZTtnQ0FDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsT0FBTyxFQUFFLFFBQVE7Z0NBQ2pCLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsWUFBWTtnQ0FDdkIsTUFBTSxFQUFFOztvQ0FFTixJQUFJLEVBQUUsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBQztvQ0FDbkUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGNBQWM7aUNBQzVDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLFVBQUMsTUFBTSxFQUFFLEdBQUc7b0NBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBQyxlQUFlLEVBQUUsZUFBYSxHQUFLLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDLENBQUM7Z0NBQ3BGLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDO2dDQUN4RSxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixpQ0FBaUM7b0JBQ2pDLGNBQWM7b0JBQ2QsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLCtCQUErQjtvQkFDL0Isa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QixrQ0FBa0M7b0JBQ2xDLGtFQUFrRTtvQkFDbEUsdUVBQXVFO29CQUN2RSxnQ0FBZ0M7b0JBQ2hDLGtEQUFrRDtvQkFDbEQsa0JBQWtCO29CQUNsQixNQUFNO29CQUNOLE1BQU07b0JBQ04sY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGdDQUFnQztvQkFDaEMsK0JBQStCO29CQUMvQixnQkFBZ0I7b0JBQ2hCLCtDQUErQztvQkFDL0MsNkVBQTZFO29CQUM3RSxvREFBb0Q7b0JBQ3BELFNBQVM7b0JBQ1QseUNBQXlDO29CQUN6Qyx3Q0FBd0M7b0JBQ3hDLDJGQUEyRjtvQkFDM0YsU0FBUztvQkFDVCxnRkFBZ0Y7b0JBQ2hGLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsY0FBYztnQkFDcEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxPQUFPO2dCQUMvQixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsZ0JBQWdCO2dDQUN2QixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsU0FBUyxFQUFFLGdCQUFnQjtnQ0FDM0IsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxnQkFBZ0I7Z0NBQ3ZCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxNQUFNO2dDQUNmLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE9BQU87Z0NBQ2hCLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGVBQWU7Z0NBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE9BQU8sRUFBRSxRQUFRO2dDQUNqQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFlBQVk7Z0NBQ3ZCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2lDQUM1QztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxVQUFDLE1BQU0sRUFBRSxHQUFHO29DQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUMsZUFBZSxFQUFFLGVBQWEsR0FBSyxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBQyxDQUFDO2dDQUNwRixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFLEVBQUMsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBQztnQ0FDeEUsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7OztvQkFBQyxVQUFDLE1BQU07d0JBQ3JCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwyQ0FBMkM7Z0NBQ3BELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxrQkFBa0I7Z0NBQzlDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUseUJBQXlCO2dDQUNoQyxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSx3REFBd0Q7Z0NBQ2pFLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw4QkFBOEI7Z0NBQzFELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsaUJBQWlCO2dDQUM3QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLHVCQUF1QjtnQ0FDOUIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsc0RBQXNEO2dDQUMvRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsNEJBQTRCO2dDQUN4RCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDZDQUE2QztnQ0FDdEQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG9CQUFvQjtnQ0FDaEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUE5M0JlLHNDQUFpQixHQUFHLGlCQUFpQixDQUFDO0lBQ3RDLDBDQUFxQixHQUFHO1FBQ3RDLG9CQUFvQixDQUFDLGlCQUFpQjtRQUN0QyxTQUFTO1FBQ1QsV0FBVztRQUNYLGVBQWU7UUFDZixRQUFRO1FBQ1IsYUFBYTtLQUNkLENBQUM7SUFDYyxvQ0FBZSxHQUFHLEVBQUUsQ0FBQztJQUNyQix1Q0FBa0IsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDeEMsd0NBQW1CLEdBQUc7UUFDcEMsSUFBSSxFQUFFLFFBQVE7UUFDZCxNQUFNLEVBQUU7WUFDTixJQUFJLEVBQUUsRUFBRTtZQUNSLEdBQUcsRUFBRSxFQUFFO1lBQ1AsVUFBVSxFQUFFLG9CQUFvQixDQUFDLGlCQUFpQjtZQUNsRCxRQUFRLEVBQUUsb0JBQW9CLENBQUMsZUFBZTtZQUM5QyxXQUFXLEVBQUUsU0FBUztZQUN0QixVQUFVLEVBQUUsQ0FBQztZQUNiLGtCQUFrQixFQUFFLEtBQUs7U0FDMUI7S0FDRixDQUFDO0lBQ2MsdUNBQWtCLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLDJDQUFzQixHQUFHLENBQUMsQ0FBQztJQUMzQiwwQ0FBcUIsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDM0MseUNBQW9CLEdBQUc7UUFDckMsT0FBTyxFQUFFLEVBQUU7UUFDWCxPQUFPLEVBQUUsSUFBSTtRQUNiLE1BQU0sRUFBRSxFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFDO0tBQUMsQ0FBQztJQUVsSCw2Q0FBd0IsR0FBRztRQUN6QyxNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFDO0tBQzVILENBQUM7SUFDYyx3Q0FBbUIsR0FBRztRQUNwQyxNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLG9CQUFvQixDQUFDLHNCQUFzQixFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUM7UUFDdEcsTUFBTSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDO0tBQ3pCLENBQUM7SUFDYywwQ0FBcUIsR0FBRztRQUN0QyxNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsb0JBQW9CLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBQztLQUMvRyxDQUFDO0lBQ2MsNENBQXVCLEdBQUc7UUFDeEMsTUFBTSxFQUFFLEVBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFDO1FBQ2xHLE1BQU0sRUFBRSxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLEVBQUMsRUFBRSxFQUFDLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQyxDQUFDO0tBQ3hELENBQUM7SUFtMUJKLDJCQUFDO0NBQUEsQUFoNEJELElBZzRCQztTQWg0Qlksb0JBQW9COzs7SUFDL0IsdUNBQXNEOztJQUN0RCwyQ0FPRTs7SUFDRixxQ0FBcUM7O0lBQ3JDLHdDQUF3RDs7SUFDeEQseUNBV0U7O0lBQ0Ysd0NBQXVDOztJQUN2Qyw0Q0FBMkM7O0lBQzNDLDJDQUEyRDs7SUFDM0QsMENBR2tJOztJQUVsSSw4Q0FFRTs7SUFDRix5Q0FHRTs7SUFDRiwyQ0FFRTs7SUFDRiw2Q0FHRTs7SUFFRixxQ0FBb0I7O0lBQ3BCLG1EQUFrQzs7SUFDbEMsNkNBQTZCOztJQUM3QiwwREFBeUM7O0lBQ3pDLHVDQUFzQjs7SUFDdEIsc0NBQXFCOztJQUNyQiw0Q0FBMkI7O0lBQzNCLDJDQUEwQjs7SUFDMUIseUNBQTZCOztJQUM3QiwyQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbmltcG9ydCB7IFZpc3VhbEVkaXRvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0aW9uVHlwZXMsIE9iamVjdFR5cGVzIH0gZnJvbSAnLi9hY3Rpb24tdHlwZXMnO1xuXG5leHBvcnQgY2xhc3MgQWRWaXN1YWxFZGl0b3JDb25maWcge1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdEZvbnRGYW1pbHkgPSAnVGltZXMgTmV3IFJvbWFuJztcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZUZvbnRGYW1pbGllcyA9IFtcbiAgICBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAnR2VvcmdpYScsXG4gICAgJ0hlbHZldGljYScsXG4gICAgJ0NvbWljIFNhbnMgTVMnLFxuICAgICdJbXBhY3QnLFxuICAgICdDb3VyaWVyIE5ldydcbiAgXTtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250U2l6ZSA9IDM0O1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlRm9udFNpemVzID0gXy5yYW5nZSg4LCA1MCArIDEpO1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkVGV4dENvbmZpZ1BhcmFtcyA9IHtcbiAgICB0ZXh0OiAnPHRleHQ+JyxcbiAgICBjb25maWc6IHtcbiAgICAgIGxlZnQ6IDEwLFxuICAgICAgdG9wOiAxMCxcbiAgICAgIGZvbnRGYW1pbHk6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250U2l6ZSxcbiAgICAgIGNvcm5lckNvbG9yOiAnIzVjNTlmMCcsXG4gICAgICBjb3JuZXJTaXplOiA4LFxuICAgICAgdHJhbnNwYXJlbnRDb3JuZXJzOiBmYWxzZVxuICAgIH1cbiAgfTtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRTdHJva2VXaWR0aCA9IDA7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0TGluZVN0cm9rZVdpZHRoID0gNTtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZVN0cm9rZVdpZHRocyA9IF8ucmFuZ2UoMCwgNTAgKyAxKTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZEltYWdlQ29uZmlnUGFyYW1zID0ge1xuICAgIGZpbGVVcmw6ICcnLFxuICAgIHNjYWxpbmc6IDAuMzUsXG4gICAgY29uZmlnOiB7bGVmdDogMTAsIHRvcDogMTAsIHN0cm9rZTogJyMwMDAwMDAnLCBzdHJva2VXaWR0aDogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdFN0cm9rZVdpZHRoLCBjcm9zc09yaWdpbjogJ2Fub255bW91cyd9fTtcblxuICBzdGF0aWMgcmVhZG9ubHkgYWRkUmVjdGFuZ2xlQ29uZmlnUGFyYW1zID0ge1xuICAgIGNvbmZpZzoge2xlZnQ6IDEwLCB0b3A6IDEwLCBoZWlnaHQ6IDEwMCwgd2lkdGg6IDEwMCwgc3Ryb2tlV2lkdGg6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCwgc3Ryb2tlOiAnYmxhY2snfVxuICB9O1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkTGluZUNvbmZpZ1BhcmFtcyA9IHtcbiAgICBjb25maWc6IHtsZWZ0OiAxMCwgdG9wOiAxMCwgc3Ryb2tlV2lkdGg6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRMaW5lU3Ryb2tlV2lkdGgsIHN0cm9rZTogJ2JsYWNrJ30sXG4gICAgY29vcmRzOiBbMCwgMCwgMTAwLCAxMDBdXG4gIH07XG4gIHN0YXRpYyByZWFkb25seSBhZGRDaXJjbGVDb25maWdQYXJhbXMgPSB7XG4gICAgY29uZmlnOiB7bGVmdDogMTAsIHRvcDogMTAsIHJhZGl1czogNTAsIHN0cm9rZVdpZHRoOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsIHN0cm9rZTogJ2JsYWNrJ31cbiAgfTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZFRyaWFuZ2xlQ29uZmlnUGFyYW1zID0ge1xuICAgIGNvbmZpZzoge2xlZnQ6IDEwLCB0b3A6IDEwLCBzdHJva2VXaWR0aDogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdFN0cm9rZVdpZHRoLCBzdHJva2U6ICdibGFjayd9LFxuICAgIGNvb3JkczogW3t4OiA1MCwgeTogMH0sIHt4OiAwLCB5OiA4Nn0sIHt4OiAxMDAsIHk6IDg2fV1cbiAgfTtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scyA9IHRydWU7XG4gIHB1YmxpYyBoYXNab29tID0gdHJ1ZTtcbiAgcHVibGljIGhhc1BhbiA9IHRydWU7XG4gIHB1YmxpYyBpc1dpZGVDYW52YXMgPSB0cnVlO1xuICBwdWJsaWMgaXNTaG93RnJhbWUgPSB0cnVlO1xuICBwdWJsaWMgdG9vbHNGbGV4ID0gJzAgMiAxMCUnO1xuICBwdWJsaWMgYWN0aW9uc0ZsZXggPSAnMCAyIDIwJSc7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy50b29scyA9IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbmZpZycsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgc2hhcGUnLFxuICAgICAgICBpY29uSW1nVXJsOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FkZC1zaGFwZS1pY29uLnBuZycsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0FERCBTSEFQRScsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FERCBSRUNUQU5HTEUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2hhcGUtcmVjdGFuZ2xlLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZFJlY3RhbmdsZSxcbiAgICAgICAgICAgICAgb25TZWxlY3RQYXJhbXM6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmFkZFJlY3RhbmdsZUNvbmZpZ1BhcmFtcyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1JlY3RhbmdsZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FERCBMSU5FJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NoYXBlLWxpbmUtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkTGluZSxcbiAgICAgICAgICAgICAgb25TZWxlY3RQYXJhbXM6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmFkZExpbmVDb25maWdQYXJhbXMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdMaW5lJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUREIEVMTElQU0UnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2hhcGUtZWxsaXBzZS1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRDaXJjbGUsXG4gICAgICAgICAgICAgIG9uU2VsZWN0UGFyYW1zOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hZGRDaXJjbGVDb25maWdQYXJhbXMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdFbGxpcHNlJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUREIFRSSUFOR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NoYXBlLXRyaWFuZ2xlLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZFRyaWFuZ2xlLFxuICAgICAgICAgICAgICBvblNlbGVjdFBhcmFtczogQWRWaXN1YWxFZGl0b3JDb25maWcuYWRkVHJpYW5nbGVDb25maWdQYXJhbXMsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1RyaWFuZ2xlJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgc2hhcGUnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuU0hBUEUsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZpbGwgY29sb3InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZJTEwgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1maWxsLWRyaXAnLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZpbGxDb2xvclxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7J2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7J2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJywgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQkxVUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdibHVyJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBhbmdsZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEFOR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAga2V5TmFtZTogJ2FuZ2xlJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyB3aWR0aCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIExFTkdUSCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdsZW5ndGgnLFxuICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgIGZsZXg6ICcyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgY29sb3InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBDT0xPUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgIC8vY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93Q29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAgLy8gTk9URTogZnVuY3Rpb25hbGl0eSB0dXJuZWQgb2ZmXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2Ugd2lkdGgnLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzc1JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIFdJRFRIJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hdmFpbGFibGVTdHJva2VXaWR0aHMsXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgIC8vICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICcyNSUnXG4gICAgICAgICAgLy8gICB9XG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2UgY29sb3InLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzY3JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIENPTE9SJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgIC8vICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtcGVuJyxcbiAgICAgICAgICAvLyAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgLy8gICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAvLyAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgIC8vICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlQ29sb3JcbiAgICAgICAgICAvLyAgICAgfSxcbiAgICAgICAgICAvLyAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgIC8vICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgbGluZScsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5MSU5FLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTdHJva2Ugd2lkdGgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0TGluZVN0cm9rZVdpZHRoLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VXaWR0aCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTdHJva2UgY29sb3InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NUUk9LRSBDT0xPUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgIC8vY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlQ29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2FkZCB0ZXh0JyxcbiAgICAgICAgaWNvbkltZ1VybDogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hZGQtdGV4dC1pY29uLnBuZycsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0FERCBURVhUJyxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRUZXh0LFxuICAgICAgICBvblNlbGVjdFBhcmFtczogQWRWaXN1YWxFZGl0b3JDb25maWcuYWRkVGV4dENvbmZpZ1BhcmFtcyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHRleHQnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuVEVYQk9YLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGT05UIEZBTUlMWScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlRm9udEZhbWlsaWVzLFxuICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0Rm9udEZhbWlseSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAxMDAlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnRk9OVCBDT0xPUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZpbGxDb2xvclxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7J2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWB9O1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snfSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZPTlQgU0laRScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlRm9udFNpemVzLFxuICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udFNpemUsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZvbnRTaXplLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gTEVGVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWFsaWduLWxlZnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25MZWZ0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gQ0VOVEVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtYWxpZ24tY2VudGVyJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduQ2VudGVyLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gUklHSFQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1hbGlnbi1yaWdodCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblJpZ2h0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQk9MRCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWJvbGQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0Qm9sZCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0lUQUxJQycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWl0YWxpYycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRJdGFsaWMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdVTkRFUkxJTkUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS11bmRlcmxpbmUnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0VW5kZXJsaW5lLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgaW1hZ2UnLFxuICAgICAgICBpY29uSW1nVXJsOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FkZC1pbWFnZS1pY29uLnBuZycsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0FERCBJTUFHRScsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuVVBMT0FELFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRJbWFnZSxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmFkZEltYWdlQ29uZmlnUGFyYW1zLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgaW1hZ2UnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuSU1BR0UsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQkxVUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdibHVyJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBhbmdsZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEFOR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAga2V5TmFtZTogJ2FuZ2xlJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyB3aWR0aCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIExFTkdUSCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdsZW5ndGgnLFxuICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGNvbG9yJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1wZW4nLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvd0NvbG9yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIC8vIE5PVEU6IGZ1bmN0aW9uYWxpdHkgdHVybmVkIG9mZlxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIHdpZHRoJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAvLyAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZVdpZHRoLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgIC8vICAgfVxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBDT0xPUicsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgLy8gICAgIGRpYWxvZzoge1xuICAgICAgICAgIC8vICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgLy8gICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAvLyAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgLy8gICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgIC8vICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgcHJvZHVjdCcsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5QUk9EVUNULFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDSEFOR0UgUFJPRFVDVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1yZXR3ZWV0JyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NoYW5nZSBwcm9kdWN0JyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGJsdXInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBCTFVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAga2V5TmFtZTogJ2JsdXInLFxuICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGFuZ2xlJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQU5HTEUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICBrZXlOYW1lOiAnYW5nbGUnLFxuICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IHdpZHRoJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgTEVOR1RIJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAga2V5TmFtZTogJ2xlbmd0aCcsXG4gICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgY29sb3InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBDT0xPUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgIC8vY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93Q29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBncm91cCcsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5HUk9VUCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gTEVGVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1sZWZ0LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uTGVmdCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIEhPUklaT05UQUwgQ0VOVEVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLWhvcml6b250YWwtY2VudGVyLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFJJR0hUJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLXJpZ2h0LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uUmlnaHQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gVE9QJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLXRvcC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblRvcCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFZFUlRJQ0FMIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi12ZXJ0aWNhbC1jZW50ZXItaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIEJPVFRPTScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1ib3R0b20taWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Cb3R0b20sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uR3JvdXAsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF0sXG4gICAgICB9XG4gICAgXTtcbiAgfVxufVxuIl19