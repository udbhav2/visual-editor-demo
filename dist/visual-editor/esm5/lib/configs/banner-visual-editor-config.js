/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';
var BannerVisualEditorConfig = /** @class */ (function () {
    function BannerVisualEditorConfig() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: BannerVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: BannerVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
    BannerVisualEditorConfig.defaultFontFamily = 'Times New Roman';
    BannerVisualEditorConfig.availableFontFamilies = [
        BannerVisualEditorConfig.defaultFontFamily,
        'Georgia',
        'mc-extra-lt',
        'mc-bold',
        'mc-book',
        'Helvetica',
        'Comic Sans MS',
        'Impact',
        'Courier New'
    ];
    BannerVisualEditorConfig.defaultFontSize = 34;
    BannerVisualEditorConfig.availableFontSizes = _.range(8, 50 + 1);
    BannerVisualEditorConfig.addTextConfigParams = {
        text: '<text>',
        config: {
            left: 10,
            top: 10,
            fontFamily: BannerVisualEditorConfig.defaultFontFamily,
            fontSize: BannerVisualEditorConfig.defaultFontSize,
            cornerColor: '#5c59f0',
            cornerSize: 8,
            transparentCorners: false
        }
    };
    BannerVisualEditorConfig.defaultStrokeWidth = 0;
    BannerVisualEditorConfig.defaultLineStrokeWidth = 5;
    BannerVisualEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
    BannerVisualEditorConfig.addImageConfigParams = {
        fileUrl: '',
        scaling: 0.35,
        config: { left: 10, top: 10, stroke: '#000000', strokeWidth: BannerVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
    };
    return BannerVisualEditorConfig;
}());
export { BannerVisualEditorConfig };
if (false) {
    /** @type {?} */
    BannerVisualEditorConfig.defaultFontFamily;
    /** @type {?} */
    BannerVisualEditorConfig.availableFontFamilies;
    /** @type {?} */
    BannerVisualEditorConfig.defaultFontSize;
    /** @type {?} */
    BannerVisualEditorConfig.availableFontSizes;
    /** @type {?} */
    BannerVisualEditorConfig.addTextConfigParams;
    /** @type {?} */
    BannerVisualEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    BannerVisualEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    BannerVisualEditorConfig.availableStrokeWidths;
    /** @type {?} */
    BannerVisualEditorConfig.addImageConfigParams;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.tools;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFubmVyLXZpc3VhbC1lZGl0b3ItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWdzL2Jhbm5lci12aXN1YWwtZWRpdG9yLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUxRDtJQThDRTtRQVZPLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QiwrQkFBMEIsR0FBRyxJQUFJLENBQUM7UUFDbEMsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBRzdCLElBQUksQ0FBQyxLQUFLLEdBQUc7WUFDWDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2FBQ0g7WUFDRDtnQkFDRSxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsVUFBVSxFQUFFLHlDQUF5QztnQkFDckQsZUFBZSxFQUFFLFVBQVU7Z0JBQzNCLGFBQWEsRUFBRTs7OztvQkFBQyxVQUFDLE1BQU07d0JBQ3JCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0JBQ3JDLGNBQWMsRUFBRSx3QkFBd0IsQ0FBQyxtQkFBbUI7YUFDN0Q7WUFDRDtnQkFDRSxJQUFJLEVBQUUsV0FBVztnQkFDakIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dCQUM5QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsd0JBQXdCLENBQUMscUJBQXFCO2dDQUM5RCxzQkFBc0IsRUFBRSx3QkFBd0IsQ0FBQyxpQkFBaUI7Z0NBQ2xFLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFVBQVU7NkJBQ2pCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2lDQUMxQztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxVQUFDLE1BQU0sRUFBRSxHQUFHO29DQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUMsZUFBZSxFQUFFLGVBQWEsR0FBSyxFQUFDLENBQUM7Z0NBQzNELENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUM7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsd0JBQXdCLENBQUMsa0JBQWtCO2dDQUMzRCxzQkFBc0IsRUFBRSx3QkFBd0IsQ0FBQyxlQUFlO2dDQUNoRSxTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxtQkFBbUI7Z0NBQzlCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLHFCQUFxQjtnQ0FDaEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsb0JBQW9CO2dDQUMvQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsVUFBVTtnQ0FDdEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxlQUFlO2dDQUMxQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELGVBQWUsRUFBRSxXQUFXO2dCQUM1QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQkFDeEIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLFFBQVE7Z0JBQ3RDLGNBQWMsRUFBRSx3QkFBd0IsQ0FBQyxvQkFBb0I7YUFDOUQ7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7O29CQUFDLFVBQUMsTUFBTTt3QkFDckIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUU7b0JBQ1IsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDJDQUEyQztnQ0FDcEQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGtCQUFrQjtnQ0FDOUMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx5QkFBeUI7Z0NBQ2hDLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLHdEQUF3RDtnQ0FDakUsTUFBTSxFQUFFLG1CQUFtQixDQUFDLDhCQUE4QjtnQ0FDMUQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUI7Z0NBQzdDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsdUJBQXVCO2dDQUM5QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSxzREFBc0Q7Z0NBQy9ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw0QkFBNEI7Z0NBQ3hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNkNBQTZDO2dDQUN0RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQztJQTdUZSwwQ0FBaUIsR0FBRyxpQkFBaUIsQ0FBQztJQUN0Qyw4Q0FBcUIsR0FBRztRQUN0Qyx3QkFBd0IsQ0FBQyxpQkFBaUI7UUFDMUMsU0FBUztRQUNULGFBQWE7UUFDYixTQUFTO1FBQ1QsU0FBUztRQUNULFdBQVc7UUFDWCxlQUFlO1FBQ2YsUUFBUTtRQUNSLGFBQWE7S0FDZCxDQUFDO0lBQ2Msd0NBQWUsR0FBRyxFQUFFLENBQUM7SUFDckIsMkNBQWtCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3hDLDRDQUFtQixHQUFHO1FBQ3BDLElBQUksRUFBRSxRQUFRO1FBQ2QsTUFBTSxFQUFFO1lBQ04sSUFBSSxFQUFFLEVBQUU7WUFDUixHQUFHLEVBQUUsRUFBRTtZQUNQLFVBQVUsRUFBRSx3QkFBd0IsQ0FBQyxpQkFBaUI7WUFDdEQsUUFBUSxFQUFFLHdCQUF3QixDQUFDLGVBQWU7WUFDbEQsV0FBVyxFQUFFLFNBQVM7WUFDdEIsVUFBVSxFQUFFLENBQUM7WUFDYixrQkFBa0IsRUFBRSxLQUFLO1NBQzFCO0tBQ0YsQ0FBQztJQUNjLDJDQUFrQixHQUFHLENBQUMsQ0FBQztJQUN2QiwrQ0FBc0IsR0FBRyxDQUFDLENBQUM7SUFDM0IsOENBQXFCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzNDLDZDQUFvQixHQUFHO1FBQ3JDLE9BQU8sRUFBRSxFQUFFO1FBQ1gsT0FBTyxFQUFFLElBQUk7UUFDYixNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsd0JBQXdCLENBQUMsa0JBQWtCLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBQztLQUFDLENBQUM7SUE4UnhJLCtCQUFDO0NBQUEsQUEvVEQsSUErVEM7U0EvVFksd0JBQXdCOzs7SUFDbkMsMkNBQXNEOztJQUN0RCwrQ0FVRTs7SUFDRix5Q0FBcUM7O0lBQ3JDLDRDQUF3RDs7SUFDeEQsNkNBV0U7O0lBQ0YsNENBQXVDOztJQUN2QyxnREFBMkM7O0lBQzNDLCtDQUEyRDs7SUFDM0QsOENBR3NJOztJQUV0SSx5Q0FBb0I7O0lBQ3BCLHVEQUFrQzs7SUFDbEMsaURBQTZCOztJQUM3Qiw4REFBeUM7O0lBQ3pDLDJDQUFzQjs7SUFDdEIsMENBQXFCOztJQUNyQixnREFBMkI7O0lBQzNCLCtDQUEwQjs7SUFDMUIsNkNBQTZCOztJQUM3QiwrQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbmltcG9ydCB7IFZpc3VhbEVkaXRvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0aW9uVHlwZXMsIE9iamVjdFR5cGVzIH0gZnJvbSAnLi9hY3Rpb24tdHlwZXMnO1xuXG5leHBvcnQgY2xhc3MgQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnIHtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250RmFtaWx5ID0gJ1RpbWVzIE5ldyBSb21hbic7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVGb250RmFtaWxpZXMgPSBbXG4gICAgQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICdHZW9yZ2lhJyxcbiAgICAnbWMtZXh0cmEtbHQnLFxuICAgICdtYy1ib2xkJyxcbiAgICAnbWMtYm9vaycsXG4gICAgJ0hlbHZldGljYScsXG4gICAgJ0NvbWljIFNhbnMgTVMnLFxuICAgICdJbXBhY3QnLFxuICAgICdDb3VyaWVyIE5ldydcbiAgXTtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250U2l6ZSA9IDM0O1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlRm9udFNpemVzID0gXy5yYW5nZSg4LCA1MCArIDEpO1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkVGV4dENvbmZpZ1BhcmFtcyA9IHtcbiAgICB0ZXh0OiAnPHRleHQ+JyxcbiAgICBjb25maWc6IHtcbiAgICAgIGxlZnQ6IDEwLFxuICAgICAgdG9wOiAxMCxcbiAgICAgIGZvbnRGYW1pbHk6IEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAgIGZvbnRTaXplOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRTaXplLFxuICAgICAgY29ybmVyQ29sb3I6ICcjNWM1OWYwJyxcbiAgICAgIGNvcm5lclNpemU6IDgsXG4gICAgICB0cmFuc3BhcmVudENvcm5lcnM6IGZhbHNlXG4gICAgfVxuICB9O1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdFN0cm9rZVdpZHRoID0gMDtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRMaW5lU3Ryb2tlV2lkdGggPSA1O1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlU3Ryb2tlV2lkdGhzID0gXy5yYW5nZSgwLCA1MCArIDEpO1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkSW1hZ2VDb25maWdQYXJhbXMgPSB7XG4gICAgZmlsZVVybDogJycsXG4gICAgc2NhbGluZzogMC4zNSxcbiAgICBjb25maWc6IHtsZWZ0OiAxMCwgdG9wOiAxMCwgc3Ryb2tlOiAnIzAwMDAwMCcsIHN0cm9rZVdpZHRoOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdFN0cm9rZVdpZHRoLCBjcm9zc09yaWdpbjogJ2Fub255bW91cyd9fTtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scyA9IHRydWU7XG4gIHB1YmxpYyBoYXNab29tID0gdHJ1ZTtcbiAgcHVibGljIGhhc1BhbiA9IHRydWU7XG4gIHB1YmxpYyBpc1dpZGVDYW52YXMgPSB0cnVlO1xuICBwdWJsaWMgaXNTaG93RnJhbWUgPSB0cnVlO1xuICBwdWJsaWMgdG9vbHNGbGV4ID0gJzAgMiAxMCUnO1xuICBwdWJsaWMgYWN0aW9uc0ZsZXggPSAnMCAyIDIwJSc7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy50b29scyA9IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbmZpZycsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgdGV4dCcsXG4gICAgICAgIGljb25JbWdVcmw6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWRkLXRleHQtaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgVEVYVCcsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkVGV4dCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5hZGRUZXh0Q29uZmlnUGFyYW1zLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgdGV4dCcsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5URVhCT1gsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZPTlQgRkFNSUxZJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgIGRyb3Bkb3duT3B0aW9uOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlRm9udEZhbWlsaWVzLFxuICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMTAwJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZPTlQgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgLy9jb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGRhdGE6IHtjb2xvcjogJyMwMDAwMDAnLCB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLCBidXR0b25UZXh0OiAnU0VUJ30sXG4gICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGaWxsQ29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gfTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7J2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJ30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGT05UIFNJWkUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5hdmFpbGFibGVGb250U2l6ZXMsXG4gICAgICAgICAgICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246IEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udFNpemUsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZvbnRTaXplLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gTEVGVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWFsaWduLWxlZnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25MZWZ0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gQ0VOVEVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtYWxpZ24tY2VudGVyJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduQ2VudGVyLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gUklHSFQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1hbGlnbi1yaWdodCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblJpZ2h0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQk9MRCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWJvbGQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0Qm9sZCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0lUQUxJQycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWl0YWxpYycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRJdGFsaWMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdVTkRFUkxJTkUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS11bmRlcmxpbmUnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0VW5kZXJsaW5lLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgaW1hZ2UnLFxuICAgICAgICBpY29uSW1nVXJsOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FkZC1pbWFnZS1pY29uLnBuZycsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0FERCBJTUFHRScsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuVVBMT0FELFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRJbWFnZSxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5hZGRJbWFnZUNvbmZpZ1BhcmFtcyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGltYWdlJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLklNQUdFLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBncm91cCcsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5HUk9VUCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gTEVGVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1sZWZ0LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uTGVmdCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIEhPUklaT05UQUwgQ0VOVEVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLWhvcml6b250YWwtY2VudGVyLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFJJR0hUJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLXJpZ2h0LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uUmlnaHQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gVE9QJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLXRvcC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblRvcCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFZFUlRJQ0FMIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi12ZXJ0aWNhbC1jZW50ZXItaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIEJPVFRPTScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1ib3R0b20taWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Cb3R0b20sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uR3JvdXAsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF0sXG4gICAgICB9XG4gICAgXTtcbiAgfVxufVxuIl19