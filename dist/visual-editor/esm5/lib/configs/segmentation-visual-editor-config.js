/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes } from './action-types';
var SegmentationVisualEditorConfig = /** @class */ (function () {
    function SegmentationVisualEditorConfig() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = false;
        this.hasZoom = false;
        this.hasPan = false;
        this.toolsFlex = '0 1 10%';
        this.actionsFlex = '0 1 10%';
        this.tools = [
            {
                name: 'draw',
                iconImgUrl: '@app/../assets/images/target_icon.png',
                iconDisplayText: 'POINTER',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: true });
                        canvas.freeDrawingBrush.color = SegmentationVisualEditorConfig.colorBackground;
                        canvas.freeDrawingBrush.width = SegmentationVisualEditorConfig.drawingBrushWidth;
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'TOGGLE',
                                type: ActionTypes.TOGGLE,
                                action: VisualEditorService.setColor,
                                isOn: false,
                                onValue: SegmentationVisualEditorConfig.colorForeground,
                                offValue: SegmentationVisualEditorConfig.colorBackground,
                                onLabel: 'Foreground',
                                offLabel: 'Background',
                                flex: '100%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'UNDO',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-undo-alt',
                                action: VisualEditorService.undoDrawingTransparentBg,
                                flex: '50%'
                            },
                            {
                                label: 'CLEAR',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.clearDrawingTransparentBg,
                                flex: '50%'
                            }
                        ] }
                ],
            }
        ];
    }
    SegmentationVisualEditorConfig.colorBackground = '#cb0c93';
    SegmentationVisualEditorConfig.colorForeground = '#2bc4b6';
    SegmentationVisualEditorConfig.drawingBrushWidth = 4;
    return SegmentationVisualEditorConfig;
}());
export { SegmentationVisualEditorConfig };
if (false) {
    /** @type {?} */
    SegmentationVisualEditorConfig.colorBackground;
    /** @type {?} */
    SegmentationVisualEditorConfig.colorForeground;
    /** @type {?} */
    SegmentationVisualEditorConfig.drawingBrushWidth;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.tools;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VnbWVudGF0aW9uLXZpc3VhbC1lZGl0b3ItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWdzL3NlZ21lbnRhdGlvbi12aXN1YWwtZWRpdG9yLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFFeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTdDO0lBY0U7UUFSTyx3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsK0JBQTBCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsZ0JBQVcsR0FBRyxTQUFTLENBQUM7UUFHN0IsSUFBSSxDQUFDLEtBQUssR0FBRztZQUNYO2dCQUNFLElBQUksRUFBRSxNQUFNO2dCQUNaLFVBQVUsRUFBRSx1Q0FBdUM7Z0JBQ25ELGVBQWUsRUFBRSxTQUFTO2dCQUMxQixhQUFhLEVBQUU7Ozs7b0JBQUMsVUFBQyxNQUFNO3dCQUNyQixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ3BDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEdBQUcsOEJBQThCLENBQUMsZUFBZSxDQUFDO3dCQUMvRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLDhCQUE4QixDQUFDLGlCQUFpQixDQUFDO3dCQUNqRixPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxRQUFRO2dDQUNwQyxJQUFJLEVBQUUsS0FBSztnQ0FDWCxPQUFPLEVBQUUsOEJBQThCLENBQUMsZUFBZTtnQ0FDdkQsUUFBUSxFQUFFLDhCQUE4QixDQUFDLGVBQWU7Z0NBQ3hELE9BQU8sRUFBRSxZQUFZO2dDQUNyQixRQUFRLEVBQUUsWUFBWTtnQ0FDdEIsSUFBSSxFQUFFLE1BQU07NkJBQ2I7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsTUFBTTtnQ0FDYixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxpQkFBaUI7Z0NBQzVCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyx3QkFBd0I7Z0NBQ3BELElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxPQUFPO2dDQUNkLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLHlCQUF5QjtnQ0FDckQsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUExRGUsOENBQWUsR0FBRyxTQUFTLENBQUM7SUFDNUIsOENBQWUsR0FBRyxTQUFTLENBQUM7SUFDNUIsZ0RBQWlCLEdBQUcsQ0FBQyxDQUFDO0lBeUR4QyxxQ0FBQztDQUFBLEFBNURELElBNERDO1NBNURZLDhCQUE4Qjs7O0lBQ3pDLCtDQUE0Qzs7SUFDNUMsK0NBQTRDOztJQUM1QyxpREFBc0M7O0lBRXRDLCtDQUFvQjs7SUFDcEIsNkRBQWtDOztJQUNsQyx1REFBNEI7O0lBQzVCLG9FQUEwQzs7SUFDMUMsaURBQXVCOztJQUN2QixnREFBc0I7O0lBQ3RCLG1EQUE2Qjs7SUFDN0IscURBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVmlzdWFsRWRpdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Zpc3VhbC1lZGl0b3Iuc2VydmljZSc7XG5cbmltcG9ydCB7IEFjdGlvblR5cGVzIH0gZnJvbSAnLi9hY3Rpb24tdHlwZXMnO1xuXG5leHBvcnQgY2xhc3MgU2VnbWVudGF0aW9uVmlzdWFsRWRpdG9yQ29uZmlnIHtcbiAgc3RhdGljIHJlYWRvbmx5IGNvbG9yQmFja2dyb3VuZCA9ICcjY2IwYzkzJztcbiAgc3RhdGljIHJlYWRvbmx5IGNvbG9yRm9yZWdyb3VuZCA9ICcjMmJjNGI2JztcbiAgc3RhdGljIHJlYWRvbmx5IGRyYXdpbmdCcnVzaFdpZHRoID0gNDtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gdHJ1ZTtcbiAgcHVibGljIGhhc0hpZGRlblNlbGVjdGlvbkNvbnRyb2xzID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNab29tID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNQYW4gPSBmYWxzZTtcbiAgcHVibGljIHRvb2xzRmxleCA9ICcwIDEgMTAlJztcbiAgcHVibGljIGFjdGlvbnNGbGV4ID0gJzAgMSAxMCUnO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudG9vbHMgPSBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdkcmF3JyxcbiAgICAgICAgaWNvbkltZ1VybDogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy90YXJnZXRfaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdQT0lOVEVSJyxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IHRydWUgfSk7XG4gICAgICAgICAgY2FudmFzLmZyZWVEcmF3aW5nQnJ1c2guY29sb3IgPSBTZWdtZW50YXRpb25WaXN1YWxFZGl0b3JDb25maWcuY29sb3JCYWNrZ3JvdW5kO1xuICAgICAgICAgIGNhbnZhcy5mcmVlRHJhd2luZ0JydXNoLndpZHRoID0gU2VnbWVudGF0aW9uVmlzdWFsRWRpdG9yQ29uZmlnLmRyYXdpbmdCcnVzaFdpZHRoO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnVE9HR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuVE9HR0xFLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0Q29sb3IsXG4gICAgICAgICAgICAgIGlzT246IGZhbHNlLFxuICAgICAgICAgICAgICBvblZhbHVlOiBTZWdtZW50YXRpb25WaXN1YWxFZGl0b3JDb25maWcuY29sb3JGb3JlZ3JvdW5kLFxuICAgICAgICAgICAgICBvZmZWYWx1ZTogU2VnbWVudGF0aW9uVmlzdWFsRWRpdG9yQ29uZmlnLmNvbG9yQmFja2dyb3VuZCxcbiAgICAgICAgICAgICAgb25MYWJlbDogJ0ZvcmVncm91bmQnLFxuICAgICAgICAgICAgICBvZmZMYWJlbDogJ0JhY2tncm91bmQnLFxuICAgICAgICAgICAgICBmbGV4OiAnMTAwJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnVU5ETycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXVuZG8tYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnVuZG9EcmF3aW5nVHJhbnNwYXJlbnRCZyxcbiAgICAgICAgICAgICAgZmxleDogJzUwJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS10cmFzaC1hbHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuY2xlYXJEcmF3aW5nVHJhbnNwYXJlbnRCZyxcbiAgICAgICAgICAgICAgZmxleDogJzUwJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfVxuICAgIF07XG4gIH1cbn1cbiJdfQ==