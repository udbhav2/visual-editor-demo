/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ColorPickerDialogComponent } from '../components/color-picker-dialog/color-picker-dialog.component';
import { faFont, faTrashAlt, faBold, faAlignRight, faFillDrip, faPen, faAlignLeft, faItalic, faUnderline, faAlignCenter, faRetweet, faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { ActionTypes, ObjectTypes } from './action-types';
var DesignEditorConfig = /** @class */ (function () {
    function DesignEditorConfig() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = false;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasBringForwardShortcut = true;
        this.hasSendBackwardsShortcut = true;
        // FontAwesome Icons
        this.faTrashAlt = faTrashAlt;
        this.faFont = faFont;
        this.faBold = faBold;
        this.faAlignRight = faAlignRight;
        this.faFillDrip = faFillDrip;
        this.faPen = faPen;
        this.faAlignLeft = faAlignLeft;
        this.faItalic = faItalic;
        this.faUnderline = faUnderline;
        this.faAlignCenter = faAlignCenter;
        this.faRetweet = faRetweet;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faFillDrip,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': "3px solid " + val,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': "3px solid " + val,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineStrokeWidth,
                                iconClass: faFont,
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': "3px solid " + val,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Font Family',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontFamily,
                                iconClass: faFont,
                                action: VisualEditorService.setFontFamily,
                                flex: '0 1 75%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Font Size',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontSizes,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontSize,
                                iconClass: faFont,
                                action: VisualEditorService.setFontSize,
                                flex: '0 1 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Line Height',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableLineHeights,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineHeight,
                                action: VisualEditorService.setLineHeight,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Character Spacing',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableCharSpacings,
                                dropdownSelectedOption: DesignEditorConfig.defaultCharSpacing,
                                action: VisualEditorService.setCharacterSpacing,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bold',
                                type: ActionTypes.BUTTON,
                                iconClass: faBold,
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Italic',
                                type: ActionTypes.BUTTON,
                                iconClass: faItalic,
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Underline',
                                type: ActionTypes.BUTTON,
                                iconClass: faUnderline,
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Font Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faFont,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': "3px solid " + val };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Blur',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Angle',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Length',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'Shadow Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': "3px solid " + val,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                function (action, val) {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': "3px solid " + val,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    function (canvas) {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
    DesignEditorConfig.defaultFontFamily = 'Times New Roman';
    DesignEditorConfig.availableFontFamilies = [
        DesignEditorConfig.defaultFontFamily,
        'Georgia',
        'Helvetica',
        'Comic Sans MS',
        'Impact',
        'Courier New'
    ];
    DesignEditorConfig.defaultFontSize = 34;
    DesignEditorConfig.availableFontSizes = _.range(8, 50 + 1);
    DesignEditorConfig.defaultLineHeight = 1;
    DesignEditorConfig.availableLineHeights = _.range(0, 11, 0.5);
    DesignEditorConfig.defaultCharSpacing = 0;
    DesignEditorConfig.availableCharSpacings = _.range(0, 801, 100);
    DesignEditorConfig.addTextConfigParams = {
        text: '<text>',
        config: {
            left: 10,
            top: 10,
            fontFamily: DesignEditorConfig.defaultFontFamily,
            fontSize: DesignEditorConfig.defaultFontSize,
            cornerColor: '#5c59f0',
            cornerSize: 8,
            transparentCorners: false
        }
    };
    DesignEditorConfig.defaultStrokeWidth = 0;
    DesignEditorConfig.defaultLineStrokeWidth = 5;
    DesignEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
    return DesignEditorConfig;
}());
export { DesignEditorConfig };
if (false) {
    /** @type {?} */
    DesignEditorConfig.defaultFontFamily;
    /** @type {?} */
    DesignEditorConfig.availableFontFamilies;
    /** @type {?} */
    DesignEditorConfig.defaultFontSize;
    /** @type {?} */
    DesignEditorConfig.availableFontSizes;
    /** @type {?} */
    DesignEditorConfig.defaultLineHeight;
    /** @type {?} */
    DesignEditorConfig.availableLineHeights;
    /** @type {?} */
    DesignEditorConfig.defaultCharSpacing;
    /** @type {?} */
    DesignEditorConfig.availableCharSpacings;
    /** @type {?} */
    DesignEditorConfig.addTextConfigParams;
    /** @type {?} */
    DesignEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    DesignEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    DesignEditorConfig.availableStrokeWidths;
    /** @type {?} */
    DesignEditorConfig.prototype.tools;
    /** @type {?} */
    DesignEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    DesignEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    DesignEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    DesignEditorConfig.prototype.hasZoom;
    /** @type {?} */
    DesignEditorConfig.prototype.hasPan;
    /** @type {?} */
    DesignEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    DesignEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    DesignEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    DesignEditorConfig.prototype.actionsFlex;
    /** @type {?} */
    DesignEditorConfig.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    DesignEditorConfig.prototype.hasCloneShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasRemoveShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasBringForwardShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.faTrashAlt;
    /** @type {?} */
    DesignEditorConfig.prototype.faFont;
    /** @type {?} */
    DesignEditorConfig.prototype.faBold;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignRight;
    /** @type {?} */
    DesignEditorConfig.prototype.faFillDrip;
    /** @type {?} */
    DesignEditorConfig.prototype.faPen;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignLeft;
    /** @type {?} */
    DesignEditorConfig.prototype.faItalic;
    /** @type {?} */
    DesignEditorConfig.prototype.faUnderline;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignCenter;
    /** @type {?} */
    DesignEditorConfig.prototype.faRetweet;
    /** @type {?} */
    DesignEditorConfig.prototype.faArrowUp;
    /** @type {?} */
    DesignEditorConfig.prototype.faArrowDown;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVzaWduLWVkaXRvci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbmZpZ3MvZGVzaWduLWVkaXRvci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGlFQUFpRSxDQUFDO0FBQzdHLE9BQU8sRUFDTCxNQUFNLEVBQ04sVUFBVSxFQUNWLE1BQU0sRUFDTixZQUFZLEVBQ1osVUFBVSxFQUNWLEtBQUssRUFDTCxXQUFXLEVBQ1gsUUFBUSxFQUNSLFdBQVcsRUFDWCxhQUFhLEVBQ2IsU0FBUyxFQUNULFNBQVMsRUFDVCxXQUFXLEVBQ1osTUFBTSxtQ0FBbUMsQ0FBQztBQUUzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTFEO0lBaUVFO1FBL0JPLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QiwrQkFBMEIsR0FBRyxJQUFJLENBQUM7UUFDbEMsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBQ3hCLCtCQUEwQixHQUFHLElBQUksQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1Qiw0QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDL0IsNkJBQXdCLEdBQUcsSUFBSSxDQUFDOztRQUdoQyxlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLFdBQU0sR0FBRyxNQUFNLENBQUM7UUFDaEIsV0FBTSxHQUFHLE1BQU0sQ0FBQztRQUNoQixpQkFBWSxHQUFHLFlBQVksQ0FBQztRQUM1QixlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLFVBQUssR0FBRyxLQUFLLENBQUM7UUFDZCxnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUMxQixhQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3BCLGdCQUFXLEdBQUcsV0FBVyxDQUFDO1FBQzFCLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBQzlCLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUN0QixnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUcvQixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1g7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsYUFBYSxFQUFFOzs7OztvQkFDYixVQUFBLE1BQU07d0JBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsVUFBQSxNQUFNO3dCQUNKLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDO2lCQUNGO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxVQUFVO2dDQUNyQixNQUFNLEVBQUU7b0NBQ04sU0FBUyxFQUFFLDBCQUEwQjtvQ0FDckMsSUFBSSxFQUFFO3dDQUNKLEtBQUssRUFBRSxTQUFTO3dDQUNoQixTQUFTLEVBQUUsV0FBVzt3Q0FDdEIsVUFBVSxFQUFFLEtBQUs7cUNBQ2xCO29DQUNELE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2lDQUMxQztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxVQUFDLE1BQU0sRUFBRSxHQUFHO29DQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHO3dDQUNqQixlQUFlLEVBQUUsZUFBYSxHQUFLO3dDQUNuQyxnQkFBZ0IsRUFBRSxLQUFLO3FDQUN4QixDQUFDO2dDQUNKLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUU7b0NBQ1QsZUFBZSxFQUFFLGlCQUFpQjtvQ0FDbEMsZ0JBQWdCLEVBQUUsS0FBSztpQ0FDeEI7Z0NBQ0QsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxNQUFNO2dDQUNmLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsT0FBTztnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxlQUFlO2dDQUN0QixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixPQUFPLEVBQUUsUUFBUTtnQ0FDakIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsS0FBSztnQ0FDaEIsTUFBTSxFQUFFO29DQUNOLFNBQVMsRUFBRSwwQkFBMEI7b0NBQ3JDLElBQUksRUFBRTt3Q0FDSixLQUFLLEVBQUUsU0FBUzt3Q0FDaEIsU0FBUyxFQUFFLFdBQVc7d0NBQ3RCLFVBQVUsRUFBRSxLQUFLO3FDQUNsQjtvQ0FDRCxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsVUFBQyxNQUFNLEVBQUUsR0FBRztvQ0FDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRzt3Q0FDakIsZUFBZSxFQUFFLGVBQWEsR0FBSzt3Q0FDbkMsZ0JBQWdCLEVBQUUsS0FBSztxQ0FDeEIsQ0FBQztnQ0FDSixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFO29DQUNULGVBQWUsRUFBRSxpQkFBaUI7b0NBQ2xDLGdCQUFnQixFQUFFLEtBQUs7aUNBQ3hCO2dDQUNELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0QsaUNBQWlDO29CQUNqQyxjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0Isa0NBQWtDO29CQUNsQyxrRUFBa0U7b0JBQ2xFLHVFQUF1RTtvQkFDdkUsZ0NBQWdDO29CQUNoQyxrREFBa0Q7b0JBQ2xELGtCQUFrQjtvQkFDbEIsTUFBTTtvQkFDTixNQUFNO29CQUNOLGNBQWM7b0JBQ2QsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLCtCQUErQjtvQkFDL0Isa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QixnQ0FBZ0M7b0JBQ2hDLCtCQUErQjtvQkFDL0IsZ0JBQWdCO29CQUNoQiwrQ0FBK0M7b0JBQy9DLDZFQUE2RTtvQkFDN0Usb0RBQW9EO29CQUNwRCxTQUFTO29CQUNULHlDQUF5QztvQkFDekMsd0NBQXdDO29CQUN4QywyRkFBMkY7b0JBQzNGLFNBQVM7b0JBQ1QsZ0ZBQWdGO29CQUNoRixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFNBQVM7Z0NBQ3BCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxVQUFVO2dDQUNyQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsV0FBVztnQkFDakIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxJQUFJO2dCQUM1QixhQUFhLEVBQUU7Ozs7O29CQUNiLFVBQUEsTUFBTTt3QkFDSixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsa0JBQWtCLENBQUMscUJBQXFCO2dDQUN4RCxzQkFBc0IsRUFDcEIsa0JBQWtCLENBQUMsc0JBQXNCO2dDQUMzQyxTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0NBQzFDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxLQUFLO2dDQUNoQixNQUFNLEVBQUU7b0NBQ04sU0FBUyxFQUFFLDBCQUEwQjtvQ0FDckMsSUFBSSxFQUFFO3dDQUNKLEtBQUssRUFBRSxTQUFTO3dDQUNoQixTQUFTLEVBQUUsV0FBVzt3Q0FDdEIsVUFBVSxFQUFFLEtBQUs7cUNBQ2xCO29DQUNELE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2lDQUM1QztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxVQUFDLE1BQU0sRUFBRSxHQUFHO29DQUMxQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHO3dDQUNqQixlQUFlLEVBQUUsZUFBYSxHQUFLO3dDQUNuQyxnQkFBZ0IsRUFBRSxLQUFLO3FDQUN4QixDQUFDO2dDQUNKLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUU7b0NBQ1QsZUFBZSxFQUFFLGlCQUFpQjtvQ0FDbEMsZ0JBQWdCLEVBQUUsS0FBSztpQ0FDeEI7Z0NBQ0QsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFNBQVM7Z0NBQ3BCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxVQUFVO2dDQUNyQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsV0FBVztnQkFDakIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dCQUM5QixhQUFhLEVBQUU7Ozs7O29CQUNiLFVBQUEsTUFBTTt3QkFDSixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLFFBQVE7Z0NBQzFCLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxxQkFBcUI7Z0NBQ3hELHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLGlCQUFpQjtnQ0FDNUQsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLFFBQVE7Z0NBQzFCLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxrQkFBa0I7Z0NBQ3JELHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLGVBQWU7Z0NBQzFELFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsV0FBVztnQ0FDdkMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUTtnQ0FDMUIsY0FBYyxFQUFFLGtCQUFrQixDQUFDLG9CQUFvQjtnQ0FDdkQsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCO2dDQUM1RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLG1CQUFtQjtnQ0FDMUIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsa0JBQWtCLENBQUMscUJBQXFCO2dDQUN4RCxzQkFBc0IsRUFBRSxrQkFBa0IsQ0FBQyxrQkFBa0I7Z0NBQzdELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsV0FBVztnQ0FDdkMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsVUFBVTtnQ0FDdEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRTtvQ0FDTixTQUFTLEVBQUUsMEJBQTBCO29DQUNyQyxJQUFJLEVBQUU7d0NBQ0osS0FBSyxFQUFFLFNBQVM7d0NBQ2hCLFNBQVMsRUFBRSxXQUFXO3dDQUN0QixVQUFVLEVBQUUsS0FBSztxQ0FDbEI7b0NBQ0QsT0FBTyxFQUFFLG1CQUFtQixDQUFDLFlBQVk7aUNBQzFDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLFVBQUMsTUFBTSxFQUFFLEdBQUc7b0NBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBRSxlQUFlLEVBQUUsZUFBYSxHQUFLLEVBQUUsQ0FBQztnQ0FDN0QsQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRSxFQUFFLGVBQWUsRUFBRSxpQkFBaUIsRUFBRTtnQ0FDakQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7OztvQkFDYixVQUFBLE1BQU07d0JBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxPQUFPO2dDQUNoQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGVBQWU7Z0NBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE9BQU8sRUFBRSxRQUFRO2dDQUNqQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsS0FBSztnQ0FDaEIsTUFBTSxFQUFFOztvQ0FFTixJQUFJLEVBQUU7d0NBQ0osS0FBSyxFQUFFLFNBQVM7d0NBQ2hCLFNBQVMsRUFBRSxXQUFXO3dDQUN0QixVQUFVLEVBQUUsS0FBSztxQ0FDbEI7b0NBQ0QsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGNBQWM7aUNBQzVDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLFVBQUMsTUFBTSxFQUFFLEdBQUc7b0NBQzFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUc7d0NBQ2pCLGVBQWUsRUFBRSxlQUFhLEdBQUs7d0NBQ25DLGdCQUFnQixFQUFFLEtBQUs7cUNBQ3hCLENBQUM7Z0NBQ0osQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRTtvQ0FDVCxlQUFlLEVBQUUsaUJBQWlCO29DQUNsQyxnQkFBZ0IsRUFBRSxLQUFLO2lDQUN4QjtnQ0FDRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNELGlDQUFpQztvQkFDakMsY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGtDQUFrQztvQkFDbEMsa0VBQWtFO29CQUNsRSx1RUFBdUU7b0JBQ3ZFLGdDQUFnQztvQkFDaEMsa0RBQWtEO29CQUNsRCxrQkFBa0I7b0JBQ2xCLE1BQU07b0JBQ04sTUFBTTtvQkFDTixjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsZ0NBQWdDO29CQUNoQywrQkFBK0I7b0JBQy9CLGdCQUFnQjtvQkFDaEIsK0NBQStDO29CQUMvQyw2RUFBNkU7b0JBQzdFLG9EQUFvRDtvQkFDcEQsU0FBUztvQkFDVCx5Q0FBeUM7b0JBQ3pDLHdDQUF3QztvQkFDeEMsMkZBQTJGO29CQUMzRixTQUFTO29CQUNULGdGQUFnRjtvQkFDaEYsa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ047d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsT0FBTztnQkFDL0IsYUFBYSxFQUFFOzs7OztvQkFDYixVQUFBLE1BQU07d0JBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxPQUFPO2dDQUNoQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGVBQWU7Z0NBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE9BQU8sRUFBRSxRQUFRO2dDQUNqQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsS0FBSztnQ0FDaEIsTUFBTSxFQUFFO29DQUNOLFNBQVMsRUFBRSwwQkFBMEI7b0NBQ3JDLElBQUksRUFBRTt3Q0FDSixLQUFLLEVBQUUsU0FBUzt3Q0FDaEIsU0FBUyxFQUFFLFdBQVc7d0NBQ3RCLFVBQVUsRUFBRSxLQUFLO3FDQUNsQjtvQ0FDRCxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsVUFBQyxNQUFNLEVBQUUsR0FBRztvQ0FDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRzt3Q0FDakIsZUFBZSxFQUFFLGVBQWEsR0FBSzt3Q0FDbkMsZ0JBQWdCLEVBQUUsS0FBSztxQ0FDeEIsQ0FBQztnQ0FDSixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFO29DQUNULGVBQWUsRUFBRSxpQkFBaUI7b0NBQ2xDLGdCQUFnQixFQUFFLEtBQUs7aUNBQ3hCO2dDQUNELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7OztvQkFDYixVQUFBLE1BQU07d0JBQ0osTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsV0FBVztnQ0FDdEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGtCQUFrQjtnQ0FDOUMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx5QkFBeUI7Z0NBQ2hDLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw4QkFBOEI7Z0NBQzFELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsWUFBWTtnQ0FDdkIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixZQUFZLEVBQUUsb0JBQW9CO2dDQUNsQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsaUJBQWlCO2dDQUM3QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLHVCQUF1QjtnQ0FDOUIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixZQUFZLEVBQUUsdUJBQXVCO2dDQUNyQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsNEJBQTRCO2dDQUN4RCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsWUFBWSxFQUFFLHVCQUF1QjtnQ0FDckMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG9CQUFvQjtnQ0FDaEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFNBQVM7Z0NBQ3BCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDO0lBejhCZSxvQ0FBaUIsR0FBRyxpQkFBaUIsQ0FBQztJQUN0Qyx3Q0FBcUIsR0FBRztRQUN0QyxrQkFBa0IsQ0FBQyxpQkFBaUI7UUFDcEMsU0FBUztRQUNULFdBQVc7UUFDWCxlQUFlO1FBQ2YsUUFBUTtRQUNSLGFBQWE7S0FDZCxDQUFDO0lBQ2Msa0NBQWUsR0FBRyxFQUFFLENBQUM7SUFDckIscUNBQWtCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3hDLG9DQUFpQixHQUFHLENBQUMsQ0FBQztJQUN0Qix1Q0FBb0IsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDM0MscUNBQWtCLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZCLHdDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM3QyxzQ0FBbUIsR0FBRztRQUNwQyxJQUFJLEVBQUUsUUFBUTtRQUNkLE1BQU0sRUFBRTtZQUNOLElBQUksRUFBRSxFQUFFO1lBQ1IsR0FBRyxFQUFFLEVBQUU7WUFDUCxVQUFVLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCO1lBQ2hELFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxlQUFlO1lBQzVDLFdBQVcsRUFBRSxTQUFTO1lBQ3RCLFVBQVUsRUFBRSxDQUFDO1lBQ2Isa0JBQWtCLEVBQUUsS0FBSztTQUMxQjtLQUNGLENBQUM7SUFFYyxxQ0FBa0IsR0FBRyxDQUFDLENBQUM7SUFDdkIseUNBQXNCLEdBQUcsQ0FBQyxDQUFDO0lBQzNCLHdDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztJQTQ2QjdELHlCQUFDO0NBQUEsQUEzOEJELElBMjhCQztTQTM4Qlksa0JBQWtCOzs7SUFDN0IscUNBQXNEOztJQUN0RCx5Q0FPRTs7SUFDRixtQ0FBcUM7O0lBQ3JDLHNDQUF3RDs7SUFDeEQscUNBQXNDOztJQUN0Qyx3Q0FBMkQ7O0lBQzNELHNDQUF1Qzs7SUFDdkMseUNBQTZEOztJQUM3RCx1Q0FXRTs7SUFFRixzQ0FBdUM7O0lBQ3ZDLDBDQUEyQzs7SUFDM0MseUNBQTJEOztJQUUzRCxtQ0FBb0I7O0lBQ3BCLGlEQUFrQzs7SUFDbEMsMkNBQTZCOztJQUM3Qix3REFBeUM7O0lBQ3pDLHFDQUFzQjs7SUFDdEIsb0NBQXFCOztJQUNyQiwwQ0FBMkI7O0lBQzNCLHlDQUEwQjs7SUFDMUIsdUNBQTZCOztJQUM3Qix5Q0FBK0I7O0lBQy9CLHdEQUF5Qzs7SUFDekMsOENBQWdDOztJQUNoQywrQ0FBZ0M7O0lBQ2hDLGtEQUFtQzs7SUFDbkMscURBQXNDOztJQUN0QyxzREFBdUM7O0lBR3ZDLHdDQUErQjs7SUFDL0Isb0NBQXVCOztJQUN2QixvQ0FBdUI7O0lBQ3ZCLDBDQUFtQzs7SUFDbkMsd0NBQStCOztJQUMvQixtQ0FBcUI7O0lBQ3JCLHlDQUFpQzs7SUFDakMsc0NBQTJCOztJQUMzQix5Q0FBaUM7O0lBQ2pDLDJDQUFxQzs7SUFDckMsdUNBQTZCOztJQUM3Qix1Q0FBNkI7O0lBQzdCLHlDQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcblxuaW1wb3J0IHsgVmlzdWFsRWRpdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Zpc3VhbC1lZGl0b3Iuc2VydmljZSc7XG5pbXBvcnQgeyBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29sb3ItcGlja2VyLWRpYWxvZy9jb2xvci1waWNrZXItZGlhbG9nLmNvbXBvbmVudCc7XG5pbXBvcnQge1xuICBmYUZvbnQsXG4gIGZhVHJhc2hBbHQsXG4gIGZhQm9sZCxcbiAgZmFBbGlnblJpZ2h0LFxuICBmYUZpbGxEcmlwLFxuICBmYVBlbixcbiAgZmFBbGlnbkxlZnQsXG4gIGZhSXRhbGljLFxuICBmYVVuZGVybGluZSxcbiAgZmFBbGlnbkNlbnRlcixcbiAgZmFSZXR3ZWV0LFxuICBmYUFycm93VXAsXG4gIGZhQXJyb3dEb3duXG59IGZyb20gJ0Bmb3J0YXdlc29tZS9mcmVlLXNvbGlkLXN2Zy1pY29ucyc7XG5cbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vYWN0aW9uLXR5cGVzJztcblxuZXhwb3J0IGNsYXNzIERlc2lnbkVkaXRvckNvbmZpZyB7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0Rm9udEZhbWlseSA9ICdUaW1lcyBOZXcgUm9tYW4nO1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlRm9udEZhbWlsaWVzID0gW1xuICAgIERlc2lnbkVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAnR2VvcmdpYScsXG4gICAgJ0hlbHZldGljYScsXG4gICAgJ0NvbWljIFNhbnMgTVMnLFxuICAgICdJbXBhY3QnLFxuICAgICdDb3VyaWVyIE5ldydcbiAgXTtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250U2l6ZSA9IDM0O1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlRm9udFNpemVzID0gXy5yYW5nZSg4LCA1MCArIDEpO1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdExpbmVIZWlnaHQgPSAxO1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlTGluZUhlaWdodHMgPSBfLnJhbmdlKDAsIDExLCAwLjUpO1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdENoYXJTcGFjaW5nID0gMDtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZUNoYXJTcGFjaW5ncyA9IF8ucmFuZ2UoMCwgODAxLCAxMDApO1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkVGV4dENvbmZpZ1BhcmFtcyA9IHtcbiAgICB0ZXh0OiAnPHRleHQ+JyxcbiAgICBjb25maWc6IHtcbiAgICAgIGxlZnQ6IDEwLFxuICAgICAgdG9wOiAxMCxcbiAgICAgIGZvbnRGYW1pbHk6IERlc2lnbkVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAgIGZvbnRTaXplOiBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdEZvbnRTaXplLFxuICAgICAgY29ybmVyQ29sb3I6ICcjNWM1OWYwJyxcbiAgICAgIGNvcm5lclNpemU6IDgsXG4gICAgICB0cmFuc3BhcmVudENvcm5lcnM6IGZhbHNlXG4gICAgfVxuICB9O1xuXG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0U3Ryb2tlV2lkdGggPSAwO1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdExpbmVTdHJva2VXaWR0aCA9IDU7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVTdHJva2VXaWR0aHMgPSBfLnJhbmdlKDAsIDUwICsgMSk7XG5cbiAgcHVibGljIHRvb2xzOiBhbnlbXTtcbiAgcHVibGljIGlzRmlyc3RUb29sU2VsZWN0ZWQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2hlY2tlcnNCZyA9IGZhbHNlO1xuICBwdWJsaWMgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHMgPSB0cnVlO1xuICBwdWJsaWMgaGFzWm9vbSA9IHRydWU7XG4gIHB1YmxpYyBoYXNQYW4gPSB0cnVlO1xuICBwdWJsaWMgaXNXaWRlQ2FudmFzID0gdHJ1ZTtcbiAgcHVibGljIGlzU2hvd0ZyYW1lID0gdHJ1ZTtcbiAgcHVibGljIHRvb2xzRmxleCA9ICcwIDEgMTMlJztcbiAgcHVibGljIGFjdGlvbnNGbGV4ID0gJzAgMSAyMCUnO1xuICBwdWJsaWMgaGFzU2hvcnRjdXRzRXhwYW5zaW9uUGFuZWwgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2xvbmVTaG9ydGN1dCA9IGZhbHNlO1xuICBwdWJsaWMgaGFzUmVtb3ZlU2hvcnRjdXQgPSB0cnVlO1xuICBwdWJsaWMgaGFzUmVtb3ZlQWxsU2hvcnRjdXQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQgPSB0cnVlO1xuICBwdWJsaWMgaGFzU2VuZEJhY2t3YXJkc1Nob3J0Y3V0ID0gdHJ1ZTtcblxuICAvLyBGb250QXdlc29tZSBJY29uc1xuICBwdWJsaWMgZmFUcmFzaEFsdCA9IGZhVHJhc2hBbHQ7XG4gIHB1YmxpYyBmYUZvbnQgPSBmYUZvbnQ7XG4gIHB1YmxpYyBmYUJvbGQgPSBmYUJvbGQ7XG4gIHB1YmxpYyBmYUFsaWduUmlnaHQgPSBmYUFsaWduUmlnaHQ7XG4gIHB1YmxpYyBmYUZpbGxEcmlwID0gZmFGaWxsRHJpcDtcbiAgcHVibGljIGZhUGVuID0gZmFQZW47XG4gIHB1YmxpYyBmYUFsaWduTGVmdCA9IGZhQWxpZ25MZWZ0O1xuICBwdWJsaWMgZmFJdGFsaWMgPSBmYUl0YWxpYztcbiAgcHVibGljIGZhVW5kZXJsaW5lID0gZmFVbmRlcmxpbmU7XG4gIHB1YmxpYyBmYUFsaWduQ2VudGVyID0gZmFBbGlnbkNlbnRlcjtcbiAgcHVibGljIGZhUmV0d2VldCA9IGZhUmV0d2VldDtcbiAgcHVibGljIGZhQXJyb3dVcCA9IGZhQXJyb3dVcDtcbiAgcHVibGljIGZhQXJyb3dEb3duID0gZmFBcnJvd0Rvd247XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy50b29scyA9IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbmZpZycsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBzaGFwZScsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5TSEFQRSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0ZpbGwgY29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdGSUxMIENPTE9SJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZpbGxEcmlwLFxuICAgICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICcjMDAwMDAwJyxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGVUZXh0OiAnU0VUIENPTE9SJyxcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uVGV4dDogJ1NFVCdcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZpbGxDb2xvclxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsXG4gICAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWNvblN0eWxlOiB7XG4gICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLFxuICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEJMVVInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2JsdXInLFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGFuZ2xlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEFOR0xFJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGtleU5hbWU6ICdhbmdsZScsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgd2lkdGgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgTEVOR1RIJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2xlbmd0aCcsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgY29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQ09MT1InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhUGVuLFxuICAgICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICcjMDAwMDAwJyxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGVUZXh0OiAnU0VUIENPTE9SJyxcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uVGV4dDogJ1NFVCdcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvd0NvbG9yXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCxcbiAgICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uU3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsXG4gICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIC8vIE5PVEU6IGZ1bmN0aW9uYWxpdHkgdHVybmVkIG9mZlxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIHdpZHRoJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc3NSUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAvLyAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZVdpZHRoLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnMjUlJ1xuICAgICAgICAgIC8vICAgfVxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBDT0xPUicsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgLy8gICAgIGRpYWxvZzoge1xuICAgICAgICAgIC8vICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgLy8gICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAvLyAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgLy8gICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgIC8vICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFUcmFzaEFsdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBsaW5lJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLkxJTkUsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdHJva2Ugd2lkdGgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTVFJPS0UgV0lEVEgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICAgIGRyb3Bkb3duT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgICAgICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246XG4gICAgICAgICAgICAgICAgICBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdExpbmVTdHJva2VXaWR0aCxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU1RST0tFIENPTE9SJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVBlbixcbiAgICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnIzAwMDAwMCcsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvblRleHQ6ICdTRVQnXG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VDb2xvclxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsXG4gICAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWNvblN0eWxlOiB7XG4gICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLFxuICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0JSSU5HIEZST05UJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93VXAsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93RG93bixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhVHJhc2hBbHQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgdGV4dCcsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5URVhCT1gsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdGb250IEZhbWlseScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IERlc2lnbkVkaXRvckNvbmZpZy5hdmFpbGFibGVGb250RmFtaWxpZXMsXG4gICAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGb250RmFtaWx5LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDEgNzUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0ZvbnQgU2l6ZScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IERlc2lnbkVkaXRvckNvbmZpZy5hdmFpbGFibGVGb250U2l6ZXMsXG4gICAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250U2l6ZSxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0Rm9udFNpemUsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMSA3NSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnTGluZSBIZWlnaHQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICAgIGRyb3Bkb3duT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuYXZhaWxhYmxlTGluZUhlaWdodHMsXG4gICAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmRlZmF1bHRMaW5lSGVpZ2h0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRMaW5lSGVpZ2h0LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgNzUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NoYXJhY3RlciBTcGFjaW5nJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUNoYXJTcGFjaW5ncyxcbiAgICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdENoYXJTcGFjaW5nLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRDaGFyYWN0ZXJTcGFjaW5nLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgNzUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsaWduIExlZnQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25MZWZ0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnbkxlZnQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsaWduIENlbnRlcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25DZW50ZXIsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsaWduIFJpZ2h0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFsaWduUmlnaHQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduUmlnaHQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQm9sZCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFCb2xkLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRCb2xkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdJdGFsaWMnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhSXRhbGljLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRJdGFsaWMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1VuZGVybGluZScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFVbmRlcmxpbmUsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFVuZGVybGluZSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdGb250IENvbG9yJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyMwMDAwMDAnLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25UZXh0OiAnU0VUJ1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0RmlsbENvbG9yXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geyAnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCB9O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWNvblN0eWxlOiB7ICdib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycgfSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQnJpbmcgRnJvbnQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZW5kIEJhY2snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFUcmFzaEFsdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBpbWFnZScsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5JTUFHRSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IEJsdXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2JsdXInLFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGFuZ2xlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IEFuZ2xlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGtleU5hbWU6ICdhbmdsZScsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgd2lkdGgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgTGVuZ3RoJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2xlbmd0aCcsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGNvbG9yJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IENvbG9yJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVBlbixcbiAgICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAgIC8vY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICcjMDAwMDAwJyxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGVUZXh0OiAnU0VUIENPTE9SJyxcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uVGV4dDogJ1NFVCdcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvd0NvbG9yXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCxcbiAgICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uU3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsXG4gICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIC8vIE5PVEU6IGZ1bmN0aW9uYWxpdHkgdHVybmVkIG9mZlxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIHdpZHRoJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAvLyAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZVdpZHRoLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgIC8vICAgfVxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBDT0xPUicsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgLy8gICAgIGRpYWxvZzoge1xuICAgICAgICAgIC8vICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgLy8gICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAvLyAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgLy8gICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgIC8vICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFUcmFzaEFsdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBwcm9kdWN0JyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLlBST0RVQ1QsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYmx1cicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBCTFVSJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGtleU5hbWU6ICdibHVyJyxcbiAgICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBhbmdsZScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBBTkdMRScsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAgICBrZXlOYW1lOiAnYW5nbGUnLFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IHdpZHRoJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIExFTkdUSCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICAgIGtleU5hbWU6ICdsZW5ndGgnLFxuICAgICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBjb2xvcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBDT0xPUicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFQZW4sXG4gICAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyMwMDAwMDAnLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25UZXh0OiAnU0VUJ1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93Q29sb3JcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLFxuICAgICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGljb25TdHlsZToge1xuICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJyxcbiAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVRyYXNoQWx0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGdyb3VwJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLkdST1VQLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gTGVmdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkxlZnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uTGVmdCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gSG9yaXpvbnRhbCBDZW50ZXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25DZW50ZXIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gUmlnaHQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25SaWdodCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBUb3AnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl90b3AnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblRvcCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gVmVydGljYWwgQ2VudGVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbk1hdGVyaWFsOiAndmVydGljYWxfYWxpZ25fY2VudGVyJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gQm90dG9tJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbk1hdGVyaWFsOiAndmVydGljYWxfYWxpZ25fYm90dG9tJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Cb3R0b20sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQnJpbmcgRnJvbnQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZW5kIEJhY2snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uR3JvdXAsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH1cbiAgICBdO1xuICB9XG59XG4iXX0=