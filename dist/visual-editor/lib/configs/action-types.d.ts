export declare enum ActionTypes {
    BUTTON = 0,
    TOGGLE = 1,
    DROPDOWN = 2,
    DIALOG = 3,
    UPLOAD = 4,
    LABEL = 5,
    RELATED_INPUT = 6,
    OUTGOING_EVENT_TRIGGER = 7
}
export declare enum ObjectTypes {
    TEXBOX = "textbox",
    IMAGE = "image",
    SHAPE = "shape",
    RECT = "rect",
    CIRCLE = "circle",
    LINE = "line",
    POLYGON = "polygon",
    GROUP = "group",
    PRODUCT = "product"
}
