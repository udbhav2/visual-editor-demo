export declare class SegmentationVisualEditorConfig {
    static readonly colorBackground = "#cb0c93";
    static readonly colorForeground = "#2bc4b6";
    static readonly drawingBrushWidth = 4;
    tools: any[];
    isFirstToolSelected: boolean;
    hasCheckersBg: boolean;
    hasHiddenSelectionControls: boolean;
    hasZoom: boolean;
    hasPan: boolean;
    toolsFlex: string;
    actionsFlex: string;
    constructor();
}
