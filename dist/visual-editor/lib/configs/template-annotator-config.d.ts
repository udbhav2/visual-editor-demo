export declare class TemplateAnnotatorConfig {
    static readonly addBoundingBoxConfigParams: {
        config: {
            left: number;
            top: number;
            height: number;
            width: number;
            strokeWidth: number;
            stroke: string;
            originX: string;
            originY: string;
        };
    };
    static readonly buttonStyles: {
        'font-weight': string;
    };
    tools: any[];
    isFirstToolSelected: boolean;
    hasCheckersBg: boolean;
    hasHiddenSelectionControls: boolean;
    hasZoom: boolean;
    isWideCanvas: boolean;
    toolsFlex: string;
    actionsFlex: string;
    hasPan: boolean;
    resizeCanvasToBackgroundImage: boolean;
    isCenterBackgroundImage: boolean;
    hasShortcutsExpansionPanel: boolean;
    hasCloneShortcut: boolean;
    hasRemoveShortcut: boolean;
    hasRemoveAllShortcut: boolean;
    hasSendBackwardsShortcut: boolean;
    hasBringForwardShortcut: boolean;
    faClone: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faTrash: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faTrashAlt: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faArrowUp: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faArrowDown: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faAlignLeft: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faAlignCenter: import("@fortawesome/fontawesome-common-types").IconDefinition;
    faAlignRight: import("@fortawesome/fontawesome-common-types").IconDefinition;
    constructor();
}
