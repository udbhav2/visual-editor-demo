export declare class TemplateEditorConfig {
    static readonly addRectangleConfigParams: {
        config: {
            left: number;
            top: number;
            height: number;
            width: number;
            strokeWidth: number;
            stroke: string;
            originX: string;
            originY: string;
            fill: string;
        };
    };
    tools: any[];
    isFirstToolSelected: boolean;
    hasCheckersBg: boolean;
    hasHiddenSelectionControls: boolean;
    hasZoom: boolean;
    hasPan: boolean;
    toolsFlex: string;
    actionsFlex: string;
    constructor();
}
