export declare class AdVisualEditorConfig {
    static readonly defaultFontFamily = "Times New Roman";
    static readonly availableFontFamilies: string[];
    static readonly defaultFontSize = 34;
    static readonly availableFontSizes: any;
    static readonly addTextConfigParams: {
        text: string;
        config: {
            left: number;
            top: number;
            fontFamily: string;
            fontSize: number;
            cornerColor: string;
            cornerSize: number;
            transparentCorners: boolean;
        };
    };
    static readonly defaultStrokeWidth = 0;
    static readonly defaultLineStrokeWidth = 5;
    static readonly availableStrokeWidths: any;
    static readonly addImageConfigParams: {
        fileUrl: string;
        scaling: number;
        config: {
            left: number;
            top: number;
            stroke: string;
            strokeWidth: number;
            crossOrigin: string;
        };
    };
    static readonly addRectangleConfigParams: {
        config: {
            left: number;
            top: number;
            height: number;
            width: number;
            strokeWidth: number;
            stroke: string;
        };
    };
    static readonly addLineConfigParams: {
        config: {
            left: number;
            top: number;
            strokeWidth: number;
            stroke: string;
        };
        coords: number[];
    };
    static readonly addCircleConfigParams: {
        config: {
            left: number;
            top: number;
            radius: number;
            strokeWidth: number;
            stroke: string;
        };
    };
    static readonly addTriangleConfigParams: {
        config: {
            left: number;
            top: number;
            strokeWidth: number;
            stroke: string;
        };
        coords: {
            x: number;
            y: number;
        }[];
    };
    tools: any[];
    isFirstToolSelected: boolean;
    hasCheckersBg: boolean;
    hasHiddenSelectionControls: boolean;
    hasZoom: boolean;
    hasPan: boolean;
    isWideCanvas: boolean;
    isShowFrame: boolean;
    toolsFlex: string;
    actionsFlex: string;
    constructor();
}
