export declare class BannerVisualEditorConfig {
    static readonly defaultFontFamily = "Times New Roman";
    static readonly availableFontFamilies: string[];
    static readonly defaultFontSize = 34;
    static readonly availableFontSizes: any;
    static readonly addTextConfigParams: {
        text: string;
        config: {
            left: number;
            top: number;
            fontFamily: string;
            fontSize: number;
            cornerColor: string;
            cornerSize: number;
            transparentCorners: boolean;
        };
    };
    static readonly defaultStrokeWidth = 0;
    static readonly defaultLineStrokeWidth = 5;
    static readonly availableStrokeWidths: any;
    static readonly addImageConfigParams: {
        fileUrl: string;
        scaling: number;
        config: {
            left: number;
            top: number;
            stroke: string;
            strokeWidth: number;
            crossOrigin: string;
        };
    };
    tools: any[];
    isFirstToolSelected: boolean;
    hasCheckersBg: boolean;
    hasHiddenSelectionControls: boolean;
    hasZoom: boolean;
    hasPan: boolean;
    isWideCanvas: boolean;
    isShowFrame: boolean;
    toolsFlex: string;
    actionsFlex: string;
    constructor();
}
