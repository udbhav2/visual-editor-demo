import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
export declare class ColorPickerDialogComponent implements OnInit {
    data: any;
    private colorPickerDialog;
    addAll: boolean;
    color: string;
    titleText: string;
    buttonText: string;
    constructor(data: any, colorPickerDialog: MatDialogRef<ColorPickerDialogComponent>);
    ngOnInit(): void;
    onAdd(addAll: any): void;
    onCancel(): void;
}
