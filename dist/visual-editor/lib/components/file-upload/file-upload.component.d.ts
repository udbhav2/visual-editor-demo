import { ElementRef, EventEmitter } from '@angular/core';
export declare class FileUploadComponent {
    accept: string;
    multiple: boolean;
    selectedFiles: EventEmitter<File[]>;
    selectedFilesWithCaller: EventEmitter<{
        files: File[];
        caller: any;
    }>;
    fileInput: ElementRef;
    caller: any;
    click(caller?: any): void;
    selected(event: any): void;
}
