import 'fabric';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ColorPickerModule } from 'ngx-color-picker';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Injectable, NgModule, Component, EventEmitter, Input, Output, ViewChild, HostListener, Inject, defineInjectable } from '@angular/core';
import { min, each, sortBy, union, map, isUndefined, replace, range } from 'lodash';
import { faClone, faTrashAlt, faTrash, faArrowUp, faArrowDown, faAlignLeft, faAlignCenter, faAlignRight, faFont, faBold, faFillDrip, faPen, faItalic, faUnderline, faRetweet } from '@fortawesome/free-solid-svg-icons';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const ActionTypes = {
    BUTTON: 0,
    TOGGLE: 1,
    DROPDOWN: 2,
    DIALOG: 3,
    UPLOAD: 4,
    LABEL: 5,
    RELATED_INPUT: 6,
    OUTGOING_EVENT_TRIGGER: 7,
};
ActionTypes[ActionTypes.BUTTON] = 'BUTTON';
ActionTypes[ActionTypes.TOGGLE] = 'TOGGLE';
ActionTypes[ActionTypes.DROPDOWN] = 'DROPDOWN';
ActionTypes[ActionTypes.DIALOG] = 'DIALOG';
ActionTypes[ActionTypes.UPLOAD] = 'UPLOAD';
ActionTypes[ActionTypes.LABEL] = 'LABEL';
ActionTypes[ActionTypes.RELATED_INPUT] = 'RELATED_INPUT';
ActionTypes[ActionTypes.OUTGOING_EVENT_TRIGGER] = 'OUTGOING_EVENT_TRIGGER';
/** @enum {string} */
const ObjectTypes = {
    TEXBOX: 'textbox',
    IMAGE: 'image',
    SHAPE: 'shape',
    RECT: 'rect',
    CIRCLE: 'circle',
    LINE: 'line',
    POLYGON: 'polygon',
    GROUP: 'group',
    PRODUCT: 'product',
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class VisualEditorService {
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setColor(canvas, color) {
        canvas.freeDrawingBrush.color = color;
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static clearDrawingTransparentBg(canvas) {
        VisualEditorService.clearDrawing(canvas, 1); // NOTE: first object is the image itself
    }
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    static clearDrawing(canvas, offset = 0) {
        canvas
            .getObjects()
            .slice(offset)
            .forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static undoDrawingTransparentBg(canvas) {
        VisualEditorService.undoDrawing(canvas, 1); // NOTE: first object is the image itself
    }
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    static undoDrawing(canvas, offset = 0) {
        /** @type {?} */
        const objs = canvas.getObjects();
        if (objs.length > offset) {
            canvas.remove(objs[objs.length - 1]);
        }
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addText(canvas, params) {
        const { text, config } = params;
        /** @type {?} */
        const textbox = new fabric.Textbox(text, config);
        canvas.add(textbox);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static bringForward(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.bringForward(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static sendBackwards(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.sendBackwards(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setBold(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontWeight', selectedObject.fontWeight === 'bold' ? 'normal' : 'bold');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setItalic(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontStyle', selectedObject.fontStyle === 'italic' ? 'normal' : 'italic');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setUnderline(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('underline', !selectedObject.underline);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignLeft(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'left');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignCenter(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'center');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignRight(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'right');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} fontFamily
     * @return {?}
     */
    static setFontFamily(canvas, fontFamily) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontFamily', fontFamily);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setFillColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fill', color);
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} canvas
     * @param {?} fontSize
     * @return {?}
     */
    static setFontSize(canvas, fontSize) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontSize', fontSize);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} lineHeight
     * @return {?}
     */
    static setLineHeight(canvas, lineHeight) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('lineHeight', lineHeight);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} characterSpacing
     * @return {?}
     */
    static setCharacterSpacing(canvas, characterSpacing) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('charSpacing', characterSpacing);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addImage(canvas, params) {
        fabric.Image.fromURL(params.fileUrl, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            if (params.scaling) {
                /** @type {?} */
                const scaleFactor = img.height > img.width
                    ? (canvas.height * params.scaling) / img.height
                    : (canvas.width * params.scaling) / img.width;
                img.set({
                    scaleX: scaleFactor,
                    scaleY: scaleFactor
                });
            }
            canvas.add(img);
            canvas.requestRenderAll();
        }), params.config);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} url
     * @param {?} callback
     * @return {?}
     */
    static updateImage(canvas, url, callback) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        const { width, height, scaleX, scaleY } = selectedObject;
        selectedObject.setSrc(url, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            /** @type {?} */
            const scaleFactor = min([
                (width * scaleX) / img.width,
                (height * scaleY) / img.height
            ]);
            img.set({
                scaleX: scaleFactor,
                scaleY: scaleFactor
            });
            canvas.requestRenderAll();
            callback();
        }), { crossOrigin: 'anonymous' });
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setStrokeColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('stroke', color);
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} canvas
     * @param {?} width
     * @return {?}
     */
    static setStrokeWidth(canvas, width) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('strokeWidth', width);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteSelection(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.remove(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteSelectionGroup(canvas) {
        /** @type {?} */
        const selectedObjects = canvas.getActiveObject();
        selectedObjects.forEachObject((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteAllSelections(canvas) {
        canvas.remove(...canvas.getObjects());
    }
    /**
     * @return {?}
     */
    static resetObjectTypeCount() {
        VisualEditorService.objectTypeCount = {
            product: 0,
            primary_logo: 0,
            secondary_logo: 0,
            lockup: 0,
            headline_textbox: 0,
            other_textbox: 0,
            graphic_accent: 0,
            photo_accent: 0,
            button: 0,
            banner: 0,
            sticker: 0,
            frame: 0
        };
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addRectangle(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addBoundingBox(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
        /** @type {?} */
        const objectType = params.additionalProperties.objectType;
        /** @type {?} */
        const objectId = VisualEditorService.createObjectId(objectType);
        VisualEditorService.extendObjectJson(rectangle, objectId, objectType);
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addLine(canvas, params) {
        const { config, coords } = params;
        /** @type {?} */
        const line = new fabric.Line(coords, config);
        canvas.add(line);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addCircle(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const circle = new fabric.Circle(config);
        canvas.add(circle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addTriangle(canvas, params) {
        const { config, coords } = params;
        /** @type {?} */
        const triangle = new fabric.Polygon(coords, config);
        canvas.add(triangle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static cloneObject(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const clone = new fabric.Rect(selectedObject.toObject());
        clone.set({ left: 0, top: 0 });
        canvas.add(clone);
        VisualEditorService.extendObjectJson(clone, selectedObject.toObject()._oId, selectedObject.toObject().objectType);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionLeft(canvas) {
        VisualEditorService.alignSelection(canvas, 'left');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionHorizontalCenter(canvas) {
        VisualEditorService.alignSelection(canvas, 'horizontal-center');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionRight(canvas) {
        VisualEditorService.alignSelection(canvas, 'right');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionTop(canvas) {
        VisualEditorService.alignSelection(canvas, 'top');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionVerticalCenter(canvas) {
        VisualEditorService.alignSelection(canvas, 'vertical-center');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionBottom(canvas) {
        VisualEditorService.alignSelection(canvas, 'bottom');
    }
    /**
     * @param {?} canvas
     * @param {?} direction
     * @return {?}
     */
    static alignSelection(canvas, direction) {
        /** @type {?} */
        const group = canvas.getActiveObject();
        /** @type {?} */
        const groupBoundingRect = group.getBoundingRect(true);
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            /** @type {?} */
            const itemBoundingRect = item.getBoundingRect();
            switch (direction) {
                case 'left': {
                    item.set({
                        left: -groupBoundingRect.width / 2 + (item.left - itemBoundingRect.left)
                    });
                    break;
                }
                case 'horizontal-center': {
                    item.set({
                        left: item.left - itemBoundingRect.left - itemBoundingRect.width / 2
                    });
                    break;
                }
                case 'right': {
                    item.set({
                        left: item.left +
                            (groupBoundingRect.width / 2 - itemBoundingRect.left) -
                            itemBoundingRect.width
                    });
                    break;
                }
                case 'top': {
                    item.set({
                        top: -groupBoundingRect.height / 2 + (item.top - itemBoundingRect.top)
                    });
                    break;
                }
                case 'vertical-center': {
                    item.set({
                        top: item.top - itemBoundingRect.top - itemBoundingRect.height / 2
                    });
                    break;
                }
                case 'bottom': {
                    item.set({
                        top: item.top +
                            (groupBoundingRect.height / 2 - itemBoundingRect.top) -
                            itemBoundingRect.height
                    });
                    break;
                }
            }
        }));
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            group.removeWithUpdate(item).addWithUpdate(item);
        }));
        group.setCoords();
        canvas.requestRenderAll();
    }
    /**
     * @param {?} length
     * @param {?} angle
     * @param {?} pos
     * @return {?}
     */
    static getShadowOffset(length, angle, pos) {
        /** @type {?} */
        let x;
        /** @type {?} */
        let y;
        switch (pos) {
            case 'BOTTOM':
                x = 0;
                y = 10;
                break;
            case 'BOTTOM_LEFT':
                x = -6;
                y = 10;
                break;
            case 'BOTTOM_RIGHT':
                x = 6;
                y = 10;
                break;
            case 'TOP':
                x = 0;
                y = -10;
                break;
            case 'TOP_LEFT':
                x = -6;
                y = -10;
                break;
            case 'TOP_RIGHT':
                x = 6;
                y = -10;
                break;
            default:
                x = 0;
                y = 0;
                break;
        }
        return fabric.util.rotatePoint(new fabric.Point(x * length, y * length), new fabric.Point(0, 0), fabric.util.degreesToRadians(360 - angle));
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static setShadow(canvas, params) {
        const { angle, length, blur } = params;
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const shadowOffset = VisualEditorService.getShadowOffset(length, -angle, 'BOTTOM');
        /** @type {?} */
        const color = selectedObject.shadow && selectedObject.shadow.color
            ? selectedObject.shadow.color
            : 'rgba(0,0,0,0.5)';
        selectedObject.setShadow(Object.assign({}, selectedObject.shadow, { color,
            blur, offsetX: shadowOffset.x, offsetY: shadowOffset.y, affectStroke: false }));
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setShadowColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const alpha = selectedObject.shadow &&
            selectedObject.shadow.color &&
            fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            ? fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            : 0.5;
        /** @type {?} */
        const rgbaColor = fabric.Color.fromHex(color)
            .setAlpha(alpha)
            .toRgba();
        selectedObject.setShadow(Object.assign({}, selectedObject.shadow, { color: rgbaColor, blur: 0.5 }));
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} imgUrl
     * @param {?} canvasObj
     * @return {?}
     */
    static transformCanvas(imgUrl, canvasObj) {
        /** @type {?} */
        const backgroundPlaceholderObject = {
            src: 'background_placeholder',
            top: 300,
            _key: 'background_placeholder',
            fill: 'rgb(0,0,0)',
            left: 300,
            type: 'image',
            angle: 0,
            cropX: 0,
            cropY: 0,
            flipX: false,
            flipY: false,
            skewX: 0,
            skewY: 0,
            width: canvasObj.backgroundImage.width,
            clipTo: null,
            height: canvasObj.backgroundImage.height,
            scaleX: 1,
            scaleY: 1,
            shadow: null,
            stroke: null,
            filters: [],
            opacity: 1,
            originX: 'center',
            originY: 'center',
            version: '2.3.6',
            visible: true,
            fillRule: 'nonzero',
            paintFirst: 'fill',
            crossOrigin: '',
            strokeWidth: 0,
            strokeLineCap: 'butt',
            strokeLineJoin: 'miter',
            backgroundColor: '',
            strokeDashArray: null,
            transformMatrix: null,
            strokeMiterLimit: 4,
            globalCompositeOperation: 'source-over'
        };
        canvasObj.objects.forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => {
            o.stroke = null;
            o.strokeWidth = 0;
            o.type = 'image';
            o.width = o.width * o.scaleX;
            o.height = o.height * o.scaleY;
            o.scaleX = 1;
            o.scaleY = 1;
            o.src = o._key;
            delete o._oId;
            delete o.objectType;
        }));
        canvasObj.objects.unshift(backgroundPlaceholderObject);
        canvasObj.backgroundImage.src = '';
        canvasObj.backgroundImage.crossOrigin = 'anonymous';
        canvasObj._originalImgUrl = imgUrl;
        /** @type {?} */
        const canvasJson = JSON.stringify(canvasObj, null, 2);
        console.log(canvasJson);
        return canvasJson;
    }
    /**
     * @param {?} objectType
     * @return {?}
     */
    static createObjectId(objectType) {
        /** @type {?} */
        const id = VisualEditorService.objectTypeCount[objectType];
        VisualEditorService.objectTypeCount[objectType] += 1;
        return id;
    }
    /**
     * @param {?} obj
     * @param {?} id
     * @param {?} objectType
     * @return {?}
     */
    static extendObjectJson(obj, id, objectType) {
        obj.toObject = ((/**
         * @param {?} toObject
         * @return {?}
         */
        function (toObject) {
            return (/**
             * @return {?}
             */
            function () {
                return fabric.util.object.extend(toObject.call(this), {
                    _oId: id,
                    _key: `${objectType}_placeholder_${id}`,
                    objectType
                });
            });
        }))(obj.toObject);
    }
}
VisualEditorService.objectTypeCount = {
    product: 0,
    primary_logo: 0,
    secondary_logo: 0,
    lockup: 0,
    headline_textbox: 0,
    other_textbox: 0,
    graphic_accent: 0,
    photo_accent: 0,
    button: 0,
    banner: 0,
    sticker: 0,
    frame: 0
};
VisualEditorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ VisualEditorService.ngInjectableDef = defineInjectable({ factory: function VisualEditorService_Factory() { return new VisualEditorService(); }, token: VisualEditorService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class VisualEditorComponent {
    /**
     * @param {?} dialog
     */
    constructor(dialog) {
        this.dialog = dialog;
        this.customFonts = [];
        this.outgoingEventTriggered = new EventEmitter();
        this.outgoingSelectionEventTriggered = new EventEmitter();
        this.ActionTypes = ActionTypes;
        this.VisualEditorService = VisualEditorService;
        this.checkersImageUrl = '@app/../assets/images/checkers_bg2.png';
        this.imageConfig = {
            width: 0,
            height: 0,
            scaleFactor: 1,
            xOffset: 0,
            yOffset: 0
        };
        this.tools = [];
        this.minZoom = 1;
    }
    // Start of ngOnInit function
    /**
     * @return {?}
     */
    ngOnInit() {
        this.addCustomFonts();
        this.loadCustomFonts();
        this.tools = this.visualEditorConfig.tools;
        this.isFirstToolSelected = this.visualEditorConfig.isFirstToolSelected;
        this.hasCheckersBg = this.visualEditorConfig.hasCheckersBg;
        this.hasHiddenSelectionControls = this.visualEditorConfig.hasHiddenSelectionControls;
        this.hasZoom = this.visualEditorConfig.hasZoom;
        this.hasPan = this.visualEditorConfig.hasPan;
        this.isWideCanvas = this.visualEditorConfig.isWideCanvas;
        this.isShowFrame = this.visualEditorConfig.isShowFrame;
        this.resizeCanvasToBackgroundImage = this.visualEditorConfig.resizeCanvasToBackgroundImage;
        this.isCenterBackgroundImage = this.visualEditorConfig.isCenterBackgroundImage;
        this.toolsFlex = this.visualEditorConfig.toolsFlex;
        this.actionsFlex = this.visualEditorConfig.actionsFlex;
        // Keyboard Shortcuts
        this.hasShortcutsExpansionPanel = this.visualEditorConfig.hasShortcutsExpansionPanel;
        this.hasCloneShortcut = this.visualEditorConfig.hasCloneShortcut;
        this.hasRemoveShortcut = this.visualEditorConfig.hasRemoveShortcut;
        this.hasRemoveAllShortcut = this.visualEditorConfig.hasRemoveAllShortcut;
        this.hasSendBackwardsShortcut = this.visualEditorConfig.hasSendBackwardsShortcut;
        this.hasBringForwardShortcut = this.visualEditorConfig.hasBringForwardShortcut;
        // FontAwesome Icons
        this.faClone = this.visualEditorConfig.faClone;
        this.faTrash = this.visualEditorConfig.faTrash;
        this.faTrashAlt = this.visualEditorConfig.faTrashAlt;
        this.faArrowUp = this.visualEditorConfig.faArrowUp;
        this.faArrowDown = this.visualEditorConfig.faArrowDown;
        this.fileService = this.visualEditorConfig.fileService;
        this.s3Service = this.visualEditorConfig.s3Service;
        if (!this.isWideCanvas) {
            this.editedImageCanvas = new fabric.Canvas('editedImageCanvas', {
                isDrawingMode: true
            });
        }
        else {
            this.editedImageCanvas = new fabric.Canvas('wideCanvas', {
                isDrawingMode: true
            });
        }
        this.loadCanvas();
        if (this.hasHiddenSelectionControls) {
            this.editedImageCanvas.on('selection:created', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                this.selectHiddenControls(e.selected);
            }));
            this.editedImageCanvas.on('selection:updated', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                if (this.editedImageCanvas.getActiveObject().isType('activeSelection')) {
                    this.selectHiddenControls(this.editedImageCanvas.getActiveObject().getObjects());
                }
                else {
                    this.selectHiddenControls(e.selected);
                }
            }));
            this.editedImageCanvas.on('selection:cleared', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                this.activeTool = this.tools[0]; // Code changed to render the 'Remove All' and 'Submit' buttons all the time
            }));
        }
        if (this.isFirstToolSelected) {
            this.activeTool = this.tools[0];
            this.activeTool.canvasConfigs.forEach((/**
             * @param {?} confFunc
             * @return {?}
             */
            confFunc => {
                this.editedImageCanvas = confFunc(this.editedImageCanvas);
            }));
        }
        else {
            this.activeTool = {};
        }
        if (this.hasZoom) {
            this.addZoomControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addZoomControls(this.originalImageCanvas);
            }
        }
        if (this.hasPan) {
            this.addPanControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addPanControls(this.originalImageCanvas);
            }
        }
    }
    // End of ngOnInit function
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes.visualJson.isFirstChange()) {
            this.loadCanvas();
        }
    }
    /**
     * @return {?}
     */
    loadCanvas() {
        this.VisualEditorService.resetObjectTypeCount();
        if (this.visualJson) {
            this.loadJson(this.visualJson, this.editedImageCanvas);
            this.editedImageCanvas.requestRenderAll();
        }
        else {
            this.setScaledImageToCanvas(this.editedImageCanvas, this.editedImageUrl, true);
            this.editedImageCanvas.requestRenderAll();
            if (this.originalImageUrl) {
                this.originalImageCanvas = new fabric.StaticCanvas('originalImageCanvas');
                this.setScaledImageToCanvas(this.originalImageCanvas, this.originalImageUrl);
                this.originalImageCanvas.requestRenderAll();
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    addCustomFonts() {
        each(this.visualEditorConfig.tools, (/**
         * @param {?} t
         * @return {?}
         */
        t => {
            each(t.sections, (/**
             * @param {?} s
             * @return {?}
             */
            s => {
                each(s.actions, (/**
                 * @param {?} a
                 * @return {?}
                 */
                a => {
                    if (a.label === 'FONT FAMILY') {
                        a.dropdownOption = sortBy(union(a.dropdownOption, map(this.customFonts, (/**
                         * @param {?} f
                         * @return {?}
                         */
                        f => f.fontFamily))));
                    }
                }));
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    loadCustomFonts() {
        // FIXME: IE support
        /** @type {?} */
        const customFontFaces = map(this.customFonts, (/**
         * @param {?} f
         * @return {?}
         */
        f => new FontFace(f.fontFamily, `url(${f.fontFileUrl})`)));
        each(customFontFaces, (/**
         * @param {?} cff
         * @return {?}
         */
        cff => {
            // NOTE: need to specify a font size for check() function
            if (!((/** @type {?} */ (document))).fonts.check(`12px ${cff.family}`)) {
                cff
                    .load()
                    .then((/**
                 * @param {?} res
                 * @return {?}
                 */
                res => {
                    ((/** @type {?} */ (document))).fonts.add(res);
                }))
                    .catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                error => {
                    // FIXME: Error display
                }));
            }
        }));
    }
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    addZoomControls(canvas) {
        canvas.on('mouse:wheel', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            /** @type {?} */
            const delta = opt.e.deltaY;
            /** @type {?} */
            let zoom = canvas.getZoom();
            zoom = zoom - delta / min([canvas.getWidth(), canvas.getHeight()]);
            if (zoom > 20) {
                zoom = 20;
            }
            if (zoom < this.minZoom) {
                zoom = this.minZoom;
            }
            canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
            opt.e.preventDefault();
            opt.e.stopPropagation();
        }));
    }
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    addPanControls(canvas) {
        canvas.on('mouse:down', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            /** @type {?} */
            const evt = opt.e;
            if (evt.altKey === true) {
                this.isDragging = true;
                canvas.selection = false;
                this.lastPosX = evt.clientX;
                this.lastPosY = evt.clientY;
            }
        }));
        canvas.on('mouse:move', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            if (this.isDragging) {
                /** @type {?} */
                const e = opt.e;
                canvas.viewportTransform[4] += e.clientX - this.lastPosX;
                canvas.viewportTransform[5] += e.clientY - this.lastPosY;
                canvas.forEachObject((/**
                 * @param {?} o
                 * @return {?}
                 */
                o => o.setCoords()));
                canvas.requestRenderAll();
                this.lastPosX = e.clientX;
                this.lastPosY = e.clientY;
            }
        }));
        canvas.on('mouse:up', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            this.isDragging = false;
            canvas.selection = true;
        }));
    }
    // Start of loadJson function
    /**
     * @private
     * @param {?} inputJson
     * @param {?} canvas
     * @return {?}
     */
    loadJson(inputJson, canvas) {
        /** @type {?} */
        const img = new Image();
        img.onload = (/**
         * @return {?}
         */
        () => {
            // Extracting the images width and height and adding it into the _canvasDimensions property in the inputJson
            if (this.resizeCanvasToBackgroundImage) {
                inputJson._canvasDimensions = { width: img.width, height: img.height };
                inputJson.backgroundImage.width = inputJson._canvasDimensions.width;
                inputJson.backgroundImage.height = inputJson._canvasDimensions.height;
            }
            canvas.loadFromJSON(inputJson, (/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const scaleFactor = min([
                    canvas.getWidth() / inputJson._canvasDimensions.width,
                    canvas.getHeight() / inputJson._canvasDimensions.height
                ]);
                this.minZoom = scaleFactor;
                if (!this.isWideCanvas) {
                    canvas.setWidth(inputJson._canvasDimensions.width * scaleFactor);
                    canvas.setHeight(inputJson._canvasDimensions.height * scaleFactor);
                    canvas.setZoom(this.minZoom);
                    /** @type {?} */
                    const isWidthSmaller = inputJson._canvasDimensions.width / canvas.backgroundImage.width >
                        inputJson._canvasDimensions.height / canvas.backgroundImage.height;
                    if (isWidthSmaller) {
                        canvas.backgroundImage.scaleToWidth(inputJson._canvasDimensions.width);
                    }
                    else {
                        canvas.backgroundImage.scaleToHeight(inputJson._canvasDimensions.height);
                    }
                }
                else {
                    /** @type {?} */
                    const padding = 5;
                    /** @type {?} */
                    const border = 0;
                    /** @type {?} */
                    const newWidth = this.wideCanvas.nativeElement.offsetWidth - 2 * (padding + border);
                    /** @type {?} */
                    const newHeight = this.wideCanvas.nativeElement.offsetHeight - 2 * (padding + border);
                    canvas.setWidth(newWidth);
                    canvas.setHeight(newHeight);
                    canvas.setZoom(0.8 *
                        min([
                            canvas.getWidth() / inputJson._canvasDimensions.width,
                            canvas.getHeight() / inputJson._canvasDimensions.height
                        ]));
                    canvas.viewportTransform[4] =
                        (newWidth - inputJson._canvasDimensions.width * canvas.getZoom()) /
                            2;
                    canvas.viewportTransform[5] =
                        (newHeight -
                            inputJson._canvasDimensions.height * canvas.getZoom()) /
                            2;
                    canvas.forEachObject((/**
                     * @param {?} o
                     * @return {?}
                     */
                    o => o.setCoords()));
                }
                if (this.isCenterBackgroundImage) {
                    canvas.viewportCenterObject(canvas.backgroundImage); // Centers the image on the canvas
                }
                if (this.isShowFrame) {
                    /** @type {?} */
                    const strokeWidth = 3;
                    /** @type {?} */
                    const rectangle = new fabric.Rect({
                        left: -strokeWidth,
                        top: -strokeWidth,
                        height: inputJson._canvasDimensions.height + strokeWidth,
                        width: inputJson._canvasDimensions.width + strokeWidth,
                        stroke: 'rgb(0,255,0)',
                        strokeWidth,
                        strokeDashArray: [5 * strokeWidth, 5 * strokeWidth],
                        fill: 'rgba(0,0,0,0)',
                        selectable: false,
                        evented: false
                    });
                    rectangle._key = 'frame_rectangle';
                    canvas.add(rectangle);
                    rectangle.bringToFront();
                }
                canvas.renderAll();
            }));
        });
        img.src = inputJson.backgroundImage.src;
    }
    // End of loadJson function
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    resetCanvas(canvas) {
        canvas.getObjects().forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
    }
    /**
     * @private
     * @param {?} canvas
     * @param {?} imageUrl
     * @param {?=} isEditedImage
     * @return {?}
     */
    setScaledImageToCanvas(canvas, imageUrl, isEditedImage = false) {
        fabric.Image.fromURL(imageUrl, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            /** @type {?} */
            const isWidthSmaller = canvas.getWidth() / img.width > canvas.getHeight() / img.height;
            if (isWidthSmaller) {
                img.scaleToHeight(canvas.getHeight());
            }
            else {
                img.scaleToWidth(canvas.getWidth());
            }
            const { width, height } = img;
            /** @type {?} */
            const left = isWidthSmaller
                ? (canvas.getWidth() - width * img.scaleX) / 2.0
                : 0;
            /** @type {?} */
            const top = isWidthSmaller
                ? 0
                : (canvas.getHeight() - height * img.scaleX) / 2.0;
            img.set({
                left,
                top
            });
            if (isEditedImage) {
                this.imageConfig = {
                    width,
                    height,
                    scaleFactor: img.scaleX,
                    xOffset: left,
                    yOffset: top
                };
            }
            if (this.hasCheckersBg) {
                canvas.add(img);
                canvas.renderAll();
                fabric.Image.fromURL(this.checkersImageUrl, (/**
                 * @param {?} checkerImg
                 * @return {?}
                 */
                checkerImg => {
                    checkerImg.scaleToWidth(canvas.getWidth());
                    canvas.setBackgroundImage(checkerImg, canvas.renderAll.bind(canvas));
                }));
            }
            else {
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            }
        }));
    }
    /**
     * @private
     * @param {?} selectedObjects
     * @return {?}
     */
    selectHiddenControls(selectedObjects) {
        if (selectedObjects.length === 1) {
            /** @type {?} */
            const isShape = selectedObjects[0].isType(ObjectTypes.RECT) ||
                selectedObjects[0].isType(ObjectTypes.CIRCLE) ||
                selectedObjects[0].isType(ObjectTypes.POLYGON);
            /** @type {?} */
            const isImage = selectedObjects[0].isType(ObjectTypes.IMAGE);
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                if (t.isHidden) {
                    if (isShape) {
                        if (t.objectType === ObjectTypes.SHAPE) {
                            this.onSelectTool(t);
                        }
                    }
                    else if (isImage) {
                        if (selectedObjects[0]._key &&
                            selectedObjects[0]._key.includes(ObjectTypes.PRODUCT)) {
                            if (t.objectType === ObjectTypes.PRODUCT) {
                                this.onSelectTool(t);
                            }
                        }
                        else {
                            // Non product image
                            if (selectedObjects[0].isType(t.objectType)) {
                                this.onSelectTool(t);
                            }
                        }
                    }
                    else {
                        // Non shape and non image object
                        if (selectedObjects[0].isType(t.objectType)) {
                            this.onSelectTool(t);
                        }
                    }
                }
            }));
        }
        else if (selectedObjects.length > 1) {
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                if (t.isHidden && t.objectType === ObjectTypes.GROUP) {
                    this.onSelectTool(t);
                }
            }));
        }
    }
    /**
     * @param {?} tool
     * @return {?}
     */
    onSelectTool(tool) {
        if (tool.type === ActionTypes.UPLOAD) {
            this.fileUpload.click();
        }
        else {
            if (!isUndefined(tool.onSelect)) {
                tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                this.activeTool = {};
            }
            if (!this.activeTool || this.activeTool.name !== tool.name) {
                this.activeTool = tool;
                if (tool.canvasConfigs) {
                    tool.canvasConfigs.forEach((/**
                     * @param {?} confFunc
                     * @return {?}
                     */
                    confFunc => {
                        this.editedImageCanvas = confFunc(this.editedImageCanvas);
                    }));
                }
            }
        }
    }
    // Start of private onToolAction(action) function
    /**
     * @param {?} action
     * @return {?}
     */
    onToolAction(action) {
        switch (action.type) {
            case ActionTypes.BUTTON: {
                /** @type {?} */
                let res;
                if (!isUndefined(action.onSelectParams)) {
                    res = action.action(this.editedImageCanvas, action.onSelectParams);
                }
                else {
                    res = action.action(this.editedImageCanvas);
                }
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                if (!isUndefined(res) && !isUndefined(action.onActionReturn)) {
                    action.onActionReturn(action, res);
                }
                break;
            }
            case ActionTypes.TOGGLE: {
                action.isOn = !action.isOn;
                action.action(this.editedImageCanvas, action.isOn ? action.onValue : action.offValue);
                break;
            }
            case ActionTypes.DROPDOWN: {
                action.action(this.editedImageCanvas, action.dropdownSelectedOption);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.RELATED_INPUT: {
                /** @type {?} */
                const relatedValues = {};
                this.activeTool.sections.forEach((/**
                 * @param {?} s
                 * @return {?}
                 */
                s => {
                    s.actions.forEach((/**
                     * @param {?} a
                     * @return {?}
                     */
                    a => {
                        if (a.type === ActionTypes.RELATED_INPUT &&
                            a.groupName === action.groupName &&
                            a.label !== action.label) {
                            relatedValues[a.keyName] = a.inputValue;
                        }
                    }));
                }));
                relatedValues[action.keyName] = action.inputValue;
                action.action(this.editedImageCanvas, relatedValues);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.DIALOG: {
                if (action.dialog) {
                    /** @type {?} */
                    const dialogRef = this.dialog.open(action.dialog.component, {
                        autoFocus: false,
                        data: action.dialog.data,
                        minWidth: '300px',
                        panelClass: 'app-dialog'
                    });
                    dialogRef.afterClosed().subscribe((/**
                     * @param {?} resDialog
                     * @return {?}
                     */
                    resDialog => {
                        if (!isUndefined(resDialog)) {
                            /** @type {?} */
                            const resAction = action.dialog.onClose(this.editedImageCanvas, resDialog);
                            if (!isUndefined(resAction) &&
                                !isUndefined(action.onActionReturn)) {
                                action.onActionReturn(action, resAction);
                            }
                        }
                    }));
                }
                break;
            }
            case ActionTypes.OUTGOING_EVENT_TRIGGER: {
                this.outgoingEventTriggered.emit({
                    canvas: this.editedImageCanvas,
                    transformedCanvas: action.label === 'Submit'
                        ? action.action(this.visualJson.backgroundImage.src, this.editedImageCanvas.toJSON(['_canvasDimensions']))
                        : ''
                });
                break;
            }
            default: {
                action.action(this.editedImageCanvas);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
        }
    }
    // End of private onToolAction(action) function
    /**
     * @param {?} imgUrl
     * @return {?}
     */
    onUpdate(imgUrl) {
        this.resetCanvas(this.editedImageCanvas);
        this.setScaledImageToCanvas(this.editedImageCanvas, imgUrl, true);
    }
    /**
     * @return {?}
     */
    getCanvasAndConfig() {
        return {
            canvas: this.editedImageCanvas,
            imageConfig: this.imageConfig
        };
    }
    /**
     * @param {?} res
     * @return {?}
     */
    onFileSelect(res) {
        const { files, caller: tool } = res;
        if (!isUndefined(tool)) {
            if (!isUndefined(tool.onSelect) && files.length > 0) {
                /** @type {?} */
                const file = files[0];
                /** @type {?} */
                const filename = replace(file.name, /[^A-Z0-9.]+/gi, '_');
                this.fileService
                    .getUploadUrl({
                    filename,
                    contentType: file.type,
                    expiresInSecs: 3600
                })
                    .subscribe((/**
                 * @param {?} respUploadUrl
                 * @return {?}
                 */
                respUploadUrl => {
                    this.s3Service
                        .upload(respUploadUrl.url, file, file.type)
                        .subscribe((/**
                     * @param {?} respUpload
                     * @return {?}
                     */
                    respUpload => {
                        /** @type {?} */
                        const fileUrl = respUploadUrl.url.split('?')[0];
                        tool.onSelectParams.fileUrl = fileUrl;
                        tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                        this.activeTool = {};
                    }));
                }));
            }
        }
    }
    // Keyboard Shortcuts
    /**
     * @param {?} event
     * @return {?}
     */
    canvasKeyboardEvent(event) {
        if (this.hasCloneShortcut &&
            ((event.ctrlKey || event.metaKey) &&
                event.shiftKey &&
                event.keyCode === 86)) {
            this.VisualEditorService.cloneObject(this.editedImageCanvas);
        }
        if (this.hasRemoveShortcut && (!event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteSelection(this.editedImageCanvas);
        }
        if (this.hasRemoveAllShortcut && (event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteAllSelections(this.editedImageCanvas);
        }
        if (this.hasSendBackwardsShortcut &&
            (event.shiftKey && event.keyCode === 40)) {
            this.VisualEditorService.sendBackwards(this.editedImageCanvas);
        }
        if (this.hasBringForwardShortcut &&
            (event.shiftKey && event.keyCode === 38)) {
            this.VisualEditorService.bringForward(this.editedImageCanvas);
        }
    }
}
VisualEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-visual-editor',
                template: "<lib-file-upload #fileUpload\n[accept]=\"'image/png, image/jpg, image/gif'\"\n[multiple]=\"false\"\n(selectedFilesWithCaller)=\"onFileSelect($event)\"></lib-file-upload>\n\n<!-- Template starts from here -->\n<div fxFill fxLayout=\"row\" fxLayoutGap='2%' fxLayoutAlign=\"space-between stretch\" (keydown)=\"canvasKeyboardEvent($event)\">\n\n  <!-- Start of LHS Container -->\n  <div [fxFlex]=\"toolsFlex\" class=\"side-panel left\" fxLayout=\"column\" fxLayoutAlign=\"center stretch\" fxLayoutGap='7%'>\n      <div *ngFor=\"let tool of tools\"  fxLayoutAlign=\"center\"> \n          \n          <button *ngIf=\"!tool.isHidden && tool.type !== ActionTypes.UPLOAD\"\n               [ngStyle]='tool.style'\n                mat-flat-button\n                color=\"primary\"\n                class='button-width-lhs button-sharp button-font-size'\n                (click)=\"onSelectTool(tool)\">\n            <img *ngIf='tool.iconImgUrl'class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n          <button *ngIf=\"!tool.isHidden && tool.type === ActionTypes.UPLOAD\"\n                mat-flat-button\n                fxLayoutAlign=\"center\"\n                color=\"primary\"\n                class=\"button-width-lhs button-sharp button-font-size\"\n                (click)=\"fileUpload.click(tool)\">\n            <img *ngIf='tool.iconImgUrl' class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n      </div> \n    </div> \n  <!-- /End LHS Container -->\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!originalImageUrl || isWideCanvas\">\n    <canvas id=\"originalImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"isWideCanvas\">\n    <canvas id=\"editedImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div class=\"wideCanvasContainer\" #wideCanvas fxFlex=\"1 0 auto\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!isWideCanvas\">\n    <canvas id=\"wideCanvas\">\n    </canvas>\n  </div>\n\n  <!-- Start of RHS Container -->\n  <div [fxFlex]=\"actionsFlex\" class=\"side-panel right\" fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap='5%'>\n      <div fxLayout=\"row\" fxLayoutAlign=\"center stretch\">\n          <mat-expansion-panel *ngIf='hasShortcutsExpansionPanel' class='expansion-panel-spacing-bottom'>\n              <mat-expansion-panel-header>\n                <mat-panel-description class='expansion-panel-header-font-size'>Keyboard shortcuts</mat-panel-description>\n              </mat-expansion-panel-header>\n                    <mat-list role=\"list\">\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasCloneShortcut'><fa-icon [icon]='faClone' class='action-icon-spacing'></fa-icon>Clone - Ctrl/Cmd + Shift + V</mat-list-item>\n                      <mat-divider *ngIf='hasCloneShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveShortcut'><fa-icon [icon]='faTrashAlt' class='action-icon-spacing'></fa-icon>Remove - Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveAllShortcut'><fa-icon [icon]='faTrash' class='action-icon-spacing'></fa-icon>Remove All - Shift + Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveAllShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasBringForwardShortcut'><fa-icon [icon]='faArrowUp' class='action-icon-spacing'></fa-icon>Bring Forward - Shift + Up Arrow</mat-list-item>\n                      <mat-divider *ngIf='hasBringForwardShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasSendBackwardsShortcut'><fa-icon [icon]='faArrowDown' class='action-icon-spacing'></fa-icon>Send Backwards - Shift + Down Arrow</mat-list-item>\n                    </mat-list>\n            </mat-expansion-panel>\n      </div>\n\n      <div *ngFor=\"let section of activeTool.sections\" [fxFlex]=\"section.flex\" fxLayout=\"row\" fxLayoutAlign=\"center stretch\" class='action-section' [class.end-section]='section.isEndSection' fxLayoutGap='2%'>\n        \n        <div *ngFor=\"let action of section.actions\" [fxFlex]=\"action.flex\"> \n\n          <div [ngSwitch]=\"action.type\">\n\n            <div *ngSwitchCase=\"ActionTypes.BUTTON\">\n              <button fxFill mat-stroked-button\n                      [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                      matTooltipPosition='above'\n                      color=\"primary\"\n                      class='button-sharp button-font-size'\n                      [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                      (click)=\"onToolAction(action)\">\n                      <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                      <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                      <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                      <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n              </button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.OUTGOING_EVENT_TRIGGER\">\n              <button fxFill mat-flat-button\n              color=\"primary\"\n              class='button-sharp button-font-size'\n              (click)=\"onToolAction(action)\">{{ action.label }}</button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.DIALOG\">\n                <button fxFill mat-stroked-button\n                        [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                        matTooltipPosition='above'\n                        color=\"primary\"\n                        class=\"button-sharp button-font-size\"\n                        [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                        (click)=\"onToolAction(action)\">\n                  <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                  <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                  <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                  <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n                </button>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.DROPDOWN\">\n                <mat-form-field fxFill>\n                  <mat-label>{{ action.label }}</mat-label>\n                  <mat-select [(value)]=\"action.dropdownSelectedOption\" (selectionChange)=\"onToolAction(action)\">\n                    <mat-option *ngFor=\"let value of action.dropdownOption\" [value]=\"value\">\n                      {{ value }}\n                    </mat-option>\n                  </mat-select>\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.RELATED_INPUT\">\n                <mat-form-field floatLabel=\"never\" class=\"editor-input-field\">\n                  <input matInput\n                         [(ngModel)]=\"action.inputValue\"\n                         (change)=\"onToolAction(action)\"\n                         [type]=\"action.inputType\">\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.TOGGLE\">\n                <div>\n                  {{ action.isOn ? action.onLabel : action.offLabel}}\n                </div>\n                <mat-slide-toggle (change)=\"onToolAction(action)\" class=\"visual-editor-toggle\"></mat-slide-toggle>\n              </div>\n  \n              <div *ngSwitchDefault>\n                <button fxFill mat-button\n                        color=\"primary\"\n                        class=\"primary-button\"\n                        (click)=\"onToolAction(action)\">\n                  <i [class]=\"action.iconClass + ' tool-icon'\"></i>\n                </button>\n              </div>\n            \n          </div> <!-- End of [ngSwitch]=\"action.type\" div -->\n          \n        </div> <!-- End of *ngFor=\"let action of section.actions\" div -->\n      </div> <!-- End of *ngFor=\"let section of activeTool.sections\" div-->\n  </div>\n  <!-- /End of RHS Container  -->\n</div> \n\n\n",
                styles: ["div.canvas-invisible{display:none!important}.button-sharp{border-radius:0}.button-font-size{font-size:.8vw}.button-width-lhs{width:8vw}.button-width-rhs{width:6vw}.action-icon{max-width:40px;max-height:40px}.action-icon-spacing{margin-right:3px}.canvas{border:1px solid #d5d5d5}.wideCanvasContainer{padding:5px;background-image:url(/assets/images/checkers_bg2.png)}.expansion-panel-header-font-size{font-size:.9vw}.expansion-panel-spacing-bottom{margin-bottom:15vh}.list-item-font-size{font-size:.7vw}.side-panel{padding:0}.side-panel.left{border-right:1px solid #eee}.side-panel.right{border-left:1px solid #eee}"]
            }] }
];
/** @nocollapse */
VisualEditorComponent.ctorParameters = () => [
    { type: MatDialog }
];
VisualEditorComponent.propDecorators = {
    originalImageUrl: [{ type: Input }],
    editedImageUrl: [{ type: Input }],
    visualJson: [{ type: Input }],
    visualEditorConfig: [{ type: Input }],
    customFonts: [{ type: Input }],
    outgoingEventTriggered: [{ type: Output }],
    outgoingSelectionEventTriggered: [{ type: Output }],
    wideCanvas: [{ type: ViewChild, args: ['wideCanvas',] }],
    canvasKeyboardEvent: [{ type: HostListener, args: ['window:keydown', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ColorPickerDialogComponent {
    /**
     * @param {?} data
     * @param {?} colorPickerDialog
     */
    constructor(data, colorPickerDialog) {
        this.data = data;
        this.colorPickerDialog = colorPickerDialog;
        this.addAll = false;
        this.color = '#ffffff';
        this.titleText = 'ADD COLOR';
        this.buttonText = 'Add';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.data.color) {
            this.color = this.data.color;
        }
        if (this.data.titleText) {
            this.titleText = this.data.titleText;
        }
        if (this.data.buttonText) {
            this.buttonText = this.data.buttonText;
        }
        if (this.data.addAll) {
            this.addAll = this.data.addAll;
        }
    }
    /**
     * @param {?} addAll
     * @return {?}
     */
    onAdd(addAll) {
        this.colorPickerDialog.close({
            addAll,
            colorHex: this.color
        });
    }
    /**
     * @return {?}
     */
    onCancel() {
        this.colorPickerDialog.close();
    }
}
ColorPickerDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-color-picker-dialog',
                template: "<div>\n    <div mat-dialog-title>\n      <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n        <div class=\"subtitle\">{{titleText}}</div>\n        <i class=\"fas fa-times fa-xs clickable\" (click)=\"onCancel()\"></i>\n      </div>\n      <mat-divider></mat-divider>\n    </div>\n    <div mat-dialog-content class=\"dialog-content\">\n      <div fxLayout=\"row\" fxLayoutAlign=\"center\">\n        <span [cpToggle]=\"true\"\n              [cpDialogDisplay]=\"'inline'\"\n              [cpOutputFormat]=\"'hex'\"\n              [(colorPicker)]=\"color\"></span>\n      </div>\n    </div>\n    <mat-dialog-actions>\n      <div fxFill fxLayout=\"row\"\n           fxLayoutAlign=\"end center\"\n           fxLayoutGap=\"5px\">\n        <button *ngIf=\"addAll\"\n                mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                [disableRipple]=\"true\"\n                (click)=\"onAdd(true)\">Add to all</button>\n        <button mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                (click)=\"onAdd(false)\">Add</button>\n      </div>\n    </mat-dialog-actions>\n  </div>\n  \n",
                styles: [""]
            }] }
];
/** @nocollapse */
ColorPickerDialogComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: MatDialogRef }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FileUploadComponent {
    constructor() {
        this.multiple = false;
        this.selectedFiles = new EventEmitter();
        this.selectedFilesWithCaller = new EventEmitter();
    }
    /**
     * @param {?=} caller
     * @return {?}
     */
    click(caller) {
        if (caller) {
            this.caller = caller;
        }
        this.fileInput.nativeElement.click();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    selected(event) {
        /** @type {?} */
        const files = event.target.files;
        if (this.caller) {
            this.selectedFilesWithCaller.emit({ files, caller: this.caller });
        }
        else {
            this.selectedFiles.emit(files);
        }
    }
}
FileUploadComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-file-upload',
                template: "<input #fileInput\n       accept=\"{{accept}}\"\n       (change)=\"selected($event)\"\n       hidden\n       [multiple]=\"multiple\"\n       type=\"file\">\n",
                styles: [""]
            }] }
];
FileUploadComponent.propDecorators = {
    accept: [{ type: Input }],
    multiple: [{ type: Input }],
    selectedFiles: [{ type: Output }],
    selectedFilesWithCaller: [{ type: Output }],
    fileInput: [{ type: ViewChild, args: ['fileInput',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class VisualEditorModule {
}
VisualEditorModule.decorators = [
    { type: NgModule, args: [{
                declarations: [VisualEditorComponent, ColorPickerDialogComponent, FileUploadComponent],
                imports: [
                    CommonModule,
                    FlexLayoutModule,
                    FormsModule,
                    FontAwesomeModule,
                    ColorPickerModule,
                    MatSelectModule,
                    MatSlideToggleModule,
                    MatDialogModule,
                    MatButtonModule,
                    MatTooltipModule,
                    MatCardModule,
                    MatDividerModule,
                    MatListModule,
                    MatExpansionModule,
                    MatIconModule
                ],
                entryComponents: [ColorPickerDialogComponent],
                exports: [VisualEditorComponent, ColorPickerDialogComponent, FileUploadComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TemplateAnnotatorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = false;
        this.isWideCanvas = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasPan = false;
        this.resizeCanvasToBackgroundImage = true;
        this.isCenterBackgroundImage = true;
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = true;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasSendBackwardsShortcut = true;
        this.hasBringForwardShortcut = true;
        // FontAwesome Icons
        this.faClone = faClone;
        this.faTrash = faTrash;
        this.faTrashAlt = faTrashAlt;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.faAlignLeft = faAlignLeft;
        this.faAlignCenter = faAlignCenter;
        this.faAlignRight = faAlignRight;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Product',
                isHidden: false,
                iconDisplayText: 'Product',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#3cb44b', fill: '#3cb44b' + '88' }),
                    additionalProperties: {
                        objectType: 'product'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#3cb44b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Primary Logo',
                isHidden: false,
                iconDisplayText: 'Primary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6194b', fill: '#e6194b' + '88' }),
                    additionalProperties: {
                        objectType: 'primary_logo'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6194b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Secondary Logo',
                isHidden: false,
                iconDisplayText: 'Secondary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f032e6', fill: '#f032e6' + '88' }),
                    additionalProperties: {
                        objectType: 'secondary_logo'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f032e6' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Lockup',
                isHidden: false,
                iconDisplayText: 'Lockup',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#bcf60c', fill: '#bcf60c' + '88' }),
                    additionalProperties: {
                        objectType: 'lockup'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#bcf60c', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Headline Textbox',
                isHidden: false,
                iconDisplayText: 'Headline Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#4363d8', fill: '#4363d8' + '88' }),
                    additionalProperties: {
                        objectType: 'headline_textbox'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#4363d8' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Other Textbox',
                isHidden: false,
                iconDisplayText: 'Other Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f58231', fill: '#f58231' + '88' }),
                    additionalProperties: {
                        objectType: 'other_textbox'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f58231' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Graphic Accent',
                isHidden: false,
                iconDisplayText: 'Graphic Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#000075', fill: '#000075' + '88' }),
                    additionalProperties: {
                        objectType: 'graphic_accent'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#000075' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Photo Accent',
                isHidden: false,
                iconDisplayText: 'Photo Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#46f0f0', fill: '#46f0f0' + '88' }),
                    additionalProperties: {
                        objectType: 'photo_accent'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#46f0f0' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Button',
                isHidden: false,
                iconDisplayText: 'Button',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#008080', fill: '#008080' + '88' }),
                    additionalProperties: {
                        objectType: 'button'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#008080' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Banner',
                isHidden: false,
                iconDisplayText: 'Banner',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#ffe119', fill: '#ffe119' + '88' }),
                    additionalProperties: {
                        objectType: 'banner'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#ffe119', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Sticker',
                isHidden: false,
                iconDisplayText: 'Sticker',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#fabebe', fill: '#fabebe' + '88' }),
                    additionalProperties: {
                        objectType: 'sticker'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#fabebe' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Frame',
                isHidden: false,
                iconDisplayText: 'Frame',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6beff', fill: '#e6beff' + '88' }),
                    additionalProperties: {
                        objectType: 'frame'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6beff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Clone',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faClone,
                                action: VisualEditorService.cloneObject,
                                flex: '33%'
                            },
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
}
TemplateAnnotatorConfig.addBoundingBoxConfigParams = {
    config: {
        left: 60,
        top: 60,
        height: 100,
        width: 100,
        strokeWidth: 2,
        stroke: 'black',
        originX: 'center',
        originY: 'center'
    }
};
TemplateAnnotatorConfig.buttonStyles = { 'font-weight': 'bold' };

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AdVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add shape',
                iconImgUrl: '@app/../assets/images/add-shape-icon.png',
                iconDisplayText: 'ADD SHAPE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ADD RECTANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-rectangle-icon.png',
                                action: VisualEditorService.addRectangle,
                                onSelectParams: AdVisualEditorConfig.addRectangleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Rectangle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD LINE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-line-icon.png',
                                action: VisualEditorService.addLine,
                                onSelectParams: AdVisualEditorConfig.addLineConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Line',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD ELLIPSE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-ellipse-icon.png',
                                action: VisualEditorService.addCircle,
                                onSelectParams: AdVisualEditorConfig.addCircleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Ellipse',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD TRIANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-triangle-icon.png',
                                action: VisualEditorService.addTriangle,
                                onSelectParams: AdVisualEditorConfig.addTriangleConfigParams,
                                flex: '33%'
                            },
                            {
                                label: 'Triangle',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-fill-drip',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultLineStrokeWidth,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: AdVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: AdVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'CHANGE PRODUCT',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                iconClass: 'fas fa-retweet',
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Change product',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
AdVisualEditorConfig.defaultFontFamily = 'Times New Roman';
AdVisualEditorConfig.availableFontFamilies = [
    AdVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
AdVisualEditorConfig.defaultFontSize = 34;
AdVisualEditorConfig.availableFontSizes = range(8, 50 + 1);
AdVisualEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: AdVisualEditorConfig.defaultFontFamily,
        fontSize: AdVisualEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
AdVisualEditorConfig.defaultStrokeWidth = 0;
AdVisualEditorConfig.defaultLineStrokeWidth = 5;
AdVisualEditorConfig.availableStrokeWidths = range(0, 50 + 1);
AdVisualEditorConfig.addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: { left: 10, top: 10, stroke: '#000000', strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
};
AdVisualEditorConfig.addRectangleConfigParams = {
    config: { left: 10, top: 10, height: 100, width: 100, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
};
AdVisualEditorConfig.addLineConfigParams = {
    config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultLineStrokeWidth, stroke: 'black' },
    coords: [0, 0, 100, 100]
};
AdVisualEditorConfig.addCircleConfigParams = {
    config: { left: 10, top: 10, radius: 50, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
};
AdVisualEditorConfig.addTriangleConfigParams = {
    config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' },
    coords: [{ x: 50, y: 0 }, { x: 0, y: 86 }, { x: 100, y: 86 }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class BannerVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: BannerVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: BannerVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
BannerVisualEditorConfig.defaultFontFamily = 'Times New Roman';
BannerVisualEditorConfig.availableFontFamilies = [
    BannerVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'mc-extra-lt',
    'mc-bold',
    'mc-book',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
BannerVisualEditorConfig.defaultFontSize = 34;
BannerVisualEditorConfig.availableFontSizes = range(8, 50 + 1);
BannerVisualEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: BannerVisualEditorConfig.defaultFontFamily,
        fontSize: BannerVisualEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
BannerVisualEditorConfig.defaultStrokeWidth = 0;
BannerVisualEditorConfig.defaultLineStrokeWidth = 5;
BannerVisualEditorConfig.availableStrokeWidths = range(0, 50 + 1);
BannerVisualEditorConfig.addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: { left: 10, top: 10, stroke: '#000000', strokeWidth: BannerVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SegmentationVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = false;
        this.hasZoom = false;
        this.hasPan = false;
        this.toolsFlex = '0 1 10%';
        this.actionsFlex = '0 1 10%';
        this.tools = [
            {
                name: 'draw',
                iconImgUrl: '@app/../assets/images/target_icon.png',
                iconDisplayText: 'POINTER',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: true });
                        canvas.freeDrawingBrush.color = SegmentationVisualEditorConfig.colorBackground;
                        canvas.freeDrawingBrush.width = SegmentationVisualEditorConfig.drawingBrushWidth;
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'TOGGLE',
                                type: ActionTypes.TOGGLE,
                                action: VisualEditorService.setColor,
                                isOn: false,
                                onValue: SegmentationVisualEditorConfig.colorForeground,
                                offValue: SegmentationVisualEditorConfig.colorBackground,
                                onLabel: 'Foreground',
                                offLabel: 'Background',
                                flex: '100%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'UNDO',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-undo-alt',
                                action: VisualEditorService.undoDrawingTransparentBg,
                                flex: '50%'
                            },
                            {
                                label: 'CLEAR',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.clearDrawingTransparentBg,
                                flex: '50%'
                            }
                        ] }
                ],
            }
        ];
    }
}
SegmentationVisualEditorConfig.colorBackground = '#cb0c93';
SegmentationVisualEditorConfig.colorForeground = '#2bc4b6';
SegmentationVisualEditorConfig.drawingBrushWidth = 4;

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TemplateEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = false;
        this.hasPan = false;
        this.toolsFlex = '0 1 20%';
        this.actionsFlex = '0 1 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add placeholder',
                iconImgUrl: '@app/../assets/images/add-shape-icon.png',
                iconDisplayText: 'ADD PLACEHOLDER',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ADD PRODUCT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#5c59f0' },
                                action: VisualEditorService.addRectangle,
                                onSelectParams: { config: Object.assign({}, TemplateEditorConfig.addRectangleConfigParams.config, { stroke: '#5c59f0' }) },
                                flex: '33%'
                            },
                            {
                                label: 'Product',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD ACCENT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#2bc4b6' },
                                action: VisualEditorService.addRectangle,
                                onSelectParams: { config: Object.assign({}, TemplateEditorConfig.addRectangleConfigParams.config, { stroke: '#2bc4b6' }) },
                                flex: '33%'
                            },
                            {
                                label: 'Accent',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'SET PRODUCT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#5c59f0' },
                                action: VisualEditorService.setStrokeColor,
                                onSelectParams: '#5c59f0',
                                flex: '33%'
                            },
                            {
                                label: 'Product',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'SET ACCENT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#2bc4b6' },
                                action: VisualEditorService.setStrokeColor,
                                onSelectParams: '#2bc4b6',
                                flex: '33%'
                            },
                            {
                                label: 'Accent',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '33%'
                            }
                        ] }
                ],
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
TemplateEditorConfig.addRectangleConfigParams = {
    config: {
        left: 60, top: 60,
        height: 100, width: 100,
        strokeWidth: 2, stroke: 'black',
        originX: 'center', originY: 'center',
        fill: 'rgba(0,0,0,0)'
    }
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DesignEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = false;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasBringForwardShortcut = true;
        this.hasSendBackwardsShortcut = true;
        // FontAwesome Icons
        this.faTrashAlt = faTrashAlt;
        this.faFont = faFont;
        this.faBold = faBold;
        this.faAlignRight = faAlignRight;
        this.faFillDrip = faFillDrip;
        this.faPen = faPen;
        this.faAlignLeft = faAlignLeft;
        this.faItalic = faItalic;
        this.faUnderline = faUnderline;
        this.faAlignCenter = faAlignCenter;
        this.faRetweet = faRetweet;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faFillDrip,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineStrokeWidth,
                                iconClass: faFont,
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Font Family',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontFamily,
                                iconClass: faFont,
                                action: VisualEditorService.setFontFamily,
                                flex: '0 1 75%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Font Size',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontSizes,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontSize,
                                iconClass: faFont,
                                action: VisualEditorService.setFontSize,
                                flex: '0 1 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Line Height',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableLineHeights,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineHeight,
                                action: VisualEditorService.setLineHeight,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Character Spacing',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableCharSpacings,
                                dropdownSelectedOption: DesignEditorConfig.defaultCharSpacing,
                                action: VisualEditorService.setCharacterSpacing,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bold',
                                type: ActionTypes.BUTTON,
                                iconClass: faBold,
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Italic',
                                type: ActionTypes.BUTTON,
                                iconClass: faItalic,
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Underline',
                                type: ActionTypes.BUTTON,
                                iconClass: faUnderline,
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Font Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faFont,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Blur',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Angle',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Length',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'Shadow Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
}
DesignEditorConfig.defaultFontFamily = 'Times New Roman';
DesignEditorConfig.availableFontFamilies = [
    DesignEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
DesignEditorConfig.defaultFontSize = 34;
DesignEditorConfig.availableFontSizes = range(8, 50 + 1);
DesignEditorConfig.defaultLineHeight = 1;
DesignEditorConfig.availableLineHeights = range(0, 11, 0.5);
DesignEditorConfig.defaultCharSpacing = 0;
DesignEditorConfig.availableCharSpacings = range(0, 801, 100);
DesignEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: DesignEditorConfig.defaultFontFamily,
        fontSize: DesignEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
DesignEditorConfig.defaultStrokeWidth = 0;
DesignEditorConfig.defaultLineStrokeWidth = 5;
DesignEditorConfig.availableStrokeWidths = range(0, 50 + 1);

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { VisualEditorComponent, VisualEditorModule, VisualEditorService, FileUploadComponent, ColorPickerDialogComponent, ActionTypes, ObjectTypes, TemplateAnnotatorConfig, AdVisualEditorConfig, BannerVisualEditorConfig, SegmentationVisualEditorConfig, TemplateEditorConfig, DesignEditorConfig };

//# sourceMappingURL=visual-editor.js.map