/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
export class FileUploadComponent {
    constructor() {
        this.multiple = false;
        this.selectedFiles = new EventEmitter();
        this.selectedFilesWithCaller = new EventEmitter();
    }
    /**
     * @param {?=} caller
     * @return {?}
     */
    click(caller) {
        if (caller) {
            this.caller = caller;
        }
        this.fileInput.nativeElement.click();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    selected(event) {
        /** @type {?} */
        const files = event.target.files;
        if (this.caller) {
            this.selectedFilesWithCaller.emit({ files, caller: this.caller });
        }
        else {
            this.selectedFiles.emit(files);
        }
    }
}
FileUploadComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-file-upload',
                template: "<input #fileInput\n       accept=\"{{accept}}\"\n       (change)=\"selected($event)\"\n       hidden\n       [multiple]=\"multiple\"\n       type=\"file\">\n",
                styles: [""]
            }] }
];
FileUploadComponent.propDecorators = {
    accept: [{ type: Input }],
    multiple: [{ type: Input }],
    selectedFiles: [{ type: Output }],
    selectedFilesWithCaller: [{ type: Output }],
    fileInput: [{ type: ViewChild, args: ['fileInput',] }]
};
if (false) {
    /** @type {?} */
    FileUploadComponent.prototype.accept;
    /** @type {?} */
    FileUploadComponent.prototype.multiple;
    /** @type {?} */
    FileUploadComponent.prototype.selectedFiles;
    /** @type {?} */
    FileUploadComponent.prototype.selectedFilesWithCaller;
    /** @type {?} */
    FileUploadComponent.prototype.fileInput;
    /** @type {?} */
    FileUploadComponent.prototype.caller;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11cGxvYWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2ZpbGUtdXBsb2FkL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBTzlGLE1BQU0sT0FBTyxtQkFBbUI7SUFMaEM7UUFPVyxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUMzQyw0QkFBdUIsR0FBRyxJQUFJLFlBQVksRUFBZ0MsQ0FBQztJQXFCdkYsQ0FBQzs7Ozs7SUFmQyxLQUFLLENBQUMsTUFBTztRQUNYLElBQUksTUFBTSxFQUFFO1lBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7U0FDdEI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFLOztjQUNOLEtBQUssR0FBVyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7UUFDeEMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxFQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBQyxDQUFDLENBQUM7U0FDakU7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQzs7O1lBN0JGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQix5S0FBMkM7O2FBRTVDOzs7cUJBRUUsS0FBSzt1QkFDTCxLQUFLOzRCQUNMLE1BQU07c0NBQ04sTUFBTTt3QkFFTixTQUFTLFNBQUMsV0FBVzs7OztJQUx0QixxQ0FBd0I7O0lBQ3hCLHVDQUEwQjs7SUFDMUIsNENBQXFEOztJQUNyRCxzREFBcUY7O0lBRXJGLHdDQUE4Qzs7SUFFOUMscUNBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1maWxlLXVwbG9hZCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9maWxlLXVwbG9hZC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2ZpbGUtdXBsb2FkLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZENvbXBvbmVudCB7XG4gIEBJbnB1dCgpIGFjY2VwdDogc3RyaW5nO1xuICBASW5wdXQoKSBtdWx0aXBsZSA9IGZhbHNlO1xuICBAT3V0cHV0KCkgc2VsZWN0ZWRGaWxlcyA9IG5ldyBFdmVudEVtaXR0ZXI8RmlsZVtdPigpO1xuICBAT3V0cHV0KCkgc2VsZWN0ZWRGaWxlc1dpdGhDYWxsZXIgPSBuZXcgRXZlbnRFbWl0dGVyPHtmaWxlczogRmlsZVtdLCBjYWxsZXI6IGFueX0+KCk7XG5cbiAgQFZpZXdDaGlsZCgnZmlsZUlucHV0JykgZmlsZUlucHV0OiBFbGVtZW50UmVmO1xuXG4gIGNhbGxlcjogYW55O1xuXG4gIGNsaWNrKGNhbGxlcj8pIHtcbiAgICBpZiAoY2FsbGVyKSB7XG4gICAgICB0aGlzLmNhbGxlciA9IGNhbGxlcjtcbiAgICB9XG4gICAgdGhpcy5maWxlSW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xuICB9XG5cbiAgc2VsZWN0ZWQoZXZlbnQpIHtcbiAgICBjb25zdCBmaWxlczogRmlsZVtdID0gZXZlbnQudGFyZ2V0LmZpbGVzO1xuICAgIGlmICh0aGlzLmNhbGxlcikge1xuICAgICAgdGhpcy5zZWxlY3RlZEZpbGVzV2l0aENhbGxlci5lbWl0KHtmaWxlcywgY2FsbGVyOiB0aGlzLmNhbGxlcn0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNlbGVjdGVkRmlsZXMuZW1pdChmaWxlcyk7XG4gICAgfVxuICB9XG59XG4iXX0=