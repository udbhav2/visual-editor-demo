/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
export class ColorPickerDialogComponent {
    /**
     * @param {?} data
     * @param {?} colorPickerDialog
     */
    constructor(data, colorPickerDialog) {
        this.data = data;
        this.colorPickerDialog = colorPickerDialog;
        this.addAll = false;
        this.color = '#ffffff';
        this.titleText = 'ADD COLOR';
        this.buttonText = 'Add';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.data.color) {
            this.color = this.data.color;
        }
        if (this.data.titleText) {
            this.titleText = this.data.titleText;
        }
        if (this.data.buttonText) {
            this.buttonText = this.data.buttonText;
        }
        if (this.data.addAll) {
            this.addAll = this.data.addAll;
        }
    }
    /**
     * @param {?} addAll
     * @return {?}
     */
    onAdd(addAll) {
        this.colorPickerDialog.close({
            addAll,
            colorHex: this.color
        });
    }
    /**
     * @return {?}
     */
    onCancel() {
        this.colorPickerDialog.close();
    }
}
ColorPickerDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-color-picker-dialog',
                template: "<div>\n    <div mat-dialog-title>\n      <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\n        <div class=\"subtitle\">{{titleText}}</div>\n        <i class=\"fas fa-times fa-xs clickable\" (click)=\"onCancel()\"></i>\n      </div>\n      <mat-divider></mat-divider>\n    </div>\n    <div mat-dialog-content class=\"dialog-content\">\n      <div fxLayout=\"row\" fxLayoutAlign=\"center\">\n        <span [cpToggle]=\"true\"\n              [cpDialogDisplay]=\"'inline'\"\n              [cpOutputFormat]=\"'hex'\"\n              [(colorPicker)]=\"color\"></span>\n      </div>\n    </div>\n    <mat-dialog-actions>\n      <div fxFill fxLayout=\"row\"\n           fxLayoutAlign=\"end center\"\n           fxLayoutGap=\"5px\">\n        <button *ngIf=\"addAll\"\n                mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                [disableRipple]=\"true\"\n                (click)=\"onAdd(true)\">Add to all</button>\n        <button mat-button\n                color=\"primary\"\n                class=\"stroked-button border-primary\"\n                (click)=\"onAdd(false)\">Add</button>\n      </div>\n    </mat-dialog-actions>\n  </div>\n  \n",
                styles: [""]
            }] }
];
/** @nocollapse */
ColorPickerDialogComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: MatDialogRef }
];
if (false) {
    /** @type {?} */
    ColorPickerDialogComponent.prototype.addAll;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.color;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.titleText;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.buttonText;
    /** @type {?} */
    ColorPickerDialogComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    ColorPickerDialogComponent.prototype.colorPickerDialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sb3ItcGlja2VyLWRpYWxvZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvY29sb3ItcGlja2VyLWRpYWxvZy9jb2xvci1waWNrZXItZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU9sRSxNQUFNLE9BQU8sMEJBQTBCOzs7OztJQU1yQyxZQUNrQyxJQUFJLEVBQzVCLGlCQUEyRDtRQURuQyxTQUFJLEdBQUosSUFBSSxDQUFBO1FBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBMEM7UUFQckUsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLFVBQUssR0FBRyxTQUFTLENBQUM7UUFDbEIsY0FBUyxHQUFHLFdBQVcsQ0FBQztRQUN4QixlQUFVLEdBQUcsS0FBSyxDQUFDO0lBS2hCLENBQUM7Ozs7SUFFSixRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNwQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1NBQ2hDO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxLQUFLLENBQUMsTUFBTTtRQUNWLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUM7WUFDM0IsTUFBTTtZQUNOLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSztTQUNyQixDQUFDLENBQUM7SUFDTCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNqQyxDQUFDOzs7WUF4Q0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLHd0Q0FBbUQ7O2FBRXBEOzs7OzRDQVFJLE1BQU0sU0FBQyxlQUFlO1lBZEQsWUFBWTs7OztJQVFwQyw0Q0FBZTs7SUFDZiwyQ0FBa0I7O0lBQ2xCLCtDQUF3Qjs7SUFDeEIsZ0RBQW1COztJQUdqQiwwQ0FBb0M7Ozs7O0lBQ3BDLHVEQUFtRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1BVF9ESUFMT0dfREFUQSwgTWF0RGlhbG9nUmVmIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtY29sb3ItcGlja2VyLWRpYWxvZycsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb2xvci1waWNrZXItZGlhbG9nLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY29sb3ItcGlja2VyLWRpYWxvZy5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgYWRkQWxsID0gZmFsc2U7XG4gIGNvbG9yID0gJyNmZmZmZmYnO1xuICB0aXRsZVRleHQgPSAnQUREIENPTE9SJztcbiAgYnV0dG9uVGV4dCA9ICdBZGQnO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YSxcbiAgICBwcml2YXRlIGNvbG9yUGlja2VyRGlhbG9nOiBNYXREaWFsb2dSZWY8Q29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQ+XG4gICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5kYXRhLmNvbG9yKSB7XG4gICAgICB0aGlzLmNvbG9yID0gdGhpcy5kYXRhLmNvbG9yO1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLnRpdGxlVGV4dCkge1xuICAgICAgdGhpcy50aXRsZVRleHQgPSB0aGlzLmRhdGEudGl0bGVUZXh0O1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLmJ1dHRvblRleHQpIHtcbiAgICAgIHRoaXMuYnV0dG9uVGV4dCA9IHRoaXMuZGF0YS5idXR0b25UZXh0O1xuICAgIH1cbiAgICBpZiAodGhpcy5kYXRhLmFkZEFsbCkge1xuICAgICAgdGhpcy5hZGRBbGwgPSB0aGlzLmRhdGEuYWRkQWxsO1xuICAgIH1cbiAgfVxuXG4gIG9uQWRkKGFkZEFsbCk6IHZvaWQge1xuICAgIHRoaXMuY29sb3JQaWNrZXJEaWFsb2cuY2xvc2Uoe1xuICAgICAgYWRkQWxsLFxuICAgICAgY29sb3JIZXg6IHRoaXMuY29sb3JcbiAgICB9KTtcbiAgfVxuXG4gIG9uQ2FuY2VsKCk6IHZvaWQge1xuICAgIHRoaXMuY29sb3JQaWNrZXJEaWFsb2cuY2xvc2UoKTtcbiAgfVxufVxuIl19