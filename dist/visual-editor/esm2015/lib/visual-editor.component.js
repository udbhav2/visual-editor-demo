/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, HostListener } from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
import { MatDialog } from '@angular/material';
import { ActionTypes, ObjectTypes } from './configs/action-types';
import { VisualEditorService } from './services/visual-editor.service';
export class VisualEditorComponent {
    /**
     * @param {?} dialog
     */
    constructor(dialog) {
        this.dialog = dialog;
        this.customFonts = [];
        this.outgoingEventTriggered = new EventEmitter();
        this.outgoingSelectionEventTriggered = new EventEmitter();
        this.ActionTypes = ActionTypes;
        this.VisualEditorService = VisualEditorService;
        this.checkersImageUrl = '@app/../assets/images/checkers_bg2.png';
        this.imageConfig = {
            width: 0,
            height: 0,
            scaleFactor: 1,
            xOffset: 0,
            yOffset: 0
        };
        this.tools = [];
        this.minZoom = 1;
    }
    // Start of ngOnInit function
    /**
     * @return {?}
     */
    ngOnInit() {
        this.addCustomFonts();
        this.loadCustomFonts();
        this.tools = this.visualEditorConfig.tools;
        this.isFirstToolSelected = this.visualEditorConfig.isFirstToolSelected;
        this.hasCheckersBg = this.visualEditorConfig.hasCheckersBg;
        this.hasHiddenSelectionControls = this.visualEditorConfig.hasHiddenSelectionControls;
        this.hasZoom = this.visualEditorConfig.hasZoom;
        this.hasPan = this.visualEditorConfig.hasPan;
        this.isWideCanvas = this.visualEditorConfig.isWideCanvas;
        this.isShowFrame = this.visualEditorConfig.isShowFrame;
        this.resizeCanvasToBackgroundImage = this.visualEditorConfig.resizeCanvasToBackgroundImage;
        this.isCenterBackgroundImage = this.visualEditorConfig.isCenterBackgroundImage;
        this.toolsFlex = this.visualEditorConfig.toolsFlex;
        this.actionsFlex = this.visualEditorConfig.actionsFlex;
        // Keyboard Shortcuts
        this.hasShortcutsExpansionPanel = this.visualEditorConfig.hasShortcutsExpansionPanel;
        this.hasCloneShortcut = this.visualEditorConfig.hasCloneShortcut;
        this.hasRemoveShortcut = this.visualEditorConfig.hasRemoveShortcut;
        this.hasRemoveAllShortcut = this.visualEditorConfig.hasRemoveAllShortcut;
        this.hasSendBackwardsShortcut = this.visualEditorConfig.hasSendBackwardsShortcut;
        this.hasBringForwardShortcut = this.visualEditorConfig.hasBringForwardShortcut;
        // FontAwesome Icons
        this.faClone = this.visualEditorConfig.faClone;
        this.faTrash = this.visualEditorConfig.faTrash;
        this.faTrashAlt = this.visualEditorConfig.faTrashAlt;
        this.faArrowUp = this.visualEditorConfig.faArrowUp;
        this.faArrowDown = this.visualEditorConfig.faArrowDown;
        this.fileService = this.visualEditorConfig.fileService;
        this.s3Service = this.visualEditorConfig.s3Service;
        if (!this.isWideCanvas) {
            this.editedImageCanvas = new fabric.Canvas('editedImageCanvas', {
                isDrawingMode: true
            });
        }
        else {
            this.editedImageCanvas = new fabric.Canvas('wideCanvas', {
                isDrawingMode: true
            });
        }
        this.loadCanvas();
        if (this.hasHiddenSelectionControls) {
            this.editedImageCanvas.on('selection:created', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                this.selectHiddenControls(e.selected);
            }));
            this.editedImageCanvas.on('selection:updated', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                if (this.editedImageCanvas.getActiveObject().isType('activeSelection')) {
                    this.selectHiddenControls(this.editedImageCanvas.getActiveObject().getObjects());
                }
                else {
                    this.selectHiddenControls(e.selected);
                }
            }));
            this.editedImageCanvas.on('selection:cleared', (/**
             * @param {?} e
             * @return {?}
             */
            e => {
                this.activeTool = this.tools[0]; // Code changed to render the 'Remove All' and 'Submit' buttons all the time
            }));
        }
        if (this.isFirstToolSelected) {
            this.activeTool = this.tools[0];
            this.activeTool.canvasConfigs.forEach((/**
             * @param {?} confFunc
             * @return {?}
             */
            confFunc => {
                this.editedImageCanvas = confFunc(this.editedImageCanvas);
            }));
        }
        else {
            this.activeTool = {};
        }
        if (this.hasZoom) {
            this.addZoomControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addZoomControls(this.originalImageCanvas);
            }
        }
        if (this.hasPan) {
            this.addPanControls(this.editedImageCanvas);
            if (this.originalImageUrl) {
                this.addPanControls(this.originalImageCanvas);
            }
        }
    }
    // End of ngOnInit function
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes.visualJson.isFirstChange()) {
            this.loadCanvas();
        }
    }
    /**
     * @return {?}
     */
    loadCanvas() {
        this.VisualEditorService.resetObjectTypeCount();
        if (this.visualJson) {
            this.loadJson(this.visualJson, this.editedImageCanvas);
            this.editedImageCanvas.requestRenderAll();
        }
        else {
            this.setScaledImageToCanvas(this.editedImageCanvas, this.editedImageUrl, true);
            this.editedImageCanvas.requestRenderAll();
            if (this.originalImageUrl) {
                this.originalImageCanvas = new fabric.StaticCanvas('originalImageCanvas');
                this.setScaledImageToCanvas(this.originalImageCanvas, this.originalImageUrl);
                this.originalImageCanvas.requestRenderAll();
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    addCustomFonts() {
        _.each(this.visualEditorConfig.tools, (/**
         * @param {?} t
         * @return {?}
         */
        t => {
            _.each(t.sections, (/**
             * @param {?} s
             * @return {?}
             */
            s => {
                _.each(s.actions, (/**
                 * @param {?} a
                 * @return {?}
                 */
                a => {
                    if (a.label === 'FONT FAMILY') {
                        a.dropdownOption = _.sortBy(_.union(a.dropdownOption, _.map(this.customFonts, (/**
                         * @param {?} f
                         * @return {?}
                         */
                        f => f.fontFamily))));
                    }
                }));
            }));
        }));
    }
    /**
     * @private
     * @return {?}
     */
    loadCustomFonts() {
        // FIXME: IE support
        /** @type {?} */
        const customFontFaces = _.map(this.customFonts, (/**
         * @param {?} f
         * @return {?}
         */
        f => new FontFace(f.fontFamily, `url(${f.fontFileUrl})`)));
        _.each(customFontFaces, (/**
         * @param {?} cff
         * @return {?}
         */
        cff => {
            // NOTE: need to specify a font size for check() function
            if (!((/** @type {?} */ (document))).fonts.check(`12px ${cff.family}`)) {
                cff
                    .load()
                    .then((/**
                 * @param {?} res
                 * @return {?}
                 */
                res => {
                    ((/** @type {?} */ (document))).fonts.add(res);
                }))
                    .catch((/**
                 * @param {?} error
                 * @return {?}
                 */
                error => {
                    // FIXME: Error display
                }));
            }
        }));
    }
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    addZoomControls(canvas) {
        canvas.on('mouse:wheel', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            /** @type {?} */
            const delta = opt.e.deltaY;
            /** @type {?} */
            let zoom = canvas.getZoom();
            zoom = zoom - delta / _.min([canvas.getWidth(), canvas.getHeight()]);
            if (zoom > 20) {
                zoom = 20;
            }
            if (zoom < this.minZoom) {
                zoom = this.minZoom;
            }
            canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
            opt.e.preventDefault();
            opt.e.stopPropagation();
        }));
    }
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    addPanControls(canvas) {
        canvas.on('mouse:down', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            /** @type {?} */
            const evt = opt.e;
            if (evt.altKey === true) {
                this.isDragging = true;
                canvas.selection = false;
                this.lastPosX = evt.clientX;
                this.lastPosY = evt.clientY;
            }
        }));
        canvas.on('mouse:move', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            if (this.isDragging) {
                /** @type {?} */
                const e = opt.e;
                canvas.viewportTransform[4] += e.clientX - this.lastPosX;
                canvas.viewportTransform[5] += e.clientY - this.lastPosY;
                canvas.forEachObject((/**
                 * @param {?} o
                 * @return {?}
                 */
                o => o.setCoords()));
                canvas.requestRenderAll();
                this.lastPosX = e.clientX;
                this.lastPosY = e.clientY;
            }
        }));
        canvas.on('mouse:up', (/**
         * @param {?} opt
         * @return {?}
         */
        opt => {
            this.isDragging = false;
            canvas.selection = true;
        }));
    }
    // Start of loadJson function
    /**
     * @private
     * @param {?} inputJson
     * @param {?} canvas
     * @return {?}
     */
    loadJson(inputJson, canvas) {
        /** @type {?} */
        const img = new Image();
        img.onload = (/**
         * @return {?}
         */
        () => {
            // Extracting the images width and height and adding it into the _canvasDimensions property in the inputJson
            if (this.resizeCanvasToBackgroundImage) {
                inputJson._canvasDimensions = { width: img.width, height: img.height };
                inputJson.backgroundImage.width = inputJson._canvasDimensions.width;
                inputJson.backgroundImage.height = inputJson._canvasDimensions.height;
            }
            canvas.loadFromJSON(inputJson, (/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const scaleFactor = _.min([
                    canvas.getWidth() / inputJson._canvasDimensions.width,
                    canvas.getHeight() / inputJson._canvasDimensions.height
                ]);
                this.minZoom = scaleFactor;
                if (!this.isWideCanvas) {
                    canvas.setWidth(inputJson._canvasDimensions.width * scaleFactor);
                    canvas.setHeight(inputJson._canvasDimensions.height * scaleFactor);
                    canvas.setZoom(this.minZoom);
                    /** @type {?} */
                    const isWidthSmaller = inputJson._canvasDimensions.width / canvas.backgroundImage.width >
                        inputJson._canvasDimensions.height / canvas.backgroundImage.height;
                    if (isWidthSmaller) {
                        canvas.backgroundImage.scaleToWidth(inputJson._canvasDimensions.width);
                    }
                    else {
                        canvas.backgroundImage.scaleToHeight(inputJson._canvasDimensions.height);
                    }
                }
                else {
                    /** @type {?} */
                    const padding = 5;
                    /** @type {?} */
                    const border = 0;
                    /** @type {?} */
                    const newWidth = this.wideCanvas.nativeElement.offsetWidth - 2 * (padding + border);
                    /** @type {?} */
                    const newHeight = this.wideCanvas.nativeElement.offsetHeight - 2 * (padding + border);
                    canvas.setWidth(newWidth);
                    canvas.setHeight(newHeight);
                    canvas.setZoom(0.8 *
                        _.min([
                            canvas.getWidth() / inputJson._canvasDimensions.width,
                            canvas.getHeight() / inputJson._canvasDimensions.height
                        ]));
                    canvas.viewportTransform[4] =
                        (newWidth - inputJson._canvasDimensions.width * canvas.getZoom()) /
                            2;
                    canvas.viewportTransform[5] =
                        (newHeight -
                            inputJson._canvasDimensions.height * canvas.getZoom()) /
                            2;
                    canvas.forEachObject((/**
                     * @param {?} o
                     * @return {?}
                     */
                    o => o.setCoords()));
                }
                if (this.isCenterBackgroundImage) {
                    canvas.viewportCenterObject(canvas.backgroundImage); // Centers the image on the canvas
                }
                if (this.isShowFrame) {
                    /** @type {?} */
                    const strokeWidth = 3;
                    /** @type {?} */
                    const rectangle = new fabric.Rect({
                        left: -strokeWidth,
                        top: -strokeWidth,
                        height: inputJson._canvasDimensions.height + strokeWidth,
                        width: inputJson._canvasDimensions.width + strokeWidth,
                        stroke: 'rgb(0,255,0)',
                        strokeWidth,
                        strokeDashArray: [5 * strokeWidth, 5 * strokeWidth],
                        fill: 'rgba(0,0,0,0)',
                        selectable: false,
                        evented: false
                    });
                    rectangle._key = 'frame_rectangle';
                    canvas.add(rectangle);
                    rectangle.bringToFront();
                }
                canvas.renderAll();
            }));
        });
        img.src = inputJson.backgroundImage.src;
    }
    // End of loadJson function
    /**
     * @private
     * @param {?} canvas
     * @return {?}
     */
    resetCanvas(canvas) {
        canvas.getObjects().forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
    }
    /**
     * @private
     * @param {?} canvas
     * @param {?} imageUrl
     * @param {?=} isEditedImage
     * @return {?}
     */
    setScaledImageToCanvas(canvas, imageUrl, isEditedImage = false) {
        fabric.Image.fromURL(imageUrl, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            /** @type {?} */
            const isWidthSmaller = canvas.getWidth() / img.width > canvas.getHeight() / img.height;
            if (isWidthSmaller) {
                img.scaleToHeight(canvas.getHeight());
            }
            else {
                img.scaleToWidth(canvas.getWidth());
            }
            const { width, height } = img;
            /** @type {?} */
            const left = isWidthSmaller
                ? (canvas.getWidth() - width * img.scaleX) / 2.0
                : 0;
            /** @type {?} */
            const top = isWidthSmaller
                ? 0
                : (canvas.getHeight() - height * img.scaleX) / 2.0;
            img.set({
                left,
                top
            });
            if (isEditedImage) {
                this.imageConfig = {
                    width,
                    height,
                    scaleFactor: img.scaleX,
                    xOffset: left,
                    yOffset: top
                };
            }
            if (this.hasCheckersBg) {
                canvas.add(img);
                canvas.renderAll();
                fabric.Image.fromURL(this.checkersImageUrl, (/**
                 * @param {?} checkerImg
                 * @return {?}
                 */
                checkerImg => {
                    checkerImg.scaleToWidth(canvas.getWidth());
                    canvas.setBackgroundImage(checkerImg, canvas.renderAll.bind(canvas));
                }));
            }
            else {
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
            }
        }));
    }
    /**
     * @private
     * @param {?} selectedObjects
     * @return {?}
     */
    selectHiddenControls(selectedObjects) {
        if (selectedObjects.length === 1) {
            /** @type {?} */
            const isShape = selectedObjects[0].isType(ObjectTypes.RECT) ||
                selectedObjects[0].isType(ObjectTypes.CIRCLE) ||
                selectedObjects[0].isType(ObjectTypes.POLYGON);
            /** @type {?} */
            const isImage = selectedObjects[0].isType(ObjectTypes.IMAGE);
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                if (t.isHidden) {
                    if (isShape) {
                        if (t.objectType === ObjectTypes.SHAPE) {
                            this.onSelectTool(t);
                        }
                    }
                    else if (isImage) {
                        if (selectedObjects[0]._key &&
                            selectedObjects[0]._key.includes(ObjectTypes.PRODUCT)) {
                            if (t.objectType === ObjectTypes.PRODUCT) {
                                this.onSelectTool(t);
                            }
                        }
                        else {
                            // Non product image
                            if (selectedObjects[0].isType(t.objectType)) {
                                this.onSelectTool(t);
                            }
                        }
                    }
                    else {
                        // Non shape and non image object
                        if (selectedObjects[0].isType(t.objectType)) {
                            this.onSelectTool(t);
                        }
                    }
                }
            }));
        }
        else if (selectedObjects.length > 1) {
            this.tools.forEach((/**
             * @param {?} t
             * @return {?}
             */
            t => {
                if (t.isHidden && t.objectType === ObjectTypes.GROUP) {
                    this.onSelectTool(t);
                }
            }));
        }
    }
    /**
     * @param {?} tool
     * @return {?}
     */
    onSelectTool(tool) {
        if (tool.type === ActionTypes.UPLOAD) {
            this.fileUpload.click();
        }
        else {
            if (!_.isUndefined(tool.onSelect)) {
                tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                this.activeTool = {};
            }
            if (!this.activeTool || this.activeTool.name !== tool.name) {
                this.activeTool = tool;
                if (tool.canvasConfigs) {
                    tool.canvasConfigs.forEach((/**
                     * @param {?} confFunc
                     * @return {?}
                     */
                    confFunc => {
                        this.editedImageCanvas = confFunc(this.editedImageCanvas);
                    }));
                }
            }
        }
    }
    // Start of private onToolAction(action) function
    /**
     * @param {?} action
     * @return {?}
     */
    onToolAction(action) {
        switch (action.type) {
            case ActionTypes.BUTTON: {
                /** @type {?} */
                let res;
                if (!_.isUndefined(action.onSelectParams)) {
                    res = action.action(this.editedImageCanvas, action.onSelectParams);
                }
                else {
                    res = action.action(this.editedImageCanvas);
                }
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                if (!_.isUndefined(res) && !_.isUndefined(action.onActionReturn)) {
                    action.onActionReturn(action, res);
                }
                break;
            }
            case ActionTypes.TOGGLE: {
                action.isOn = !action.isOn;
                action.action(this.editedImageCanvas, action.isOn ? action.onValue : action.offValue);
                break;
            }
            case ActionTypes.DROPDOWN: {
                action.action(this.editedImageCanvas, action.dropdownSelectedOption);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.RELATED_INPUT: {
                /** @type {?} */
                const relatedValues = {};
                this.activeTool.sections.forEach((/**
                 * @param {?} s
                 * @return {?}
                 */
                s => {
                    s.actions.forEach((/**
                     * @param {?} a
                     * @return {?}
                     */
                    a => {
                        if (a.type === ActionTypes.RELATED_INPUT &&
                            a.groupName === action.groupName &&
                            a.label !== action.label) {
                            relatedValues[a.keyName] = a.inputValue;
                        }
                    }));
                }));
                relatedValues[action.keyName] = action.inputValue;
                action.action(this.editedImageCanvas, relatedValues);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
            case ActionTypes.DIALOG: {
                if (action.dialog) {
                    /** @type {?} */
                    const dialogRef = this.dialog.open(action.dialog.component, {
                        autoFocus: false,
                        data: action.dialog.data,
                        minWidth: '300px',
                        panelClass: 'app-dialog'
                    });
                    dialogRef.afterClosed().subscribe((/**
                     * @param {?} resDialog
                     * @return {?}
                     */
                    resDialog => {
                        if (!_.isUndefined(resDialog)) {
                            /** @type {?} */
                            const resAction = action.dialog.onClose(this.editedImageCanvas, resDialog);
                            if (!_.isUndefined(resAction) &&
                                !_.isUndefined(action.onActionReturn)) {
                                action.onActionReturn(action, resAction);
                            }
                        }
                    }));
                }
                break;
            }
            case ActionTypes.OUTGOING_EVENT_TRIGGER: {
                this.outgoingEventTriggered.emit({
                    canvas: this.editedImageCanvas,
                    transformedCanvas: action.label === 'Submit'
                        ? action.action(this.visualJson.backgroundImage.src, this.editedImageCanvas.toJSON(['_canvasDimensions']))
                        : ''
                });
                break;
            }
            default: {
                action.action(this.editedImageCanvas);
                this.editedImageCanvas.renderAll.bind(this.editedImageCanvas);
                break;
            }
        }
    }
    // End of private onToolAction(action) function
    /**
     * @param {?} imgUrl
     * @return {?}
     */
    onUpdate(imgUrl) {
        this.resetCanvas(this.editedImageCanvas);
        this.setScaledImageToCanvas(this.editedImageCanvas, imgUrl, true);
    }
    /**
     * @return {?}
     */
    getCanvasAndConfig() {
        return {
            canvas: this.editedImageCanvas,
            imageConfig: this.imageConfig
        };
    }
    /**
     * @param {?} res
     * @return {?}
     */
    onFileSelect(res) {
        const { files, caller: tool } = res;
        if (!_.isUndefined(tool)) {
            if (!_.isUndefined(tool.onSelect) && files.length > 0) {
                /** @type {?} */
                const file = files[0];
                /** @type {?} */
                const filename = _.replace(file.name, /[^A-Z0-9.]+/gi, '_');
                this.fileService
                    .getUploadUrl({
                    filename,
                    contentType: file.type,
                    expiresInSecs: 3600
                })
                    .subscribe((/**
                 * @param {?} respUploadUrl
                 * @return {?}
                 */
                respUploadUrl => {
                    this.s3Service
                        .upload(respUploadUrl.url, file, file.type)
                        .subscribe((/**
                     * @param {?} respUpload
                     * @return {?}
                     */
                    respUpload => {
                        /** @type {?} */
                        const fileUrl = respUploadUrl.url.split('?')[0];
                        tool.onSelectParams.fileUrl = fileUrl;
                        tool.onSelect(this.editedImageCanvas, tool.onSelectParams);
                        this.activeTool = {};
                    }));
                }));
            }
        }
    }
    // Keyboard Shortcuts
    /**
     * @param {?} event
     * @return {?}
     */
    canvasKeyboardEvent(event) {
        if (this.hasCloneShortcut &&
            ((event.ctrlKey || event.metaKey) &&
                event.shiftKey &&
                event.keyCode === 86)) {
            this.VisualEditorService.cloneObject(this.editedImageCanvas);
        }
        if (this.hasRemoveShortcut && (!event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteSelection(this.editedImageCanvas);
        }
        if (this.hasRemoveAllShortcut && (event.shiftKey && event.keyCode === 8)) {
            this.VisualEditorService.deleteAllSelections(this.editedImageCanvas);
        }
        if (this.hasSendBackwardsShortcut &&
            (event.shiftKey && event.keyCode === 40)) {
            this.VisualEditorService.sendBackwards(this.editedImageCanvas);
        }
        if (this.hasBringForwardShortcut &&
            (event.shiftKey && event.keyCode === 38)) {
            this.VisualEditorService.bringForward(this.editedImageCanvas);
        }
    }
}
VisualEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-visual-editor',
                template: "<lib-file-upload #fileUpload\n[accept]=\"'image/png, image/jpg, image/gif'\"\n[multiple]=\"false\"\n(selectedFilesWithCaller)=\"onFileSelect($event)\"></lib-file-upload>\n\n<!-- Template starts from here -->\n<div fxFill fxLayout=\"row\" fxLayoutGap='2%' fxLayoutAlign=\"space-between stretch\" (keydown)=\"canvasKeyboardEvent($event)\">\n\n  <!-- Start of LHS Container -->\n  <div [fxFlex]=\"toolsFlex\" class=\"side-panel left\" fxLayout=\"column\" fxLayoutAlign=\"center stretch\" fxLayoutGap='7%'>\n      <div *ngFor=\"let tool of tools\"  fxLayoutAlign=\"center\"> \n          \n          <button *ngIf=\"!tool.isHidden && tool.type !== ActionTypes.UPLOAD\"\n               [ngStyle]='tool.style'\n                mat-flat-button\n                color=\"primary\"\n                class='button-width-lhs button-sharp button-font-size'\n                (click)=\"onSelectTool(tool)\">\n            <img *ngIf='tool.iconImgUrl'class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n          <button *ngIf=\"!tool.isHidden && tool.type === ActionTypes.UPLOAD\"\n                mat-flat-button\n                fxLayoutAlign=\"center\"\n                color=\"primary\"\n                class=\"button-width-lhs button-sharp button-font-size\"\n                (click)=\"fileUpload.click(tool)\">\n            <img *ngIf='tool.iconImgUrl' class=\"tool-icon\" [src]=\"tool.iconImgUrl\"/>\n            <span>{{tool.iconDisplayText}}</span>\n          </button>\n\n      </div> \n    </div> \n  <!-- /End LHS Container -->\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!originalImageUrl || isWideCanvas\">\n    <canvas id=\"originalImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div fxFlex=\"0 0 400px\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"isWideCanvas\">\n    <canvas id=\"editedImageCanvas\" class=\"canvas\" width=\"400\" height=\"400\">\n    </canvas>\n  </div>\n\n  <div class=\"wideCanvasContainer\" #wideCanvas fxFlex=\"1 0 auto\" fxLayout=\"column\" fxLayoutAlign=\"center\" [class.canvas-invisible]=\"!isWideCanvas\">\n    <canvas id=\"wideCanvas\">\n    </canvas>\n  </div>\n\n  <!-- Start of RHS Container -->\n  <div [fxFlex]=\"actionsFlex\" class=\"side-panel right\" fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap='5%'>\n      <div fxLayout=\"row\" fxLayoutAlign=\"center stretch\">\n          <mat-expansion-panel *ngIf='hasShortcutsExpansionPanel' class='expansion-panel-spacing-bottom'>\n              <mat-expansion-panel-header>\n                <mat-panel-description class='expansion-panel-header-font-size'>Keyboard shortcuts</mat-panel-description>\n              </mat-expansion-panel-header>\n                    <mat-list role=\"list\">\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasCloneShortcut'><fa-icon [icon]='faClone' class='action-icon-spacing'></fa-icon>Clone - Ctrl/Cmd + Shift + V</mat-list-item>\n                      <mat-divider *ngIf='hasCloneShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveShortcut'><fa-icon [icon]='faTrashAlt' class='action-icon-spacing'></fa-icon>Remove - Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasRemoveAllShortcut'><fa-icon [icon]='faTrash' class='action-icon-spacing'></fa-icon>Remove All - Shift + Delete/Del</mat-list-item>\n                      <mat-divider *ngIf='hasRemoveAllShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasBringForwardShortcut'><fa-icon [icon]='faArrowUp' class='action-icon-spacing'></fa-icon>Bring Forward - Shift + Up Arrow</mat-list-item>\n                      <mat-divider *ngIf='hasBringForwardShortcut'></mat-divider>\n                      <mat-list-item role=\"listitem\" class='list-item-font-size' *ngIf='hasSendBackwardsShortcut'><fa-icon [icon]='faArrowDown' class='action-icon-spacing'></fa-icon>Send Backwards - Shift + Down Arrow</mat-list-item>\n                    </mat-list>\n            </mat-expansion-panel>\n      </div>\n\n      <div *ngFor=\"let section of activeTool.sections\" [fxFlex]=\"section.flex\" fxLayout=\"row\" fxLayoutAlign=\"center stretch\" class='action-section' [class.end-section]='section.isEndSection' fxLayoutGap='2%'>\n        \n        <div *ngFor=\"let action of section.actions\" [fxFlex]=\"action.flex\"> \n\n          <div [ngSwitch]=\"action.type\">\n\n            <div *ngSwitchCase=\"ActionTypes.BUTTON\">\n              <button fxFill mat-stroked-button\n                      [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                      matTooltipPosition='above'\n                      color=\"primary\"\n                      class='button-sharp button-font-size'\n                      [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                      (click)=\"onToolAction(action)\">\n                      <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                      <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                      <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                      <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n              </button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.OUTGOING_EVENT_TRIGGER\">\n              <button fxFill mat-flat-button\n              color=\"primary\"\n              class='button-sharp button-font-size'\n              (click)=\"onToolAction(action)\">{{ action.label }}</button>\n            </div>\n\n            <div *ngSwitchCase=\"ActionTypes.DIALOG\">\n                <button fxFill mat-stroked-button\n                        [matTooltip]=\"action.iconSrc || action.iconClass || action.iconMaterial? action.label : ''\"\n                        matTooltipPosition='above'\n                        color=\"primary\"\n                        class=\"button-sharp button-font-size\"\n                        [class.selected]=\"action.isSelected !== undefined && action.isSelected\"\n                        (click)=\"onToolAction(action)\">\n                  <fa-icon *ngIf='action.iconClass' [icon]='action.iconClass'></fa-icon>\n                  <i *ngIf='action.iconMaterial' class=\"material-icons\">{{ action.iconMaterial }}</i>\n                  <img *ngIf=\"action.iconSrc\" class=\"action-icon\" [src]=\"action.iconSrc\"/>\n                  <span *ngIf=\"!action.iconSrc && !action.iconClass && !action.iconMaterial\">{{ action.label }}</span>\n                </button>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.DROPDOWN\">\n                <mat-form-field fxFill>\n                  <mat-label>{{ action.label }}</mat-label>\n                  <mat-select [(value)]=\"action.dropdownSelectedOption\" (selectionChange)=\"onToolAction(action)\">\n                    <mat-option *ngFor=\"let value of action.dropdownOption\" [value]=\"value\">\n                      {{ value }}\n                    </mat-option>\n                  </mat-select>\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.RELATED_INPUT\">\n                <mat-form-field floatLabel=\"never\" class=\"editor-input-field\">\n                  <input matInput\n                         [(ngModel)]=\"action.inputValue\"\n                         (change)=\"onToolAction(action)\"\n                         [type]=\"action.inputType\">\n                </mat-form-field>\n              </div>\n  \n              <div *ngSwitchCase=\"ActionTypes.TOGGLE\">\n                <div>\n                  {{ action.isOn ? action.onLabel : action.offLabel}}\n                </div>\n                <mat-slide-toggle (change)=\"onToolAction(action)\" class=\"visual-editor-toggle\"></mat-slide-toggle>\n              </div>\n  \n              <div *ngSwitchDefault>\n                <button fxFill mat-button\n                        color=\"primary\"\n                        class=\"primary-button\"\n                        (click)=\"onToolAction(action)\">\n                  <i [class]=\"action.iconClass + ' tool-icon'\"></i>\n                </button>\n              </div>\n            \n          </div> <!-- End of [ngSwitch]=\"action.type\" div -->\n          \n        </div> <!-- End of *ngFor=\"let action of section.actions\" div -->\n      </div> <!-- End of *ngFor=\"let section of activeTool.sections\" div-->\n  </div>\n  <!-- /End of RHS Container  -->\n</div> \n\n\n",
                styles: ["div.canvas-invisible{display:none!important}.button-sharp{border-radius:0}.button-font-size{font-size:.8vw}.button-width-lhs{width:8vw}.button-width-rhs{width:6vw}.action-icon{max-width:40px;max-height:40px}.action-icon-spacing{margin-right:3px}.canvas{border:1px solid #d5d5d5}.wideCanvasContainer{padding:5px;background-image:url(/assets/images/checkers_bg2.png)}.expansion-panel-header-font-size{font-size:.9vw}.expansion-panel-spacing-bottom{margin-bottom:15vh}.list-item-font-size{font-size:.7vw}.side-panel{padding:0}.side-panel.left{border-right:1px solid #eee}.side-panel.right{border-left:1px solid #eee}"]
            }] }
];
/** @nocollapse */
VisualEditorComponent.ctorParameters = () => [
    { type: MatDialog }
];
VisualEditorComponent.propDecorators = {
    originalImageUrl: [{ type: Input }],
    editedImageUrl: [{ type: Input }],
    visualJson: [{ type: Input }],
    visualEditorConfig: [{ type: Input }],
    customFonts: [{ type: Input }],
    outgoingEventTriggered: [{ type: Output }],
    outgoingSelectionEventTriggered: [{ type: Output }],
    wideCanvas: [{ type: ViewChild, args: ['wideCanvas',] }],
    canvasKeyboardEvent: [{ type: HostListener, args: ['window:keydown', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    VisualEditorComponent.prototype.originalImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.editedImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.visualJson;
    /** @type {?} */
    VisualEditorComponent.prototype.visualEditorConfig;
    /** @type {?} */
    VisualEditorComponent.prototype.customFonts;
    /** @type {?} */
    VisualEditorComponent.prototype.outgoingEventTriggered;
    /** @type {?} */
    VisualEditorComponent.prototype.outgoingSelectionEventTriggered;
    /** @type {?} */
    VisualEditorComponent.prototype.wideCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.ActionTypes;
    /** @type {?} */
    VisualEditorComponent.prototype.VisualEditorService;
    /** @type {?} */
    VisualEditorComponent.prototype.checkersImageUrl;
    /** @type {?} */
    VisualEditorComponent.prototype.originalImageCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.editedImageCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.imageConfig;
    /** @type {?} */
    VisualEditorComponent.prototype.tools;
    /** @type {?} */
    VisualEditorComponent.prototype.activeTool;
    /** @type {?} */
    VisualEditorComponent.prototype.objectTypeCount;
    /** @type {?} */
    VisualEditorComponent.prototype.isFirstToolSelected;
    /** @type {?} */
    VisualEditorComponent.prototype.hasCheckersBg;
    /** @type {?} */
    VisualEditorComponent.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    VisualEditorComponent.prototype.hasZoom;
    /** @type {?} */
    VisualEditorComponent.prototype.hasPan;
    /** @type {?} */
    VisualEditorComponent.prototype.isWideCanvas;
    /** @type {?} */
    VisualEditorComponent.prototype.isShowFrame;
    /** @type {?} */
    VisualEditorComponent.prototype.resizeCanvasToBackgroundImage;
    /** @type {?} */
    VisualEditorComponent.prototype.isCenterBackgroundImage;
    /** @type {?} */
    VisualEditorComponent.prototype.toolsFlex;
    /** @type {?} */
    VisualEditorComponent.prototype.actionsFlex;
    /** @type {?} */
    VisualEditorComponent.prototype.selection;
    /** @type {?} */
    VisualEditorComponent.prototype.fileUpload;
    /** @type {?} */
    VisualEditorComponent.prototype.fileService;
    /** @type {?} */
    VisualEditorComponent.prototype.s3Service;
    /** @type {?} */
    VisualEditorComponent.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    VisualEditorComponent.prototype.hasCloneShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasRemoveShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.hasBringForwardShortcut;
    /** @type {?} */
    VisualEditorComponent.prototype.faClone;
    /** @type {?} */
    VisualEditorComponent.prototype.faTrash;
    /** @type {?} */
    VisualEditorComponent.prototype.faTrashAlt;
    /** @type {?} */
    VisualEditorComponent.prototype.faArrowUp;
    /** @type {?} */
    VisualEditorComponent.prototype.faArrowDown;
    /** @type {?} */
    VisualEditorComponent.prototype.isDragging;
    /** @type {?} */
    VisualEditorComponent.prototype.lastPosY;
    /** @type {?} */
    VisualEditorComponent.prototype.lastPosX;
    /** @type {?} */
    VisualEditorComponent.prototype.minZoom;
    /**
     * @type {?}
     * @private
     */
    VisualEditorComponent.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlzdWFsLWVkaXRvci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL3Zpc3VhbC1lZGl0b3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUdULFlBQVksRUFDWixLQUFLLEVBQ0wsTUFBTSxFQUNOLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUViLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sUUFBUSxDQUFDO0FBR2hCLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUU5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBT3ZFLE1BQU0sT0FBTyxxQkFBcUI7Ozs7SUFnRWhDLFlBQW9CLE1BQWlCO1FBQWpCLFdBQU0sR0FBTixNQUFNLENBQVc7UUEzRDVCLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDcEQsb0NBQStCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUl2RSxnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUMxQix3QkFBbUIsR0FBRyxtQkFBbUIsQ0FBQztRQUUxQyxxQkFBZ0IsR0FBRyx3Q0FBd0MsQ0FBQztRQUc1RCxnQkFBVyxHQUFHO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixNQUFNLEVBQUUsQ0FBQztZQUNULFdBQVcsRUFBRSxDQUFDO1lBQ2QsT0FBTyxFQUFFLENBQUM7WUFDVixPQUFPLEVBQUUsQ0FBQztTQUNYLENBQUM7UUFFRixVQUFLLEdBQUcsRUFBRSxDQUFDO1FBcUNYLFlBQU8sR0FBRyxDQUFDLENBQUM7SUFFNEIsQ0FBQzs7Ozs7SUFHekMsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFdkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDO1FBQzNDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsbUJBQW1CLENBQUM7UUFDdkUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDO1FBQzNELElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsMEJBQTBCLENBQUM7UUFDckYsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztRQUM3QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUM7UUFDekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDO1FBQ3ZELElBQUksQ0FBQyw2QkFBNkIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsNkJBQTZCLENBQUM7UUFDM0YsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsQ0FBQztRQUMvRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUM7UUFDbkQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDO1FBRXZELHFCQUFxQjtRQUNyQixJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDBCQUEwQixDQUFDO1FBQ3JGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUM7UUFDakUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQztRQUNuRSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLG9CQUFvQixDQUFDO1FBQ3pFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsd0JBQXdCLENBQUM7UUFDakYsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyx1QkFBdUIsQ0FBQztRQUUvRSxvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDO1FBQy9DLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztRQUMvQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUM7UUFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO1FBQ25ELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQztRQUV2RCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUM7UUFDdkQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO1FBRW5ELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzlELGFBQWEsRUFBRSxJQUFJO2FBQ3BCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRTtnQkFDdkQsYUFBYSxFQUFFLElBQUk7YUFDcEIsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFFbEIsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDbkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUI7Ozs7WUFBRSxDQUFDLENBQUMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN4QyxDQUFDLEVBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsbUJBQW1COzs7O1lBQUUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2pELElBQ0UsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxFQUNsRTtvQkFDQSxJQUFJLENBQUMsb0JBQW9CLENBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FDdEQsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QztZQUNILENBQUMsRUFBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxtQkFBbUI7Ozs7WUFBRSxDQUFDLENBQUMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsNEVBQTRFO1lBQy9HLENBQUMsRUFBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUM1QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztZQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUMvQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1lBQzVELENBQUMsRUFBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1NBQ3RCO1FBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDN0MsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDaEQ7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDNUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDL0M7U0FDRjtJQUNILENBQUM7Ozs7OztJQUdELFdBQVcsQ0FBQyxPQUFzQjtRQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUN2QyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDbkI7SUFDSCxDQUFDOzs7O0lBRU0sVUFBVTtRQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQ2hELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDM0M7YUFBTTtZQUNMLElBQUksQ0FBQyxzQkFBc0IsQ0FDekIsSUFBSSxDQUFDLGlCQUFpQixFQUN0QixJQUFJLENBQUMsY0FBYyxFQUNuQixJQUFJLENBQ0wsQ0FBQztZQUNGLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQzFDLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN6QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxNQUFNLENBQUMsWUFBWSxDQUNoRCxxQkFBcUIsQ0FDdEIsQ0FBQztnQkFDRixJQUFJLENBQUMsc0JBQXNCLENBQ3pCLElBQUksQ0FBQyxtQkFBbUIsRUFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUN0QixDQUFDO2dCQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQzdDO1NBQ0Y7SUFDSCxDQUFDOzs7OztJQUVPLGNBQWM7UUFDcEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSzs7OztRQUFFLENBQUMsQ0FBQyxFQUFFO1lBQ3hDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFFBQVE7Ozs7WUFBRSxDQUFDLENBQUMsRUFBRTtnQkFDckIsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTzs7OztnQkFBRSxDQUFDLENBQUMsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLENBQUMsS0FBSyxLQUFLLGFBQWEsRUFBRTt3QkFDN0IsQ0FBQyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUN6QixDQUFDLENBQUMsS0FBSyxDQUNMLENBQUMsQ0FBQyxjQUFjLEVBQ2hCLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVc7Ozs7d0JBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxFQUFDLENBQzNDLENBQ0YsQ0FBQztxQkFDSDtnQkFDSCxDQUFDLEVBQUMsQ0FBQztZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVPLGVBQWU7OztjQUVmLGVBQWUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUMzQixJQUFJLENBQUMsV0FBVzs7OztRQUNoQixDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsV0FBVyxHQUFHLENBQUMsRUFDekQ7UUFDRCxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWU7Ozs7UUFBRSxHQUFHLENBQUMsRUFBRTtZQUM1Qix5REFBeUQ7WUFDekQsSUFBSSxDQUFDLENBQUMsbUJBQUEsUUFBUSxFQUFPLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7Z0JBQ3hELEdBQUc7cUJBQ0EsSUFBSSxFQUFFO3FCQUNOLElBQUk7Ozs7Z0JBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ1YsQ0FBQyxtQkFBQSxRQUFRLEVBQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ25DLENBQUMsRUFBQztxQkFDRCxLQUFLOzs7O2dCQUFDLEtBQUssQ0FBQyxFQUFFO29CQUNiLHVCQUF1QjtnQkFDekIsQ0FBQyxFQUFDLENBQUM7YUFDTjtRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLE1BQU07UUFDNUIsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUU7O2tCQUN2QixLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxNQUFNOztnQkFDdEIsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFFM0IsSUFBSSxHQUFHLElBQUksR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksSUFBSSxHQUFHLEVBQUUsRUFBRTtnQkFDYixJQUFJLEdBQUcsRUFBRSxDQUFDO2FBQ1g7WUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUN2QixJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUNyQjtZQUVELE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFakUsR0FBRyxDQUFDLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixHQUFHLENBQUMsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sY0FBYyxDQUFDLE1BQU07UUFDM0IsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUU7O2tCQUN0QixHQUFHLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDakIsSUFBSSxHQUFHLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtnQkFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLE1BQU0sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN6QixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUM7Z0JBQzVCLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQzthQUM3QjtRQUNILENBQUMsRUFBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZOzs7O1FBQUUsR0FBRyxDQUFDLEVBQUU7WUFDNUIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOztzQkFDYixDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ2YsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDekQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDekQsTUFBTSxDQUFDLGFBQWE7Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUMsQ0FBQztnQkFDekMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDO2FBQzNCO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsRUFBRSxDQUFDLFVBQVU7Ozs7UUFBRSxHQUFHLENBQUMsRUFBRTtZQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztZQUN4QixNQUFNLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUMxQixDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7O0lBR08sUUFBUSxDQUFDLFNBQVMsRUFBRSxNQUFNOztjQUMxQixHQUFHLEdBQUcsSUFBSSxLQUFLLEVBQUU7UUFDdkIsR0FBRyxDQUFDLE1BQU07OztRQUFHLEdBQUcsRUFBRTtZQUNoQiw0R0FBNEc7WUFDNUcsSUFBSSxJQUFJLENBQUMsNkJBQTZCLEVBQUU7Z0JBQ3RDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ3ZFLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BFLFNBQVMsQ0FBQyxlQUFlLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7YUFDdkU7WUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLFNBQVM7OztZQUFFLEdBQUcsRUFBRTs7c0JBQzVCLFdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUN4QixNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUs7b0JBQ3JELE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTTtpQkFDeEQsQ0FBQztnQkFDRixJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQztnQkFFM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ3RCLE1BQU0sQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsQ0FBQztvQkFDakUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDO29CQUNuRSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs7MEJBRXZCLGNBQWMsR0FDbEIsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsZUFBZSxDQUFDLEtBQUs7d0JBQ2hFLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNO29CQUNwRSxJQUFJLGNBQWMsRUFBRTt3QkFDbEIsTUFBTSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQ2pDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQ2xDLENBQUM7cUJBQ0g7eUJBQU07d0JBQ0wsTUFBTSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQ2xDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQ25DLENBQUM7cUJBQ0g7aUJBQ0Y7cUJBQU07OzBCQUNDLE9BQU8sR0FBRyxDQUFDOzswQkFDWCxNQUFNLEdBQUcsQ0FBQzs7MEJBQ1YsUUFBUSxHQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDOzswQkFDOUQsU0FBUyxHQUNiLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO29CQUNyRSxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMxQixNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUM1QixNQUFNLENBQUMsT0FBTyxDQUNaLEdBQUc7d0JBQ0QsQ0FBQyxDQUFDLEdBQUcsQ0FBQzs0QkFDSixNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUs7NEJBQ3JELE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsTUFBTTt5QkFDeEQsQ0FBQyxDQUNMLENBQUM7b0JBQ0YsTUFBTSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQzt3QkFDekIsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLGlCQUFpQixDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7NEJBQ2pFLENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO3dCQUN6QixDQUFDLFNBQVM7NEJBQ1IsU0FBUyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7NEJBQ3hELENBQUMsQ0FBQztvQkFDSixNQUFNLENBQUMsYUFBYTs7OztvQkFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQyxDQUFDO2lCQUMxQztnQkFFRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtvQkFDaEMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLGtDQUFrQztpQkFDeEY7Z0JBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOzswQkFDZCxXQUFXLEdBQUcsQ0FBQzs7MEJBQ2YsU0FBUyxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFDaEMsSUFBSSxFQUFFLENBQUMsV0FBVzt3QkFDbEIsR0FBRyxFQUFFLENBQUMsV0FBVzt3QkFDakIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEdBQUcsV0FBVzt3QkFDeEQsS0FBSyxFQUFFLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsV0FBVzt3QkFDdEQsTUFBTSxFQUFFLGNBQWM7d0JBQ3RCLFdBQVc7d0JBQ1gsZUFBZSxFQUFFLENBQUMsQ0FBQyxHQUFHLFdBQVcsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDO3dCQUNuRCxJQUFJLEVBQUUsZUFBZTt3QkFDckIsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxLQUFLO3FCQUNmLENBQUM7b0JBQ0YsU0FBUyxDQUFDLElBQUksR0FBRyxpQkFBaUIsQ0FBQztvQkFDbkMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDdEIsU0FBUyxDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUMxQjtnQkFFRCxNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDckIsQ0FBQyxFQUFDLENBQUM7UUFDTCxDQUFDLENBQUEsQ0FBQztRQUNGLEdBQUcsQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUM7SUFDMUMsQ0FBQzs7Ozs7OztJQUdPLFdBQVcsQ0FBQyxNQUFNO1FBQ3hCLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7Ozs7SUFFTyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLGFBQWEsR0FBRyxLQUFLO1FBQ3BFLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFFBQVE7Ozs7UUFBRSxHQUFHLENBQUMsRUFBRTs7a0JBQzdCLGNBQWMsR0FDbEIsTUFBTSxDQUFDLFFBQVEsRUFBRSxHQUFHLEdBQUcsQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLFNBQVMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxNQUFNO1lBQ2pFLElBQUksY0FBYyxFQUFFO2dCQUNsQixHQUFHLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZDO2lCQUFNO2dCQUNMLEdBQUcsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDckM7a0JBRUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsR0FBRzs7a0JBQ3ZCLElBQUksR0FBRyxjQUFjO2dCQUN6QixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHO2dCQUNoRCxDQUFDLENBQUMsQ0FBQzs7a0JBQ0MsR0FBRyxHQUFHLGNBQWM7Z0JBQ3hCLENBQUMsQ0FBQyxDQUFDO2dCQUNILENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsR0FBRyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUc7WUFFcEQsR0FBRyxDQUFDLEdBQUcsQ0FBQztnQkFDTixJQUFJO2dCQUNKLEdBQUc7YUFDSixDQUFDLENBQUM7WUFFSCxJQUFJLGFBQWEsRUFBRTtnQkFDakIsSUFBSSxDQUFDLFdBQVcsR0FBRztvQkFDakIsS0FBSztvQkFDTCxNQUFNO29CQUNOLFdBQVcsRUFBRSxHQUFHLENBQUMsTUFBTTtvQkFDdkIsT0FBTyxFQUFFLElBQUk7b0JBQ2IsT0FBTyxFQUFFLEdBQUc7aUJBQ2IsQ0FBQzthQUNIO1lBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN0QixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixNQUFNLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBRW5CLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0I7Ozs7Z0JBQUUsVUFBVSxDQUFDLEVBQUU7b0JBQ3ZELFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7b0JBQzNDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDdkUsQ0FBQyxFQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7YUFDL0Q7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUVPLG9CQUFvQixDQUFDLGVBQWU7UUFDMUMsSUFBSSxlQUFlLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTs7a0JBQzFCLE9BQU8sR0FDWCxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7Z0JBQzNDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztnQkFDN0MsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDOztrQkFDMUMsT0FBTyxHQUFHLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUM1RCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLENBQUMsRUFBRTtnQkFDckIsSUFBSSxDQUFDLENBQUMsUUFBUSxFQUFFO29CQUNkLElBQUksT0FBTyxFQUFFO3dCQUNYLElBQUksQ0FBQyxDQUFDLFVBQVUsS0FBSyxXQUFXLENBQUMsS0FBSyxFQUFFOzRCQUN0QyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUN0QjtxQkFDRjt5QkFBTSxJQUFJLE9BQU8sRUFBRTt3QkFDbEIsSUFDRSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs0QkFDdkIsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUNyRDs0QkFDQSxJQUFJLENBQUMsQ0FBQyxVQUFVLEtBQUssV0FBVyxDQUFDLE9BQU8sRUFBRTtnQ0FDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDdEI7eUJBQ0Y7NkJBQU07NEJBQ0wsb0JBQW9COzRCQUNwQixJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dDQUMzQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUN0Qjt5QkFDRjtxQkFDRjt5QkFBTTt3QkFDTCxpQ0FBaUM7d0JBQ2pDLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUU7NEJBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3RCO3FCQUNGO2lCQUNGO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjthQUFNLElBQUksZUFBZSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxDQUFDLFFBQVEsSUFBSSxDQUFDLENBQUMsVUFBVSxLQUFLLFdBQVcsQ0FBQyxLQUFLLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3RCO1lBQ0gsQ0FBQyxFQUFDLENBQUM7U0FDSjtJQUNILENBQUM7Ozs7O0lBRU0sWUFBWSxDQUFDLElBQUk7UUFDdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFdBQVcsQ0FBQyxNQUFNLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN6QjthQUFNO1lBQ0wsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2FBQ3RCO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDMUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O29CQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUNwQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUM1RCxDQUFDLEVBQUMsQ0FBQztpQkFDSjthQUNGO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7SUFHRCxZQUFZLENBQUMsTUFBTTtRQUNqQixRQUFRLE1BQU0sQ0FBQyxJQUFJLEVBQUU7WUFDbkIsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7O29CQUNuQixHQUFHO2dCQUVQLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsRUFBRTtvQkFDekMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDcEU7cUJBQU07b0JBQ0wsR0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7aUJBQzdDO2dCQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM5RCxJQUFJLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxFQUFFO29CQUNoRSxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztpQkFDcEM7Z0JBQ0QsTUFBTTthQUNQO1lBQ0QsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3ZCLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUMzQixNQUFNLENBQUMsTUFBTSxDQUNYLElBQUksQ0FBQyxpQkFBaUIsRUFDdEIsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FDL0MsQ0FBQztnQkFDRixNQUFNO2FBQ1A7WUFDRCxLQUFLLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLHNCQUFzQixDQUFDLENBQUM7Z0JBQ3JFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM5RCxNQUFNO2FBQ1A7WUFDRCxLQUFLLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQzs7c0JBQ3hCLGFBQWEsR0FBRyxFQUFFO2dCQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxPQUFPOzs7O2dCQUFDLENBQUMsQ0FBQyxFQUFFO29CQUNuQyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU87Ozs7b0JBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3BCLElBQ0UsQ0FBQyxDQUFDLElBQUksS0FBSyxXQUFXLENBQUMsYUFBYTs0QkFDcEMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsU0FBUzs0QkFDaEMsQ0FBQyxDQUFDLEtBQUssS0FBSyxNQUFNLENBQUMsS0FBSyxFQUN4Qjs0QkFDQSxhQUFhLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUM7eUJBQ3pDO29CQUNILENBQUMsRUFBQyxDQUFDO2dCQUNMLENBQUMsRUFBQyxDQUFDO2dCQUNILGFBQWEsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztnQkFDbEQsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUM5RCxNQUFNO2FBQ1A7WUFDRCxLQUFLLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFOzswQkFDWCxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUU7d0JBQzFELFNBQVMsRUFBRSxLQUFLO3dCQUNoQixJQUFJLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJO3dCQUN4QixRQUFRLEVBQUUsT0FBTzt3QkFDakIsVUFBVSxFQUFFLFlBQVk7cUJBQ3pCLENBQUM7b0JBQ0YsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVM7Ozs7b0JBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQzVDLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxFQUFFOztrQ0FDdkIsU0FBUyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUNyQyxJQUFJLENBQUMsaUJBQWlCLEVBQ3RCLFNBQVMsQ0FDVjs0QkFDRCxJQUNFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7Z0NBQ3pCLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEVBQ3JDO2dDQUNBLE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDOzZCQUMxQzt5QkFDRjtvQkFDSCxDQUFDLEVBQUMsQ0FBQztpQkFDSjtnQkFDRCxNQUFNO2FBQ1A7WUFDRCxLQUFLLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUN2QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDO29CQUMvQixNQUFNLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtvQkFDOUIsaUJBQWlCLEVBQ2YsTUFBTSxDQUFDLEtBQUssS0FBSyxRQUFRO3dCQUN2QixDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FDWCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQ3JEO3dCQUNILENBQUMsQ0FBQyxFQUFFO2lCQUNULENBQUMsQ0FBQztnQkFDSCxNQUFNO2FBQ1A7WUFDRCxPQUFPLENBQUMsQ0FBQztnQkFDUCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDOUQsTUFBTTthQUNQO1NBQ0Y7SUFDSCxDQUFDOzs7Ozs7SUFHTSxRQUFRLENBQUMsTUFBTTtRQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3BFLENBQUM7Ozs7SUFFTSxrQkFBa0I7UUFDdkIsT0FBTztZQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsaUJBQWlCO1lBQzlCLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVztTQUM5QixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsR0FBRztjQUNSLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsR0FBRyxHQUFHO1FBQ25DLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7c0JBQy9DLElBQUksR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDOztzQkFDZixRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLGVBQWUsRUFBRSxHQUFHLENBQUM7Z0JBRTNELElBQUksQ0FBQyxXQUFXO3FCQUNiLFlBQVksQ0FBQztvQkFDWixRQUFRO29CQUNSLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSTtvQkFDdEIsYUFBYSxFQUFFLElBQUk7aUJBQ3BCLENBQUM7cUJBQ0QsU0FBUzs7OztnQkFBQyxhQUFhLENBQUMsRUFBRTtvQkFDekIsSUFBSSxDQUFDLFNBQVM7eUJBQ1gsTUFBTSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUM7eUJBQzFDLFNBQVM7Ozs7b0JBQUMsVUFBVSxDQUFDLEVBQUU7OzhCQUNoQixPQUFPLEdBQUcsYUFBYSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMvQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7d0JBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDM0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7b0JBQ3ZCLENBQUMsRUFBQyxDQUFDO2dCQUNQLENBQUMsRUFBQyxDQUFDO2FBQ047U0FDRjtJQUNILENBQUM7Ozs7OztJQUlELG1CQUFtQixDQUFDLEtBQW9CO1FBQ3RDLElBQ0UsSUFBSSxDQUFDLGdCQUFnQjtZQUNyQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDO2dCQUMvQixLQUFLLENBQUMsUUFBUTtnQkFDZCxLQUFLLENBQUMsT0FBTyxLQUFLLEVBQUUsQ0FBQyxFQUN2QjtZQUNBLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDOUQ7UUFFRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3RFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDbEU7UUFFRCxJQUFJLElBQUksQ0FBQyxvQkFBb0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxDQUFDLENBQUMsRUFBRTtZQUN4RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDdEU7UUFFRCxJQUNFLElBQUksQ0FBQyx3QkFBd0I7WUFDN0IsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxDQUFDLEVBQ3hDO1lBQ0EsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNoRTtRQUVELElBQ0UsSUFBSSxDQUFDLHVCQUF1QjtZQUM1QixDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxFQUFFLENBQUMsRUFDeEM7WUFDQSxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQy9EO0lBQ0gsQ0FBQzs7O1lBNW9CRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IseTFSQUE2Qzs7YUFFOUM7Ozs7WUFUUSxTQUFTOzs7K0JBV2YsS0FBSzs2QkFDTCxLQUFLO3lCQUNMLEtBQUs7aUNBQ0wsS0FBSzswQkFDTCxLQUFLO3FDQUNMLE1BQU07OENBQ04sTUFBTTt5QkFFTixTQUFTLFNBQUMsWUFBWTtrQ0E4bEJ0QixZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUF0bUIxQyxpREFBa0M7O0lBQ2xDLCtDQUFnQzs7SUFDaEMsMkNBQXlCOztJQUN6QixtREFBaUM7O0lBQ2pDLDRDQUEwQjs7SUFDMUIsdURBQThEOztJQUM5RCxnRUFBdUU7O0lBRXZFLDJDQUFnRDs7SUFFaEQsNENBQTBCOztJQUMxQixvREFBMEM7O0lBRTFDLGlEQUE0RDs7SUFDNUQsb0RBQXlCOztJQUN6QixrREFBdUI7O0lBQ3ZCLDRDQU1FOztJQUVGLHNDQUFXOztJQUNYLDJDQUFnQjs7SUFDaEIsZ0RBQXFCOztJQUNyQixvREFBNkI7O0lBQzdCLDhDQUF1Qjs7SUFDdkIsMkRBQW9DOztJQUNwQyx3Q0FBaUI7O0lBQ2pCLHVDQUFnQjs7SUFDaEIsNkNBQXNCOztJQUN0Qiw0Q0FBcUI7O0lBQ3JCLDhEQUF1Qzs7SUFDdkMsd0RBQWlDOztJQUNqQywwQ0FBZTs7SUFDZiw0Q0FBaUI7O0lBQ2pCLDBDQUFlOztJQUNmLDJDQUFnQjs7SUFDaEIsNENBQWlCOztJQUNqQiwwQ0FBZTs7SUFHZiwyREFBb0M7O0lBQ3BDLGlEQUEwQjs7SUFDMUIsa0RBQTJCOztJQUMzQixxREFBOEI7O0lBQzlCLHlEQUFrQzs7SUFDbEMsd0RBQWlDOztJQUdqQyx3Q0FBUTs7SUFDUix3Q0FBUTs7SUFDUiwyQ0FBVzs7SUFDWCwwQ0FBVTs7SUFDViw0Q0FBWTs7SUFFWiwyQ0FBb0I7O0lBQ3BCLHlDQUFpQjs7SUFDakIseUNBQWlCOztJQUNqQix3Q0FBWTs7Ozs7SUFFQSx1Q0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsXG4gIE9uSW5pdCxcbiAgT25DaGFuZ2VzLFxuICBFdmVudEVtaXR0ZXIsXG4gIElucHV0LFxuICBPdXRwdXQsXG4gIFZpZXdDaGlsZCxcbiAgRWxlbWVudFJlZixcbiAgSG9zdExpc3RlbmVyLFxuICBTaW1wbGVDaGFuZ2VzXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0ICdmYWJyaWMnO1xuZGVjbGFyZSBjb25zdCBmYWJyaWM6IGFueTtcbmRlY2xhcmUgbGV0IEZvbnRGYWNlOiBhbnk7XG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5cbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vY29uZmlncy9hY3Rpb24tdHlwZXMnO1xuaW1wb3J0IHsgVmlzdWFsRWRpdG9yU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLXZpc3VhbC1lZGl0b3InLFxuICB0ZW1wbGF0ZVVybDogJy4vdmlzdWFsLWVkaXRvci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3Zpc3VhbC1lZGl0b3IuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBWaXN1YWxFZGl0b3JDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XG4gIEBJbnB1dCgpIG9yaWdpbmFsSW1hZ2VVcmw6IHN0cmluZztcbiAgQElucHV0KCkgZWRpdGVkSW1hZ2VVcmw6IHN0cmluZztcbiAgQElucHV0KCkgdmlzdWFsSnNvbjogYW55O1xuICBASW5wdXQoKSB2aXN1YWxFZGl0b3JDb25maWc6IGFueTtcbiAgQElucHV0KCkgY3VzdG9tRm9udHMgPSBbXTtcbiAgQE91dHB1dCgpIG91dGdvaW5nRXZlbnRUcmlnZ2VyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcbiAgQE91dHB1dCgpIG91dGdvaW5nU2VsZWN0aW9uRXZlbnRUcmlnZ2VyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcblxuICBAVmlld0NoaWxkKCd3aWRlQ2FudmFzJykgd2lkZUNhbnZhczogRWxlbWVudFJlZjtcblxuICBBY3Rpb25UeXBlcyA9IEFjdGlvblR5cGVzO1xuICBWaXN1YWxFZGl0b3JTZXJ2aWNlID0gVmlzdWFsRWRpdG9yU2VydmljZTtcblxuICBjaGVja2Vyc0ltYWdlVXJsID0gJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9jaGVja2Vyc19iZzIucG5nJztcbiAgb3JpZ2luYWxJbWFnZUNhbnZhczogYW55O1xuICBlZGl0ZWRJbWFnZUNhbnZhczogYW55O1xuICBpbWFnZUNvbmZpZyA9IHtcbiAgICB3aWR0aDogMCxcbiAgICBoZWlnaHQ6IDAsXG4gICAgc2NhbGVGYWN0b3I6IDEsXG4gICAgeE9mZnNldDogMCxcbiAgICB5T2Zmc2V0OiAwXG4gIH07XG5cbiAgdG9vbHMgPSBbXTtcbiAgYWN0aXZlVG9vbDogYW55O1xuICBvYmplY3RUeXBlQ291bnQ6IGFueTtcbiAgaXNGaXJzdFRvb2xTZWxlY3RlZDogYm9vbGVhbjtcbiAgaGFzQ2hlY2tlcnNCZzogYm9vbGVhbjtcbiAgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHM6IGJvb2xlYW47XG4gIGhhc1pvb206IGJvb2xlYW47XG4gIGhhc1BhbjogYm9vbGVhbjtcbiAgaXNXaWRlQ2FudmFzOiBib29sZWFuO1xuICBpc1Nob3dGcmFtZTogYm9vbGVhbjtcbiAgcmVzaXplQ2FudmFzVG9CYWNrZ3JvdW5kSW1hZ2U6IGJvb2xlYW47XG4gIGlzQ2VudGVyQmFja2dyb3VuZEltYWdlOiBib29sZWFuO1xuICB0b29sc0ZsZXg6IGFueTtcbiAgYWN0aW9uc0ZsZXg6IGFueTtcbiAgc2VsZWN0aW9uOiBhbnk7XG4gIGZpbGVVcGxvYWQ6IGFueTtcbiAgZmlsZVNlcnZpY2U6IGFueTtcbiAgczNTZXJ2aWNlOiBhbnk7XG5cbiAgLy8gS2V5Ym9hcmQgU2hvcnRjdXRzXG4gIGhhc1Nob3J0Y3V0c0V4cGFuc2lvblBhbmVsOiBib29sZWFuO1xuICBoYXNDbG9uZVNob3J0Y3V0OiBib29sZWFuO1xuICBoYXNSZW1vdmVTaG9ydGN1dDogYm9vbGVhbjtcbiAgaGFzUmVtb3ZlQWxsU2hvcnRjdXQ6IGJvb2xlYW47XG4gIGhhc1NlbmRCYWNrd2FyZHNTaG9ydGN1dDogYm9vbGVhbjtcbiAgaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQ6IGJvb2xlYW47XG5cbiAgLy8gRm9udEF3ZXNvbWUgSWNvbnNcbiAgZmFDbG9uZTtcbiAgZmFUcmFzaDtcbiAgZmFUcmFzaEFsdDtcbiAgZmFBcnJvd1VwO1xuICBmYUFycm93RG93bjtcblxuICBpc0RyYWdnaW5nOiBib29sZWFuO1xuICBsYXN0UG9zWTogbnVtYmVyO1xuICBsYXN0UG9zWDogbnVtYmVyO1xuICBtaW5ab29tID0gMTtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRpYWxvZzogTWF0RGlhbG9nKSB7fVxuXG4gIC8vIFN0YXJ0IG9mIG5nT25Jbml0IGZ1bmN0aW9uXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuYWRkQ3VzdG9tRm9udHMoKTtcbiAgICB0aGlzLmxvYWRDdXN0b21Gb250cygpO1xuXG4gICAgdGhpcy50b29scyA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnRvb2xzO1xuICAgIHRoaXMuaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmlzRmlyc3RUb29sU2VsZWN0ZWQ7XG4gICAgdGhpcy5oYXNDaGVja2Vyc0JnID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzQ2hlY2tlcnNCZztcbiAgICB0aGlzLmhhc0hpZGRlblNlbGVjdGlvbkNvbnRyb2xzID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHM7XG4gICAgdGhpcy5oYXNab29tID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzWm9vbTtcbiAgICB0aGlzLmhhc1BhbiA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmhhc1BhbjtcbiAgICB0aGlzLmlzV2lkZUNhbnZhcyA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmlzV2lkZUNhbnZhcztcbiAgICB0aGlzLmlzU2hvd0ZyYW1lID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaXNTaG93RnJhbWU7XG4gICAgdGhpcy5yZXNpemVDYW52YXNUb0JhY2tncm91bmRJbWFnZSA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnJlc2l6ZUNhbnZhc1RvQmFja2dyb3VuZEltYWdlO1xuICAgIHRoaXMuaXNDZW50ZXJCYWNrZ3JvdW5kSW1hZ2UgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5pc0NlbnRlckJhY2tncm91bmRJbWFnZTtcbiAgICB0aGlzLnRvb2xzRmxleCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLnRvb2xzRmxleDtcbiAgICB0aGlzLmFjdGlvbnNGbGV4ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuYWN0aW9uc0ZsZXg7XG5cbiAgICAvLyBLZXlib2FyZCBTaG9ydGN1dHNcbiAgICB0aGlzLmhhc1Nob3J0Y3V0c0V4cGFuc2lvblBhbmVsID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzU2hvcnRjdXRzRXhwYW5zaW9uUGFuZWw7XG4gICAgdGhpcy5oYXNDbG9uZVNob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzQ2xvbmVTaG9ydGN1dDtcbiAgICB0aGlzLmhhc1JlbW92ZVNob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzUmVtb3ZlU2hvcnRjdXQ7XG4gICAgdGhpcy5oYXNSZW1vdmVBbGxTaG9ydGN1dCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmhhc1JlbW92ZUFsbFNob3J0Y3V0O1xuICAgIHRoaXMuaGFzU2VuZEJhY2t3YXJkc1Nob3J0Y3V0ID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuaGFzU2VuZEJhY2t3YXJkc1Nob3J0Y3V0O1xuICAgIHRoaXMuaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5oYXNCcmluZ0ZvcndhcmRTaG9ydGN1dDtcblxuICAgIC8vIEZvbnRBd2Vzb21lIEljb25zXG4gICAgdGhpcy5mYUNsb25lID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuZmFDbG9uZTtcbiAgICB0aGlzLmZhVHJhc2ggPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5mYVRyYXNoO1xuICAgIHRoaXMuZmFUcmFzaEFsdCA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZhVHJhc2hBbHQ7XG4gICAgdGhpcy5mYUFycm93VXAgPSB0aGlzLnZpc3VhbEVkaXRvckNvbmZpZy5mYUFycm93VXA7XG4gICAgdGhpcy5mYUFycm93RG93biA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZhQXJyb3dEb3duO1xuXG4gICAgdGhpcy5maWxlU2VydmljZSA9IHRoaXMudmlzdWFsRWRpdG9yQ29uZmlnLmZpbGVTZXJ2aWNlO1xuICAgIHRoaXMuczNTZXJ2aWNlID0gdGhpcy52aXN1YWxFZGl0b3JDb25maWcuczNTZXJ2aWNlO1xuXG4gICAgaWYgKCF0aGlzLmlzV2lkZUNhbnZhcykge1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyA9IG5ldyBmYWJyaWMuQ2FudmFzKCdlZGl0ZWRJbWFnZUNhbnZhcycsIHtcbiAgICAgICAgaXNEcmF3aW5nTW9kZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMgPSBuZXcgZmFicmljLkNhbnZhcygnd2lkZUNhbnZhcycsIHtcbiAgICAgICAgaXNEcmF3aW5nTW9kZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgdGhpcy5sb2FkQ2FudmFzKCk7XG5cbiAgICBpZiAodGhpcy5oYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scykge1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOmNyZWF0ZWQnLCBlID0+IHtcbiAgICAgICAgdGhpcy5zZWxlY3RIaWRkZW5Db250cm9scyhlLnNlbGVjdGVkKTtcbiAgICAgIH0pO1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOnVwZGF0ZWQnLCBlID0+IHtcbiAgICAgICAgaWYgKFxuICAgICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCkuaXNUeXBlKCdhY3RpdmVTZWxlY3Rpb24nKVxuICAgICAgICApIHtcbiAgICAgICAgICB0aGlzLnNlbGVjdEhpZGRlbkNvbnRyb2xzKFxuICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKS5nZXRPYmplY3RzKClcbiAgICAgICAgICApO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuc2VsZWN0SGlkZGVuQ29udHJvbHMoZS5zZWxlY3RlZCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5vbignc2VsZWN0aW9uOmNsZWFyZWQnLCBlID0+IHtcbiAgICAgICAgdGhpcy5hY3RpdmVUb29sID0gdGhpcy50b29sc1swXTsgLy8gQ29kZSBjaGFuZ2VkIHRvIHJlbmRlciB0aGUgJ1JlbW92ZSBBbGwnIGFuZCAnU3VibWl0JyBidXR0b25zIGFsbCB0aGUgdGltZVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaXNGaXJzdFRvb2xTZWxlY3RlZCkge1xuICAgICAgdGhpcy5hY3RpdmVUb29sID0gdGhpcy50b29sc1swXTtcbiAgICAgIHRoaXMuYWN0aXZlVG9vbC5jYW52YXNDb25maWdzLmZvckVhY2goY29uZkZ1bmMgPT4ge1xuICAgICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzID0gY29uZkZ1bmModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5hY3RpdmVUb29sID0ge307XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzWm9vbSkge1xuICAgICAgdGhpcy5hZGRab29tQ29udHJvbHModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICBpZiAodGhpcy5vcmlnaW5hbEltYWdlVXJsKSB7XG4gICAgICAgIHRoaXMuYWRkWm9vbUNvbnRyb2xzKHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzUGFuKSB7XG4gICAgICB0aGlzLmFkZFBhbkNvbnRyb2xzKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgaWYgKHRoaXMub3JpZ2luYWxJbWFnZVVybCkge1xuICAgICAgICB0aGlzLmFkZFBhbkNvbnRyb2xzKHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcyk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIC8vIEVuZCBvZiBuZ09uSW5pdCBmdW5jdGlvblxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICBpZiAoIWNoYW5nZXMudmlzdWFsSnNvbi5pc0ZpcnN0Q2hhbmdlKCkpIHtcbiAgICAgIHRoaXMubG9hZENhbnZhcygpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBsb2FkQ2FudmFzKCkge1xuICAgIHRoaXMuVmlzdWFsRWRpdG9yU2VydmljZS5yZXNldE9iamVjdFR5cGVDb3VudCgpO1xuICAgIGlmICh0aGlzLnZpc3VhbEpzb24pIHtcbiAgICAgIHRoaXMubG9hZEpzb24odGhpcy52aXN1YWxKc29uLCB0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLnNldFNjYWxlZEltYWdlVG9DYW52YXMoXG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsXG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VVcmwsXG4gICAgICAgIHRydWVcbiAgICAgICk7XG4gICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgIGlmICh0aGlzLm9yaWdpbmFsSW1hZ2VVcmwpIHtcbiAgICAgICAgdGhpcy5vcmlnaW5hbEltYWdlQ2FudmFzID0gbmV3IGZhYnJpYy5TdGF0aWNDYW52YXMoXG4gICAgICAgICAgJ29yaWdpbmFsSW1hZ2VDYW52YXMnXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyhcbiAgICAgICAgICB0aGlzLm9yaWdpbmFsSW1hZ2VDYW52YXMsXG4gICAgICAgICAgdGhpcy5vcmlnaW5hbEltYWdlVXJsXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMub3JpZ2luYWxJbWFnZUNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBhZGRDdXN0b21Gb250cygpIHtcbiAgICBfLmVhY2godGhpcy52aXN1YWxFZGl0b3JDb25maWcudG9vbHMsIHQgPT4ge1xuICAgICAgXy5lYWNoKHQuc2VjdGlvbnMsIHMgPT4ge1xuICAgICAgICBfLmVhY2gocy5hY3Rpb25zLCBhID0+IHtcbiAgICAgICAgICBpZiAoYS5sYWJlbCA9PT0gJ0ZPTlQgRkFNSUxZJykge1xuICAgICAgICAgICAgYS5kcm9wZG93bk9wdGlvbiA9IF8uc29ydEJ5KFxuICAgICAgICAgICAgICBfLnVuaW9uKFxuICAgICAgICAgICAgICAgIGEuZHJvcGRvd25PcHRpb24sXG4gICAgICAgICAgICAgICAgXy5tYXAodGhpcy5jdXN0b21Gb250cywgZiA9PiBmLmZvbnRGYW1pbHkpXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBsb2FkQ3VzdG9tRm9udHMoKSB7XG4gICAgLy8gRklYTUU6IElFIHN1cHBvcnRcbiAgICBjb25zdCBjdXN0b21Gb250RmFjZXMgPSBfLm1hcChcbiAgICAgIHRoaXMuY3VzdG9tRm9udHMsXG4gICAgICBmID0+IG5ldyBGb250RmFjZShmLmZvbnRGYW1pbHksIGB1cmwoJHtmLmZvbnRGaWxlVXJsfSlgKVxuICAgICk7XG4gICAgXy5lYWNoKGN1c3RvbUZvbnRGYWNlcywgY2ZmID0+IHtcbiAgICAgIC8vIE5PVEU6IG5lZWQgdG8gc3BlY2lmeSBhIGZvbnQgc2l6ZSBmb3IgY2hlY2soKSBmdW5jdGlvblxuICAgICAgaWYgKCEoZG9jdW1lbnQgYXMgYW55KS5mb250cy5jaGVjayhgMTJweCAke2NmZi5mYW1pbHl9YCkpIHtcbiAgICAgICAgY2ZmXG4gICAgICAgICAgLmxvYWQoKVxuICAgICAgICAgIC50aGVuKHJlcyA9PiB7XG4gICAgICAgICAgICAoZG9jdW1lbnQgYXMgYW55KS5mb250cy5hZGQocmVzKTtcbiAgICAgICAgICB9KVxuICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgICAgICAvLyBGSVhNRTogRXJyb3IgZGlzcGxheVxuICAgICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBhZGRab29tQ29udHJvbHMoY2FudmFzKSB7XG4gICAgY2FudmFzLm9uKCdtb3VzZTp3aGVlbCcsIG9wdCA9PiB7XG4gICAgICBjb25zdCBkZWx0YSA9IG9wdC5lLmRlbHRhWTtcbiAgICAgIGxldCB6b29tID0gY2FudmFzLmdldFpvb20oKTtcblxuICAgICAgem9vbSA9IHpvb20gLSBkZWx0YSAvIF8ubWluKFtjYW52YXMuZ2V0V2lkdGgoKSwgY2FudmFzLmdldEhlaWdodCgpXSk7XG4gICAgICBpZiAoem9vbSA+IDIwKSB7XG4gICAgICAgIHpvb20gPSAyMDtcbiAgICAgIH1cbiAgICAgIGlmICh6b29tIDwgdGhpcy5taW5ab29tKSB7XG4gICAgICAgIHpvb20gPSB0aGlzLm1pblpvb207XG4gICAgICB9XG5cbiAgICAgIGNhbnZhcy56b29tVG9Qb2ludCh7IHg6IG9wdC5lLm9mZnNldFgsIHk6IG9wdC5lLm9mZnNldFkgfSwgem9vbSk7XG5cbiAgICAgIG9wdC5lLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBvcHQuZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgYWRkUGFuQ29udHJvbHMoY2FudmFzKSB7XG4gICAgY2FudmFzLm9uKCdtb3VzZTpkb3duJywgb3B0ID0+IHtcbiAgICAgIGNvbnN0IGV2dCA9IG9wdC5lO1xuICAgICAgaWYgKGV2dC5hbHRLZXkgPT09IHRydWUpIHtcbiAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gdHJ1ZTtcbiAgICAgICAgY2FudmFzLnNlbGVjdGlvbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLmxhc3RQb3NYID0gZXZ0LmNsaWVudFg7XG4gICAgICAgIHRoaXMubGFzdFBvc1kgPSBldnQuY2xpZW50WTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjYW52YXMub24oJ21vdXNlOm1vdmUnLCBvcHQgPT4ge1xuICAgICAgaWYgKHRoaXMuaXNEcmFnZ2luZykge1xuICAgICAgICBjb25zdCBlID0gb3B0LmU7XG4gICAgICAgIGNhbnZhcy52aWV3cG9ydFRyYW5zZm9ybVs0XSArPSBlLmNsaWVudFggLSB0aGlzLmxhc3RQb3NYO1xuICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNV0gKz0gZS5jbGllbnRZIC0gdGhpcy5sYXN0UG9zWTtcbiAgICAgICAgY2FudmFzLmZvckVhY2hPYmplY3QobyA9PiBvLnNldENvb3JkcygpKTtcbiAgICAgICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgICAgdGhpcy5sYXN0UG9zWCA9IGUuY2xpZW50WDtcbiAgICAgICAgdGhpcy5sYXN0UG9zWSA9IGUuY2xpZW50WTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjYW52YXMub24oJ21vdXNlOnVwJywgb3B0ID0+IHtcbiAgICAgIHRoaXMuaXNEcmFnZ2luZyA9IGZhbHNlO1xuICAgICAgY2FudmFzLnNlbGVjdGlvbiA9IHRydWU7XG4gICAgfSk7XG4gIH1cblxuICAvLyBTdGFydCBvZiBsb2FkSnNvbiBmdW5jdGlvblxuICBwcml2YXRlIGxvYWRKc29uKGlucHV0SnNvbiwgY2FudmFzKSB7XG4gICAgY29uc3QgaW1nID0gbmV3IEltYWdlKCk7XG4gICAgaW1nLm9ubG9hZCA9ICgpID0+IHtcbiAgICAgIC8vIEV4dHJhY3RpbmcgdGhlIGltYWdlcyB3aWR0aCBhbmQgaGVpZ2h0IGFuZCBhZGRpbmcgaXQgaW50byB0aGUgX2NhbnZhc0RpbWVuc2lvbnMgcHJvcGVydHkgaW4gdGhlIGlucHV0SnNvblxuICAgICAgaWYgKHRoaXMucmVzaXplQ2FudmFzVG9CYWNrZ3JvdW5kSW1hZ2UpIHtcbiAgICAgICAgaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zID0geyB3aWR0aDogaW1nLndpZHRoLCBoZWlnaHQ6IGltZy5oZWlnaHQgfTtcbiAgICAgICAgaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS53aWR0aCA9IGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aDtcbiAgICAgICAgaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS5oZWlnaHQgPSBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0O1xuICAgICAgfVxuXG4gICAgICBjYW52YXMubG9hZEZyb21KU09OKGlucHV0SnNvbiwgKCkgPT4ge1xuICAgICAgICBjb25zdCBzY2FsZUZhY3RvciA9IF8ubWluKFtcbiAgICAgICAgICBjYW52YXMuZ2V0V2lkdGgoKSAvIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCxcbiAgICAgICAgICBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgIF0pO1xuICAgICAgICB0aGlzLm1pblpvb20gPSBzY2FsZUZhY3RvcjtcblxuICAgICAgICBpZiAoIXRoaXMuaXNXaWRlQ2FudmFzKSB7XG4gICAgICAgICAgY2FudmFzLnNldFdpZHRoKGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCAqIHNjYWxlRmFjdG9yKTtcbiAgICAgICAgICBjYW52YXMuc2V0SGVpZ2h0KGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy5oZWlnaHQgKiBzY2FsZUZhY3Rvcik7XG4gICAgICAgICAgY2FudmFzLnNldFpvb20odGhpcy5taW5ab29tKTtcblxuICAgICAgICAgIGNvbnN0IGlzV2lkdGhTbWFsbGVyID1cbiAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCAvIGNhbnZhcy5iYWNrZ3JvdW5kSW1hZ2Uud2lkdGggPlxuICAgICAgICAgICAgaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLmhlaWdodCAvIGNhbnZhcy5iYWNrZ3JvdW5kSW1hZ2UuaGVpZ2h0O1xuICAgICAgICAgIGlmIChpc1dpZHRoU21hbGxlcikge1xuICAgICAgICAgICAgY2FudmFzLmJhY2tncm91bmRJbWFnZS5zY2FsZVRvV2lkdGgoXG4gICAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2FudmFzLmJhY2tncm91bmRJbWFnZS5zY2FsZVRvSGVpZ2h0KFxuICAgICAgICAgICAgICBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgICAgICApO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjb25zdCBwYWRkaW5nID0gNTtcbiAgICAgICAgICBjb25zdCBib3JkZXIgPSAwO1xuICAgICAgICAgIGNvbnN0IG5ld1dpZHRoID1cbiAgICAgICAgICAgIHRoaXMud2lkZUNhbnZhcy5uYXRpdmVFbGVtZW50Lm9mZnNldFdpZHRoIC0gMiAqIChwYWRkaW5nICsgYm9yZGVyKTtcbiAgICAgICAgICBjb25zdCBuZXdIZWlnaHQgPVxuICAgICAgICAgICAgdGhpcy53aWRlQ2FudmFzLm5hdGl2ZUVsZW1lbnQub2Zmc2V0SGVpZ2h0IC0gMiAqIChwYWRkaW5nICsgYm9yZGVyKTtcbiAgICAgICAgICBjYW52YXMuc2V0V2lkdGgobmV3V2lkdGgpO1xuICAgICAgICAgIGNhbnZhcy5zZXRIZWlnaHQobmV3SGVpZ2h0KTtcbiAgICAgICAgICBjYW52YXMuc2V0Wm9vbShcbiAgICAgICAgICAgIDAuOCAqXG4gICAgICAgICAgICAgIF8ubWluKFtcbiAgICAgICAgICAgICAgICBjYW52YXMuZ2V0V2lkdGgoKSAvIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy53aWR0aCxcbiAgICAgICAgICAgICAgICBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0XG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgKTtcbiAgICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNF0gPVxuICAgICAgICAgICAgKG5ld1dpZHRoIC0gaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLndpZHRoICogY2FudmFzLmdldFpvb20oKSkgL1xuICAgICAgICAgICAgMjtcbiAgICAgICAgICBjYW52YXMudmlld3BvcnRUcmFuc2Zvcm1bNV0gPVxuICAgICAgICAgICAgKG5ld0hlaWdodCAtXG4gICAgICAgICAgICAgIGlucHV0SnNvbi5fY2FudmFzRGltZW5zaW9ucy5oZWlnaHQgKiBjYW52YXMuZ2V0Wm9vbSgpKSAvXG4gICAgICAgICAgICAyO1xuICAgICAgICAgIGNhbnZhcy5mb3JFYWNoT2JqZWN0KG8gPT4gby5zZXRDb29yZHMoKSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0NlbnRlckJhY2tncm91bmRJbWFnZSkge1xuICAgICAgICAgIGNhbnZhcy52aWV3cG9ydENlbnRlck9iamVjdChjYW52YXMuYmFja2dyb3VuZEltYWdlKTsgLy8gQ2VudGVycyB0aGUgaW1hZ2Ugb24gdGhlIGNhbnZhc1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNTaG93RnJhbWUpIHtcbiAgICAgICAgICBjb25zdCBzdHJva2VXaWR0aCA9IDM7XG4gICAgICAgICAgY29uc3QgcmVjdGFuZ2xlID0gbmV3IGZhYnJpYy5SZWN0KHtcbiAgICAgICAgICAgIGxlZnQ6IC1zdHJva2VXaWR0aCxcbiAgICAgICAgICAgIHRvcDogLXN0cm9rZVdpZHRoLFxuICAgICAgICAgICAgaGVpZ2h0OiBpbnB1dEpzb24uX2NhbnZhc0RpbWVuc2lvbnMuaGVpZ2h0ICsgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICB3aWR0aDogaW5wdXRKc29uLl9jYW52YXNEaW1lbnNpb25zLndpZHRoICsgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICBzdHJva2U6ICdyZ2IoMCwyNTUsMCknLFxuICAgICAgICAgICAgc3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICBzdHJva2VEYXNoQXJyYXk6IFs1ICogc3Ryb2tlV2lkdGgsIDUgKiBzdHJva2VXaWR0aF0sXG4gICAgICAgICAgICBmaWxsOiAncmdiYSgwLDAsMCwwKScsXG4gICAgICAgICAgICBzZWxlY3RhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIGV2ZW50ZWQ6IGZhbHNlXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmVjdGFuZ2xlLl9rZXkgPSAnZnJhbWVfcmVjdGFuZ2xlJztcbiAgICAgICAgICBjYW52YXMuYWRkKHJlY3RhbmdsZSk7XG4gICAgICAgICAgcmVjdGFuZ2xlLmJyaW5nVG9Gcm9udCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgY2FudmFzLnJlbmRlckFsbCgpO1xuICAgICAgfSk7XG4gICAgfTtcbiAgICBpbWcuc3JjID0gaW5wdXRKc29uLmJhY2tncm91bmRJbWFnZS5zcmM7XG4gIH1cbiAgLy8gRW5kIG9mIGxvYWRKc29uIGZ1bmN0aW9uXG5cbiAgcHJpdmF0ZSByZXNldENhbnZhcyhjYW52YXMpIHtcbiAgICBjYW52YXMuZ2V0T2JqZWN0cygpLmZvckVhY2gobyA9PiBjYW52YXMucmVtb3ZlKG8pKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyhjYW52YXMsIGltYWdlVXJsLCBpc0VkaXRlZEltYWdlID0gZmFsc2UpIHtcbiAgICBmYWJyaWMuSW1hZ2UuZnJvbVVSTChpbWFnZVVybCwgaW1nID0+IHtcbiAgICAgIGNvbnN0IGlzV2lkdGhTbWFsbGVyID1cbiAgICAgICAgY2FudmFzLmdldFdpZHRoKCkgLyBpbWcud2lkdGggPiBjYW52YXMuZ2V0SGVpZ2h0KCkgLyBpbWcuaGVpZ2h0O1xuICAgICAgaWYgKGlzV2lkdGhTbWFsbGVyKSB7XG4gICAgICAgIGltZy5zY2FsZVRvSGVpZ2h0KGNhbnZhcy5nZXRIZWlnaHQoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpbWcuc2NhbGVUb1dpZHRoKGNhbnZhcy5nZXRXaWR0aCgpKTtcbiAgICAgIH1cblxuICAgICAgY29uc3QgeyB3aWR0aCwgaGVpZ2h0IH0gPSBpbWc7XG4gICAgICBjb25zdCBsZWZ0ID0gaXNXaWR0aFNtYWxsZXJcbiAgICAgICAgPyAoY2FudmFzLmdldFdpZHRoKCkgLSB3aWR0aCAqIGltZy5zY2FsZVgpIC8gMi4wXG4gICAgICAgIDogMDtcbiAgICAgIGNvbnN0IHRvcCA9IGlzV2lkdGhTbWFsbGVyXG4gICAgICAgID8gMFxuICAgICAgICA6IChjYW52YXMuZ2V0SGVpZ2h0KCkgLSBoZWlnaHQgKiBpbWcuc2NhbGVYKSAvIDIuMDtcblxuICAgICAgaW1nLnNldCh7XG4gICAgICAgIGxlZnQsXG4gICAgICAgIHRvcFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChpc0VkaXRlZEltYWdlKSB7XG4gICAgICAgIHRoaXMuaW1hZ2VDb25maWcgPSB7XG4gICAgICAgICAgd2lkdGgsXG4gICAgICAgICAgaGVpZ2h0LFxuICAgICAgICAgIHNjYWxlRmFjdG9yOiBpbWcuc2NhbGVYLFxuICAgICAgICAgIHhPZmZzZXQ6IGxlZnQsXG4gICAgICAgICAgeU9mZnNldDogdG9wXG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIGlmICh0aGlzLmhhc0NoZWNrZXJzQmcpIHtcbiAgICAgICAgY2FudmFzLmFkZChpbWcpO1xuICAgICAgICBjYW52YXMucmVuZGVyQWxsKCk7XG5cbiAgICAgICAgZmFicmljLkltYWdlLmZyb21VUkwodGhpcy5jaGVja2Vyc0ltYWdlVXJsLCBjaGVja2VySW1nID0+IHtcbiAgICAgICAgICBjaGVja2VySW1nLnNjYWxlVG9XaWR0aChjYW52YXMuZ2V0V2lkdGgoKSk7XG4gICAgICAgICAgY2FudmFzLnNldEJhY2tncm91bmRJbWFnZShjaGVja2VySW1nLCBjYW52YXMucmVuZGVyQWxsLmJpbmQoY2FudmFzKSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgY2FudmFzLnNldEJhY2tncm91bmRJbWFnZShpbWcsIGNhbnZhcy5yZW5kZXJBbGwuYmluZChjYW52YXMpKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgc2VsZWN0SGlkZGVuQ29udHJvbHMoc2VsZWN0ZWRPYmplY3RzKSB7XG4gICAgaWYgKHNlbGVjdGVkT2JqZWN0cy5sZW5ndGggPT09IDEpIHtcbiAgICAgIGNvbnN0IGlzU2hhcGUgPVxuICAgICAgICBzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKE9iamVjdFR5cGVzLlJFQ1QpIHx8XG4gICAgICAgIHNlbGVjdGVkT2JqZWN0c1swXS5pc1R5cGUoT2JqZWN0VHlwZXMuQ0lSQ0xFKSB8fFxuICAgICAgICBzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKE9iamVjdFR5cGVzLlBPTFlHT04pO1xuICAgICAgY29uc3QgaXNJbWFnZSA9IHNlbGVjdGVkT2JqZWN0c1swXS5pc1R5cGUoT2JqZWN0VHlwZXMuSU1BR0UpO1xuICAgICAgdGhpcy50b29scy5mb3JFYWNoKHQgPT4ge1xuICAgICAgICBpZiAodC5pc0hpZGRlbikge1xuICAgICAgICAgIGlmIChpc1NoYXBlKSB7XG4gICAgICAgICAgICBpZiAodC5vYmplY3RUeXBlID09PSBPYmplY3RUeXBlcy5TSEFQRSkge1xuICAgICAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9IGVsc2UgaWYgKGlzSW1hZ2UpIHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgc2VsZWN0ZWRPYmplY3RzWzBdLl9rZXkgJiZcbiAgICAgICAgICAgICAgc2VsZWN0ZWRPYmplY3RzWzBdLl9rZXkuaW5jbHVkZXMoT2JqZWN0VHlwZXMuUFJPRFVDVClcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICBpZiAodC5vYmplY3RUeXBlID09PSBPYmplY3RUeXBlcy5QUk9EVUNUKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vblNlbGVjdFRvb2wodCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIC8vIE5vbiBwcm9kdWN0IGltYWdlXG4gICAgICAgICAgICAgIGlmIChzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKHQub2JqZWN0VHlwZSkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAvLyBOb24gc2hhcGUgYW5kIG5vbiBpbWFnZSBvYmplY3RcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZE9iamVjdHNbMF0uaXNUeXBlKHQub2JqZWN0VHlwZSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5vblNlbGVjdFRvb2wodCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKHNlbGVjdGVkT2JqZWN0cy5sZW5ndGggPiAxKSB7XG4gICAgICB0aGlzLnRvb2xzLmZvckVhY2godCA9PiB7XG4gICAgICAgIGlmICh0LmlzSGlkZGVuICYmIHQub2JqZWN0VHlwZSA9PT0gT2JqZWN0VHlwZXMuR1JPVVApIHtcbiAgICAgICAgICB0aGlzLm9uU2VsZWN0VG9vbCh0KTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIG9uU2VsZWN0VG9vbCh0b29sKSB7XG4gICAgaWYgKHRvb2wudHlwZSA9PT0gQWN0aW9uVHlwZXMuVVBMT0FEKSB7XG4gICAgICB0aGlzLmZpbGVVcGxvYWQuY2xpY2soKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKCFfLmlzVW5kZWZpbmVkKHRvb2wub25TZWxlY3QpKSB7XG4gICAgICAgIHRvb2wub25TZWxlY3QodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgdG9vbC5vblNlbGVjdFBhcmFtcyk7XG4gICAgICAgIHRoaXMuYWN0aXZlVG9vbCA9IHt9O1xuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLmFjdGl2ZVRvb2wgfHwgdGhpcy5hY3RpdmVUb29sLm5hbWUgIT09IHRvb2wubmFtZSkge1xuICAgICAgICB0aGlzLmFjdGl2ZVRvb2wgPSB0b29sO1xuICAgICAgICBpZiAodG9vbC5jYW52YXNDb25maWdzKSB7XG4gICAgICAgICAgdG9vbC5jYW52YXNDb25maWdzLmZvckVhY2goY29uZkZ1bmMgPT4ge1xuICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyA9IGNvbmZGdW5jKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gU3RhcnQgb2YgcHJpdmF0ZSBvblRvb2xBY3Rpb24oYWN0aW9uKSBmdW5jdGlvblxuICBvblRvb2xBY3Rpb24oYWN0aW9uKSB7XG4gICAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5CVVRUT046IHtcbiAgICAgICAgbGV0IHJlcztcblxuICAgICAgICBpZiAoIV8uaXNVbmRlZmluZWQoYWN0aW9uLm9uU2VsZWN0UGFyYW1zKSkge1xuICAgICAgICAgIHJlcyA9IGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgYWN0aW9uLm9uU2VsZWN0UGFyYW1zKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXMgPSBhY3Rpb24uYWN0aW9uKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGlmICghXy5pc1VuZGVmaW5lZChyZXMpICYmICFfLmlzVW5kZWZpbmVkKGFjdGlvbi5vbkFjdGlvblJldHVybikpIHtcbiAgICAgICAgICBhY3Rpb24ub25BY3Rpb25SZXR1cm4oYWN0aW9uLCByZXMpO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5UT0dHTEU6IHtcbiAgICAgICAgYWN0aW9uLmlzT24gPSAhYWN0aW9uLmlzT247XG4gICAgICAgIGFjdGlvbi5hY3Rpb24oXG4gICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyxcbiAgICAgICAgICBhY3Rpb24uaXNPbiA/IGFjdGlvbi5vblZhbHVlIDogYWN0aW9uLm9mZlZhbHVlXG4gICAgICAgICk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5EUk9QRE9XTjoge1xuICAgICAgICBhY3Rpb24uYWN0aW9uKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsIGFjdGlvbi5kcm9wZG93blNlbGVjdGVkT3B0aW9uKTtcbiAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy5yZW5kZXJBbGwuYmluZCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgICBjYXNlIEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQ6IHtcbiAgICAgICAgY29uc3QgcmVsYXRlZFZhbHVlcyA9IHt9O1xuICAgICAgICB0aGlzLmFjdGl2ZVRvb2wuc2VjdGlvbnMuZm9yRWFjaChzID0+IHtcbiAgICAgICAgICBzLmFjdGlvbnMuZm9yRWFjaChhID0+IHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgYS50eXBlID09PSBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVUICYmXG4gICAgICAgICAgICAgIGEuZ3JvdXBOYW1lID09PSBhY3Rpb24uZ3JvdXBOYW1lICYmXG4gICAgICAgICAgICAgIGEubGFiZWwgIT09IGFjdGlvbi5sYWJlbFxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgIHJlbGF0ZWRWYWx1ZXNbYS5rZXlOYW1lXSA9IGEuaW5wdXRWYWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJlbGF0ZWRWYWx1ZXNbYWN0aW9uLmtleU5hbWVdID0gYWN0aW9uLmlucHV0VmFsdWU7XG4gICAgICAgIGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcywgcmVsYXRlZFZhbHVlcyk7XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5ESUFMT0c6IHtcbiAgICAgICAgaWYgKGFjdGlvbi5kaWFsb2cpIHtcbiAgICAgICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKGFjdGlvbi5kaWFsb2cuY29tcG9uZW50LCB7XG4gICAgICAgICAgICBhdXRvRm9jdXM6IGZhbHNlLFxuICAgICAgICAgICAgZGF0YTogYWN0aW9uLmRpYWxvZy5kYXRhLFxuICAgICAgICAgICAgbWluV2lkdGg6ICczMDBweCcsXG4gICAgICAgICAgICBwYW5lbENsYXNzOiAnYXBwLWRpYWxvZydcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzRGlhbG9nID0+IHtcbiAgICAgICAgICAgIGlmICghXy5pc1VuZGVmaW5lZChyZXNEaWFsb2cpKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHJlc0FjdGlvbiA9IGFjdGlvbi5kaWFsb2cub25DbG9zZShcbiAgICAgICAgICAgICAgICB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLFxuICAgICAgICAgICAgICAgIHJlc0RpYWxvZ1xuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgIV8uaXNVbmRlZmluZWQocmVzQWN0aW9uKSAmJlxuICAgICAgICAgICAgICAgICFfLmlzVW5kZWZpbmVkKGFjdGlvbi5vbkFjdGlvblJldHVybilcbiAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgYWN0aW9uLm9uQWN0aW9uUmV0dXJuKGFjdGlvbiwgcmVzQWN0aW9uKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgICAgY2FzZSBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSOiB7XG4gICAgICAgIHRoaXMub3V0Z29pbmdFdmVudFRyaWdnZXJlZC5lbWl0KHtcbiAgICAgICAgICBjYW52YXM6IHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMsXG4gICAgICAgICAgdHJhbnNmb3JtZWRDYW52YXM6XG4gICAgICAgICAgICBhY3Rpb24ubGFiZWwgPT09ICdTdWJtaXQnXG4gICAgICAgICAgICAgID8gYWN0aW9uLmFjdGlvbihcbiAgICAgICAgICAgICAgICAgIHRoaXMudmlzdWFsSnNvbi5iYWNrZ3JvdW5kSW1hZ2Uuc3JjLFxuICAgICAgICAgICAgICAgICAgdGhpcy5lZGl0ZWRJbWFnZUNhbnZhcy50b0pTT04oWydfY2FudmFzRGltZW5zaW9ucyddKVxuICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgOiAnJ1xuICAgICAgICB9KTtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgICBkZWZhdWx0OiB7XG4gICAgICAgIGFjdGlvbi5hY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMucmVuZGVyQWxsLmJpbmQodGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuICAvLyBFbmQgb2YgcHJpdmF0ZSBvblRvb2xBY3Rpb24oYWN0aW9uKSBmdW5jdGlvblxuXG4gIHB1YmxpYyBvblVwZGF0ZShpbWdVcmwpIHtcbiAgICB0aGlzLnJlc2V0Q2FudmFzKHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgIHRoaXMuc2V0U2NhbGVkSW1hZ2VUb0NhbnZhcyh0aGlzLmVkaXRlZEltYWdlQ2FudmFzLCBpbWdVcmwsIHRydWUpO1xuICB9XG5cbiAgcHVibGljIGdldENhbnZhc0FuZENvbmZpZygpIHtcbiAgICByZXR1cm4ge1xuICAgICAgY2FudmFzOiB0aGlzLmVkaXRlZEltYWdlQ2FudmFzLFxuICAgICAgaW1hZ2VDb25maWc6IHRoaXMuaW1hZ2VDb25maWdcbiAgICB9O1xuICB9XG5cbiAgb25GaWxlU2VsZWN0KHJlcykge1xuICAgIGNvbnN0IHsgZmlsZXMsIGNhbGxlcjogdG9vbCB9ID0gcmVzO1xuICAgIGlmICghXy5pc1VuZGVmaW5lZCh0b29sKSkge1xuICAgICAgaWYgKCFfLmlzVW5kZWZpbmVkKHRvb2wub25TZWxlY3QpICYmIGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgY29uc3QgZmlsZSA9IGZpbGVzWzBdO1xuICAgICAgICBjb25zdCBmaWxlbmFtZSA9IF8ucmVwbGFjZShmaWxlLm5hbWUsIC9bXkEtWjAtOS5dKy9naSwgJ18nKTtcblxuICAgICAgICB0aGlzLmZpbGVTZXJ2aWNlXG4gICAgICAgICAgLmdldFVwbG9hZFVybCh7XG4gICAgICAgICAgICBmaWxlbmFtZSxcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlOiBmaWxlLnR5cGUsXG4gICAgICAgICAgICBleHBpcmVzSW5TZWNzOiAzNjAwXG4gICAgICAgICAgfSlcbiAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BVcGxvYWRVcmwgPT4ge1xuICAgICAgICAgICAgdGhpcy5zM1NlcnZpY2VcbiAgICAgICAgICAgICAgLnVwbG9hZChyZXNwVXBsb2FkVXJsLnVybCwgZmlsZSwgZmlsZS50eXBlKVxuICAgICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3BVcGxvYWQgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGZpbGVVcmwgPSByZXNwVXBsb2FkVXJsLnVybC5zcGxpdCgnPycpWzBdO1xuICAgICAgICAgICAgICAgIHRvb2wub25TZWxlY3RQYXJhbXMuZmlsZVVybCA9IGZpbGVVcmw7XG4gICAgICAgICAgICAgICAgdG9vbC5vblNlbGVjdCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzLCB0b29sLm9uU2VsZWN0UGFyYW1zKTtcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZVRvb2wgPSB7fTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gS2V5Ym9hcmQgU2hvcnRjdXRzXG4gIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzprZXlkb3duJywgWyckZXZlbnQnXSlcbiAgY2FudmFzS2V5Ym9hcmRFdmVudChldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgIGlmIChcbiAgICAgIHRoaXMuaGFzQ2xvbmVTaG9ydGN1dCAmJlxuICAgICAgKChldmVudC5jdHJsS2V5IHx8IGV2ZW50Lm1ldGFLZXkpICYmXG4gICAgICAgIGV2ZW50LnNoaWZ0S2V5ICYmXG4gICAgICAgIGV2ZW50LmtleUNvZGUgPT09IDg2KVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLmNsb25lT2JqZWN0KHRoaXMuZWRpdGVkSW1hZ2VDYW52YXMpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmhhc1JlbW92ZVNob3J0Y3V0ICYmICghZXZlbnQuc2hpZnRLZXkgJiYgZXZlbnQua2V5Q29kZSA9PT0gOCkpIHtcbiAgICAgIHRoaXMuVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24odGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaGFzUmVtb3ZlQWxsU2hvcnRjdXQgJiYgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDgpKSB7XG4gICAgICB0aGlzLlZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICB9XG5cbiAgICBpZiAoXG4gICAgICB0aGlzLmhhc1NlbmRCYWNrd2FyZHNTaG9ydGN1dCAmJlxuICAgICAgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDQwKVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHModGhpcy5lZGl0ZWRJbWFnZUNhbnZhcyk7XG4gICAgfVxuXG4gICAgaWYgKFxuICAgICAgdGhpcy5oYXNCcmluZ0ZvcndhcmRTaG9ydGN1dCAmJlxuICAgICAgKGV2ZW50LnNoaWZ0S2V5ICYmIGV2ZW50LmtleUNvZGUgPT09IDM4KVxuICAgICkge1xuICAgICAgdGhpcy5WaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCh0aGlzLmVkaXRlZEltYWdlQ2FudmFzKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==