/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { VisualEditorService } from '../services/visual-editor.service';
import { faClone, faTrashAlt, faTrash, faArrowUp, faArrowDown, faAlignLeft, faAlignCenter, faAlignRight } from '@fortawesome/free-solid-svg-icons';
import { ActionTypes, ObjectTypes } from './action-types';
export class TemplateAnnotatorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = false;
        this.isWideCanvas = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasPan = false;
        this.resizeCanvasToBackgroundImage = true;
        this.isCenterBackgroundImage = true;
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = true;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasSendBackwardsShortcut = true;
        this.hasBringForwardShortcut = true;
        // FontAwesome Icons
        this.faClone = faClone;
        this.faTrash = faTrash;
        this.faTrashAlt = faTrashAlt;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.faAlignLeft = faAlignLeft;
        this.faAlignCenter = faAlignCenter;
        this.faAlignRight = faAlignRight;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Product',
                isHidden: false,
                iconDisplayText: 'Product',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#3cb44b', fill: '#3cb44b' + '88' }),
                    additionalProperties: {
                        objectType: 'product'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#3cb44b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Primary Logo',
                isHidden: false,
                iconDisplayText: 'Primary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6194b', fill: '#e6194b' + '88' }),
                    additionalProperties: {
                        objectType: 'primary_logo'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6194b' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Secondary Logo',
                isHidden: false,
                iconDisplayText: 'Secondary Logo',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f032e6', fill: '#f032e6' + '88' }),
                    additionalProperties: {
                        objectType: 'secondary_logo'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f032e6' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Lockup',
                isHidden: false,
                iconDisplayText: 'Lockup',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#bcf60c', fill: '#bcf60c' + '88' }),
                    additionalProperties: {
                        objectType: 'lockup'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#bcf60c', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Headline Textbox',
                isHidden: false,
                iconDisplayText: 'Headline Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#4363d8', fill: '#4363d8' + '88' }),
                    additionalProperties: {
                        objectType: 'headline_textbox'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#4363d8' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Other Textbox',
                isHidden: false,
                iconDisplayText: 'Other Textbox',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#f58231', fill: '#f58231' + '88' }),
                    additionalProperties: {
                        objectType: 'other_textbox'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#f58231' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Graphic Accent',
                isHidden: false,
                iconDisplayText: 'Graphic Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#000075', fill: '#000075' + '88' }),
                    additionalProperties: {
                        objectType: 'graphic_accent'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#000075' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Photo Accent',
                isHidden: false,
                iconDisplayText: 'Photo Accent',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#46f0f0', fill: '#46f0f0' + '88' }),
                    additionalProperties: {
                        objectType: 'photo_accent'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#46f0f0' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Button',
                isHidden: false,
                iconDisplayText: 'Button',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#008080', fill: '#008080' + '88' }),
                    additionalProperties: {
                        objectType: 'button'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#008080' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Banner',
                isHidden: false,
                iconDisplayText: 'Banner',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#ffe119', fill: '#ffe119' + '88' }),
                    additionalProperties: {
                        objectType: 'banner'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#ffe119', 'text-shadow': '0px 0.7px #ffffff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Sticker',
                isHidden: false,
                iconDisplayText: 'Sticker',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#fabebe', fill: '#fabebe' + '88' }),
                    additionalProperties: {
                        objectType: 'sticker'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#fabebe' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'Frame',
                isHidden: false,
                iconDisplayText: 'Frame',
                onSelect: VisualEditorService.addBoundingBox,
                onSelectParams: {
                    config: Object.assign({}, TemplateAnnotatorConfig.addBoundingBoxConfigParams.config, { stroke: '#e6beff', fill: '#e6beff' + '88' }),
                    additionalProperties: {
                        objectType: 'frame'
                    }
                },
                flex: '33%',
                style: Object.assign({}, TemplateAnnotatorConfig.buttonStyles, { 'background-color': '#e6beff' }),
                sections: [
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Clone',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faClone,
                                action: VisualEditorService.cloneObject,
                                flex: '33%'
                            },
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                position: 'RHS',
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteAllSelections,
                                flex: '33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '33%'
                            }
                        ],
                        isEndSection: true
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Submit',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                action: VisualEditorService.transformCanvas,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
}
TemplateAnnotatorConfig.addBoundingBoxConfigParams = {
    config: {
        left: 60,
        top: 60,
        height: 100,
        width: 100,
        strokeWidth: 2,
        stroke: 'black',
        originX: 'center',
        originY: 'center'
    }
};
TemplateAnnotatorConfig.buttonStyles = { 'font-weight': 'bold' };
if (false) {
    /** @type {?} */
    TemplateAnnotatorConfig.addBoundingBoxConfigParams;
    /** @type {?} */
    TemplateAnnotatorConfig.buttonStyles;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.tools;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasZoom;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isWideCanvas;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.toolsFlex;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.actionsFlex;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasPan;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.resizeCanvasToBackgroundImage;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.isCenterBackgroundImage;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasCloneShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasRemoveShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.hasBringForwardShortcut;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faClone;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faTrash;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faTrashAlt;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faArrowUp;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faArrowDown;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignLeft;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignCenter;
    /** @type {?} */
    TemplateAnnotatorConfig.prototype.faAlignRight;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGUtYW5ub3RhdG9yLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3Zpc3VhbC1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvY29uZmlncy90ZW1wbGF0ZS1hbm5vdGF0b3ItY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RSxPQUFPLEVBQ0wsT0FBTyxFQUNQLFVBQVUsRUFDVixPQUFPLEVBQ1AsU0FBUyxFQUNULFdBQVcsRUFDWCxXQUFXLEVBQ1gsYUFBYSxFQUNiLFlBQVksRUFDYixNQUFNLG1DQUFtQyxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUQsTUFBTSxPQUFPLHVCQUF1QjtJQTRDbEM7UUEzQk8sd0JBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQzNCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLCtCQUEwQixHQUFHLElBQUksQ0FBQztRQUNsQyxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsZ0JBQVcsR0FBRyxTQUFTLENBQUM7UUFDeEIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGtDQUE2QixHQUFHLElBQUksQ0FBQztRQUNyQyw0QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDL0IsK0JBQTBCLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUN4QixzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDekIseUJBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQzVCLDZCQUF3QixHQUFHLElBQUksQ0FBQztRQUNoQyw0QkFBdUIsR0FBRyxJQUFJLENBQUM7O1FBRy9CLFlBQU8sR0FBRyxPQUFPLENBQUM7UUFDbEIsWUFBTyxHQUFHLE9BQU8sQ0FBQztRQUNsQixlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsZ0JBQVcsR0FBRyxXQUFXLENBQUM7UUFDMUIsZ0JBQVcsR0FBRyxXQUFXLENBQUM7UUFDMUIsa0JBQWEsR0FBRyxhQUFhLENBQUM7UUFDOUIsaUJBQVksR0FBRyxZQUFZLENBQUM7UUFHakMsSUFBSSxDQUFDLEtBQUssR0FBRztZQUNYO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLFFBQVEsRUFBRSxJQUFJO2dCQUNkLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsTUFBTSxDQUFDLEVBQUU7d0JBQ1AsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLFNBQVM7Z0JBQzFCLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dCQUM1QyxjQUFjLEVBQUU7b0JBQ2QsTUFBTSxvQkFDRCx1QkFBdUIsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLElBQzVELE1BQU0sRUFBRSxTQUFTLEVBQ2pCLElBQUksRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUN2QjtvQkFDRCxvQkFBb0IsRUFBRTt3QkFDcEIsVUFBVSxFQUFFLFNBQVM7cUJBQ3RCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssb0JBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxjQUFjO2dCQUNwQixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsY0FBYztnQkFDL0IsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsY0FBYztxQkFDM0I7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxvQkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLGdCQUFnQjtnQkFDakMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsZ0JBQWdCO3FCQUM3QjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLG9CQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsUUFBUTtnQkFDekIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsUUFBUTtxQkFDckI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxvQkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsRUFDN0IsYUFBYSxFQUFFLG1CQUFtQixHQUNuQztnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsa0JBQWtCO2dCQUN4QixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsa0JBQWtCO2dCQUNuQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sb0JBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxrQkFBa0I7cUJBQy9CO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssb0JBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxlQUFlO2dCQUNyQixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsZUFBZTtnQkFDaEMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsZUFBZTtxQkFDNUI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxvQkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLGdCQUFnQjtnQkFDakMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsZ0JBQWdCO3FCQUM3QjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLG9CQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsY0FBYztnQkFDcEIsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLGNBQWM7Z0JBQy9CLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dCQUM1QyxjQUFjLEVBQUU7b0JBQ2QsTUFBTSxvQkFDRCx1QkFBdUIsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLElBQzVELE1BQU0sRUFBRSxTQUFTLEVBQ2pCLElBQUksRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUN2QjtvQkFDRCxvQkFBb0IsRUFBRTt3QkFDcEIsVUFBVSxFQUFFLGNBQWM7cUJBQzNCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssb0JBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLFFBQVEsRUFBRSxLQUFLO2dCQUNmLGVBQWUsRUFBRSxRQUFRO2dCQUN6QixRQUFRLEVBQUUsbUJBQW1CLENBQUMsY0FBYztnQkFDNUMsY0FBYyxFQUFFO29CQUNkLE1BQU0sb0JBQ0QsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsTUFBTSxJQUM1RCxNQUFNLEVBQUUsU0FBUyxFQUNqQixJQUFJLEVBQUUsU0FBUyxHQUFHLElBQUksR0FDdkI7b0JBQ0Qsb0JBQW9CLEVBQUU7d0JBQ3BCLFVBQVUsRUFBRSxRQUFRO3FCQUNyQjtpQkFDRjtnQkFDRCxJQUFJLEVBQUUsS0FBSztnQkFDWCxLQUFLLG9CQUNBLHVCQUF1QixDQUFDLFlBQVksSUFDdkMsa0JBQWtCLEVBQUUsU0FBUyxHQUM5QjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsUUFBUTtnQkFDekIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsUUFBUTtxQkFDckI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxvQkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsRUFDN0IsYUFBYSxFQUFFLG1CQUFtQixHQUNuQztnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsU0FBUztnQkFDZixRQUFRLEVBQUUsS0FBSztnQkFDZixlQUFlLEVBQUUsU0FBUztnQkFDMUIsUUFBUSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0JBQzVDLGNBQWMsRUFBRTtvQkFDZCxNQUFNLG9CQUNELHVCQUF1QixDQUFDLDBCQUEwQixDQUFDLE1BQU0sSUFDNUQsTUFBTSxFQUFFLFNBQVMsRUFDakIsSUFBSSxFQUFFLFNBQVMsR0FBRyxJQUFJLEdBQ3ZCO29CQUNELG9CQUFvQixFQUFFO3dCQUNwQixVQUFVLEVBQUUsU0FBUztxQkFDdEI7aUJBQ0Y7Z0JBQ0QsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsS0FBSyxvQkFDQSx1QkFBdUIsQ0FBQyxZQUFZLElBQ3ZDLGtCQUFrQixFQUFFLFNBQVMsR0FDOUI7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLE9BQU87Z0JBQ2IsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsZUFBZSxFQUFFLE9BQU87Z0JBQ3hCLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dCQUM1QyxjQUFjLEVBQUU7b0JBQ2QsTUFBTSxvQkFDRCx1QkFBdUIsQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLElBQzVELE1BQU0sRUFBRSxTQUFTLEVBQ2pCLElBQUksRUFBRSxTQUFTLEdBQUcsSUFBSSxHQUN2QjtvQkFDRCxvQkFBb0IsRUFBRTt3QkFDcEIsVUFBVSxFQUFFLE9BQU87cUJBQ3BCO2lCQUNGO2dCQUNELElBQUksRUFBRSxLQUFLO2dCQUNYLEtBQUssb0JBQ0EsdUJBQXVCLENBQUMsWUFBWSxJQUN2QyxrQkFBa0IsRUFBRSxTQUFTLEdBQzlCO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsTUFBTSxDQUFDLEVBQUU7d0JBQ1AsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsU0FBUztnQ0FDcEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsT0FBTztnQ0FDZCxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFFBQVEsRUFBRSxLQUFLO2dDQUNmLFNBQVMsRUFBRSxPQUFPO2dDQUNsQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsV0FBVztnQ0FDdkMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixRQUFRLEVBQUUsS0FBSztnQ0FDZixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxzQkFBc0I7Z0NBQ3hDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsTUFBTSxDQUFDLEVBQUU7d0JBQ1AsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsV0FBVztnQ0FDdEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGtCQUFrQjtnQ0FDOUMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx5QkFBeUI7Z0NBQ2hDLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw4QkFBOEI7Z0NBQzFELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsWUFBWTtnQ0FDdkIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsWUFBWSxFQUFFLG9CQUFvQjtnQ0FDbEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQjtnQ0FDN0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx1QkFBdUI7Z0NBQzlCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsWUFBWSxFQUFFLHVCQUF1QjtnQ0FDckMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLDRCQUE0QjtnQ0FDeEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFlBQVksRUFBRSx1QkFBdUI7Z0NBQ3JDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG9CQUFvQjtnQ0FDaEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsc0JBQXNCO2dDQUN4QyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7QUF2dEJlLGtEQUEwQixHQUFHO0lBQzNDLE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxFQUFFO1FBQ1IsR0FBRyxFQUFFLEVBQUU7UUFDUCxNQUFNLEVBQUUsR0FBRztRQUNYLEtBQUssRUFBRSxHQUFHO1FBQ1YsV0FBVyxFQUFFLENBQUM7UUFDZCxNQUFNLEVBQUUsT0FBTztRQUNmLE9BQU8sRUFBRSxRQUFRO1FBQ2pCLE9BQU8sRUFBRSxRQUFRO0tBQ2xCO0NBQ0YsQ0FBQztBQUVjLG9DQUFZLEdBQUcsRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLENBQUM7OztJQWJ6RCxtREFXRTs7SUFFRixxQ0FBeUQ7O0lBRXpELHdDQUFvQjs7SUFDcEIsc0RBQWtDOztJQUNsQyxnREFBNEI7O0lBQzVCLDZEQUF5Qzs7SUFDekMsMENBQXVCOztJQUN2QiwrQ0FBMkI7O0lBQzNCLDRDQUE2Qjs7SUFDN0IsOENBQStCOztJQUMvQix5Q0FBc0I7O0lBQ3RCLGdFQUE0Qzs7SUFDNUMsMERBQXNDOztJQUN0Qyw2REFBeUM7O0lBQ3pDLG1EQUErQjs7SUFDL0Isb0RBQWdDOztJQUNoQyx1REFBbUM7O0lBQ25DLDJEQUF1Qzs7SUFDdkMsMERBQXNDOztJQUd0QywwQ0FBeUI7O0lBQ3pCLDBDQUF5Qjs7SUFDekIsNkNBQStCOztJQUMvQiw0Q0FBNkI7O0lBQzdCLDhDQUFpQzs7SUFDakMsOENBQWlDOztJQUNqQyxnREFBcUM7O0lBQ3JDLCtDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcblxuaW1wb3J0IHsgVmlzdWFsRWRpdG9yU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Zpc3VhbC1lZGl0b3Iuc2VydmljZSc7XG5pbXBvcnQge1xuICBmYUNsb25lLFxuICBmYVRyYXNoQWx0LFxuICBmYVRyYXNoLFxuICBmYUFycm93VXAsXG4gIGZhQXJyb3dEb3duLFxuICBmYUFsaWduTGVmdCxcbiAgZmFBbGlnbkNlbnRlcixcbiAgZmFBbGlnblJpZ2h0XG59IGZyb20gJ0Bmb3J0YXdlc29tZS9mcmVlLXNvbGlkLXN2Zy1pY29ucyc7XG5pbXBvcnQgeyBBY3Rpb25UeXBlcywgT2JqZWN0VHlwZXMgfSBmcm9tICcuL2FjdGlvbi10eXBlcyc7XG5cbmV4cG9ydCBjbGFzcyBUZW1wbGF0ZUFubm90YXRvckNvbmZpZyB7XG4gIHN0YXRpYyByZWFkb25seSBhZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcyA9IHtcbiAgICBjb25maWc6IHtcbiAgICAgIGxlZnQ6IDYwLFxuICAgICAgdG9wOiA2MCxcbiAgICAgIGhlaWdodDogMTAwLFxuICAgICAgd2lkdGg6IDEwMCxcbiAgICAgIHN0cm9rZVdpZHRoOiAyLFxuICAgICAgc3Ryb2tlOiAnYmxhY2snLFxuICAgICAgb3JpZ2luWDogJ2NlbnRlcicsXG4gICAgICBvcmlnaW5ZOiAnY2VudGVyJ1xuICAgIH1cbiAgfTtcblxuICBzdGF0aWMgcmVhZG9ubHkgYnV0dG9uU3R5bGVzID0geyAnZm9udC13ZWlnaHQnOiAnYm9sZCcgfTtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gdHJ1ZTtcbiAgcHVibGljIGhhc0hpZGRlblNlbGVjdGlvbkNvbnRyb2xzID0gdHJ1ZTtcbiAgcHVibGljIGhhc1pvb20gPSBmYWxzZTtcbiAgcHVibGljIGlzV2lkZUNhbnZhcyA9IHRydWU7XG4gIHB1YmxpYyB0b29sc0ZsZXggPSAnMCAxIDEzJSc7XG4gIHB1YmxpYyBhY3Rpb25zRmxleCA9ICcwIDEgMjAlJztcbiAgcHVibGljIGhhc1BhbiA9IGZhbHNlO1xuICBwdWJsaWMgcmVzaXplQ2FudmFzVG9CYWNrZ3JvdW5kSW1hZ2UgPSB0cnVlO1xuICBwdWJsaWMgaXNDZW50ZXJCYWNrZ3JvdW5kSW1hZ2UgPSB0cnVlO1xuICBwdWJsaWMgaGFzU2hvcnRjdXRzRXhwYW5zaW9uUGFuZWwgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2xvbmVTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNSZW1vdmVTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNSZW1vdmVBbGxTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNTZW5kQmFja3dhcmRzU2hvcnRjdXQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQnJpbmdGb3J3YXJkU2hvcnRjdXQgPSB0cnVlO1xuXG4gIC8vIEZvbnRBd2Vzb21lIEljb25zXG4gIHB1YmxpYyBmYUNsb25lID0gZmFDbG9uZTtcbiAgcHVibGljIGZhVHJhc2ggPSBmYVRyYXNoO1xuICBwdWJsaWMgZmFUcmFzaEFsdCA9IGZhVHJhc2hBbHQ7XG4gIHB1YmxpYyBmYUFycm93VXAgPSBmYUFycm93VXA7XG4gIHB1YmxpYyBmYUFycm93RG93biA9IGZhQXJyb3dEb3duO1xuICBwdWJsaWMgZmFBbGlnbkxlZnQgPSBmYUFsaWduTGVmdDtcbiAgcHVibGljIGZhQWxpZ25DZW50ZXIgPSBmYUFsaWduQ2VudGVyO1xuICBwdWJsaWMgZmFBbGlnblJpZ2h0ID0gZmFBbGlnblJpZ2h0O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudG9vbHMgPSBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdjb25maWcnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnUHJvZHVjdCcsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnUHJvZHVjdCcsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnIzNjYjQ0YicsXG4gICAgICAgICAgICBmaWxsOiAnIzNjYjQ0YicgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ3Byb2R1Y3QnXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnIzNjYjQ0YidcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnUHJpbWFyeSBMb2dvJyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdQcmltYXJ5IExvZ28nLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyNlNjE5NGInLFxuICAgICAgICAgICAgZmlsbDogJyNlNjE5NGInICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdwcmltYXJ5X2xvZ28nXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnI2U2MTk0YidcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnU2Vjb25kYXJ5IExvZ28nLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ1NlY29uZGFyeSBMb2dvJyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjZjAzMmU2JyxcbiAgICAgICAgICAgIGZpbGw6ICcjZjAzMmU2JyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnc2Vjb25kYXJ5X2xvZ28nXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnI2YwMzJlNidcbiAgICAgICAgfSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlQWxsU2VsZWN0aW9ucyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdWJtaXQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLk9VVEdPSU5HX0VWRU5UX1RSSUdHRVIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnRyYW5zZm9ybUNhbnZhcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnTG9ja3VwJyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdMb2NrdXAnLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyNiY2Y2MGMnLFxuICAgICAgICAgICAgZmlsbDogJyNiY2Y2MGMnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdsb2NrdXAnXG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmbGV4OiAnMzMlJyxcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5idXR0b25TdHlsZXMsXG4gICAgICAgICAgJ2JhY2tncm91bmQtY29sb3InOiAnI2JjZjYwYycsXG4gICAgICAgICAgJ3RleHQtc2hhZG93JzogJzBweCAwLjdweCAjZmZmZmZmJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdIZWFkbGluZSBUZXh0Ym94JyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdIZWFkbGluZSBUZXh0Ym94JyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjNDM2M2Q4JyxcbiAgICAgICAgICAgIGZpbGw6ICcjNDM2M2Q4JyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnaGVhZGxpbmVfdGV4dGJveCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjNDM2M2Q4J1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdPdGhlciBUZXh0Ym94JyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdPdGhlciBUZXh0Ym94JyxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQm91bmRpbmdCb3gsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICAuLi5UZW1wbGF0ZUFubm90YXRvckNvbmZpZy5hZGRCb3VuZGluZ0JveENvbmZpZ1BhcmFtcy5jb25maWcsXG4gICAgICAgICAgICBzdHJva2U6ICcjZjU4MjMxJyxcbiAgICAgICAgICAgIGZpbGw6ICcjZjU4MjMxJyArICc4OCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFkZGl0aW9uYWxQcm9wZXJ0aWVzOiB7XG4gICAgICAgICAgICBvYmplY3RUeXBlOiAnb3RoZXJfdGV4dGJveCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjZjU4MjMxJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdHcmFwaGljIEFjY2VudCcsXG4gICAgICAgIGlzSGlkZGVuOiBmYWxzZSxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnR3JhcGhpYyBBY2NlbnQnLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyMwMDAwNzUnLFxuICAgICAgICAgICAgZmlsbDogJyMwMDAwNzUnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdncmFwaGljX2FjY2VudCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjMDAwMDc1J1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdQaG90byBBY2NlbnQnLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ1Bob3RvIEFjY2VudCcsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnIzQ2ZjBmMCcsXG4gICAgICAgICAgICBmaWxsOiAnIzQ2ZjBmMCcgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ3Bob3RvX2FjY2VudCdcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjNDZmMGYwJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdCdXR0b24nLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0J1dHRvbicsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnIzAwODA4MCcsXG4gICAgICAgICAgICBmaWxsOiAnIzAwODA4MCcgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ2J1dHRvbidcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjMDA4MDgwJ1xuICAgICAgICB9LFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdCYW5uZXInLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ0Jhbm5lcicsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnI2ZmZTExOScsXG4gICAgICAgICAgICBmaWxsOiAnI2ZmZTExOScgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ2Jhbm5lcidcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZsZXg6ICczMyUnLFxuICAgICAgICBzdHlsZToge1xuICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmJ1dHRvblN0eWxlcyxcbiAgICAgICAgICAnYmFja2dyb3VuZC1jb2xvcic6ICcjZmZlMTE5JyxcbiAgICAgICAgICAndGV4dC1zaGFkb3cnOiAnMHB4IDAuN3B4ICNmZmZmZmYnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ1N0aWNrZXInLFxuICAgICAgICBpc0hpZGRlbjogZmFsc2UsXG4gICAgICAgIGljb25EaXNwbGF5VGV4dDogJ1N0aWNrZXInLFxuICAgICAgICBvblNlbGVjdDogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRCb3VuZGluZ0JveCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIC4uLlRlbXBsYXRlQW5ub3RhdG9yQ29uZmlnLmFkZEJvdW5kaW5nQm94Q29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgIHN0cm9rZTogJyNmYWJlYmUnLFxuICAgICAgICAgICAgZmlsbDogJyNmYWJlYmUnICsgJzg4J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgYWRkaXRpb25hbFByb3BlcnRpZXM6IHtcbiAgICAgICAgICAgIG9iamVjdFR5cGU6ICdzdGlja2VyJ1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyNmYWJlYmUnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ0ZyYW1lJyxcbiAgICAgICAgaXNIaWRkZW46IGZhbHNlLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdGcmFtZScsXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZEJvdW5kaW5nQm94LFxuICAgICAgICBvblNlbGVjdFBhcmFtczoge1xuICAgICAgICAgIGNvbmZpZzoge1xuICAgICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYWRkQm91bmRpbmdCb3hDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgc3Ryb2tlOiAnI2U2YmVmZicsXG4gICAgICAgICAgICBmaWxsOiAnI2U2YmVmZicgKyAnODgnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsUHJvcGVydGllczoge1xuICAgICAgICAgICAgb2JqZWN0VHlwZTogJ2ZyYW1lJ1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZmxleDogJzMzJScsXG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgLi4uVGVtcGxhdGVBbm5vdGF0b3JDb25maWcuYnV0dG9uU3R5bGVzLFxuICAgICAgICAgICdiYWNrZ3JvdW5kLWNvbG9yJzogJyNlNmJlZmYnXG4gICAgICAgIH0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdSZW1vdmUgQWxsJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZUFsbFNlbGVjdGlvbnMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU3VibWl0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS50cmFuc2Zvcm1DYW52YXMsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgc2hhcGUnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuU0hBUEUsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCcmluZyBGcm9udCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2VuZCBCYWNrJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93RG93bixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0Nsb25lJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgcG9zaXRpb246ICdSSFMnLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFDbG9uZSxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuY2xvbmVPYmplY3QsXG4gICAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgcG9zaXRpb246ICdSSFMnLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFUcmFzaEFsdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVBbGxTZWxlY3Rpb25zLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGdyb3VwJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLkdST1VQLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gTGVmdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkxlZnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uTGVmdCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gSG9yaXpvbnRhbCBDZW50ZXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25DZW50ZXIsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gUmlnaHQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25SaWdodCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBUb3AnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl90b3AnLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblRvcCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gVmVydGljYWwgQ2VudGVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbk1hdGVyaWFsOiAndmVydGljYWxfYWxpZ25fY2VudGVyJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gQm90dG9tJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbk1hdGVyaWFsOiAndmVydGljYWxfYWxpZ25fYm90dG9tJyxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Cb3R0b20sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQnJpbmcgRnJvbnQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTZW5kIEJhY2snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlbW92ZSBBbGwnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uR3JvdXAsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N1Ym1pdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuT1VUR09JTkdfRVZFTlRfVFJJR0dFUixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UudHJhbnNmb3JtQ2FudmFzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9XG4gICAgXTtcbiAgfVxufVxuIl19