/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ColorPickerDialogComponent } from '../components/color-picker-dialog/color-picker-dialog.component';
import { faFont, faTrashAlt, faBold, faAlignRight, faFillDrip, faPen, faAlignLeft, faItalic, faUnderline, faAlignCenter, faRetweet, faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { ActionTypes, ObjectTypes } from './action-types';
export class DesignEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 1 13%';
        this.actionsFlex = '0 1 20%';
        this.hasShortcutsExpansionPanel = true;
        this.hasCloneShortcut = false;
        this.hasRemoveShortcut = true;
        this.hasRemoveAllShortcut = true;
        this.hasBringForwardShortcut = true;
        this.hasSendBackwardsShortcut = true;
        // FontAwesome Icons
        this.faTrashAlt = faTrashAlt;
        this.faFont = faFont;
        this.faBold = faBold;
        this.faAlignRight = faAlignRight;
        this.faFillDrip = faFillDrip;
        this.faPen = faPen;
        this.faAlignLeft = faAlignLeft;
        this.faItalic = faItalic;
        this.faUnderline = faUnderline;
        this.faAlignCenter = faAlignCenter;
        this.faRetweet = faRetweet;
        this.faArrowUp = faArrowUp;
        this.faArrowDown = faArrowDown;
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faFillDrip,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineStrokeWidth,
                                iconClass: faFont,
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Font Family',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontFamily,
                                iconClass: faFont,
                                action: VisualEditorService.setFontFamily,
                                flex: '0 1 75%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Font Size',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableFontSizes,
                                dropdownSelectedOption: DesignEditorConfig.defaultFontSize,
                                iconClass: faFont,
                                action: VisualEditorService.setFontSize,
                                flex: '0 1 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Line Height',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableLineHeights,
                                dropdownSelectedOption: DesignEditorConfig.defaultLineHeight,
                                action: VisualEditorService.setLineHeight,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Character Spacing',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: DesignEditorConfig.availableCharSpacings,
                                dropdownSelectedOption: DesignEditorConfig.defaultCharSpacing,
                                action: VisualEditorService.setCharacterSpacing,
                                flex: '0 3 75%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bold',
                                type: ActionTypes.BUTTON,
                                iconClass: faBold,
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Italic',
                                type: ActionTypes.BUTTON,
                                iconClass: faItalic,
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Underline',
                                type: ActionTypes.BUTTON,
                                iconClass: faUnderline,
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Font Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faFont,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Blur',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Angle',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'Shadow Length',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'Shadow Color',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: faFont,
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: faPen,
                                dialog: {
                                    component: ColorPickerDialogComponent,
                                    data: {
                                        color: '#000000',
                                        titleText: 'SET COLOR',
                                        buttonText: 'SET'
                                    },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = {
                                        'border-bottom': `3px solid ${val}`,
                                        'padding-bottom': '2px'
                                    };
                                }),
                                iconStyle: {
                                    'border-bottom': '3px solid black',
                                    'padding-bottom': '2px'
                                },
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: faTrashAlt,
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [
                    (/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    canvas => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })
                ],
                sections: [
                    {
                        actions: [
                            {
                                label: 'Align Left',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignLeft,
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Horizontal Center',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignCenter,
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Right',
                                type: ActionTypes.BUTTON,
                                iconClass: faAlignRight,
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ]
                    },
                    {
                        actions: [
                            {
                                label: 'Align Top',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_top',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Vertical Center',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_center',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Align Bottom',
                                type: ActionTypes.BUTTON,
                                iconMaterial: 'vertical_align_bottom',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Bring Front',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowUp,
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Send Back',
                                type: ActionTypes.BUTTON,
                                iconClass: faArrowDown,
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ],
                        isEndSection: true
                    },
                    {
                        actions: [
                            {
                                label: 'Remove All',
                                type: ActionTypes.BUTTON,
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ]
                    }
                ]
            }
        ];
    }
}
DesignEditorConfig.defaultFontFamily = 'Times New Roman';
DesignEditorConfig.availableFontFamilies = [
    DesignEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
DesignEditorConfig.defaultFontSize = 34;
DesignEditorConfig.availableFontSizes = _.range(8, 50 + 1);
DesignEditorConfig.defaultLineHeight = 1;
DesignEditorConfig.availableLineHeights = _.range(0, 11, 0.5);
DesignEditorConfig.defaultCharSpacing = 0;
DesignEditorConfig.availableCharSpacings = _.range(0, 801, 100);
DesignEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: DesignEditorConfig.defaultFontFamily,
        fontSize: DesignEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
DesignEditorConfig.defaultStrokeWidth = 0;
DesignEditorConfig.defaultLineStrokeWidth = 5;
DesignEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
if (false) {
    /** @type {?} */
    DesignEditorConfig.defaultFontFamily;
    /** @type {?} */
    DesignEditorConfig.availableFontFamilies;
    /** @type {?} */
    DesignEditorConfig.defaultFontSize;
    /** @type {?} */
    DesignEditorConfig.availableFontSizes;
    /** @type {?} */
    DesignEditorConfig.defaultLineHeight;
    /** @type {?} */
    DesignEditorConfig.availableLineHeights;
    /** @type {?} */
    DesignEditorConfig.defaultCharSpacing;
    /** @type {?} */
    DesignEditorConfig.availableCharSpacings;
    /** @type {?} */
    DesignEditorConfig.addTextConfigParams;
    /** @type {?} */
    DesignEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    DesignEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    DesignEditorConfig.availableStrokeWidths;
    /** @type {?} */
    DesignEditorConfig.prototype.tools;
    /** @type {?} */
    DesignEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    DesignEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    DesignEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    DesignEditorConfig.prototype.hasZoom;
    /** @type {?} */
    DesignEditorConfig.prototype.hasPan;
    /** @type {?} */
    DesignEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    DesignEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    DesignEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    DesignEditorConfig.prototype.actionsFlex;
    /** @type {?} */
    DesignEditorConfig.prototype.hasShortcutsExpansionPanel;
    /** @type {?} */
    DesignEditorConfig.prototype.hasCloneShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasRemoveShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasRemoveAllShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasBringForwardShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.hasSendBackwardsShortcut;
    /** @type {?} */
    DesignEditorConfig.prototype.faTrashAlt;
    /** @type {?} */
    DesignEditorConfig.prototype.faFont;
    /** @type {?} */
    DesignEditorConfig.prototype.faBold;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignRight;
    /** @type {?} */
    DesignEditorConfig.prototype.faFillDrip;
    /** @type {?} */
    DesignEditorConfig.prototype.faPen;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignLeft;
    /** @type {?} */
    DesignEditorConfig.prototype.faItalic;
    /** @type {?} */
    DesignEditorConfig.prototype.faUnderline;
    /** @type {?} */
    DesignEditorConfig.prototype.faAlignCenter;
    /** @type {?} */
    DesignEditorConfig.prototype.faRetweet;
    /** @type {?} */
    DesignEditorConfig.prototype.faArrowUp;
    /** @type {?} */
    DesignEditorConfig.prototype.faArrowDown;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVzaWduLWVkaXRvci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbmZpZ3MvZGVzaWduLWVkaXRvci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGlFQUFpRSxDQUFDO0FBQzdHLE9BQU8sRUFDTCxNQUFNLEVBQ04sVUFBVSxFQUNWLE1BQU0sRUFDTixZQUFZLEVBQ1osVUFBVSxFQUNWLEtBQUssRUFDTCxXQUFXLEVBQ1gsUUFBUSxFQUNSLFdBQVcsRUFDWCxhQUFhLEVBQ2IsU0FBUyxFQUNULFNBQVMsRUFDVCxXQUFXLEVBQ1osTUFBTSxtQ0FBbUMsQ0FBQztBQUUzQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTFELE1BQU0sT0FBTyxrQkFBa0I7SUFpRTdCO1FBL0JPLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QiwrQkFBMEIsR0FBRyxJQUFJLENBQUM7UUFDbEMsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBQ3hCLCtCQUEwQixHQUFHLElBQUksQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1Qiw0QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDL0IsNkJBQXdCLEdBQUcsSUFBSSxDQUFDOztRQUdoQyxlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLFdBQU0sR0FBRyxNQUFNLENBQUM7UUFDaEIsV0FBTSxHQUFHLE1BQU0sQ0FBQztRQUNoQixpQkFBWSxHQUFHLFlBQVksQ0FBQztRQUM1QixlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLFVBQUssR0FBRyxLQUFLLENBQUM7UUFDZCxnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUMxQixhQUFRLEdBQUcsUUFBUSxDQUFDO1FBQ3BCLGdCQUFXLEdBQUcsV0FBVyxDQUFDO1FBQzFCLGtCQUFhLEdBQUcsYUFBYSxDQUFDO1FBQzlCLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUN0QixnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUcvQixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1g7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsYUFBYSxFQUFFOzs7OztvQkFDYixNQUFNLENBQUMsRUFBRTt3QkFDUCxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7OztvQkFDYixNQUFNLENBQUMsRUFBRTt3QkFDUCxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFO29DQUNOLFNBQVMsRUFBRSwwQkFBMEI7b0NBQ3JDLElBQUksRUFBRTt3Q0FDSixLQUFLLEVBQUUsU0FBUzt3Q0FDaEIsU0FBUyxFQUFFLFdBQVc7d0NBQ3RCLFVBQVUsRUFBRSxLQUFLO3FDQUNsQjtvQ0FDRCxPQUFPLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtpQ0FDMUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0NBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUc7d0NBQ2pCLGVBQWUsRUFBRSxhQUFhLEdBQUcsRUFBRTt3Q0FDbkMsZ0JBQWdCLEVBQUUsS0FBSztxQ0FDeEIsQ0FBQztnQ0FDSixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFO29DQUNULGVBQWUsRUFBRSxpQkFBaUI7b0NBQ2xDLGdCQUFnQixFQUFFLEtBQUs7aUNBQ3hCO2dDQUNELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsTUFBTTtnQ0FDZixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE9BQU87Z0NBQ2hCLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsZUFBZTtnQ0FDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsT0FBTyxFQUFFLFFBQVE7Z0NBQ2pCLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLEtBQUs7Z0NBQ2hCLE1BQU0sRUFBRTtvQ0FDTixTQUFTLEVBQUUsMEJBQTBCO29DQUNyQyxJQUFJLEVBQUU7d0NBQ0osS0FBSyxFQUFFLFNBQVM7d0NBQ2hCLFNBQVMsRUFBRSxXQUFXO3dDQUN0QixVQUFVLEVBQUUsS0FBSztxQ0FDbEI7b0NBQ0QsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGNBQWM7aUNBQzVDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFO29DQUM5QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHO3dDQUNqQixlQUFlLEVBQUUsYUFBYSxHQUFHLEVBQUU7d0NBQ25DLGdCQUFnQixFQUFFLEtBQUs7cUNBQ3hCLENBQUM7Z0NBQ0osQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRTtvQ0FDVCxlQUFlLEVBQUUsaUJBQWlCO29DQUNsQyxnQkFBZ0IsRUFBRSxLQUFLO2lDQUN4QjtnQ0FDRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNELGlDQUFpQztvQkFDakMsY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGtDQUFrQztvQkFDbEMsa0VBQWtFO29CQUNsRSx1RUFBdUU7b0JBQ3ZFLGdDQUFnQztvQkFDaEMsa0RBQWtEO29CQUNsRCxrQkFBa0I7b0JBQ2xCLE1BQU07b0JBQ04sTUFBTTtvQkFDTixjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsZ0NBQWdDO29CQUNoQywrQkFBK0I7b0JBQy9CLGdCQUFnQjtvQkFDaEIsK0NBQStDO29CQUMvQyw2RUFBNkU7b0JBQzdFLG9EQUFvRDtvQkFDcEQsU0FBUztvQkFDVCx5Q0FBeUM7b0JBQ3pDLHdDQUF3QztvQkFDeEMsMkZBQTJGO29CQUMzRixTQUFTO29CQUNULGdGQUFnRjtvQkFDaEYsa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ047d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsSUFBSTtnQkFDNUIsYUFBYSxFQUFFOzs7OztvQkFDYixNQUFNLENBQUMsRUFBRTt3QkFDUCxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsa0JBQWtCLENBQUMscUJBQXFCO2dDQUN4RCxzQkFBc0IsRUFDcEIsa0JBQWtCLENBQUMsc0JBQXNCO2dDQUMzQyxTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0NBQzFDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxLQUFLO2dDQUNoQixNQUFNLEVBQUU7b0NBQ04sU0FBUyxFQUFFLDBCQUEwQjtvQ0FDckMsSUFBSSxFQUFFO3dDQUNKLEtBQUssRUFBRSxTQUFTO3dDQUNoQixTQUFTLEVBQUUsV0FBVzt3Q0FDdEIsVUFBVSxFQUFFLEtBQUs7cUNBQ2xCO29DQUNELE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2lDQUM1QztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRzt3Q0FDakIsZUFBZSxFQUFFLGFBQWEsR0FBRyxFQUFFO3dDQUNuQyxnQkFBZ0IsRUFBRSxLQUFLO3FDQUN4QixDQUFDO2dDQUNKLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUU7b0NBQ1QsZUFBZSxFQUFFLGlCQUFpQjtvQ0FDbEMsZ0JBQWdCLEVBQUUsS0FBSztpQ0FDeEI7Z0NBQ0QsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFNBQVM7Z0NBQ3BCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxVQUFVO2dDQUNyQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsV0FBVztnQkFDakIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dCQUM5QixhQUFhLEVBQUU7Ozs7O29CQUNiLE1BQU0sQ0FBQyxFQUFFO3dCQUNQLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDO2lCQUNGO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUTtnQ0FDMUIsY0FBYyxFQUFFLGtCQUFrQixDQUFDLHFCQUFxQjtnQ0FDeEQsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCO2dDQUM1RCxTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUTtnQ0FDMUIsY0FBYyxFQUFFLGtCQUFrQixDQUFDLGtCQUFrQjtnQ0FDckQsc0JBQXNCLEVBQUUsa0JBQWtCLENBQUMsZUFBZTtnQ0FDMUQsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxXQUFXO2dDQUN2QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsa0JBQWtCLENBQUMsb0JBQW9CO2dDQUN2RCxzQkFBc0IsRUFBRSxrQkFBa0IsQ0FBQyxpQkFBaUI7Z0NBQzVELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsbUJBQW1CO2dDQUMxQixJQUFJLEVBQUUsV0FBVyxDQUFDLFFBQVE7Z0NBQzFCLGNBQWMsRUFBRSxrQkFBa0IsQ0FBQyxxQkFBcUI7Z0NBQ3hELHNCQUFzQixFQUFFLGtCQUFrQixDQUFDLGtCQUFrQjtnQ0FDN0QsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxXQUFXO2dDQUN2QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFlBQVk7Z0NBQ3ZCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxVQUFVO2dDQUN0QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsTUFBTTtnQ0FDYixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsT0FBTztnQ0FDbkMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxRQUFRO2dDQUNmLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFO29DQUNOLFNBQVMsRUFBRSwwQkFBMEI7b0NBQ3JDLElBQUksRUFBRTt3Q0FDSixLQUFLLEVBQUUsU0FBUzt3Q0FDaEIsU0FBUyxFQUFFLFdBQVc7d0NBQ3RCLFVBQVUsRUFBRSxLQUFLO3FDQUNsQjtvQ0FDRCxPQUFPLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtpQ0FDMUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0NBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBRSxlQUFlLEVBQUUsYUFBYSxHQUFHLEVBQUUsRUFBRSxDQUFDO2dDQUM3RCxDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFLEVBQUUsZUFBZSxFQUFFLGlCQUFpQixFQUFFO2dDQUNqRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFNBQVM7Z0NBQ3BCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFdBQVc7Z0NBQ3RCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxVQUFVO2dDQUNyQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7O29CQUNiLE1BQU0sQ0FBQyxFQUFFO3dCQUNQLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDO2lCQUNGO2dCQUNELFFBQVEsRUFBRTtvQkFDUjt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxNQUFNO2dDQUNmLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsT0FBTztnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxlQUFlO2dDQUN0QixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixPQUFPLEVBQUUsUUFBUTtnQ0FDakIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLEtBQUs7Z0NBQ2hCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFO3dDQUNKLEtBQUssRUFBRSxTQUFTO3dDQUNoQixTQUFTLEVBQUUsV0FBVzt3Q0FDdEIsVUFBVSxFQUFFLEtBQUs7cUNBQ2xCO29DQUNELE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2lDQUM1QztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRzt3Q0FDakIsZUFBZSxFQUFFLGFBQWEsR0FBRyxFQUFFO3dDQUNuQyxnQkFBZ0IsRUFBRSxLQUFLO3FDQUN4QixDQUFDO2dDQUNKLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUU7b0NBQ1QsZUFBZSxFQUFFLGlCQUFpQjtvQ0FDbEMsZ0JBQWdCLEVBQUUsS0FBSztpQ0FDeEI7Z0NBQ0QsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRCxpQ0FBaUM7b0JBQ2pDLGNBQWM7b0JBQ2QsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLCtCQUErQjtvQkFDL0Isa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QixrQ0FBa0M7b0JBQ2xDLGtFQUFrRTtvQkFDbEUsdUVBQXVFO29CQUN2RSxnQ0FBZ0M7b0JBQ2hDLGtEQUFrRDtvQkFDbEQsa0JBQWtCO29CQUNsQixNQUFNO29CQUNOLE1BQU07b0JBQ04sY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGdDQUFnQztvQkFDaEMsK0JBQStCO29CQUMvQixnQkFBZ0I7b0JBQ2hCLCtDQUErQztvQkFDL0MsNkVBQTZFO29CQUM3RSxvREFBb0Q7b0JBQ3BELFNBQVM7b0JBQ1QseUNBQXlDO29CQUN6Qyx3Q0FBd0M7b0JBQ3hDLDJGQUEyRjtvQkFDM0YsU0FBUztvQkFDVCxnRkFBZ0Y7b0JBQ2hGLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsU0FBUztnQ0FDcEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsV0FBVztnQ0FDdEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFVBQVU7Z0NBQ3JCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxjQUFjO2dCQUNwQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLE9BQU87Z0JBQy9CLGFBQWEsRUFBRTs7Ozs7b0JBQ2IsTUFBTSxDQUFDLEVBQUU7d0JBQ1AsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUM7aUJBQ0Y7Z0JBQ0QsUUFBUSxFQUFFO29CQUNSO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsTUFBTTtnQ0FDakIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxPQUFPO2dDQUNoQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxNQUFNO2dDQUNqQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3FCQUNGO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGVBQWU7Z0NBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE9BQU8sRUFBRSxRQUFRO2dDQUNqQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLE1BQU07Z0NBQ2pCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsS0FBSztnQ0FDaEIsTUFBTSxFQUFFO29DQUNOLFNBQVMsRUFBRSwwQkFBMEI7b0NBQ3JDLElBQUksRUFBRTt3Q0FDSixLQUFLLEVBQUUsU0FBUzt3Q0FDaEIsU0FBUyxFQUFFLFdBQVc7d0NBQ3RCLFVBQVUsRUFBRSxLQUFLO3FDQUNsQjtvQ0FDRCxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0NBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUc7d0NBQ2pCLGVBQWUsRUFBRSxhQUFhLEdBQUcsRUFBRTt3Q0FDbkMsZ0JBQWdCLEVBQUUsS0FBSztxQ0FDeEIsQ0FBQztnQ0FDSixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFO29DQUNULGVBQWUsRUFBRSxpQkFBaUI7b0NBQ2xDLGdCQUFnQixFQUFFLEtBQUs7aUNBQ3hCO2dDQUNELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxTQUFTO2dDQUNwQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGO3dCQUNELFlBQVksRUFBRSxJQUFJO3FCQUNuQjtvQkFDRDt3QkFDRSxPQUFPLEVBQUU7NEJBQ1A7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsVUFBVTtnQ0FDckIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7OztvQkFDYixNQUFNLENBQUMsRUFBRTt3QkFDUCxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQztpQkFDRjtnQkFDRCxRQUFRLEVBQUU7b0JBQ1I7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxXQUFXO2dDQUN0QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsa0JBQWtCO2dDQUM5QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLHlCQUF5QjtnQ0FDaEMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLDhCQUE4QjtnQ0FDMUQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsbUJBQW1CO2dDQUMvQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7cUJBQ0Y7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFlBQVksRUFBRSxvQkFBb0I7Z0NBQ2xDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUI7Z0NBQzdDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsdUJBQXVCO2dDQUM5QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFlBQVksRUFBRSx1QkFBdUI7Z0NBQ3JDLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw0QkFBNEI7Z0NBQ3hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixZQUFZLEVBQUUsdUJBQXVCO2dDQUNyQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0Y7d0JBQ0QsWUFBWSxFQUFFLElBQUk7cUJBQ25CO29CQUNEO3dCQUNFLE9BQU8sRUFBRTs0QkFDUDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsU0FBUztnQ0FDcEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsV0FBVztnQ0FDdEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjt3QkFDRCxZQUFZLEVBQUUsSUFBSTtxQkFDbkI7b0JBQ0Q7d0JBQ0UsT0FBTyxFQUFFOzRCQUNQO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7O0FBejhCZSxvQ0FBaUIsR0FBRyxpQkFBaUIsQ0FBQztBQUN0Qyx3Q0FBcUIsR0FBRztJQUN0QyxrQkFBa0IsQ0FBQyxpQkFBaUI7SUFDcEMsU0FBUztJQUNULFdBQVc7SUFDWCxlQUFlO0lBQ2YsUUFBUTtJQUNSLGFBQWE7Q0FDZCxDQUFDO0FBQ2Msa0NBQWUsR0FBRyxFQUFFLENBQUM7QUFDckIscUNBQWtCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQ3hDLG9DQUFpQixHQUFHLENBQUMsQ0FBQztBQUN0Qix1Q0FBb0IsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7QUFDM0MscUNBQWtCLEdBQUcsQ0FBQyxDQUFDO0FBQ3ZCLHdDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztBQUM3QyxzQ0FBbUIsR0FBRztJQUNwQyxJQUFJLEVBQUUsUUFBUTtJQUNkLE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxFQUFFO1FBQ1IsR0FBRyxFQUFFLEVBQUU7UUFDUCxVQUFVLEVBQUUsa0JBQWtCLENBQUMsaUJBQWlCO1FBQ2hELFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxlQUFlO1FBQzVDLFdBQVcsRUFBRSxTQUFTO1FBQ3RCLFVBQVUsRUFBRSxDQUFDO1FBQ2Isa0JBQWtCLEVBQUUsS0FBSztLQUMxQjtDQUNGLENBQUM7QUFFYyxxQ0FBa0IsR0FBRyxDQUFDLENBQUM7QUFDdkIseUNBQXNCLEdBQUcsQ0FBQyxDQUFDO0FBQzNCLHdDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQzs7O0lBOUIzRCxxQ0FBc0Q7O0lBQ3RELHlDQU9FOztJQUNGLG1DQUFxQzs7SUFDckMsc0NBQXdEOztJQUN4RCxxQ0FBc0M7O0lBQ3RDLHdDQUEyRDs7SUFDM0Qsc0NBQXVDOztJQUN2Qyx5Q0FBNkQ7O0lBQzdELHVDQVdFOztJQUVGLHNDQUF1Qzs7SUFDdkMsMENBQTJDOztJQUMzQyx5Q0FBMkQ7O0lBRTNELG1DQUFvQjs7SUFDcEIsaURBQWtDOztJQUNsQywyQ0FBNkI7O0lBQzdCLHdEQUF5Qzs7SUFDekMscUNBQXNCOztJQUN0QixvQ0FBcUI7O0lBQ3JCLDBDQUEyQjs7SUFDM0IseUNBQTBCOztJQUMxQix1Q0FBNkI7O0lBQzdCLHlDQUErQjs7SUFDL0Isd0RBQXlDOztJQUN6Qyw4Q0FBZ0M7O0lBQ2hDLCtDQUFnQzs7SUFDaEMsa0RBQW1DOztJQUNuQyxxREFBc0M7O0lBQ3RDLHNEQUF1Qzs7SUFHdkMsd0NBQStCOztJQUMvQixvQ0FBdUI7O0lBQ3ZCLG9DQUF1Qjs7SUFDdkIsMENBQW1DOztJQUNuQyx3Q0FBK0I7O0lBQy9CLG1DQUFxQjs7SUFDckIseUNBQWlDOztJQUNqQyxzQ0FBMkI7O0lBQzNCLHlDQUFpQzs7SUFDakMsMkNBQXFDOztJQUNyQyx1Q0FBNkI7O0lBQzdCLHVDQUE2Qjs7SUFDN0IseUNBQWlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuXG5pbXBvcnQgeyBWaXN1YWxFZGl0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcbmltcG9ydCB7IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jb2xvci1waWNrZXItZGlhbG9nL2NvbG9yLXBpY2tlci1kaWFsb2cuY29tcG9uZW50JztcbmltcG9ydCB7XG4gIGZhRm9udCxcbiAgZmFUcmFzaEFsdCxcbiAgZmFCb2xkLFxuICBmYUFsaWduUmlnaHQsXG4gIGZhRmlsbERyaXAsXG4gIGZhUGVuLFxuICBmYUFsaWduTGVmdCxcbiAgZmFJdGFsaWMsXG4gIGZhVW5kZXJsaW5lLFxuICBmYUFsaWduQ2VudGVyLFxuICBmYVJldHdlZXQsXG4gIGZhQXJyb3dVcCxcbiAgZmFBcnJvd0Rvd25cbn0gZnJvbSAnQGZvcnRhd2Vzb21lL2ZyZWUtc29saWQtc3ZnLWljb25zJztcblxuaW1wb3J0IHsgQWN0aW9uVHlwZXMsIE9iamVjdFR5cGVzIH0gZnJvbSAnLi9hY3Rpb24tdHlwZXMnO1xuXG5leHBvcnQgY2xhc3MgRGVzaWduRWRpdG9yQ29uZmlnIHtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250RmFtaWx5ID0gJ1RpbWVzIE5ldyBSb21hbic7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVGb250RmFtaWxpZXMgPSBbXG4gICAgRGVzaWduRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICdHZW9yZ2lhJyxcbiAgICAnSGVsdmV0aWNhJyxcbiAgICAnQ29taWMgU2FucyBNUycsXG4gICAgJ0ltcGFjdCcsXG4gICAgJ0NvdXJpZXIgTmV3J1xuICBdO1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdEZvbnRTaXplID0gMzQ7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVGb250U2l6ZXMgPSBfLnJhbmdlKDgsIDUwICsgMSk7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0TGluZUhlaWdodCA9IDE7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVMaW5lSGVpZ2h0cyA9IF8ucmFuZ2UoMCwgMTEsIDAuNSk7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0Q2hhclNwYWNpbmcgPSAwO1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlQ2hhclNwYWNpbmdzID0gXy5yYW5nZSgwLCA4MDEsIDEwMCk7XG4gIHN0YXRpYyByZWFkb25seSBhZGRUZXh0Q29uZmlnUGFyYW1zID0ge1xuICAgIHRleHQ6ICc8dGV4dD4nLFxuICAgIGNvbmZpZzoge1xuICAgICAgbGVmdDogMTAsXG4gICAgICB0b3A6IDEwLFxuICAgICAgZm9udEZhbWlseTogRGVzaWduRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IERlc2lnbkVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udFNpemUsXG4gICAgICBjb3JuZXJDb2xvcjogJyM1YzU5ZjAnLFxuICAgICAgY29ybmVyU2l6ZTogOCxcbiAgICAgIHRyYW5zcGFyZW50Q29ybmVyczogZmFsc2VcbiAgICB9XG4gIH07XG5cbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRTdHJva2VXaWR0aCA9IDA7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0TGluZVN0cm9rZVdpZHRoID0gNTtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZVN0cm9rZVdpZHRocyA9IF8ucmFuZ2UoMCwgNTAgKyAxKTtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scyA9IHRydWU7XG4gIHB1YmxpYyBoYXNab29tID0gdHJ1ZTtcbiAgcHVibGljIGhhc1BhbiA9IHRydWU7XG4gIHB1YmxpYyBpc1dpZGVDYW52YXMgPSB0cnVlO1xuICBwdWJsaWMgaXNTaG93RnJhbWUgPSB0cnVlO1xuICBwdWJsaWMgdG9vbHNGbGV4ID0gJzAgMSAxMyUnO1xuICBwdWJsaWMgYWN0aW9uc0ZsZXggPSAnMCAxIDIwJSc7XG4gIHB1YmxpYyBoYXNTaG9ydGN1dHNFeHBhbnNpb25QYW5lbCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDbG9uZVNob3J0Y3V0ID0gZmFsc2U7XG4gIHB1YmxpYyBoYXNSZW1vdmVTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNSZW1vdmVBbGxTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNCcmluZ0ZvcndhcmRTaG9ydGN1dCA9IHRydWU7XG4gIHB1YmxpYyBoYXNTZW5kQmFja3dhcmRzU2hvcnRjdXQgPSB0cnVlO1xuXG4gIC8vIEZvbnRBd2Vzb21lIEljb25zXG4gIHB1YmxpYyBmYVRyYXNoQWx0ID0gZmFUcmFzaEFsdDtcbiAgcHVibGljIGZhRm9udCA9IGZhRm9udDtcbiAgcHVibGljIGZhQm9sZCA9IGZhQm9sZDtcbiAgcHVibGljIGZhQWxpZ25SaWdodCA9IGZhQWxpZ25SaWdodDtcbiAgcHVibGljIGZhRmlsbERyaXAgPSBmYUZpbGxEcmlwO1xuICBwdWJsaWMgZmFQZW4gPSBmYVBlbjtcbiAgcHVibGljIGZhQWxpZ25MZWZ0ID0gZmFBbGlnbkxlZnQ7XG4gIHB1YmxpYyBmYUl0YWxpYyA9IGZhSXRhbGljO1xuICBwdWJsaWMgZmFVbmRlcmxpbmUgPSBmYVVuZGVybGluZTtcbiAgcHVibGljIGZhQWxpZ25DZW50ZXIgPSBmYUFsaWduQ2VudGVyO1xuICBwdWJsaWMgZmFSZXR3ZWV0ID0gZmFSZXR3ZWV0O1xuICBwdWJsaWMgZmFBcnJvd1VwID0gZmFBcnJvd1VwO1xuICBwdWJsaWMgZmFBcnJvd0Rvd24gPSBmYUFycm93RG93bjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnRvb2xzID0gW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnY29uZmlnJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHNoYXBlJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLlNIQVBFLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRmlsbCBjb2xvcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0ZJTEwgQ09MT1InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRmlsbERyaXAsXG4gICAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyMwMDAwMDAnLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25UZXh0OiAnU0VUJ1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0RmlsbENvbG9yXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCxcbiAgICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uU3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsXG4gICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGJsdXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQkxVUicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAgICBrZXlOYW1lOiAnYmx1cicsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYW5nbGUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQU5HTEUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2FuZ2xlJyxcbiAgICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyB3aWR0aCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBMRU5HVEgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBrZXlOYW1lOiAnbGVuZ3RoJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMjUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBjb2xvcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBDT0xPUicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFQZW4sXG4gICAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgICBjb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyMwMDAwMDAnLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25UZXh0OiAnU0VUJ1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93Q29sb3JcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLFxuICAgICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGljb25TdHlsZToge1xuICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJyxcbiAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAgLy8gTk9URTogZnVuY3Rpb25hbGl0eSB0dXJuZWQgb2ZmXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2Ugd2lkdGgnLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzc1JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIFdJRFRIJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hdmFpbGFibGVTdHJva2VXaWR0aHMsXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgIC8vICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICcyNSUnXG4gICAgICAgICAgLy8gICB9XG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2UgY29sb3InLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzY3JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIENPTE9SJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgIC8vICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtcGVuJyxcbiAgICAgICAgICAvLyAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgLy8gICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAvLyAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgIC8vICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlQ29sb3JcbiAgICAgICAgICAvLyAgICAgfSxcbiAgICAgICAgICAvLyAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgIC8vICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVRyYXNoQWx0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGxpbmUnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuTElORSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1N0cm9rZSB3aWR0aCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IERlc2lnbkVkaXRvckNvbmZpZy5hdmFpbGFibGVTdHJva2VXaWR0aHMsXG4gICAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjpcbiAgICAgICAgICAgICAgICAgIERlc2lnbkVkaXRvckNvbmZpZy5kZWZhdWx0TGluZVN0cm9rZVdpZHRoLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VXaWR0aCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTdHJva2UgY29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTVFJPS0UgQ09MT1InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhUGVuLFxuICAgICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICcjMDAwMDAwJyxcbiAgICAgICAgICAgICAgICAgICAgdGl0bGVUZXh0OiAnU0VUIENPTE9SJyxcbiAgICAgICAgICAgICAgICAgICAgYnV0dG9uVGV4dDogJ1NFVCdcbiAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0ge1xuICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCxcbiAgICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uU3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsXG4gICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dVcCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQXJyb3dEb3duLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFUcmFzaEFsdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuZGVsZXRlU2VsZWN0aW9uLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfVxuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCB0ZXh0JyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLlRFWEJPWCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0ZvbnQgRmFtaWx5JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUZvbnRGYW1pbGllcyxcbiAgICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMSA3NSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnRm9udCBTaXplJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogRGVzaWduRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUZvbnRTaXplcyxcbiAgICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdEZvbnRTaXplLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGb250U2l6ZSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAxIDc1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdMaW5lIEhlaWdodCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IERlc2lnbkVkaXRvckNvbmZpZy5hdmFpbGFibGVMaW5lSGVpZ2h0cyxcbiAgICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuZGVmYXVsdExpbmVIZWlnaHQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldExpbmVIZWlnaHQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyA3NSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ2hhcmFjdGVyIFNwYWNpbmcnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICAgIGRyb3Bkb3duT3B0aW9uOiBEZXNpZ25FZGl0b3JDb25maWcuYXZhaWxhYmxlQ2hhclNwYWNpbmdzLFxuICAgICAgICAgICAgICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246IERlc2lnbkVkaXRvckNvbmZpZy5kZWZhdWx0Q2hhclNwYWNpbmcsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldENoYXJhY3RlclNwYWNpbmcsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyA3NSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gTGVmdCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkxlZnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduTGVmdCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gQ2VudGVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFsaWduQ2VudGVyLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQWxpZ24gUmlnaHQnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhQWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCb2xkJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUJvbGQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEJvbGQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0l0YWxpYycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFJdGFsaWMsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEl0YWxpYyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnVW5kZXJsaW5lJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVVuZGVybGluZSxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0VW5kZXJsaW5lLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0ZvbnQgQ29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnIzAwMDAwMCcsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvblRleHQ6ICdTRVQnXG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGaWxsQ29sb3JcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7ICdib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gIH07XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpY29uU3R5bGU6IHsgJ2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJyB9LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCcmluZyBGcm9udCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NlbmQgQmFjaycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVRyYXNoQWx0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGltYWdlJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLklNQUdFLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbXG4gICAgICAgICAgY2FudmFzID0+IHtcbiAgICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGJsdXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgQmx1cicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAgICBrZXlOYW1lOiAnYmx1cicsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYW5nbGUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgQW5nbGUnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2FuZ2xlJyxcbiAgICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhRm9udCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyB3aWR0aCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBMZW5ndGgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBrZXlOYW1lOiAnbGVuZ3RoJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgY29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgQ29sb3InLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhUGVuLFxuICAgICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgICAgLy9jb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJyMwMDAwMDAnLFxuICAgICAgICAgICAgICAgICAgICB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLFxuICAgICAgICAgICAgICAgICAgICBidXR0b25UZXh0OiAnU0VUJ1xuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93Q29sb3JcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgICdib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLFxuICAgICAgICAgICAgICAgICAgICAncGFkZGluZy1ib3R0b20nOiAnMnB4J1xuICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGljb25TdHlsZToge1xuICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJyxcbiAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAgLy8gTk9URTogZnVuY3Rpb25hbGl0eSB0dXJuZWQgb2ZmXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2Ugd2lkdGgnLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzY3JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIFdJRFRIJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hdmFpbGFibGVTdHJva2VXaWR0aHMsXG4gICAgICAgICAgLy8gICAgIGRyb3Bkb3duU2VsZWN0ZWRPcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgIC8vICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9XG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAgLy8ge2FjdGlvbnM6IFtcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTdHJva2UgY29sb3InLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzY3JSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU1RST0tFIENPTE9SJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgIC8vICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtcGVuJyxcbiAgICAgICAgICAvLyAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgLy8gICAgICAgY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAvLyAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgIC8vICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlQ29sb3JcbiAgICAgICAgICAvLyAgICAgfSxcbiAgICAgICAgICAvLyAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgIC8vICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAvLyAgIH0sXG4gICAgICAgICAgLy8gXX0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVRyYXNoQWx0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHByb2R1Y3QnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuUFJPRFVDVCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogW1xuICAgICAgICAgIGNhbnZhcyA9PiB7XG4gICAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICAgIH1cbiAgICAgICAgXSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEJMVVInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2JsdXInLFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGFuZ2xlJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEFOR0xFJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGtleU5hbWU6ICdhbmdsZScsXG4gICAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUZvbnQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgd2lkdGgnLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgTEVOR1RIJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgICAga2V5TmFtZTogJ2xlbmd0aCcsXG4gICAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFGb250LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGNvbG9yJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIENPTE9SJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYVBlbixcbiAgICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiAnIzAwMDAwMCcsXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsXG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvblRleHQ6ICdTRVQnXG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3dDb2xvclxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgJ2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsXG4gICAgICAgICAgICAgICAgICAgICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnXG4gICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgaWNvblN0eWxlOiB7XG4gICAgICAgICAgICAgICAgICAnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLFxuICAgICAgICAgICAgICAgICAgJ3BhZGRpbmctYm90dG9tJzogJzJweCdcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgaXNFbmRTZWN0aW9uOiB0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0JSSU5HIEZST05UJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93VXAsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFycm93RG93bixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDTEVBUiBTRUxFQ1RJT04nLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uQ2xhc3M6IGZhVHJhc2hBbHQsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgZ3JvdXAnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuR1JPVVAsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFtcbiAgICAgICAgICBjYW52YXMgPT4ge1xuICAgICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgICB9XG4gICAgICAgIF0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBMZWZ0JyxcbiAgICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgICAgaWNvbkNsYXNzOiBmYUFsaWduTGVmdCxcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25MZWZ0LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBIb3Jpem9udGFsIENlbnRlcicsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Ib3Jpem9udGFsQ2VudGVyLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBSaWdodCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBbGlnblJpZ2h0LFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblJpZ2h0LFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhY3Rpb25zOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0FsaWduIFRvcCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25NYXRlcmlhbDogJ3ZlcnRpY2FsX2FsaWduX3RvcCcsXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uVG9wLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBWZXJ0aWNhbCBDZW50ZXInLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl9jZW50ZXInLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblZlcnRpY2FsQ2VudGVyLFxuICAgICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdBbGlnbiBCb3R0b20nLFxuICAgICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgICBpY29uTWF0ZXJpYWw6ICd2ZXJ0aWNhbF9hbGlnbl9ib3R0b20nLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkJvdHRvbSxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIGlzRW5kU2VjdGlvbjogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgYWN0aW9uczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbGFiZWw6ICdCcmluZyBGcm9udCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd1VwLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1NlbmQgQmFjaycsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGljb25DbGFzczogZmFBcnJvd0Rvd24sXG4gICAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBpc0VuZFNlY3Rpb246IHRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFjdGlvbnM6IFtcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGxhYmVsOiAnUmVtb3ZlIEFsbCcsXG4gICAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb25Hcm91cCxcbiAgICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfVxuICAgIF07XG4gIH1cbn1cbiJdfQ==