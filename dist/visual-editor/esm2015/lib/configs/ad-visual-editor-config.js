/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';
export class AdVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add shape',
                iconImgUrl: '@app/../assets/images/add-shape-icon.png',
                iconDisplayText: 'ADD SHAPE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ADD RECTANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-rectangle-icon.png',
                                action: VisualEditorService.addRectangle,
                                onSelectParams: AdVisualEditorConfig.addRectangleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Rectangle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD LINE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-line-icon.png',
                                action: VisualEditorService.addLine,
                                onSelectParams: AdVisualEditorConfig.addLineConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Line',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD ELLIPSE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-ellipse-icon.png',
                                action: VisualEditorService.addCircle,
                                onSelectParams: AdVisualEditorConfig.addCircleConfigParams,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Ellipse',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD TRIANGLE',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/shape-triangle-icon.png',
                                action: VisualEditorService.addTriangle,
                                onSelectParams: AdVisualEditorConfig.addTriangleConfigParams,
                                flex: '33%'
                            },
                            {
                                label: 'Triangle',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Fill color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'FILL COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-fill-drip',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '75%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '25%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'edit line',
                isHidden: true,
                objectType: ObjectTypes.LINE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Stroke width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'STROKE WIDTH',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultLineStrokeWidth,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setStrokeWidth,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Stroke color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'STROKE COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setStrokeColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: AdVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: AdVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: AdVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: AdVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    // NOTE: functionality turned off
                    // {actions: [
                    //   {
                    //     label: 'Stroke width',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE WIDTH',
                    //     type: ActionTypes.DROPDOWN,
                    //     dropdownOption: AdVisualEditorConfig.availableStrokeWidths,
                    //     dropdownSelectedOption: AdVisualEditorConfig.defaultStrokeWidth,
                    //     iconClass: 'fas fa-font',
                    //     action: VisualEditorService.setStrokeWidth,
                    //     flex: '67%'
                    //   }
                    // ]},
                    // {actions: [
                    //   {
                    //     label: 'Stroke color',
                    //     type: ActionTypes.LABEL,
                    //     flex: '67%'
                    //   },
                    //   {
                    //     label: 'STROKE COLOR',
                    //     type: ActionTypes.DIALOG,
                    //     iconClass: 'fas fa-pen',
                    //     dialog: {
                    //       component: ColorPickerDialogComponent,
                    //       data: {color: '#000000', titleText: 'SET COLOR', buttonText: 'SET'},
                    //       onClose: VisualEditorService.setStrokeColor
                    //     },
                    //     onActionReturn: (action, val) => {
                    //       action.dialog.data.color = val;
                    //       action.iconStyle = {'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px'};
                    //     },
                    //     iconStyle: {'border-bottom': '3px solid black', 'padding-bottom': '2px'},
                    //     flex: '33%'
                    //   },
                    // ]},
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit product',
                isHidden: true,
                objectType: ObjectTypes.PRODUCT,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'CHANGE PRODUCT',
                                type: ActionTypes.OUTGOING_EVENT_TRIGGER,
                                iconClass: 'fas fa-retweet',
                                flex: '0 3 33%'
                            },
                            {
                                label: 'Change product',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'Shadow blur',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW BLUR',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'blur',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow angle',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW ANGLE',
                                type: ActionTypes.RELATED_INPUT,
                                keyName: 'angle',
                                groupName: 'SHADOW',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow width',
                                type: ActionTypes.LABEL,
                                flex: '3 3 75%'
                            },
                            {
                                label: 'SHADOW LENGTH',
                                type: ActionTypes.RELATED_INPUT,
                                groupName: 'SHADOW',
                                keyName: 'length',
                                inputValue: 0,
                                inputType: 'number',
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setShadow,
                                flex: '0 3 25%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'Shadow color',
                                type: ActionTypes.LABEL,
                                flex: '3 3 67%'
                            },
                            {
                                label: 'SHADOW COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-pen',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setShadowColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}`, 'padding-bottom': '2px' };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black', 'padding-bottom': '2px' },
                                flex: '0 3 33%'
                            },
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
AdVisualEditorConfig.defaultFontFamily = 'Times New Roman';
AdVisualEditorConfig.availableFontFamilies = [
    AdVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
AdVisualEditorConfig.defaultFontSize = 34;
AdVisualEditorConfig.availableFontSizes = _.range(8, 50 + 1);
AdVisualEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: AdVisualEditorConfig.defaultFontFamily,
        fontSize: AdVisualEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
AdVisualEditorConfig.defaultStrokeWidth = 0;
AdVisualEditorConfig.defaultLineStrokeWidth = 5;
AdVisualEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
AdVisualEditorConfig.addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: { left: 10, top: 10, stroke: '#000000', strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
};
AdVisualEditorConfig.addRectangleConfigParams = {
    config: { left: 10, top: 10, height: 100, width: 100, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
};
AdVisualEditorConfig.addLineConfigParams = {
    config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultLineStrokeWidth, stroke: 'black' },
    coords: [0, 0, 100, 100]
};
AdVisualEditorConfig.addCircleConfigParams = {
    config: { left: 10, top: 10, radius: 50, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' }
};
AdVisualEditorConfig.addTriangleConfigParams = {
    config: { left: 10, top: 10, strokeWidth: AdVisualEditorConfig.defaultStrokeWidth, stroke: 'black' },
    coords: [{ x: 50, y: 0 }, { x: 0, y: 86 }, { x: 100, y: 86 }]
};
if (false) {
    /** @type {?} */
    AdVisualEditorConfig.defaultFontFamily;
    /** @type {?} */
    AdVisualEditorConfig.availableFontFamilies;
    /** @type {?} */
    AdVisualEditorConfig.defaultFontSize;
    /** @type {?} */
    AdVisualEditorConfig.availableFontSizes;
    /** @type {?} */
    AdVisualEditorConfig.addTextConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    AdVisualEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    AdVisualEditorConfig.availableStrokeWidths;
    /** @type {?} */
    AdVisualEditorConfig.addImageConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addRectangleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addLineConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addCircleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.addTriangleConfigParams;
    /** @type {?} */
    AdVisualEditorConfig.prototype.tools;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    AdVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    AdVisualEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    AdVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    AdVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWQtdmlzdWFsLWVkaXRvci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXN1YWwtZWRpdG9yLyIsInNvdXJjZXMiOlsibGliL2NvbmZpZ3MvYWQtdmlzdWFsLWVkaXRvci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFMUQsTUFBTSxPQUFPLG9CQUFvQjtJQTBEL0I7UUFWTyx3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsK0JBQTBCLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2QsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsY0FBUyxHQUFHLFNBQVMsQ0FBQztRQUN0QixnQkFBVyxHQUFHLFNBQVMsQ0FBQztRQUc3QixJQUFJLENBQUMsS0FBSyxHQUFHO1lBQ1g7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsYUFBYSxFQUFFOzs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7YUFDSDtZQUNEO2dCQUNFLElBQUksRUFBRSxXQUFXO2dCQUNqQixVQUFVLEVBQUUsMENBQTBDO2dCQUN0RCxlQUFlLEVBQUUsV0FBVztnQkFDNUIsYUFBYSxFQUFFOzs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxlQUFlO2dDQUN0QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSxnREFBZ0Q7Z0NBQ3pELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxjQUFjLEVBQUUsb0JBQW9CLENBQUMsd0JBQXdCO2dDQUM3RCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFVBQVU7Z0NBQ2pCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDJDQUEyQztnQ0FDcEQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxtQkFBbUI7Z0NBQ3hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsTUFBTTtnQ0FDYixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw4Q0FBOEM7Z0NBQ3ZELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxjQUFjLEVBQUUsb0JBQW9CLENBQUMscUJBQXFCO2dDQUMxRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFNBQVM7Z0NBQ2hCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLCtDQUErQztnQ0FDeEQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyx1QkFBdUI7Z0NBQzVELElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxVQUFVO2dDQUNqQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFOztvQ0FFTixJQUFJLEVBQUUsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBQztvQ0FDbkUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLFlBQVk7aUNBQzFDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFO29DQUM5QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUMsZUFBZSxFQUFFLGFBQWEsR0FBRyxFQUFFLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDLENBQUM7Z0NBQ3BGLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDO2dDQUN4RSxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE1BQU07Z0NBQ2YsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsT0FBTztnQ0FDaEIsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsZUFBZTtnQ0FDdEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsT0FBTyxFQUFFLFFBQVE7Z0NBQ2pCLFVBQVUsRUFBRSxDQUFDO2dDQUNiLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFNBQVM7Z0NBQ3JDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUU7O29DQUVOLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDO29DQUNuRSxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0NBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBQyxlQUFlLEVBQUUsYUFBYSxHQUFHLEVBQUUsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUMsQ0FBQztnQ0FDcEYsQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRSxFQUFDLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUM7Z0NBQ3hFLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLGlDQUFpQztvQkFDakMsY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGtDQUFrQztvQkFDbEMsa0VBQWtFO29CQUNsRSx1RUFBdUU7b0JBQ3ZFLGdDQUFnQztvQkFDaEMsa0RBQWtEO29CQUNsRCxrQkFBa0I7b0JBQ2xCLE1BQU07b0JBQ04sTUFBTTtvQkFDTixjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsZ0NBQWdDO29CQUNoQywrQkFBK0I7b0JBQy9CLGdCQUFnQjtvQkFDaEIsK0NBQStDO29CQUMvQyw2RUFBNkU7b0JBQzdFLG9EQUFvRDtvQkFDcEQsU0FBUztvQkFDVCx5Q0FBeUM7b0JBQ3pDLHdDQUF3QztvQkFDeEMsMkZBQTJGO29CQUMzRixTQUFTO29CQUNULGdGQUFnRjtvQkFDaEYsa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxXQUFXO2dCQUNqQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLElBQUk7Z0JBQzVCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUTtnQ0FDMUIsY0FBYyxFQUFFLG9CQUFvQixDQUFDLHFCQUFxQjtnQ0FDMUQsc0JBQXNCLEVBQUUsb0JBQW9CLENBQUMsc0JBQXNCO2dDQUNuRSxTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0NBQzFDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsWUFBWTtnQ0FDdkIsTUFBTSxFQUFFOztvQ0FFTixJQUFJLEVBQUUsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBQztvQ0FDbkUsT0FBTyxFQUFFLG1CQUFtQixDQUFDLGNBQWM7aUNBQzVDO2dDQUNELGNBQWM7Ozs7O2dDQUFFLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFO29DQUM5QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO29DQUMvQixNQUFNLENBQUMsU0FBUyxHQUFHLEVBQUMsZUFBZSxFQUFFLGFBQWEsR0FBRyxFQUFFLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDLENBQUM7Z0NBQ3BGLENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFDO2dDQUN4RSxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELGVBQWUsRUFBRSxVQUFVO2dCQUMzQixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUUsbUJBQW1CLENBQUMsT0FBTztnQkFDckMsY0FBYyxFQUFFLG9CQUFvQixDQUFDLG1CQUFtQjthQUN6RDtZQUNEO2dCQUNFLElBQUksRUFBRSxXQUFXO2dCQUNqQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0JBQzlCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsb0JBQW9CLENBQUMscUJBQXFCO2dDQUMxRCxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxpQkFBaUI7Z0NBQzlELFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFVBQVU7NkJBQ2pCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2lDQUMxQztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFDLGVBQWUsRUFBRSxhQUFhLEdBQUcsRUFBRSxFQUFDLENBQUM7Z0NBQzNELENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUM7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsb0JBQW9CLENBQUMsa0JBQWtCO2dDQUN2RCxzQkFBc0IsRUFBRSxvQkFBb0IsQ0FBQyxlQUFlO2dDQUM1RCxTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxtQkFBbUI7Z0NBQzlCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLHFCQUFxQjtnQ0FDaEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsb0JBQW9CO2dDQUMvQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsVUFBVTtnQ0FDdEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxlQUFlO2dDQUMxQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELGVBQWUsRUFBRSxXQUFXO2dCQUM1QixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0JBQ3hCLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxRQUFRO2dCQUN0QyxjQUFjLEVBQUUsb0JBQW9CLENBQUMsb0JBQW9CO2FBQzFEO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxhQUFhO2dDQUMvQixPQUFPLEVBQUUsTUFBTTtnQ0FDZixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxPQUFPO2dDQUNoQixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxlQUFlO2dDQUN0QixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixPQUFPLEVBQUUsUUFBUTtnQ0FDakIsVUFBVSxFQUFFLENBQUM7Z0NBQ2IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxZQUFZO2dDQUN2QixNQUFNLEVBQUU7O29DQUVOLElBQUksRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFDO29DQUNuRSxPQUFPLEVBQUUsbUJBQW1CLENBQUMsY0FBYztpQ0FDNUM7Z0NBQ0QsY0FBYzs7Ozs7Z0NBQUUsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7b0NBQzlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7b0NBQy9CLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBQyxlQUFlLEVBQUUsYUFBYSxHQUFHLEVBQUUsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUMsQ0FBQztnQ0FDcEYsQ0FBQyxDQUFBO2dDQUNELFNBQVMsRUFBRSxFQUFDLGVBQWUsRUFBRSxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxLQUFLLEVBQUM7Z0NBQ3hFLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLGlDQUFpQztvQkFDakMsY0FBYztvQkFDZCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsK0JBQStCO29CQUMvQixrQkFBa0I7b0JBQ2xCLE9BQU87b0JBQ1AsTUFBTTtvQkFDTiw2QkFBNkI7b0JBQzdCLGtDQUFrQztvQkFDbEMsa0VBQWtFO29CQUNsRSx1RUFBdUU7b0JBQ3ZFLGdDQUFnQztvQkFDaEMsa0RBQWtEO29CQUNsRCxrQkFBa0I7b0JBQ2xCLE1BQU07b0JBQ04sTUFBTTtvQkFDTixjQUFjO29CQUNkLE1BQU07b0JBQ04sNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxNQUFNO29CQUNOLDZCQUE2QjtvQkFDN0IsZ0NBQWdDO29CQUNoQywrQkFBK0I7b0JBQy9CLGdCQUFnQjtvQkFDaEIsK0NBQStDO29CQUMvQyw2RUFBNkU7b0JBQzdFLG9EQUFvRDtvQkFDcEQsU0FBUztvQkFDVCx5Q0FBeUM7b0JBQ3pDLHdDQUF3QztvQkFDeEMsMkZBQTJGO29CQUMzRixTQUFTO29CQUNULGdGQUFnRjtvQkFDaEYsa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLE1BQU07b0JBQ04sRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxjQUFjO2dCQUNwQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLE9BQU87Z0JBQy9CLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsZ0JBQWdCO2dDQUN2QixJQUFJLEVBQUUsV0FBVyxDQUFDLHNCQUFzQjtnQ0FDeEMsU0FBUyxFQUFFLGdCQUFnQjtnQ0FDM0IsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxnQkFBZ0I7Z0NBQ3ZCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLGFBQWE7Z0NBQy9CLE9BQU8sRUFBRSxNQUFNO2dDQUNmLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsT0FBTyxFQUFFLE9BQU87Z0NBQ2hCLFNBQVMsRUFBRSxRQUFRO2dDQUNuQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGVBQWU7Z0NBQ3RCLElBQUksRUFBRSxXQUFXLENBQUMsYUFBYTtnQ0FDL0IsU0FBUyxFQUFFLFFBQVE7Z0NBQ25CLE9BQU8sRUFBRSxRQUFRO2dDQUNqQixVQUFVLEVBQUUsQ0FBQztnQ0FDYixTQUFTLEVBQUUsUUFBUTtnQ0FDbkIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLFlBQVk7Z0NBQ3ZCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2lDQUM1QztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFDLGVBQWUsRUFBRSxhQUFhLEdBQUcsRUFBRSxFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBQyxDQUFDO2dDQUNwRixDQUFDLENBQUE7Z0NBQ0QsU0FBUyxFQUFFLEVBQUMsZUFBZSxFQUFFLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLEtBQUssRUFBQztnQ0FDeEUsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGlCQUFpQjtnQ0FDeEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsa0JBQWtCO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsZUFBZTtnQ0FDM0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMkNBQTJDO2dDQUNwRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsa0JBQWtCO2dDQUM5QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLHlCQUF5QjtnQ0FDaEMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsd0RBQXdEO2dDQUNqRSxNQUFNLEVBQUUsbUJBQW1CLENBQUMsOEJBQThCO2dDQUMxRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG1CQUFtQjtnQ0FDL0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQjtnQ0FDN0MsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx1QkFBdUI7Z0NBQzlCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLHNEQUFzRDtnQ0FDL0QsTUFBTSxFQUFFLG1CQUFtQixDQUFDLDRCQUE0QjtnQ0FDeEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxjQUFjO2dDQUNyQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw2Q0FBNkM7Z0NBQ3RELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLG9CQUFvQjtnQ0FDaEQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOztBQTkzQmUsc0NBQWlCLEdBQUcsaUJBQWlCLENBQUM7QUFDdEMsMENBQXFCLEdBQUc7SUFDdEMsb0JBQW9CLENBQUMsaUJBQWlCO0lBQ3RDLFNBQVM7SUFDVCxXQUFXO0lBQ1gsZUFBZTtJQUNmLFFBQVE7SUFDUixhQUFhO0NBQ2QsQ0FBQztBQUNjLG9DQUFlLEdBQUcsRUFBRSxDQUFDO0FBQ3JCLHVDQUFrQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUN4Qyx3Q0FBbUIsR0FBRztJQUNwQyxJQUFJLEVBQUUsUUFBUTtJQUNkLE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxFQUFFO1FBQ1IsR0FBRyxFQUFFLEVBQUU7UUFDUCxVQUFVLEVBQUUsb0JBQW9CLENBQUMsaUJBQWlCO1FBQ2xELFFBQVEsRUFBRSxvQkFBb0IsQ0FBQyxlQUFlO1FBQzlDLFdBQVcsRUFBRSxTQUFTO1FBQ3RCLFVBQVUsRUFBRSxDQUFDO1FBQ2Isa0JBQWtCLEVBQUUsS0FBSztLQUMxQjtDQUNGLENBQUM7QUFDYyx1Q0FBa0IsR0FBRyxDQUFDLENBQUM7QUFDdkIsMkNBQXNCLEdBQUcsQ0FBQyxDQUFDO0FBQzNCLDBDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMzQyx5Q0FBb0IsR0FBRztJQUNyQyxPQUFPLEVBQUUsRUFBRTtJQUNYLE9BQU8sRUFBRSxJQUFJO0lBQ2IsTUFBTSxFQUFFLEVBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUM7Q0FBQyxDQUFDO0FBRWxILDZDQUF3QixHQUFHO0lBQ3pDLE1BQU0sRUFBRSxFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUM7Q0FDNUgsQ0FBQztBQUNjLHdDQUFtQixHQUFHO0lBQ3BDLE1BQU0sRUFBRSxFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsb0JBQW9CLENBQUMsc0JBQXNCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBQztJQUN0RyxNQUFNLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7Q0FDekIsQ0FBQztBQUNjLDBDQUFxQixHQUFHO0lBQ3RDLE1BQU0sRUFBRSxFQUFDLElBQUksRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRSxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFDO0NBQy9HLENBQUM7QUFDYyw0Q0FBdUIsR0FBRztJQUN4QyxNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUM7SUFDbEcsTUFBTSxFQUFFLENBQUMsRUFBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUMsRUFBRSxFQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFDLENBQUM7Q0FDeEQsQ0FBQzs7O0lBNUNGLHVDQUFzRDs7SUFDdEQsMkNBT0U7O0lBQ0YscUNBQXFDOztJQUNyQyx3Q0FBd0Q7O0lBQ3hELHlDQVdFOztJQUNGLHdDQUF1Qzs7SUFDdkMsNENBQTJDOztJQUMzQywyQ0FBMkQ7O0lBQzNELDBDQUdrSTs7SUFFbEksOENBRUU7O0lBQ0YseUNBR0U7O0lBQ0YsMkNBRUU7O0lBQ0YsNkNBR0U7O0lBRUYscUNBQW9COztJQUNwQixtREFBa0M7O0lBQ2xDLDZDQUE2Qjs7SUFDN0IsMERBQXlDOztJQUN6Qyx1Q0FBc0I7O0lBQ3RCLHNDQUFxQjs7SUFDckIsNENBQTJCOztJQUMzQiwyQ0FBMEI7O0lBQzFCLHlDQUE2Qjs7SUFDN0IsMkNBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuXG5pbXBvcnQgeyBWaXN1YWxFZGl0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vYWN0aW9uLXR5cGVzJztcblxuZXhwb3J0IGNsYXNzIEFkVmlzdWFsRWRpdG9yQ29uZmlnIHtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRGb250RmFtaWx5ID0gJ1RpbWVzIE5ldyBSb21hbic7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVGb250RmFtaWxpZXMgPSBbXG4gICAgQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRGYW1pbHksXG4gICAgJ0dlb3JnaWEnLFxuICAgICdIZWx2ZXRpY2EnLFxuICAgICdDb21pYyBTYW5zIE1TJyxcbiAgICAnSW1wYWN0JyxcbiAgICAnQ291cmllciBOZXcnXG4gIF07XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0Rm9udFNpemUgPSAzNDtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZUZvbnRTaXplcyA9IF8ucmFuZ2UoOCwgNTAgKyAxKTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZFRleHRDb25maWdQYXJhbXMgPSB7XG4gICAgdGV4dDogJzx0ZXh0PicsXG4gICAgY29uZmlnOiB7XG4gICAgICBsZWZ0OiAxMCxcbiAgICAgIHRvcDogMTAsXG4gICAgICBmb250RmFtaWx5OiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAgIGZvbnRTaXplOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udFNpemUsXG4gICAgICBjb3JuZXJDb2xvcjogJyM1YzU5ZjAnLFxuICAgICAgY29ybmVyU2l6ZTogOCxcbiAgICAgIHRyYW5zcGFyZW50Q29ybmVyczogZmFsc2VcbiAgICB9XG4gIH07XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0U3Ryb2tlV2lkdGggPSAwO1xuICBzdGF0aWMgcmVhZG9ubHkgZGVmYXVsdExpbmVTdHJva2VXaWR0aCA9IDU7XG4gIHN0YXRpYyByZWFkb25seSBhdmFpbGFibGVTdHJva2VXaWR0aHMgPSBfLnJhbmdlKDAsIDUwICsgMSk7XG4gIHN0YXRpYyByZWFkb25seSBhZGRJbWFnZUNvbmZpZ1BhcmFtcyA9IHtcbiAgICBmaWxlVXJsOiAnJyxcbiAgICBzY2FsaW5nOiAwLjM1LFxuICAgIGNvbmZpZzoge2xlZnQ6IDEwLCB0b3A6IDEwLCBzdHJva2U6ICcjMDAwMDAwJywgc3Ryb2tlV2lkdGg6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCwgY3Jvc3NPcmlnaW46ICdhbm9ueW1vdXMnfX07XG5cbiAgc3RhdGljIHJlYWRvbmx5IGFkZFJlY3RhbmdsZUNvbmZpZ1BhcmFtcyA9IHtcbiAgICBjb25maWc6IHtsZWZ0OiAxMCwgdG9wOiAxMCwgaGVpZ2h0OiAxMDAsIHdpZHRoOiAxMDAsIHN0cm9rZVdpZHRoOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsIHN0cm9rZTogJ2JsYWNrJ31cbiAgfTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZExpbmVDb25maWdQYXJhbXMgPSB7XG4gICAgY29uZmlnOiB7bGVmdDogMTAsIHRvcDogMTAsIHN0cm9rZVdpZHRoOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0TGluZVN0cm9rZVdpZHRoLCBzdHJva2U6ICdibGFjayd9LFxuICAgIGNvb3JkczogWzAsIDAsIDEwMCwgMTAwXVxuICB9O1xuICBzdGF0aWMgcmVhZG9ubHkgYWRkQ2lyY2xlQ29uZmlnUGFyYW1zID0ge1xuICAgIGNvbmZpZzoge2xlZnQ6IDEwLCB0b3A6IDEwLCByYWRpdXM6IDUwLCBzdHJva2VXaWR0aDogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdFN0cm9rZVdpZHRoLCBzdHJva2U6ICdibGFjayd9XG4gIH07XG4gIHN0YXRpYyByZWFkb25seSBhZGRUcmlhbmdsZUNvbmZpZ1BhcmFtcyA9IHtcbiAgICBjb25maWc6IHtsZWZ0OiAxMCwgdG9wOiAxMCwgc3Ryb2tlV2lkdGg6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCwgc3Ryb2tlOiAnYmxhY2snfSxcbiAgICBjb29yZHM6IFt7eDogNTAsIHk6IDB9LCB7eDogMCwgeTogODZ9LCB7eDogMTAwLCB5OiA4Nn1dXG4gIH07XG5cbiAgcHVibGljIHRvb2xzOiBhbnlbXTtcbiAgcHVibGljIGlzRmlyc3RUb29sU2VsZWN0ZWQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2hlY2tlcnNCZyA9IGZhbHNlO1xuICBwdWJsaWMgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHMgPSB0cnVlO1xuICBwdWJsaWMgaGFzWm9vbSA9IHRydWU7XG4gIHB1YmxpYyBoYXNQYW4gPSB0cnVlO1xuICBwdWJsaWMgaXNXaWRlQ2FudmFzID0gdHJ1ZTtcbiAgcHVibGljIGlzU2hvd0ZyYW1lID0gdHJ1ZTtcbiAgcHVibGljIHRvb2xzRmxleCA9ICcwIDIgMTAlJztcbiAgcHVibGljIGFjdGlvbnNGbGV4ID0gJzAgMiAyMCUnO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudG9vbHMgPSBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdjb25maWcnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnYWRkIHNoYXBlJyxcbiAgICAgICAgaWNvbkltZ1VybDogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hZGQtc2hhcGUtaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgU0hBUEUnLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBREQgUkVDVEFOR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NoYXBlLXJlY3RhbmdsZS1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRSZWN0YW5nbGUsXG4gICAgICAgICAgICAgIG9uU2VsZWN0UGFyYW1zOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hZGRSZWN0YW5nbGVDb25maWdQYXJhbXMsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdSZWN0YW5nbGUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBREQgTElORScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zaGFwZS1saW5lLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZExpbmUsXG4gICAgICAgICAgICAgIG9uU2VsZWN0UGFyYW1zOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hZGRMaW5lQ29uZmlnUGFyYW1zLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnTGluZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FERCBFTExJUFNFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NoYXBlLWVsbGlwc2UtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkQ2lyY2xlLFxuICAgICAgICAgICAgICBvblNlbGVjdFBhcmFtczogQWRWaXN1YWxFZGl0b3JDb25maWcuYWRkQ2lyY2xlQ29uZmlnUGFyYW1zLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnRWxsaXBzZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FERCBUUklBTkdMRScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zaGFwZS10cmlhbmdsZS1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRUcmlhbmdsZSxcbiAgICAgICAgICAgICAgb25TZWxlY3RQYXJhbXM6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmFkZFRyaWFuZ2xlQ29uZmlnUGFyYW1zLFxuICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdUcmlhbmdsZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHNoYXBlJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLlNIQVBFLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGaWxsIGNvbG9yJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGSUxMIENPTE9SJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZmlsbC1kcmlwJyxcbiAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgLy9jb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGRhdGE6IHtjb2xvcjogJyMwMDAwMDAnLCB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLCBidXR0b25UZXh0OiAnU0VUJ30sXG4gICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGaWxsQ29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J307XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjaycsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYmx1cicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEJMVVInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICBrZXlOYW1lOiAnYmx1cicsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYW5nbGUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBBTkdMRScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdhbmdsZScsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgd2lkdGgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBMRU5HVEgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICBrZXlOYW1lOiAnbGVuZ3RoJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGNvbG9yJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1wZW4nLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvd0NvbG9yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIC8vIE5PVEU6IGZ1bmN0aW9uYWxpdHkgdHVybmVkIG9mZlxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIHdpZHRoJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc3NSUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBXSURUSCcsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93bk9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlU3Ryb2tlV2lkdGhzLFxuICAgICAgICAgIC8vICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgLy8gICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAvLyAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZVdpZHRoLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnMjUlJ1xuICAgICAgICAgIC8vICAgfVxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIC8vIHthY3Rpb25zOiBbXG4gICAgICAgICAgLy8gICB7XG4gICAgICAgICAgLy8gICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAvLyAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1NUUk9LRSBDT0xPUicsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkRJQUxPRyxcbiAgICAgICAgICAvLyAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXBlbicsXG4gICAgICAgICAgLy8gICAgIGRpYWxvZzoge1xuICAgICAgICAgIC8vICAgICAgIGNvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgLy8gICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAvLyAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgLy8gICAgIH0sXG4gICAgICAgICAgLy8gICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAvLyAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgLy8gICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgIC8vICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgLy8gICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgLy8gICB9LFxuICAgICAgICAgIC8vIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGxpbmUnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuTElORSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU3Ryb2tlIHdpZHRoJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTVFJPS0UgV0lEVEgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmF2YWlsYWJsZVN0cm9rZVdpZHRocyxcbiAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdExpbmVTdHJva2VXaWR0aCxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlV2lkdGgsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU3Ryb2tlIGNvbG9yJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTVFJPS0UgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1wZW4nLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFN0cm9rZUNvbG9yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXSxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgdGV4dCcsXG4gICAgICAgIGljb25JbWdVcmw6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWRkLXRleHQtaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgVEVYVCcsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkVGV4dCxcbiAgICAgICAgb25TZWxlY3RQYXJhbXM6IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmFkZFRleHRDb25maWdQYXJhbXMsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCB0ZXh0JyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLlRFWEJPWCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnRk9OVCBGQU1JTFknLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUZvbnRGYW1pbGllcyxcbiAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEZvbnRGYW1pbHksXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMTAwJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0ZPTlQgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgLy9jb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGRhdGE6IHtjb2xvcjogJyMwMDAwMDAnLCB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLCBidXR0b25UZXh0OiAnU0VUJ30sXG4gICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGaWxsQ29sb3JcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25BY3Rpb25SZXR1cm46IChhY3Rpb24sIHZhbCkgPT4ge1xuICAgICAgICAgICAgICAgIGFjdGlvbi5kaWFsb2cuZGF0YS5jb2xvciA9IHZhbDtcbiAgICAgICAgICAgICAgICBhY3Rpb24uaWNvblN0eWxlID0geydib3JkZXItYm90dG9tJzogYDNweCBzb2xpZCAke3ZhbH1gfTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7J2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJ30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGT05UIFNJWkUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAgICAgZHJvcGRvd25PcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUZvbnRTaXplcyxcbiAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRTaXplLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGb250U2l6ZSxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIExFRlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1hbGlnbi1sZWZ0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduTGVmdCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWFsaWduLWNlbnRlcicsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFJJR0hUJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtYWxpZ24tcmlnaHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0JPTEQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1ib2xkJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEJvbGQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdJVEFMSUMnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1pdGFsaWMnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0SXRhbGljLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnVU5ERVJMSU5FJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdW5kZXJsaW5lJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFVuZGVybGluZSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0JSSU5HIEZST05UJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2JyaW5nLWZyb250LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zZW5kLWJhY2staWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXRyYXNoLWFsdCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnYWRkIGltYWdlJyxcbiAgICAgICAgaWNvbkltZ1VybDogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hZGQtaW1hZ2UtaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgSU1BR0UnLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlVQTE9BRCxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkSW1hZ2UsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiBBZFZpc3VhbEVkaXRvckNvbmZpZy5hZGRJbWFnZUNvbmZpZ1BhcmFtcyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IGltYWdlJyxcbiAgICAgICAgaXNIaWRkZW46IHRydWUsXG4gICAgICAgIG9iamVjdFR5cGU6IE9iamVjdFR5cGVzLklNQUdFLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYmx1cicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEJMVVInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICBrZXlOYW1lOiAnYmx1cicsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgYW5nbGUnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBBTkdMRScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdhbmdsZScsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGlucHV0VmFsdWU6IDAsXG4gICAgICAgICAgICAgIGlucHV0VHlwZTogJ251bWJlcicsXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1mb250JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvdyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAyNSUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTaGFkb3cgd2lkdGgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5MQUJFTCxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA3NSUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NIQURPVyBMRU5HVEgnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRUxBVEVEX0lOUFVULFxuICAgICAgICAgICAgICBncm91cE5hbWU6ICdTSEFET1cnLFxuICAgICAgICAgICAgICBrZXlOYW1lOiAnbGVuZ3RoJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBjb2xvcicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIENPTE9SJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtcGVuJyxcbiAgICAgICAgICAgICAgZGlhbG9nOiB7XG4gICAgICAgICAgICAgICAgLy9jb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgICAgICAgIGRhdGE6IHtjb2xvcjogJyMwMDAwMDAnLCB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLCBidXR0b25UZXh0OiAnU0VUJ30sXG4gICAgICAgICAgICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3dDb2xvclxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7J2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7J2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJywgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICAvLyBOT1RFOiBmdW5jdGlvbmFsaXR5IHR1cm5lZCBvZmZcbiAgICAgICAgICAvLyB7YWN0aW9uczogW1xuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1N0cm9rZSB3aWR0aCcsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgIC8vICAgfSxcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTVFJPS0UgV0lEVEgnLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5EUk9QRE9XTixcbiAgICAgICAgICAvLyAgICAgZHJvcGRvd25PcHRpb246IEFkVmlzdWFsRWRpdG9yQ29uZmlnLmF2YWlsYWJsZVN0cm9rZVdpZHRocyxcbiAgICAgICAgICAvLyAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogQWRWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdFN0cm9rZVdpZHRoLFxuICAgICAgICAgIC8vICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgLy8gICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VXaWR0aCxcbiAgICAgICAgICAvLyAgICAgZmxleDogJzY3JSdcbiAgICAgICAgICAvLyAgIH1cbiAgICAgICAgICAvLyBdfSxcbiAgICAgICAgICAvLyB7YWN0aW9uczogW1xuICAgICAgICAgIC8vICAge1xuICAgICAgICAgIC8vICAgICBsYWJlbDogJ1N0cm9rZSBjb2xvcicsXG4gICAgICAgICAgLy8gICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgIC8vICAgfSxcbiAgICAgICAgICAvLyAgIHtcbiAgICAgICAgICAvLyAgICAgbGFiZWw6ICdTVFJPS0UgQ09MT1InLFxuICAgICAgICAgIC8vICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgLy8gICAgIGljb25DbGFzczogJ2ZhcyBmYS1wZW4nLFxuICAgICAgICAgIC8vICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAvLyAgICAgICBjb21wb25lbnQ6IENvbG9yUGlja2VyRGlhbG9nQ29tcG9uZW50LFxuICAgICAgICAgIC8vICAgICAgIGRhdGE6IHtjb2xvcjogJyMwMDAwMDAnLCB0aXRsZVRleHQ6ICdTRVQgQ09MT1InLCBidXR0b25UZXh0OiAnU0VUJ30sXG4gICAgICAgICAgLy8gICAgICAgb25DbG9zZTogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VDb2xvclxuICAgICAgICAgIC8vICAgICB9LFxuICAgICAgICAgIC8vICAgICBvbkFjdGlvblJldHVybjogKGFjdGlvbiwgdmFsKSA9PiB7XG4gICAgICAgICAgLy8gICAgICAgYWN0aW9uLmRpYWxvZy5kYXRhLmNvbG9yID0gdmFsO1xuICAgICAgICAgIC8vICAgICAgIGFjdGlvbi5pY29uU3R5bGUgPSB7J2JvcmRlci1ib3R0b20nOiBgM3B4IHNvbGlkICR7dmFsfWAsICdwYWRkaW5nLWJvdHRvbSc6ICcycHgnfTtcbiAgICAgICAgICAvLyAgICAgfSxcbiAgICAgICAgICAvLyAgICAgaWNvblN0eWxlOiB7J2JvcmRlci1ib3R0b20nOiAnM3B4IHNvbGlkIGJsYWNrJywgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9LFxuICAgICAgICAgIC8vICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgIC8vICAgfSxcbiAgICAgICAgICAvLyBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0JSSU5HIEZST05UJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2JyaW5nLWZyb250LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zZW5kLWJhY2staWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXRyYXNoLWFsdCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHByb2R1Y3QnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuUFJPRFVDVCxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0hBTkdFIFBST0RVQ1QnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5PVVRHT0lOR19FVkVOVF9UUklHR0VSLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtcmV0d2VldCcsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdDaGFuZ2UgcHJvZHVjdCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDY3JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBibHVyJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNzUlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQkxVUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdibHVyJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyBhbmdsZScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIEFOR0xFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUkVMQVRFRF9JTlBVVCxcbiAgICAgICAgICAgICAga2V5TmFtZTogJ2FuZ2xlJyxcbiAgICAgICAgICAgICAgZ3JvdXBOYW1lOiAnU0hBRE9XJyxcbiAgICAgICAgICAgICAgaW5wdXRWYWx1ZTogMCxcbiAgICAgICAgICAgICAgaW5wdXRUeXBlOiAnbnVtYmVyJyxcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWZvbnQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U2hhZG93LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDI1JSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NoYWRvdyB3aWR0aCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnMyAzIDc1JSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0hBRE9XIExFTkdUSCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlJFTEFURURfSU5QVVQsXG4gICAgICAgICAgICAgIGdyb3VwTmFtZTogJ1NIQURPVycsXG4gICAgICAgICAgICAgIGtleU5hbWU6ICdsZW5ndGgnLFxuICAgICAgICAgICAgICBpbnB1dFZhbHVlOiAwLFxuICAgICAgICAgICAgICBpbnB1dFR5cGU6ICdudW1iZXInLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTaGFkb3csXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMjUlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU2hhZG93IGNvbG9yJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICczIDMgNjclJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTSEFET1cgQ09MT1InLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5ESUFMT0csXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1wZW4nLFxuICAgICAgICAgICAgICBkaWFsb2c6IHtcbiAgICAgICAgICAgICAgICAvL2NvbXBvbmVudDogQ29sb3JQaWNrZXJEaWFsb2dDb21wb25lbnQsXG4gICAgICAgICAgICAgICAgZGF0YToge2NvbG9yOiAnIzAwMDAwMCcsIHRpdGxlVGV4dDogJ1NFVCBDT0xPUicsIGJ1dHRvblRleHQ6ICdTRVQnfSxcbiAgICAgICAgICAgICAgICBvbkNsb3NlOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFNoYWRvd0NvbG9yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YCwgJ3BhZGRpbmctYm90dG9tJzogJzJweCd9O1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHsnYm9yZGVyLWJvdHRvbSc6ICczcHggc29saWQgYmxhY2snLCAncGFkZGluZy1ib3R0b20nOiAnMnB4J30sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgZ3JvdXAnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuR1JPVVAsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIExFRlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tbGVmdC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkxlZnQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBIT1JJWk9OVEFMIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1ob3Jpem9udGFsLWNlbnRlci1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkhvcml6b250YWxDZW50ZXIsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBSSUdIVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1yaWdodC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblJpZ2h0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFRPUCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi10b3AtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Ub3AsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBWRVJUSUNBTCBDRU5URVInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tdmVydGljYWwtY2VudGVyLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uVmVydGljYWxDZW50ZXIsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBCT1RUT00nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tYm90dG9tLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uQm90dG9tLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbkdyb3VwLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfVxuICAgIF07XG4gIH1cbn1cbiJdfQ==