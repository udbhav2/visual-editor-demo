/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as _ from 'lodash';
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';
export class BannerVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = false;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = true;
        this.hasPan = true;
        this.isWideCanvas = true;
        this.isShowFrame = true;
        this.toolsFlex = '0 2 10%';
        this.actionsFlex = '0 2 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add text',
                iconImgUrl: '@app/../assets/images/add-text-icon.png',
                iconDisplayText: 'ADD TEXT',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                onSelect: VisualEditorService.addText,
                onSelectParams: BannerVisualEditorConfig.addTextConfigParams,
            },
            {
                name: 'edit text',
                isHidden: true,
                objectType: ObjectTypes.TEXBOX,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'FONT FAMILY',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontFamilies,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontFamily,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontFamily,
                                flex: '0 3 100%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'FONT COLOR',
                                type: ActionTypes.DIALOG,
                                iconClass: 'fas fa-font',
                                dialog: {
                                    //component: ColorPickerDialogComponent,
                                    data: { color: '#000000', titleText: 'SET COLOR', buttonText: 'SET' },
                                    onClose: VisualEditorService.setFillColor
                                },
                                onActionReturn: (/**
                                 * @param {?} action
                                 * @param {?} val
                                 * @return {?}
                                 */
                                (action, val) => {
                                    action.dialog.data.color = val;
                                    action.iconStyle = { 'border-bottom': `3px solid ${val}` };
                                }),
                                iconStyle: { 'border-bottom': '3px solid black' },
                                flex: '0 3 33%'
                            },
                            {
                                label: 'FONT SIZE',
                                type: ActionTypes.DROPDOWN,
                                dropdownOption: BannerVisualEditorConfig.availableFontSizes,
                                dropdownSelectedOption: BannerVisualEditorConfig.defaultFontSize,
                                iconClass: 'fas fa-font',
                                action: VisualEditorService.setFontSize,
                                flex: '3 3 67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-left',
                                action: VisualEditorService.alignLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN CENTER',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-center',
                                action: VisualEditorService.alignCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-align-right',
                                action: VisualEditorService.alignRight,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BOLD',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-bold',
                                action: VisualEditorService.setBold,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ITALIC',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-italic',
                                action: VisualEditorService.setItalic,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'UNDERLINE',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-underline',
                                action: VisualEditorService.setUnderline,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            },
            {
                name: 'add image',
                iconImgUrl: '@app/../assets/images/add-image-icon.png',
                iconDisplayText: 'ADD IMAGE',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                type: ActionTypes.UPLOAD,
                onSelect: VisualEditorService.addImage,
                onSelectParams: BannerVisualEditorConfig.addImageConfigParams,
            },
            {
                name: 'edit image',
                isHidden: true,
                objectType: ObjectTypes.IMAGE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '0 3 33%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '0 3 33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '0 3 33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '0 3 33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '0 3 33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
BannerVisualEditorConfig.defaultFontFamily = 'Times New Roman';
BannerVisualEditorConfig.availableFontFamilies = [
    BannerVisualEditorConfig.defaultFontFamily,
    'Georgia',
    'mc-extra-lt',
    'mc-bold',
    'mc-book',
    'Helvetica',
    'Comic Sans MS',
    'Impact',
    'Courier New'
];
BannerVisualEditorConfig.defaultFontSize = 34;
BannerVisualEditorConfig.availableFontSizes = _.range(8, 50 + 1);
BannerVisualEditorConfig.addTextConfigParams = {
    text: '<text>',
    config: {
        left: 10,
        top: 10,
        fontFamily: BannerVisualEditorConfig.defaultFontFamily,
        fontSize: BannerVisualEditorConfig.defaultFontSize,
        cornerColor: '#5c59f0',
        cornerSize: 8,
        transparentCorners: false
    }
};
BannerVisualEditorConfig.defaultStrokeWidth = 0;
BannerVisualEditorConfig.defaultLineStrokeWidth = 5;
BannerVisualEditorConfig.availableStrokeWidths = _.range(0, 50 + 1);
BannerVisualEditorConfig.addImageConfigParams = {
    fileUrl: '',
    scaling: 0.35,
    config: { left: 10, top: 10, stroke: '#000000', strokeWidth: BannerVisualEditorConfig.defaultStrokeWidth, crossOrigin: 'anonymous' }
};
if (false) {
    /** @type {?} */
    BannerVisualEditorConfig.defaultFontFamily;
    /** @type {?} */
    BannerVisualEditorConfig.availableFontFamilies;
    /** @type {?} */
    BannerVisualEditorConfig.defaultFontSize;
    /** @type {?} */
    BannerVisualEditorConfig.availableFontSizes;
    /** @type {?} */
    BannerVisualEditorConfig.addTextConfigParams;
    /** @type {?} */
    BannerVisualEditorConfig.defaultStrokeWidth;
    /** @type {?} */
    BannerVisualEditorConfig.defaultLineStrokeWidth;
    /** @type {?} */
    BannerVisualEditorConfig.availableStrokeWidths;
    /** @type {?} */
    BannerVisualEditorConfig.addImageConfigParams;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.tools;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isWideCanvas;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.isShowFrame;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    BannerVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFubmVyLXZpc3VhbC1lZGl0b3ItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWdzL2Jhbm5lci12aXN1YWwtZWRpdG9yLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUxRCxNQUFNLE9BQU8sd0JBQXdCO0lBOENuQztRQVZPLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QiwrQkFBMEIsR0FBRyxJQUFJLENBQUM7UUFDbEMsWUFBTyxHQUFHLElBQUksQ0FBQztRQUNmLFdBQU0sR0FBRyxJQUFJLENBQUM7UUFDZCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBRzdCLElBQUksQ0FBQyxLQUFLLEdBQUc7WUFDWDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQzthQUNIO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFVBQVU7Z0JBQ2hCLFVBQVUsRUFBRSx5Q0FBeUM7Z0JBQ3JELGVBQWUsRUFBRSxVQUFVO2dCQUMzQixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUUsbUJBQW1CLENBQUMsT0FBTztnQkFDckMsY0FBYyxFQUFFLHdCQUF3QixDQUFDLG1CQUFtQjthQUM3RDtZQUNEO2dCQUNFLElBQUksRUFBRSxXQUFXO2dCQUNqQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0JBQzlCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsd0JBQXdCLENBQUMscUJBQXFCO2dDQUM5RCxzQkFBc0IsRUFBRSx3QkFBd0IsQ0FBQyxpQkFBaUI7Z0NBQ2xFLFNBQVMsRUFBRSxhQUFhO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLFVBQVU7NkJBQ2pCO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGFBQWE7Z0NBQ3hCLE1BQU0sRUFBRTs7b0NBRU4sSUFBSSxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUM7b0NBQ25FLE9BQU8sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2lDQUMxQztnQ0FDRCxjQUFjOzs7OztnQ0FBRSxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQ0FDOUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztvQ0FDL0IsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFDLGVBQWUsRUFBRSxhQUFhLEdBQUcsRUFBRSxFQUFDLENBQUM7Z0NBQzNELENBQUMsQ0FBQTtnQ0FDRCxTQUFTLEVBQUUsRUFBQyxlQUFlLEVBQUUsaUJBQWlCLEVBQUM7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRO2dDQUMxQixjQUFjLEVBQUUsd0JBQXdCLENBQUMsa0JBQWtCO2dDQUMzRCxzQkFBc0IsRUFBRSx3QkFBd0IsQ0FBQyxlQUFlO2dDQUNoRSxTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxZQUFZO2dDQUNuQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxtQkFBbUI7Z0NBQzlCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxTQUFTO2dDQUNyQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLGNBQWM7Z0NBQ3JCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLHFCQUFxQjtnQ0FDaEMsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFdBQVc7Z0NBQ3ZDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsb0JBQW9CO2dDQUMvQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsVUFBVTtnQ0FDdEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsYUFBYTtnQ0FDeEIsTUFBTSxFQUFFLG1CQUFtQixDQUFDLE9BQU87Z0NBQ25DLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxlQUFlO2dDQUMxQixNQUFNLEVBQUUsbUJBQW1CLENBQUMsU0FBUztnQ0FDckMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxlQUFlO2dDQUMzQyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBQztpQkFDSDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFdBQVc7Z0JBQ2pCLFVBQVUsRUFBRSwwQ0FBMEM7Z0JBQ3RELGVBQWUsRUFBRSxXQUFXO2dCQUM1QixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0JBQ3hCLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxRQUFRO2dCQUN0QyxjQUFjLEVBQUUsd0JBQXdCLENBQUMsb0JBQW9CO2FBQzlEO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFlBQVk7Z0JBQ2xCLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFVBQVUsRUFBRSxXQUFXLENBQUMsS0FBSztnQkFDN0IsYUFBYSxFQUFFOzs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7Z0NBQ3pDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUU7b0JBQ1IsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDJDQUEyQztnQ0FDcEQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGtCQUFrQjtnQ0FDOUMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSx5QkFBeUI7Z0NBQ2hDLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLHdEQUF3RDtnQ0FDakUsTUFBTSxFQUFFLG1CQUFtQixDQUFDLDhCQUE4QjtnQ0FDMUQsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO29CQUNGLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxpQkFBaUI7Z0NBQzdDLElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsdUJBQXVCO2dDQUM5QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSxzREFBc0Q7Z0NBQy9ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw0QkFBNEI7Z0NBQ3hELElBQUksRUFBRSxTQUFTOzZCQUNoQjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNkNBQTZDO2dDQUN0RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsYUFBYTtnQ0FDcEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNENBQTRDO2dDQUNyRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsSUFBSSxFQUFFLFNBQVM7NkJBQ2hCOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsU0FBUzs2QkFDaEI7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxTQUFTOzZCQUNoQjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7QUE3VGUsMENBQWlCLEdBQUcsaUJBQWlCLENBQUM7QUFDdEMsOENBQXFCLEdBQUc7SUFDdEMsd0JBQXdCLENBQUMsaUJBQWlCO0lBQzFDLFNBQVM7SUFDVCxhQUFhO0lBQ2IsU0FBUztJQUNULFNBQVM7SUFDVCxXQUFXO0lBQ1gsZUFBZTtJQUNmLFFBQVE7SUFDUixhQUFhO0NBQ2QsQ0FBQztBQUNjLHdDQUFlLEdBQUcsRUFBRSxDQUFDO0FBQ3JCLDJDQUFrQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUN4Qyw0Q0FBbUIsR0FBRztJQUNwQyxJQUFJLEVBQUUsUUFBUTtJQUNkLE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxFQUFFO1FBQ1IsR0FBRyxFQUFFLEVBQUU7UUFDUCxVQUFVLEVBQUUsd0JBQXdCLENBQUMsaUJBQWlCO1FBQ3RELFFBQVEsRUFBRSx3QkFBd0IsQ0FBQyxlQUFlO1FBQ2xELFdBQVcsRUFBRSxTQUFTO1FBQ3RCLFVBQVUsRUFBRSxDQUFDO1FBQ2Isa0JBQWtCLEVBQUUsS0FBSztLQUMxQjtDQUNGLENBQUM7QUFDYywyQ0FBa0IsR0FBRyxDQUFDLENBQUM7QUFDdkIsK0NBQXNCLEdBQUcsQ0FBQyxDQUFDO0FBQzNCLDhDQUFxQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMzQyw2Q0FBb0IsR0FBRztJQUNyQyxPQUFPLEVBQUUsRUFBRTtJQUNYLE9BQU8sRUFBRSxJQUFJO0lBQ2IsTUFBTSxFQUFFLEVBQUMsSUFBSSxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLHdCQUF3QixDQUFDLGtCQUFrQixFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUM7Q0FBQyxDQUFDOzs7SUFoQ3RJLDJDQUFzRDs7SUFDdEQsK0NBVUU7O0lBQ0YseUNBQXFDOztJQUNyQyw0Q0FBd0Q7O0lBQ3hELDZDQVdFOztJQUNGLDRDQUF1Qzs7SUFDdkMsZ0RBQTJDOztJQUMzQywrQ0FBMkQ7O0lBQzNELDhDQUdzSTs7SUFFdEkseUNBQW9COztJQUNwQix1REFBa0M7O0lBQ2xDLGlEQUE2Qjs7SUFDN0IsOERBQXlDOztJQUN6QywyQ0FBc0I7O0lBQ3RCLDBDQUFxQjs7SUFDckIsZ0RBQTJCOztJQUMzQiwrQ0FBMEI7O0lBQzFCLDZDQUE2Qjs7SUFDN0IsK0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgXyBmcm9tICdsb2Rhc2gnO1xuXG5pbXBvcnQgeyBWaXN1YWxFZGl0b3JTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdmlzdWFsLWVkaXRvci5zZXJ2aWNlJztcbmltcG9ydCB7IEFjdGlvblR5cGVzLCBPYmplY3RUeXBlcyB9IGZyb20gJy4vYWN0aW9uLXR5cGVzJztcblxuZXhwb3J0IGNsYXNzIEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZyB7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0Rm9udEZhbWlseSA9ICdUaW1lcyBOZXcgUm9tYW4nO1xuICBzdGF0aWMgcmVhZG9ubHkgYXZhaWxhYmxlRm9udEZhbWlsaWVzID0gW1xuICAgIEJhbm5lclZpc3VhbEVkaXRvckNvbmZpZy5kZWZhdWx0Rm9udEZhbWlseSxcbiAgICAnR2VvcmdpYScsXG4gICAgJ21jLWV4dHJhLWx0JyxcbiAgICAnbWMtYm9sZCcsXG4gICAgJ21jLWJvb2snLFxuICAgICdIZWx2ZXRpY2EnLFxuICAgICdDb21pYyBTYW5zIE1TJyxcbiAgICAnSW1wYWN0JyxcbiAgICAnQ291cmllciBOZXcnXG4gIF07XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0Rm9udFNpemUgPSAzNDtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZUZvbnRTaXplcyA9IF8ucmFuZ2UoOCwgNTAgKyAxKTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZFRleHRDb25maWdQYXJhbXMgPSB7XG4gICAgdGV4dDogJzx0ZXh0PicsXG4gICAgY29uZmlnOiB7XG4gICAgICBsZWZ0OiAxMCxcbiAgICAgIHRvcDogMTAsXG4gICAgICBmb250RmFtaWx5OiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250U2l6ZSxcbiAgICAgIGNvcm5lckNvbG9yOiAnIzVjNTlmMCcsXG4gICAgICBjb3JuZXJTaXplOiA4LFxuICAgICAgdHJhbnNwYXJlbnRDb3JuZXJzOiBmYWxzZVxuICAgIH1cbiAgfTtcbiAgc3RhdGljIHJlYWRvbmx5IGRlZmF1bHRTdHJva2VXaWR0aCA9IDA7XG4gIHN0YXRpYyByZWFkb25seSBkZWZhdWx0TGluZVN0cm9rZVdpZHRoID0gNTtcbiAgc3RhdGljIHJlYWRvbmx5IGF2YWlsYWJsZVN0cm9rZVdpZHRocyA9IF8ucmFuZ2UoMCwgNTAgKyAxKTtcbiAgc3RhdGljIHJlYWRvbmx5IGFkZEltYWdlQ29uZmlnUGFyYW1zID0ge1xuICAgIGZpbGVVcmw6ICcnLFxuICAgIHNjYWxpbmc6IDAuMzUsXG4gICAgY29uZmlnOiB7bGVmdDogMTAsIHRvcDogMTAsIHN0cm9rZTogJyMwMDAwMDAnLCBzdHJva2VXaWR0aDogQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRTdHJva2VXaWR0aCwgY3Jvc3NPcmlnaW46ICdhbm9ueW1vdXMnfX07XG5cbiAgcHVibGljIHRvb2xzOiBhbnlbXTtcbiAgcHVibGljIGlzRmlyc3RUb29sU2VsZWN0ZWQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2hlY2tlcnNCZyA9IGZhbHNlO1xuICBwdWJsaWMgaGFzSGlkZGVuU2VsZWN0aW9uQ29udHJvbHMgPSB0cnVlO1xuICBwdWJsaWMgaGFzWm9vbSA9IHRydWU7XG4gIHB1YmxpYyBoYXNQYW4gPSB0cnVlO1xuICBwdWJsaWMgaXNXaWRlQ2FudmFzID0gdHJ1ZTtcbiAgcHVibGljIGlzU2hvd0ZyYW1lID0gdHJ1ZTtcbiAgcHVibGljIHRvb2xzRmxleCA9ICcwIDIgMTAlJztcbiAgcHVibGljIGFjdGlvbnNGbGV4ID0gJzAgMiAyMCUnO1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMudG9vbHMgPSBbXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdjb25maWcnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnYWRkIHRleHQnLFxuICAgICAgICBpY29uSW1nVXJsOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FkZC10ZXh0LWljb24ucG5nJyxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnQUREIFRFWFQnLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIG9uU2VsZWN0OiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZFRleHQsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuYWRkVGV4dENvbmZpZ1BhcmFtcyxcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdlZGl0IHRleHQnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuVEVYQk9YLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHNlY3Rpb25zOiBbXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGT05UIEZBTUlMWScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkRST1BET1dOLFxuICAgICAgICAgICAgICBkcm9wZG93bk9wdGlvbjogQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnLmF2YWlsYWJsZUZvbnRGYW1pbGllcyxcbiAgICAgICAgICAgICAgZHJvcGRvd25TZWxlY3RlZE9wdGlvbjogQmFubmVyVmlzdWFsRWRpdG9yQ29uZmlnLmRlZmF1bHRGb250RmFtaWx5LFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGb250RmFtaWx5LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDEwMCUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdGT05UIENPTE9SJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRElBTE9HLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGRpYWxvZzoge1xuICAgICAgICAgICAgICAgIC8vY29tcG9uZW50OiBDb2xvclBpY2tlckRpYWxvZ0NvbXBvbmVudCxcbiAgICAgICAgICAgICAgICBkYXRhOiB7Y29sb3I6ICcjMDAwMDAwJywgdGl0bGVUZXh0OiAnU0VUIENPTE9SJywgYnV0dG9uVGV4dDogJ1NFVCd9LFxuICAgICAgICAgICAgICAgIG9uQ2xvc2U6IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0RmlsbENvbG9yXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIG9uQWN0aW9uUmV0dXJuOiAoYWN0aW9uLCB2YWwpID0+IHtcbiAgICAgICAgICAgICAgICBhY3Rpb24uZGlhbG9nLmRhdGEuY29sb3IgPSB2YWw7XG4gICAgICAgICAgICAgICAgYWN0aW9uLmljb25TdHlsZSA9IHsnYm9yZGVyLWJvdHRvbSc6IGAzcHggc29saWQgJHt2YWx9YH07XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGljb25TdHlsZTogeydib3JkZXItYm90dG9tJzogJzNweCBzb2xpZCBibGFjayd9LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnRk9OVCBTSVpFJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuRFJPUERPV04sXG4gICAgICAgICAgICAgIGRyb3Bkb3duT3B0aW9uOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuYXZhaWxhYmxlRm9udFNpemVzLFxuICAgICAgICAgICAgICBkcm9wZG93blNlbGVjdGVkT3B0aW9uOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuZGVmYXVsdEZvbnRTaXplLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtZm9udCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRGb250U2l6ZSxcbiAgICAgICAgICAgICAgZmxleDogJzMgMyA2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIExFRlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1hbGlnbi1sZWZ0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduTGVmdCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLWFsaWduLWNlbnRlcicsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnbkNlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFJJR0hUJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtYWxpZ24tcmlnaHQnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25SaWdodCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0JPTEQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1ib2xkJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldEJvbGQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdJVEFMSUMnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1pdGFsaWMnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0SXRhbGljLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnVU5ERVJMSU5FJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdW5kZXJsaW5lJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldFVuZGVybGluZSxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0JSSU5HIEZST05UJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2JyaW5nLWZyb250LWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmJyaW5nRm9yd2FyZCxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zZW5kLWJhY2staWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXRyYXNoLWFsdCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb24sXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19XG4gICAgICAgIF0sXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnYWRkIGltYWdlJyxcbiAgICAgICAgaWNvbkltZ1VybDogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hZGQtaW1hZ2UtaWNvbi5wbmcnLFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgSU1BR0UnLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV0sXG4gICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlVQTE9BRCxcbiAgICAgICAgb25TZWxlY3Q6IFZpc3VhbEVkaXRvclNlcnZpY2UuYWRkSW1hZ2UsXG4gICAgICAgIG9uU2VsZWN0UGFyYW1zOiBCYW5uZXJWaXN1YWxFZGl0b3JDb25maWcuYWRkSW1hZ2VDb25maWdQYXJhbXMsXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBuYW1lOiAnZWRpdCBpbWFnZScsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBvYmplY3RUeXBlOiBPYmplY3RUeXBlcy5JTUFHRSxcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzAgMyAzMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgZ3JvdXAnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuR1JPVVAsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIExFRlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tbGVmdC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkxlZnQsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBIT1JJWk9OVEFMIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1ob3Jpem9udGFsLWNlbnRlci1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkhvcml6b250YWxDZW50ZXIsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBSSUdIVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi1yaWdodC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvblJpZ2h0LFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFRPUCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi10b3AtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Ub3AsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBWRVJUSUNBTCBDRU5URVInLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tdmVydGljYWwtY2VudGVyLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uVmVydGljYWxDZW50ZXIsXG4gICAgICAgICAgICAgIGZsZXg6ICcwIDMgMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBTElHTiBCT1RUT00nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tYm90dG9tLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uQm90dG9tLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VORCBCQUNLJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL3NlbmQtYmFjay1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZW5kQmFja3dhcmRzLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbkdyb3VwLFxuICAgICAgICAgICAgICBmbGV4OiAnMCAzIDMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfVxuICAgIF07XG4gIH1cbn1cbiJdfQ==