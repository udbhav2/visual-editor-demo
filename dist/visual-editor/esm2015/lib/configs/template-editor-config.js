/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes, ObjectTypes } from './action-types';
export class TemplateEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = true;
        this.hasZoom = false;
        this.hasPan = false;
        this.toolsFlex = '0 1 20%';
        this.actionsFlex = '0 1 20%';
        this.tools = [
            {
                name: 'config',
                isHidden: true,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })]
            },
            {
                name: 'add placeholder',
                iconImgUrl: '@app/../assets/images/add-shape-icon.png',
                iconDisplayText: 'ADD PLACEHOLDER',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ADD PRODUCT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#5c59f0' },
                                action: VisualEditorService.addRectangle,
                                onSelectParams: { config: Object.assign({}, TemplateEditorConfig.addRectangleConfigParams.config, { stroke: '#5c59f0' }) },
                                flex: '33%'
                            },
                            {
                                label: 'Product',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ADD ACCENT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#2bc4b6' },
                                action: VisualEditorService.addRectangle,
                                onSelectParams: { config: Object.assign({}, TemplateEditorConfig.addRectangleConfigParams.config, { stroke: '#2bc4b6' }) },
                                flex: '33%'
                            },
                            {
                                label: 'Accent',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] }
                ]
            },
            {
                name: 'edit shape',
                isHidden: true,
                objectType: ObjectTypes.SHAPE,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'SET PRODUCT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#5c59f0' },
                                action: VisualEditorService.setStrokeColor,
                                onSelectParams: '#5c59f0',
                                flex: '33%'
                            },
                            {
                                label: 'Product',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'SET ACCENT',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-square',
                                iconStyle: { color: '#2bc4b6' },
                                action: VisualEditorService.setStrokeColor,
                                onSelectParams: '#2bc4b6',
                                flex: '33%'
                            },
                            {
                                label: 'Accent',
                                type: ActionTypes.LABEL,
                                flex: '67%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelection,
                                flex: '33%'
                            }
                        ] }
                ],
            },
            {
                name: 'edit group',
                isHidden: true,
                objectType: ObjectTypes.GROUP,
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: false, preserveObjectStacking: true });
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'ALIGN LEFT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-left-icon.png',
                                action: VisualEditorService.alignSelectionLeft,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN HORIZONTAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-horizontal-center-icon.png',
                                action: VisualEditorService.alignSelectionHorizontalCenter,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN RIGHT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-right-icon.png',
                                action: VisualEditorService.alignSelectionRight,
                                flex: '33%'
                            }
                        ] },
                    { actions: [
                            {
                                label: 'ALIGN TOP',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-top-icon.png',
                                action: VisualEditorService.alignSelectionTop,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN VERTICAL CENTER',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-vertical-center-icon.png',
                                action: VisualEditorService.alignSelectionVerticalCenter,
                                flex: '33%'
                            },
                            {
                                label: 'ALIGN BOTTOM',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/align-bottom-icon.png',
                                action: VisualEditorService.alignSelectionBottom,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'BRING FRONT',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/bring-front-icon.png',
                                action: VisualEditorService.bringForward,
                                flex: '33%'
                            },
                            {
                                label: 'SEND BACK',
                                type: ActionTypes.BUTTON,
                                iconSrc: '@app/../assets/images/send-back-icon.png',
                                action: VisualEditorService.sendBackwards,
                                flex: '33%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'CLEAR SELECTION',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.deleteSelectionGroup,
                                flex: '33%'
                            }
                        ] }
                ],
            }
        ];
    }
}
TemplateEditorConfig.addRectangleConfigParams = {
    config: {
        left: 60, top: 60,
        height: 100, width: 100,
        strokeWidth: 2, stroke: 'black',
        originX: 'center', originY: 'center',
        fill: 'rgba(0,0,0,0)'
    }
};
if (false) {
    /** @type {?} */
    TemplateEditorConfig.addRectangleConfigParams;
    /** @type {?} */
    TemplateEditorConfig.prototype.tools;
    /** @type {?} */
    TemplateEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    TemplateEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    TemplateEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    TemplateEditorConfig.prototype.hasZoom;
    /** @type {?} */
    TemplateEditorConfig.prototype.hasPan;
    /** @type {?} */
    TemplateEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    TemplateEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVtcGxhdGUtZWRpdG9yLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3Zpc3VhbC1lZGl0b3IvIiwic291cmNlcyI6WyJsaWIvY29uZmlncy90ZW1wbGF0ZS1lZGl0b3ItY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFFQSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRzFELE1BQU0sT0FBTyxvQkFBb0I7SUFvQi9CO1FBUk8sd0JBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQzNCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLCtCQUEwQixHQUFHLElBQUksQ0FBQztRQUNsQyxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ2hCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFDZixjQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3RCLGdCQUFXLEdBQUcsU0FBUyxDQUFDO1FBRzdCLElBQUksQ0FBQyxLQUFLLEdBQUc7WUFDWDtnQkFDRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQzthQUNIO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsVUFBVSxFQUFFLDBDQUEwQztnQkFDdEQsZUFBZSxFQUFFLGlCQUFpQjtnQkFDbEMsYUFBYSxFQUFFOzs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7d0JBQ3pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxhQUFhLEVBQUUsS0FBSyxFQUFFLHNCQUFzQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ25FLE9BQU8sTUFBTSxDQUFDO29CQUNoQixDQUFDLEVBQUM7Z0JBQ0YsUUFBUSxFQUFFO29CQUNSLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxlQUFlO2dDQUMxQixTQUFTLEVBQUUsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFDO2dDQUM3QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsWUFBWTtnQ0FDeEMsY0FBYyxFQUFFLEVBQUMsTUFBTSxvQkFDbEIsb0JBQW9CLENBQUMsd0JBQXdCLENBQUMsTUFBTSxJQUN2RCxNQUFNLEVBQUUsU0FBUyxHQUNsQixFQUFDO2dDQUNGLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxTQUFTO2dDQUNoQixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFlBQVk7Z0NBQ25CLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGVBQWU7Z0NBQzFCLFNBQVMsRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUM7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxjQUFjLEVBQUUsRUFBQyxNQUFNLG9CQUNsQixvQkFBb0IsQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLElBQ3ZELE1BQU0sRUFBRSxTQUFTLEdBQ2xCLEVBQUM7Z0NBQ0YsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dDQUN2QixJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRixFQUFDO2lCQUNIO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsWUFBWTtnQkFDbEIsUUFBUSxFQUFFLElBQUk7Z0JBQ2QsVUFBVSxFQUFFLFdBQVcsQ0FBQyxLQUFLO2dCQUM3QixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxLQUFLLEVBQUUsc0JBQXNCLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzt3QkFDbkUsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUU7b0JBQ1IsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGVBQWU7Z0NBQzFCLFNBQVMsRUFBRSxFQUFDLEtBQUssRUFBRSxTQUFTLEVBQUM7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxjQUFjO2dDQUMxQyxjQUFjLEVBQUUsU0FBUztnQ0FDekIsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLFNBQVM7Z0NBQ2hCLElBQUksRUFBRSxXQUFXLENBQUMsS0FBSztnQ0FDdkIsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0YsRUFBQztvQkFDRixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsZUFBZTtnQ0FDMUIsU0FBUyxFQUFFLEVBQUMsS0FBSyxFQUFFLFNBQVMsRUFBQztnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGNBQWM7Z0NBQzFDLGNBQWMsRUFBRSxTQUFTO2dDQUN6QixJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsUUFBUTtnQ0FDZixJQUFJLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0NBQ3ZCLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLGFBQWE7Z0NBQ3BCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDRDQUE0QztnQ0FDckQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLFlBQVk7Z0NBQ3hDLElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxXQUFXO2dDQUNsQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSwwQ0FBMEM7Z0NBQ25ELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2dDQUN6QyxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxpQkFBaUI7Z0NBQ3hCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsU0FBUyxFQUFFLGtCQUFrQjtnQ0FDN0IsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGVBQWU7Z0NBQzNDLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtZQUNEO2dCQUNFLElBQUksRUFBRSxZQUFZO2dCQUNsQixRQUFRLEVBQUUsSUFBSTtnQkFDZCxVQUFVLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzdCLGFBQWEsRUFBRTs7OztvQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO3dCQUN6QixNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsYUFBYSxFQUFFLEtBQUssRUFBRSxzQkFBc0IsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNuRSxPQUFPLE1BQU0sQ0FBQztvQkFDaEIsQ0FBQyxFQUFDO2dCQUNGLFFBQVEsRUFBRTtvQkFDUixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsWUFBWTtnQ0FDbkIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMkNBQTJDO2dDQUNwRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsa0JBQWtCO2dDQUM5QyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUseUJBQXlCO2dDQUNoQyxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSx3REFBd0Q7Z0NBQ2pFLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyw4QkFBOEI7Z0NBQzFELElBQUksRUFBRSxLQUFLOzZCQUNaOzRCQUNEO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxtQkFBbUI7Z0NBQy9DLElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7b0JBQ0YsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFdBQVc7Z0NBQ2xCLElBQUksRUFBRSxXQUFXLENBQUMsTUFBTTtnQ0FDeEIsT0FBTyxFQUFFLDBDQUEwQztnQ0FDbkQsTUFBTSxFQUFFLG1CQUFtQixDQUFDLGlCQUFpQjtnQ0FDN0MsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7NEJBQ0Q7Z0NBQ0UsS0FBSyxFQUFFLHVCQUF1QjtnQ0FDOUIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsc0RBQXNEO2dDQUMvRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsNEJBQTRCO2dDQUN4RCxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsY0FBYztnQ0FDckIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsNkNBQTZDO2dDQUN0RCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsb0JBQW9CO2dDQUNoRCxJQUFJLEVBQUUsS0FBSzs2QkFDWjt5QkFDRixFQUFFLFlBQVksRUFBRSxJQUFJLEVBQUM7b0JBQ3RCLEVBQUMsT0FBTyxFQUFFOzRCQUNSO2dDQUNFLEtBQUssRUFBRSxhQUFhO2dDQUNwQixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLE9BQU8sRUFBRSw0Q0FBNEM7Z0NBQ3JELE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxZQUFZO2dDQUN4QyxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsV0FBVztnQ0FDbEIsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixPQUFPLEVBQUUsMENBQTBDO2dDQUNuRCxNQUFNLEVBQUUsbUJBQW1CLENBQUMsYUFBYTtnQ0FDekMsSUFBSSxFQUFFLEtBQUs7NkJBQ1o7eUJBQ0YsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDO29CQUN0QixFQUFDLE9BQU8sRUFBRTs0QkFDUjtnQ0FDRSxLQUFLLEVBQUUsaUJBQWlCO2dDQUN4QixJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyxvQkFBb0I7Z0NBQ2hELElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOztBQXJPZSw2Q0FBd0IsR0FBRztJQUN6QyxNQUFNLEVBQUU7UUFDTixJQUFJLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFO1FBQ2pCLE1BQU0sRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUc7UUFDdkIsV0FBVyxFQUFFLENBQUMsRUFBRSxNQUFNLEVBQUUsT0FBTztRQUMvQixPQUFPLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxRQUFRO1FBQ3BDLElBQUksRUFBRSxlQUFlO0tBQ3RCO0NBQ0YsQ0FBQzs7O0lBUkYsOENBUUU7O0lBRUYscUNBQW9COztJQUNwQixtREFBa0M7O0lBQ2xDLDZDQUE0Qjs7SUFDNUIsMERBQXlDOztJQUN6Qyx1Q0FBdUI7O0lBQ3ZCLHNDQUFzQjs7SUFDdEIseUNBQTZCOztJQUM3QiwyQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XG5cbmltcG9ydCB7IFZpc3VhbEVkaXRvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWN0aW9uVHlwZXMsIE9iamVjdFR5cGVzIH0gZnJvbSAnLi9hY3Rpb24tdHlwZXMnO1xuXG5cbmV4cG9ydCBjbGFzcyBUZW1wbGF0ZUVkaXRvckNvbmZpZyB7XG4gIHN0YXRpYyByZWFkb25seSBhZGRSZWN0YW5nbGVDb25maWdQYXJhbXMgPSB7XG4gICAgY29uZmlnOiB7XG4gICAgICBsZWZ0OiA2MCwgdG9wOiA2MCxcbiAgICAgIGhlaWdodDogMTAwLCB3aWR0aDogMTAwLFxuICAgICAgc3Ryb2tlV2lkdGg6IDIsIHN0cm9rZTogJ2JsYWNrJyxcbiAgICAgIG9yaWdpblg6ICdjZW50ZXInLCBvcmlnaW5ZOiAnY2VudGVyJyxcbiAgICAgIGZpbGw6ICdyZ2JhKDAsMCwwLDApJ1xuICAgIH1cbiAgfTtcblxuICBwdWJsaWMgdG9vbHM6IGFueVtdO1xuICBwdWJsaWMgaXNGaXJzdFRvb2xTZWxlY3RlZCA9IHRydWU7XG4gIHB1YmxpYyBoYXNDaGVja2Vyc0JnID0gdHJ1ZTtcbiAgcHVibGljIGhhc0hpZGRlblNlbGVjdGlvbkNvbnRyb2xzID0gdHJ1ZTtcbiAgcHVibGljIGhhc1pvb20gPSBmYWxzZTtcbiAgcHVibGljIGhhc1BhbiA9IGZhbHNlO1xuICBwdWJsaWMgdG9vbHNGbGV4ID0gJzAgMSAyMCUnO1xuICBwdWJsaWMgYWN0aW9uc0ZsZXggPSAnMCAxIDIwJSc7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy50b29scyA9IFtcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2NvbmZpZycsXG4gICAgICAgIGlzSGlkZGVuOiB0cnVlLFxuICAgICAgICBjYW52YXNDb25maWdzOiBbKGNhbnZhcykgPT4gIHtcbiAgICAgICAgICBjYW52YXMuc2V0KHsgaXNEcmF3aW5nTW9kZTogZmFsc2UsIHByZXNlcnZlT2JqZWN0U3RhY2tpbmc6IHRydWUgfSk7XG4gICAgICAgICAgcmV0dXJuIGNhbnZhcztcbiAgICAgICAgfV1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIG5hbWU6ICdhZGQgcGxhY2Vob2xkZXInLFxuICAgICAgICBpY29uSW1nVXJsOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FkZC1zaGFwZS1pY29uLnBuZycsIFxuICAgICAgICBpY29uRGlzcGxheVRleHQ6ICdBREQgUExBQ0VIT0xERVInLCBcbiAgICAgICAgY2FudmFzQ29uZmlnczogWyhjYW52YXMpID0+ICB7XG4gICAgICAgICAgY2FudmFzLnNldCh7IGlzRHJhd2luZ01vZGU6IGZhbHNlLCBwcmVzZXJ2ZU9iamVjdFN0YWNraW5nOiB0cnVlIH0pO1xuICAgICAgICAgIHJldHVybiBjYW52YXM7XG4gICAgICAgIH1dLFxuICAgICAgICBzZWN0aW9uczogW1xuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUREIFBST0RVQ1QnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS1zcXVhcmUnLFxuICAgICAgICAgICAgICBpY29uU3R5bGU6IHtjb2xvcjogJyM1YzU5ZjAnfSxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFkZFJlY3RhbmdsZSxcbiAgICAgICAgICAgICAgb25TZWxlY3RQYXJhbXM6IHtjb25maWc6IHtcbiAgICAgICAgICAgICAgICAuLi5UZW1wbGF0ZUVkaXRvckNvbmZpZy5hZGRSZWN0YW5nbGVDb25maWdQYXJhbXMuY29uZmlnLFxuICAgICAgICAgICAgICAgIHN0cm9rZTogJyM1YzU5ZjAnXG4gICAgICAgICAgICAgIH19LFxuICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdQcm9kdWN0JyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdBREQgQUNDRU5UJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtc3F1YXJlJyxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7Y29sb3I6ICcjMmJjNGI2J30sXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hZGRSZWN0YW5nbGUsXG4gICAgICAgICAgICAgIG9uU2VsZWN0UGFyYW1zOiB7Y29uZmlnOiB7XG4gICAgICAgICAgICAgICAgLi4uVGVtcGxhdGVFZGl0b3JDb25maWcuYWRkUmVjdGFuZ2xlQ29uZmlnUGFyYW1zLmNvbmZpZyxcbiAgICAgICAgICAgICAgICBzdHJva2U6ICcjMmJjNGI2J1xuICAgICAgICAgICAgICB9fSxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQWNjZW50JyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuTEFCRUwsXG4gICAgICAgICAgICAgIGZsZXg6ICc2NyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgc2hhcGUnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuU0hBUEUsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NFVCBQUk9EVUNUJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtc3F1YXJlJyxcbiAgICAgICAgICAgICAgaWNvblN0eWxlOiB7Y29sb3I6ICcjNWM1OWYwJ30sXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5zZXRTdHJva2VDb2xvcixcbiAgICAgICAgICAgICAgb25TZWxlY3RQYXJhbXM6ICcjNWM1OWYwJyxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnUHJvZHVjdCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF19LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnU0VUIEFDQ0VOVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXNxdWFyZScsXG4gICAgICAgICAgICAgIGljb25TdHlsZToge2NvbG9yOiAnIzJiYzRiNid9LFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2V0U3Ryb2tlQ29sb3IsXG4gICAgICAgICAgICAgIG9uU2VsZWN0UGFyYW1zOiAnIzJiYzRiNicsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FjY2VudCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkxBQkVMLFxuICAgICAgICAgICAgICBmbGV4OiAnNjclJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIF0sIGlzRW5kU2VjdGlvbjogdHJ1ZX0sXG4gICAgICAgICAge2FjdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdCUklORyBGUk9OVCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9icmluZy1mcm9udC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5icmluZ0ZvcndhcmQsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1NFTkQgQkFDSycsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9zZW5kLWJhY2staWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2Uuc2VuZEJhY2t3YXJkcyxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQ0xFQVIgU0VMRUNUSU9OJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmRlbGV0ZVNlbGVjdGlvbixcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgbmFtZTogJ2VkaXQgZ3JvdXAnLFxuICAgICAgICBpc0hpZGRlbjogdHJ1ZSxcbiAgICAgICAgb2JqZWN0VHlwZTogT2JqZWN0VHlwZXMuR1JPVVAsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiBmYWxzZSwgcHJlc2VydmVPYmplY3RTdGFja2luZzogdHJ1ZSB9KTtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIExFRlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tbGVmdC1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkxlZnQsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIEhPUklaT05UQUwgQ0VOVEVSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLWhvcml6b250YWwtY2VudGVyLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gUklHSFQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYWxpZ24tcmlnaHQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25SaWdodCxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFRPUCcsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi10b3AtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25Ub3AsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0FMSUdOIFZFUlRJQ0FMIENFTlRFUicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvblNyYzogJ0BhcHAvLi4vYXNzZXRzL2ltYWdlcy9hbGlnbi12ZXJ0aWNhbC1jZW50ZXItaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb25WZXJ0aWNhbENlbnRlcixcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQUxJR04gQk9UVE9NJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uU3JjOiAnQGFwcC8uLi9hc3NldHMvaW1hZ2VzL2FsaWduLWJvdHRvbS1pY29uLnBuZycsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbkJvdHRvbSxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdLCBpc0VuZFNlY3Rpb246IHRydWV9LFxuICAgICAgICAgIHthY3Rpb25zOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGxhYmVsOiAnQlJJTkcgRlJPTlQnLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvYnJpbmctZnJvbnQtaWNvbi5wbmcnLFxuICAgICAgICAgICAgICBhY3Rpb246IFZpc3VhbEVkaXRvclNlcnZpY2UuYnJpbmdGb3J3YXJkLFxuICAgICAgICAgICAgICBmbGV4OiAnMzMlJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgbGFiZWw6ICdTRU5EIEJBQ0snLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25TcmM6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvc2VuZC1iYWNrLWljb24ucG5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNlbmRCYWNrd2FyZHMsXG4gICAgICAgICAgICAgIGZsZXg6ICczMyUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSIFNFTEVDVElPTicsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLkJVVFRPTixcbiAgICAgICAgICAgICAgaWNvbkNsYXNzOiAnZmFzIGZhLXRyYXNoLWFsdCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS5kZWxldGVTZWxlY3Rpb25Hcm91cCxcbiAgICAgICAgICAgICAgZmxleDogJzMzJSdcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBdfVxuICAgICAgICBdLFxuICAgICAgfVxuICAgIF07XG4gIH1cbn1cbiJdfQ==