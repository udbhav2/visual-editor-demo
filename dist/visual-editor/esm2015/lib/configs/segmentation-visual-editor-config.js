/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { VisualEditorService } from '../services/visual-editor.service';
import { ActionTypes } from './action-types';
export class SegmentationVisualEditorConfig {
    constructor() {
        this.isFirstToolSelected = true;
        this.hasCheckersBg = true;
        this.hasHiddenSelectionControls = false;
        this.hasZoom = false;
        this.hasPan = false;
        this.toolsFlex = '0 1 10%';
        this.actionsFlex = '0 1 10%';
        this.tools = [
            {
                name: 'draw',
                iconImgUrl: '@app/../assets/images/target_icon.png',
                iconDisplayText: 'POINTER',
                canvasConfigs: [(/**
                     * @param {?} canvas
                     * @return {?}
                     */
                    (canvas) => {
                        canvas.set({ isDrawingMode: true });
                        canvas.freeDrawingBrush.color = SegmentationVisualEditorConfig.colorBackground;
                        canvas.freeDrawingBrush.width = SegmentationVisualEditorConfig.drawingBrushWidth;
                        return canvas;
                    })],
                sections: [
                    { actions: [
                            {
                                label: 'TOGGLE',
                                type: ActionTypes.TOGGLE,
                                action: VisualEditorService.setColor,
                                isOn: false,
                                onValue: SegmentationVisualEditorConfig.colorForeground,
                                offValue: SegmentationVisualEditorConfig.colorBackground,
                                onLabel: 'Foreground',
                                offLabel: 'Background',
                                flex: '100%'
                            }
                        ], isEndSection: true },
                    { actions: [
                            {
                                label: 'UNDO',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-undo-alt',
                                action: VisualEditorService.undoDrawingTransparentBg,
                                flex: '50%'
                            },
                            {
                                label: 'CLEAR',
                                type: ActionTypes.BUTTON,
                                iconClass: 'fas fa-trash-alt',
                                action: VisualEditorService.clearDrawingTransparentBg,
                                flex: '50%'
                            }
                        ] }
                ],
            }
        ];
    }
}
SegmentationVisualEditorConfig.colorBackground = '#cb0c93';
SegmentationVisualEditorConfig.colorForeground = '#2bc4b6';
SegmentationVisualEditorConfig.drawingBrushWidth = 4;
if (false) {
    /** @type {?} */
    SegmentationVisualEditorConfig.colorBackground;
    /** @type {?} */
    SegmentationVisualEditorConfig.colorForeground;
    /** @type {?} */
    SegmentationVisualEditorConfig.drawingBrushWidth;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.tools;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.isFirstToolSelected;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasCheckersBg;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasHiddenSelectionControls;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasZoom;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.hasPan;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.toolsFlex;
    /** @type {?} */
    SegmentationVisualEditorConfig.prototype.actionsFlex;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VnbWVudGF0aW9uLXZpc3VhbC1lZGl0b3ItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWdzL3NlZ21lbnRhdGlvbi12aXN1YWwtZWRpdG9yLWNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFFeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTdDLE1BQU0sT0FBTyw4QkFBOEI7SUFjekM7UUFSTyx3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDM0Isa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsK0JBQTBCLEdBQUcsS0FBSyxDQUFDO1FBQ25DLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUNmLGNBQVMsR0FBRyxTQUFTLENBQUM7UUFDdEIsZ0JBQVcsR0FBRyxTQUFTLENBQUM7UUFHN0IsSUFBSSxDQUFDLEtBQUssR0FBRztZQUNYO2dCQUNFLElBQUksRUFBRSxNQUFNO2dCQUNaLFVBQVUsRUFBRSx1Q0FBdUM7Z0JBQ25ELGVBQWUsRUFBRSxTQUFTO2dCQUMxQixhQUFhLEVBQUU7Ozs7b0JBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFDekIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLGFBQWEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUNwQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLDhCQUE4QixDQUFDLGVBQWUsQ0FBQzt3QkFDL0UsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyw4QkFBOEIsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDakYsT0FBTyxNQUFNLENBQUM7b0JBQ2hCLENBQUMsRUFBQztnQkFDRixRQUFRLEVBQUU7b0JBQ1IsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLFFBQVE7Z0NBQ2YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsUUFBUTtnQ0FDcEMsSUFBSSxFQUFFLEtBQUs7Z0NBQ1gsT0FBTyxFQUFFLDhCQUE4QixDQUFDLGVBQWU7Z0NBQ3ZELFFBQVEsRUFBRSw4QkFBOEIsQ0FBQyxlQUFlO2dDQUN4RCxPQUFPLEVBQUUsWUFBWTtnQ0FDckIsUUFBUSxFQUFFLFlBQVk7Z0NBQ3RCLElBQUksRUFBRSxNQUFNOzZCQUNiO3lCQUNGLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBQztvQkFDdEIsRUFBQyxPQUFPLEVBQUU7NEJBQ1I7Z0NBQ0UsS0FBSyxFQUFFLE1BQU07Z0NBQ2IsSUFBSSxFQUFFLFdBQVcsQ0FBQyxNQUFNO2dDQUN4QixTQUFTLEVBQUUsaUJBQWlCO2dDQUM1QixNQUFNLEVBQUUsbUJBQW1CLENBQUMsd0JBQXdCO2dDQUNwRCxJQUFJLEVBQUUsS0FBSzs2QkFDWjs0QkFDRDtnQ0FDRSxLQUFLLEVBQUUsT0FBTztnQ0FDZCxJQUFJLEVBQUUsV0FBVyxDQUFDLE1BQU07Z0NBQ3hCLFNBQVMsRUFBRSxrQkFBa0I7Z0NBQzdCLE1BQU0sRUFBRSxtQkFBbUIsQ0FBQyx5QkFBeUI7Z0NBQ3JELElBQUksRUFBRSxLQUFLOzZCQUNaO3lCQUNGLEVBQUM7aUJBQ0g7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOztBQTFEZSw4Q0FBZSxHQUFHLFNBQVMsQ0FBQztBQUM1Qiw4Q0FBZSxHQUFHLFNBQVMsQ0FBQztBQUM1QixnREFBaUIsR0FBRyxDQUFDLENBQUM7OztJQUZ0QywrQ0FBNEM7O0lBQzVDLCtDQUE0Qzs7SUFDNUMsaURBQXNDOztJQUV0QywrQ0FBb0I7O0lBQ3BCLDZEQUFrQzs7SUFDbEMsdURBQTRCOztJQUM1QixvRUFBMEM7O0lBQzFDLGlEQUF1Qjs7SUFDdkIsZ0RBQXNCOztJQUN0QixtREFBNkI7O0lBQzdCLHFEQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFZpc3VhbEVkaXRvclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UnO1xuXG5pbXBvcnQgeyBBY3Rpb25UeXBlcyB9IGZyb20gJy4vYWN0aW9uLXR5cGVzJztcblxuZXhwb3J0IGNsYXNzIFNlZ21lbnRhdGlvblZpc3VhbEVkaXRvckNvbmZpZyB7XG4gIHN0YXRpYyByZWFkb25seSBjb2xvckJhY2tncm91bmQgPSAnI2NiMGM5Myc7XG4gIHN0YXRpYyByZWFkb25seSBjb2xvckZvcmVncm91bmQgPSAnIzJiYzRiNic7XG4gIHN0YXRpYyByZWFkb25seSBkcmF3aW5nQnJ1c2hXaWR0aCA9IDQ7XG5cbiAgcHVibGljIHRvb2xzOiBhbnlbXTtcbiAgcHVibGljIGlzRmlyc3RUb29sU2VsZWN0ZWQgPSB0cnVlO1xuICBwdWJsaWMgaGFzQ2hlY2tlcnNCZyA9IHRydWU7XG4gIHB1YmxpYyBoYXNIaWRkZW5TZWxlY3Rpb25Db250cm9scyA9IGZhbHNlO1xuICBwdWJsaWMgaGFzWm9vbSA9IGZhbHNlO1xuICBwdWJsaWMgaGFzUGFuID0gZmFsc2U7XG4gIHB1YmxpYyB0b29sc0ZsZXggPSAnMCAxIDEwJSc7XG4gIHB1YmxpYyBhY3Rpb25zRmxleCA9ICcwIDEgMTAlJztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnRvb2xzID0gW1xuICAgICAge1xuICAgICAgICBuYW1lOiAnZHJhdycsXG4gICAgICAgIGljb25JbWdVcmw6ICdAYXBwLy4uL2Fzc2V0cy9pbWFnZXMvdGFyZ2V0X2ljb24ucG5nJyxcbiAgICAgICAgaWNvbkRpc3BsYXlUZXh0OiAnUE9JTlRFUicsXG4gICAgICAgIGNhbnZhc0NvbmZpZ3M6IFsoY2FudmFzKSA9PiAge1xuICAgICAgICAgIGNhbnZhcy5zZXQoeyBpc0RyYXdpbmdNb2RlOiB0cnVlIH0pO1xuICAgICAgICAgIGNhbnZhcy5mcmVlRHJhd2luZ0JydXNoLmNvbG9yID0gU2VnbWVudGF0aW9uVmlzdWFsRWRpdG9yQ29uZmlnLmNvbG9yQmFja2dyb3VuZDtcbiAgICAgICAgICBjYW52YXMuZnJlZURyYXdpbmdCcnVzaC53aWR0aCA9IFNlZ21lbnRhdGlvblZpc3VhbEVkaXRvckNvbmZpZy5kcmF3aW5nQnJ1c2hXaWR0aDtcbiAgICAgICAgICByZXR1cm4gY2FudmFzO1xuICAgICAgICB9XSxcbiAgICAgICAgc2VjdGlvbnM6IFtcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1RPR0dMRScsXG4gICAgICAgICAgICAgIHR5cGU6IEFjdGlvblR5cGVzLlRPR0dMRSxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLnNldENvbG9yLFxuICAgICAgICAgICAgICBpc09uOiBmYWxzZSxcbiAgICAgICAgICAgICAgb25WYWx1ZTogU2VnbWVudGF0aW9uVmlzdWFsRWRpdG9yQ29uZmlnLmNvbG9yRm9yZWdyb3VuZCxcbiAgICAgICAgICAgICAgb2ZmVmFsdWU6IFNlZ21lbnRhdGlvblZpc3VhbEVkaXRvckNvbmZpZy5jb2xvckJhY2tncm91bmQsXG4gICAgICAgICAgICAgIG9uTGFiZWw6ICdGb3JlZ3JvdW5kJyxcbiAgICAgICAgICAgICAgb2ZmTGFiZWw6ICdCYWNrZ3JvdW5kJyxcbiAgICAgICAgICAgICAgZmxleDogJzEwMCUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXSwgaXNFbmRTZWN0aW9uOiB0cnVlfSxcbiAgICAgICAgICB7YWN0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ1VORE8nLFxuICAgICAgICAgICAgICB0eXBlOiBBY3Rpb25UeXBlcy5CVVRUT04sXG4gICAgICAgICAgICAgIGljb25DbGFzczogJ2ZhcyBmYS11bmRvLWFsdCcsXG4gICAgICAgICAgICAgIGFjdGlvbjogVmlzdWFsRWRpdG9yU2VydmljZS51bmRvRHJhd2luZ1RyYW5zcGFyZW50QmcsXG4gICAgICAgICAgICAgIGZsZXg6ICc1MCUnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBsYWJlbDogJ0NMRUFSJyxcbiAgICAgICAgICAgICAgdHlwZTogQWN0aW9uVHlwZXMuQlVUVE9OLFxuICAgICAgICAgICAgICBpY29uQ2xhc3M6ICdmYXMgZmEtdHJhc2gtYWx0JyxcbiAgICAgICAgICAgICAgYWN0aW9uOiBWaXN1YWxFZGl0b3JTZXJ2aWNlLmNsZWFyRHJhd2luZ1RyYW5zcGFyZW50QmcsXG4gICAgICAgICAgICAgIGZsZXg6ICc1MCUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgXX1cbiAgICAgICAgXSxcbiAgICAgIH1cbiAgICBdO1xuICB9XG59XG4iXX0=