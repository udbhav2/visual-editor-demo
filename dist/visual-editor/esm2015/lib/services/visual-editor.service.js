/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import 'fabric';
import * as i0 from "@angular/core";
/**
 * @record
 */
function ImageConfig() { }
if (false) {
    /** @type {?} */
    ImageConfig.prototype.width;
    /** @type {?} */
    ImageConfig.prototype.height;
    /** @type {?} */
    ImageConfig.prototype.scaleFactor;
    /** @type {?} */
    ImageConfig.prototype.xOffset;
    /** @type {?} */
    ImageConfig.prototype.yOffset;
}
export class VisualEditorService {
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setColor(canvas, color) {
        canvas.freeDrawingBrush.color = color;
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static clearDrawingTransparentBg(canvas) {
        VisualEditorService.clearDrawing(canvas, 1); // NOTE: first object is the image itself
    }
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    static clearDrawing(canvas, offset = 0) {
        canvas
            .getObjects()
            .slice(offset)
            .forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static undoDrawingTransparentBg(canvas) {
        VisualEditorService.undoDrawing(canvas, 1); // NOTE: first object is the image itself
    }
    /**
     * @param {?} canvas
     * @param {?=} offset
     * @return {?}
     */
    static undoDrawing(canvas, offset = 0) {
        /** @type {?} */
        const objs = canvas.getObjects();
        if (objs.length > offset) {
            canvas.remove(objs[objs.length - 1]);
        }
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addText(canvas, params) {
        const { text, config } = params;
        /** @type {?} */
        const textbox = new fabric.Textbox(text, config);
        canvas.add(textbox);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static bringForward(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.bringForward(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static sendBackwards(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.sendBackwards(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setBold(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontWeight', selectedObject.fontWeight === 'bold' ? 'normal' : 'bold');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setItalic(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontStyle', selectedObject.fontStyle === 'italic' ? 'normal' : 'italic');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static setUnderline(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('underline', !selectedObject.underline);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignLeft(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'left');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignCenter(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'center');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignRight(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('textAlign', 'right');
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} fontFamily
     * @return {?}
     */
    static setFontFamily(canvas, fontFamily) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontFamily', fontFamily);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setFillColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fill', color);
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} canvas
     * @param {?} fontSize
     * @return {?}
     */
    static setFontSize(canvas, fontSize) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('fontSize', fontSize);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} lineHeight
     * @return {?}
     */
    static setLineHeight(canvas, lineHeight) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('lineHeight', lineHeight);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} characterSpacing
     * @return {?}
     */
    static setCharacterSpacing(canvas, characterSpacing) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('charSpacing', characterSpacing);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addImage(canvas, params) {
        fabric.Image.fromURL(params.fileUrl, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            if (params.scaling) {
                /** @type {?} */
                const scaleFactor = img.height > img.width
                    ? (canvas.height * params.scaling) / img.height
                    : (canvas.width * params.scaling) / img.width;
                img.set({
                    scaleX: scaleFactor,
                    scaleY: scaleFactor
                });
            }
            canvas.add(img);
            canvas.requestRenderAll();
        }), params.config);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} url
     * @param {?} callback
     * @return {?}
     */
    static updateImage(canvas, url, callback) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        const { width, height, scaleX, scaleY } = selectedObject;
        selectedObject.setSrc(url, (/**
         * @param {?} img
         * @return {?}
         */
        img => {
            /** @type {?} */
            const scaleFactor = _.min([
                (width * scaleX) / img.width,
                (height * scaleY) / img.height
            ]);
            img.set({
                scaleX: scaleFactor,
                scaleY: scaleFactor
            });
            canvas.requestRenderAll();
            callback();
        }), { crossOrigin: 'anonymous' });
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setStrokeColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('stroke', color);
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} canvas
     * @param {?} width
     * @return {?}
     */
    static setStrokeWidth(canvas, width) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        selectedObject.set('strokeWidth', width);
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteSelection(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        canvas.remove(selectedObject);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteSelectionGroup(canvas) {
        /** @type {?} */
        const selectedObjects = canvas.getActiveObject();
        selectedObjects.forEachObject((/**
         * @param {?} o
         * @return {?}
         */
        o => canvas.remove(o)));
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static deleteAllSelections(canvas) {
        canvas.remove(...canvas.getObjects());
        VisualEditorService.resetObjectTypeCount;
    }
    /**
     * @return {?}
     */
    static resetObjectTypeCount() {
        VisualEditorService.objectTypeCount = {
            product: 0,
            primary_logo: 0,
            secondary_logo: 0,
            lockup: 0,
            headline_textbox: 0,
            other_textbox: 0,
            graphic_accent: 0,
            photo_accent: 0,
            button: 0,
            banner: 0,
            sticker: 0,
            frame: 0
        };
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addRectangle(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addBoundingBox(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const rectangle = new fabric.Rect(config);
        canvas.add(rectangle);
        canvas.discardActiveObject();
        /** @type {?} */
        const objectType = params.additionalProperties.objectType;
        /** @type {?} */
        const objectId = VisualEditorService.createObjectId(objectType);
        VisualEditorService.extendObjectJson(rectangle, objectId, objectType);
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addLine(canvas, params) {
        const { config, coords } = params;
        /** @type {?} */
        const line = new fabric.Line(coords, config);
        canvas.add(line);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addCircle(canvas, params) {
        const { config } = params;
        /** @type {?} */
        const circle = new fabric.Circle(config);
        canvas.add(circle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static addTriangle(canvas, params) {
        const { config, coords } = params;
        /** @type {?} */
        const triangle = new fabric.Polygon(coords, config);
        canvas.add(triangle);
        canvas.discardActiveObject();
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static cloneObject(canvas) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const clone = new fabric.Rect(selectedObject.toObject());
        clone.set({ left: 0, top: 0 });
        canvas.add(clone);
        VisualEditorService.extendObjectJson(clone, selectedObject.toObject()._oId, selectedObject.toObject().objectType);
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionLeft(canvas) {
        VisualEditorService.alignSelection(canvas, 'left');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionHorizontalCenter(canvas) {
        VisualEditorService.alignSelection(canvas, 'horizontal-center');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionRight(canvas) {
        VisualEditorService.alignSelection(canvas, 'right');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionTop(canvas) {
        VisualEditorService.alignSelection(canvas, 'top');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionVerticalCenter(canvas) {
        VisualEditorService.alignSelection(canvas, 'vertical-center');
    }
    /**
     * @param {?} canvas
     * @return {?}
     */
    static alignSelectionBottom(canvas) {
        VisualEditorService.alignSelection(canvas, 'bottom');
    }
    /**
     * @param {?} canvas
     * @param {?} direction
     * @return {?}
     */
    static alignSelection(canvas, direction) {
        /** @type {?} */
        const group = canvas.getActiveObject();
        /** @type {?} */
        const groupBoundingRect = group.getBoundingRect(true);
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            /** @type {?} */
            const itemBoundingRect = item.getBoundingRect();
            switch (direction) {
                case 'left': {
                    item.set({
                        left: -groupBoundingRect.width / 2 + (item.left - itemBoundingRect.left)
                    });
                    break;
                }
                case 'horizontal-center': {
                    item.set({
                        left: item.left - itemBoundingRect.left - itemBoundingRect.width / 2
                    });
                    break;
                }
                case 'right': {
                    item.set({
                        left: item.left +
                            (groupBoundingRect.width / 2 - itemBoundingRect.left) -
                            itemBoundingRect.width
                    });
                    break;
                }
                case 'top': {
                    item.set({
                        top: -groupBoundingRect.height / 2 + (item.top - itemBoundingRect.top)
                    });
                    break;
                }
                case 'vertical-center': {
                    item.set({
                        top: item.top - itemBoundingRect.top - itemBoundingRect.height / 2
                    });
                    break;
                }
                case 'bottom': {
                    item.set({
                        top: item.top +
                            (groupBoundingRect.height / 2 - itemBoundingRect.top) -
                            itemBoundingRect.height
                    });
                    break;
                }
            }
        }));
        group.forEachObject((/**
         * @param {?} item
         * @return {?}
         */
        item => {
            group.removeWithUpdate(item).addWithUpdate(item);
        }));
        group.setCoords();
        canvas.requestRenderAll();
    }
    /**
     * @param {?} length
     * @param {?} angle
     * @param {?} pos
     * @return {?}
     */
    static getShadowOffset(length, angle, pos) {
        /** @type {?} */
        let x;
        /** @type {?} */
        let y;
        switch (pos) {
            case 'BOTTOM':
                x = 0;
                y = 10;
                break;
            case 'BOTTOM_LEFT':
                x = -6;
                y = 10;
                break;
            case 'BOTTOM_RIGHT':
                x = 6;
                y = 10;
                break;
            case 'TOP':
                x = 0;
                y = -10;
                break;
            case 'TOP_LEFT':
                x = -6;
                y = -10;
                break;
            case 'TOP_RIGHT':
                x = 6;
                y = -10;
                break;
            default:
                x = 0;
                y = 0;
                break;
        }
        return fabric.util.rotatePoint(new fabric.Point(x * length, y * length), new fabric.Point(0, 0), fabric.util.degreesToRadians(360 - angle));
    }
    /**
     * @param {?} canvas
     * @param {?} params
     * @return {?}
     */
    static setShadow(canvas, params) {
        const { angle, length, blur } = params;
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const shadowOffset = VisualEditorService.getShadowOffset(length, -angle, 'BOTTOM');
        /** @type {?} */
        const color = selectedObject.shadow && selectedObject.shadow.color
            ? selectedObject.shadow.color
            : 'rgba(0,0,0,0.5)';
        selectedObject.setShadow(Object.assign({}, selectedObject.shadow, { color,
            blur, offsetX: shadowOffset.x, offsetY: shadowOffset.y, affectStroke: false }));
        canvas.requestRenderAll();
    }
    /**
     * @param {?} canvas
     * @param {?} color
     * @return {?}
     */
    static setShadowColor(canvas, color) {
        /** @type {?} */
        const selectedObject = canvas.getActiveObject();
        /** @type {?} */
        const alpha = selectedObject.shadow &&
            selectedObject.shadow.color &&
            fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            ? fabric.Color.fromRgba(selectedObject.shadow.color).getAlpha()
            : 0.5;
        /** @type {?} */
        const rgbaColor = fabric.Color.fromHex(color)
            .setAlpha(alpha)
            .toRgba();
        selectedObject.setShadow(Object.assign({}, selectedObject.shadow, { color: rgbaColor, blur: 0.5 }));
        canvas.requestRenderAll();
        return color;
    }
    /**
     * @param {?} imgUrl
     * @param {?} canvasObj
     * @return {?}
     */
    static transformCanvas(imgUrl, canvasObj) {
        /** @type {?} */
        const backgroundPlaceholderObject = {
            src: 'background_placeholder',
            top: 300,
            _key: 'background_placeholder',
            fill: 'rgb(0,0,0)',
            left: 300,
            type: 'image',
            angle: 0,
            cropX: 0,
            cropY: 0,
            flipX: false,
            flipY: false,
            skewX: 0,
            skewY: 0,
            width: canvasObj.backgroundImage.width,
            clipTo: null,
            height: canvasObj.backgroundImage.height,
            scaleX: 1,
            scaleY: 1,
            shadow: null,
            stroke: null,
            filters: [],
            opacity: 1,
            originX: 'center',
            originY: 'center',
            version: '2.3.6',
            visible: true,
            fillRule: 'nonzero',
            paintFirst: 'fill',
            crossOrigin: '',
            strokeWidth: 0,
            strokeLineCap: 'butt',
            strokeLineJoin: 'miter',
            backgroundColor: '',
            strokeDashArray: null,
            transformMatrix: null,
            strokeMiterLimit: 4,
            globalCompositeOperation: 'source-over'
        };
        canvasObj.objects.forEach((/**
         * @param {?} o
         * @return {?}
         */
        o => {
            o.stroke = null;
            o.strokeWidth = 0;
            o.type = 'image';
            o.width = o.width * o.scaleX;
            o.height = o.height * o.scaleY;
            o.scaleX = 1;
            o.scaleY = 1;
            o.src = o._key;
            delete o._oId;
            delete o.objectType;
        }));
        canvasObj.objects.unshift(backgroundPlaceholderObject);
        canvasObj.backgroundImage.src = '';
        canvasObj.backgroundImage.crossOrigin = 'anonymous';
        canvasObj._originalImgUrl = imgUrl;
        /** @type {?} */
        const canvasJson = JSON.stringify(canvasObj, null, 2);
        console.log(canvasJson);
        return canvasJson;
    }
    /**
     * @param {?} objectType
     * @return {?}
     */
    static createObjectId(objectType) {
        /** @type {?} */
        const id = VisualEditorService.objectTypeCount[objectType];
        VisualEditorService.objectTypeCount[objectType] += 1;
        return id;
    }
    /**
     * @param {?} obj
     * @param {?} id
     * @param {?} objectType
     * @return {?}
     */
    static extendObjectJson(obj, id, objectType) {
        obj.toObject = ((/**
         * @param {?} toObject
         * @return {?}
         */
        function (toObject) {
            return (/**
             * @return {?}
             */
            function () {
                return fabric.util.object.extend(toObject.call(this), {
                    _oId: id,
                    _key: `${objectType}_placeholder_${id}`,
                    objectType
                });
            });
        }))(obj.toObject);
    }
}
VisualEditorService.objectTypeCount = {
    product: 0,
    primary_logo: 0,
    secondary_logo: 0,
    lockup: 0,
    headline_textbox: 0,
    other_textbox: 0,
    graphic_accent: 0,
    photo_accent: 0,
    button: 0,
    banner: 0,
    sticker: 0,
    frame: 0
};
VisualEditorService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ VisualEditorService.ngInjectableDef = i0.defineInjectable({ factory: function VisualEditorService_Factory() { return new VisualEditorService(); }, token: VisualEditorService, providedIn: "root" });
if (false) {
    /** @type {?} */
    VisualEditorService.objectTypeCount;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlzdWFsLWVkaXRvci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdmlzdWFsLWVkaXRvci8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy92aXN1YWwtZWRpdG9yLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxRQUFRLENBQUM7Ozs7O0FBR2hCLDBCQU1DOzs7SUFMQyw0QkFBYzs7SUFDZCw2QkFBZTs7SUFDZixrQ0FBb0I7O0lBQ3BCLDhCQUFnQjs7SUFDaEIsOEJBQWdCOztBQU1sQixNQUFNLE9BQU8sbUJBQW1COzs7Ozs7SUFnQjlCLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLEtBQUs7UUFDM0IsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMseUJBQXlCLENBQUMsTUFBTTtRQUNyQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMseUNBQXlDO0lBQ3hGLENBQUM7Ozs7OztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLE1BQU0sR0FBRyxDQUFDO1FBQ3BDLE1BQU07YUFDSCxVQUFVLEVBQUU7YUFDWixLQUFLLENBQUMsTUFBTSxDQUFDO2FBQ2IsT0FBTzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLHdCQUF3QixDQUFDLE1BQU07UUFDcEMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLHlDQUF5QztJQUN2RixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxNQUFNLEdBQUcsQ0FBQzs7Y0FDN0IsSUFBSSxHQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUU7UUFDaEMsSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sRUFBRTtZQUN4QixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxNQUFNO2NBQ3JCLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLE1BQU07O2NBQ3pCLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQztRQUNoRCxNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BCLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNOztjQUNsQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxNQUFNLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNOztjQUNuQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxNQUFNLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNOztjQUNiLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQ2hCLFlBQVksRUFDWixjQUFjLENBQUMsVUFBVSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQ3pELENBQUM7UUFDRixNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTTs7Y0FDZixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUNoQixXQUFXLEVBQ1gsY0FBYyxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUM1RCxDQUFDO1FBQ0YsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU07O2NBQ2xCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNELE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNOztjQUNmLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO1FBQy9DLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNOztjQUNqQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMxQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTTs7Y0FDaEIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDekMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsVUFBVTs7Y0FDL0IsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDN0MsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsS0FBSzs7Y0FDekIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbEMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDMUIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxRQUFROztjQUMzQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN6QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxVQUFVOztjQUMvQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLGdCQUFnQjs7Y0FDM0MsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUNwRCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxNQUFNO1FBQzVCLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUNsQixNQUFNLENBQUMsT0FBTzs7OztRQUNkLEdBQUcsQ0FBQyxFQUFFO1lBQ0osSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFOztzQkFDWixXQUFXLEdBQ2YsR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsS0FBSztvQkFDcEIsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU07b0JBQy9DLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxLQUFLO2dCQUNqRCxHQUFHLENBQUMsR0FBRyxDQUFDO29CQUNOLE1BQU0sRUFBRSxXQUFXO29CQUNuQixNQUFNLEVBQUUsV0FBVztpQkFDcEIsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hCLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVCLENBQUMsR0FDRCxNQUFNLENBQUMsTUFBTSxDQUNkLENBQUM7UUFFRixNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLFFBQVE7O2NBQ2hDLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFO2NBQ3pDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsY0FBYztRQUN4RCxjQUFjLENBQUMsTUFBTSxDQUNuQixHQUFHOzs7O1FBQ0gsR0FBRyxDQUFDLEVBQUU7O2tCQUNFLFdBQVcsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDO2dCQUN4QixDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsR0FBRyxHQUFHLENBQUMsS0FBSztnQkFDNUIsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsR0FBRyxDQUFDLE1BQU07YUFDL0IsQ0FBQztZQUNGLEdBQUcsQ0FBQyxHQUFHLENBQUM7Z0JBQ04sTUFBTSxFQUFFLFdBQVc7Z0JBQ25CLE1BQU0sRUFBRSxXQUFXO2FBQ3BCLENBQUMsQ0FBQztZQUNILE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQzFCLFFBQVEsRUFBRSxDQUFDO1FBQ2IsQ0FBQyxHQUNELEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxDQUM3QixDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsS0FBSzs7Y0FDM0IsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsY0FBYyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDcEMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDMUIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLOztjQUMzQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUMvQyxjQUFjLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6QyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBTTs7Y0FDckIsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7UUFDL0MsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNOztjQUMxQixlQUFlLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTtRQUNoRCxlQUFlLENBQUMsYUFBYTs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDO1FBQ3JELE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLG1CQUFtQixDQUFDLE1BQU07UUFDL0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBRXRDLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxNQUFNLENBQUMsb0JBQW9CO1FBQ3pCLG1CQUFtQixDQUFDLGVBQWUsR0FBRztZQUNwQyxPQUFPLEVBQUUsQ0FBQztZQUNWLFlBQVksRUFBRSxDQUFDO1lBQ2YsY0FBYyxFQUFFLENBQUM7WUFDakIsTUFBTSxFQUFFLENBQUM7WUFDVCxnQkFBZ0IsRUFBRSxDQUFDO1lBQ25CLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLGNBQWMsRUFBRSxDQUFDO1lBQ2pCLFlBQVksRUFBRSxDQUFDO1lBQ2YsTUFBTSxFQUFFLENBQUM7WUFDVCxNQUFNLEVBQUUsQ0FBQztZQUNULE9BQU8sRUFBRSxDQUFDO1lBQ1YsS0FBSyxFQUFFLENBQUM7U0FDVCxDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsTUFBTTtjQUMxQixFQUFFLE1BQU0sRUFBRSxHQUFHLE1BQU07O2NBQ25CLFNBQVMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEIsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsTUFBTTtjQUM1QixFQUFFLE1BQU0sRUFBRSxHQUFHLE1BQU07O2NBQ25CLFNBQVMsR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3pDLE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEIsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7O2NBRXZCLFVBQVUsR0FBRyxNQUFNLENBQUMsb0JBQW9CLENBQUMsVUFBVTs7Y0FDbkQsUUFBUSxHQUFHLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7UUFDL0QsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUN4RSxDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxNQUFNO2NBQ3JCLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxHQUFHLE1BQU07O2NBQzNCLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE1BQU0sQ0FBQztRQUM1QyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2pCLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLE1BQU07Y0FDdkIsRUFBRSxNQUFNLEVBQUUsR0FBRyxNQUFNOztjQUNuQixNQUFNLEdBQUcsSUFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUN4QyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25CLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUVELE1BQU0sQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLE1BQU07Y0FDekIsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLEdBQUcsTUFBTTs7Y0FDM0IsUUFBUSxHQUFHLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDO1FBQ25ELE1BQU0sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDckIsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU07O2NBQ2pCLGNBQWMsR0FBRyxNQUFNLENBQUMsZUFBZSxFQUFFOztjQUN6QyxLQUFLLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN4RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUvQixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRWxCLG1CQUFtQixDQUFDLGdCQUFnQixDQUNsQyxLQUFLLEVBQ0wsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDLElBQUksRUFDOUIsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDLFVBQVUsQ0FDckMsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLE1BQU07UUFDOUIsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNO1FBQzFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztJQUNsRSxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNO1FBQy9CLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsTUFBTTtRQUM3QixtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLDRCQUE0QixDQUFDLE1BQU07UUFDeEMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU07UUFDaEMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxTQUFTOztjQUMvQixLQUFLLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTs7Y0FDaEMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7UUFFckQsS0FBSyxDQUFDLGFBQWE7Ozs7UUFBQyxJQUFJLENBQUMsRUFBRTs7a0JBQ25CLGdCQUFnQixHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFFL0MsUUFBUSxTQUFTLEVBQUU7Z0JBQ2pCLEtBQUssTUFBTSxDQUFDLENBQUM7b0JBQ1gsSUFBSSxDQUFDLEdBQUcsQ0FBQzt3QkFDUCxJQUFJLEVBQ0YsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7cUJBQ3JFLENBQUMsQ0FBQztvQkFDSCxNQUFNO2lCQUNQO2dCQUNELEtBQUssbUJBQW1CLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxDQUFDLEdBQUcsQ0FBQzt3QkFDUCxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxHQUFHLENBQUM7cUJBQ3JFLENBQUMsQ0FBQztvQkFDSCxNQUFNO2lCQUNQO2dCQUNELEtBQUssT0FBTyxDQUFDLENBQUM7b0JBQ1osSUFBSSxDQUFDLEdBQUcsQ0FBQzt3QkFDUCxJQUFJLEVBQ0YsSUFBSSxDQUFDLElBQUk7NEJBQ1QsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQzs0QkFDckQsZ0JBQWdCLENBQUMsS0FBSztxQkFDekIsQ0FBQyxDQUFDO29CQUNILE1BQU07aUJBQ1A7Z0JBQ0QsS0FBSyxLQUFLLENBQUMsQ0FBQztvQkFDVixJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLEdBQUcsRUFDRCxDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLGdCQUFnQixDQUFDLEdBQUcsQ0FBQztxQkFDcEUsQ0FBQyxDQUFDO29CQUNILE1BQU07aUJBQ1A7Z0JBQ0QsS0FBSyxpQkFBaUIsQ0FBQyxDQUFDO29CQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRyxHQUFHLGdCQUFnQixDQUFDLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQztxQkFDbkUsQ0FBQyxDQUFDO29CQUNILE1BQU07aUJBQ1A7Z0JBQ0QsS0FBSyxRQUFRLENBQUMsQ0FBQztvQkFDYixJQUFJLENBQUMsR0FBRyxDQUFDO3dCQUNQLEdBQUcsRUFDRCxJQUFJLENBQUMsR0FBRzs0QkFDUixDQUFDLGlCQUFpQixDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsR0FBRyxDQUFDOzRCQUNyRCxnQkFBZ0IsQ0FBQyxNQUFNO3FCQUMxQixDQUFDLENBQUM7b0JBQ0gsTUFBTTtpQkFDUDthQUNGO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCxLQUFLLENBQUMsYUFBYTs7OztRQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3pCLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQyxFQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDbEIsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7OztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBYyxFQUFFLEtBQWEsRUFBRSxHQUFXOztZQUMzRCxDQUFDOztZQUNELENBQUM7UUFFTCxRQUFRLEdBQUcsRUFBRTtZQUNYLEtBQUssUUFBUTtnQkFDWCxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNOLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ1AsTUFBTTtZQUNSLEtBQUssYUFBYTtnQkFDaEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNQLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ1AsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNQLE1BQU07WUFDUixLQUFLLEtBQUs7Z0JBQ1IsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQ1IsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO2dCQUNSLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUM7Z0JBQ1IsTUFBTTtZQUNSO2dCQUNFLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ04sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDTixNQUFNO1NBQ1Q7UUFFRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUM1QixJQUFJLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLEVBQ3hDLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQ3RCLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUMxQyxDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsTUFBTTtjQUN2QixFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsTUFBTTs7Y0FDaEMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxlQUFlLEVBQUU7O2NBQ3pDLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxlQUFlLENBQ3RELE1BQU0sRUFDTixDQUFDLEtBQUssRUFDTixRQUFRLENBQ1Q7O2NBRUssS0FBSyxHQUNULGNBQWMsQ0FBQyxNQUFNLElBQUksY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLO1lBQ2xELENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDN0IsQ0FBQyxDQUFDLGlCQUFpQjtRQUV2QixjQUFjLENBQUMsU0FBUyxtQkFDbkIsY0FBYyxDQUFDLE1BQU0sSUFDeEIsS0FBSztZQUNMLElBQUksRUFDSixPQUFPLEVBQUUsWUFBWSxDQUFDLENBQUMsRUFDdkIsT0FBTyxFQUFFLFlBQVksQ0FBQyxDQUFDLEVBQ3ZCLFlBQVksRUFBRSxLQUFLLElBQ25CLENBQUM7UUFDSCxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLOztjQUMzQixjQUFjLEdBQUcsTUFBTSxDQUFDLGVBQWUsRUFBRTs7Y0FDekMsS0FBSyxHQUNULGNBQWMsQ0FBQyxNQUFNO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNLENBQUMsS0FBSztZQUMzQixNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUMzRCxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEVBQUU7WUFDL0QsQ0FBQyxDQUFDLEdBQUc7O2NBQ0gsU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQzthQUMxQyxRQUFRLENBQUMsS0FBSyxDQUFDO2FBQ2YsTUFBTSxFQUFFO1FBQ1gsY0FBYyxDQUFDLFNBQVMsbUJBQ25CLGNBQWMsQ0FBQyxNQUFNLElBQ3hCLEtBQUssRUFBRSxTQUFTLEVBQ2hCLElBQUksRUFBRSxHQUFHLElBQ1QsQ0FBQztRQUNILE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBRTFCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Ozs7O0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsU0FBUzs7Y0FDaEMsMkJBQTJCLEdBQUc7WUFDbEMsR0FBRyxFQUFFLHdCQUF3QjtZQUM3QixHQUFHLEVBQUUsR0FBRztZQUNSLElBQUksRUFBRSx3QkFBd0I7WUFDOUIsSUFBSSxFQUFFLFlBQVk7WUFDbEIsSUFBSSxFQUFFLEdBQUc7WUFDVCxJQUFJLEVBQUUsT0FBTztZQUNiLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxLQUFLO1lBQ1osS0FBSyxFQUFFLEtBQUs7WUFDWixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLFNBQVMsQ0FBQyxlQUFlLENBQUMsS0FBSztZQUN0QyxNQUFNLEVBQUUsSUFBSTtZQUNaLE1BQU0sRUFBRSxTQUFTLENBQUMsZUFBZSxDQUFDLE1BQU07WUFDeEMsTUFBTSxFQUFFLENBQUM7WUFDVCxNQUFNLEVBQUUsQ0FBQztZQUNULE1BQU0sRUFBRSxJQUFJO1lBQ1osTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxDQUFDO1lBQ1YsT0FBTyxFQUFFLFFBQVE7WUFDakIsT0FBTyxFQUFFLFFBQVE7WUFDakIsT0FBTyxFQUFFLE9BQU87WUFDaEIsT0FBTyxFQUFFLElBQUk7WUFDYixRQUFRLEVBQUUsU0FBUztZQUNuQixVQUFVLEVBQUUsTUFBTTtZQUNsQixXQUFXLEVBQUUsRUFBRTtZQUNmLFdBQVcsRUFBRSxDQUFDO1lBQ2QsYUFBYSxFQUFFLE1BQU07WUFDckIsY0FBYyxFQUFFLE9BQU87WUFDdkIsZUFBZSxFQUFFLEVBQUU7WUFDbkIsZUFBZSxFQUFFLElBQUk7WUFDckIsZUFBZSxFQUFFLElBQUk7WUFDckIsZ0JBQWdCLEVBQUUsQ0FBQztZQUNuQix3QkFBd0IsRUFBRSxhQUFhO1NBQ3hDO1FBRUQsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDNUIsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7WUFDbEIsQ0FBQyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7WUFDakIsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDN0IsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDL0IsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDYixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNiLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQTtZQUVkLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQztZQUNkLE9BQU8sQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztRQUNILFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDdkQsU0FBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBQ25DLFNBQVMsQ0FBQyxlQUFlLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUNwRCxTQUFTLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQzs7Y0FFN0IsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN4QixPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsVUFBa0I7O2NBQ2hDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDO1FBQzFELG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUUsVUFBVTtRQUN6QyxHQUFHLENBQUMsUUFBUSxHQUFHOzs7O1FBQUMsVUFBUyxRQUFRO1lBQy9COzs7WUFBTztnQkFDTCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNwRCxJQUFJLEVBQUUsRUFBRTtvQkFDUixJQUFJLEVBQUUsR0FBRyxVQUFVLGdCQUFnQixFQUFFLEVBQUU7b0JBQ3ZDLFVBQVU7aUJBQ1gsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxFQUFDO1FBQ0osQ0FBQyxFQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ25CLENBQUM7O0FBamhCTSxtQ0FBZSxHQUFPO0lBQzNCLE9BQU8sRUFBRSxDQUFDO0lBQ1YsWUFBWSxFQUFFLENBQUM7SUFDZixjQUFjLEVBQUUsQ0FBQztJQUNqQixNQUFNLEVBQUUsQ0FBQztJQUNULGdCQUFnQixFQUFFLENBQUM7SUFDbkIsYUFBYSxFQUFFLENBQUM7SUFDaEIsY0FBYyxFQUFFLENBQUM7SUFDakIsWUFBWSxFQUFFLENBQUM7SUFDZixNQUFNLEVBQUUsQ0FBQztJQUNULE1BQU0sRUFBRSxDQUFDO0lBQ1QsT0FBTyxFQUFFLENBQUM7SUFDVixLQUFLLEVBQUUsQ0FBQztDQUNULENBQUM7O1lBakJILFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7Ozs7SUFFQyxvQ0FhRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcbmltcG9ydCAnZmFicmljJztcbmRlY2xhcmUgY29uc3QgZmFicmljOiBhbnk7XG5cbmludGVyZmFjZSBJbWFnZUNvbmZpZyB7XG4gIHdpZHRoOiBudW1iZXI7XG4gIGhlaWdodDogbnVtYmVyO1xuICBzY2FsZUZhY3RvcjogbnVtYmVyO1xuICB4T2Zmc2V0OiBudW1iZXI7XG4gIHlPZmZzZXQ6IG51bWJlcjtcbn1cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgVmlzdWFsRWRpdG9yU2VydmljZSB7XG4gIHN0YXRpYyBvYmplY3RUeXBlQ291bnQ6IHt9ID0ge1xuICAgIHByb2R1Y3Q6IDAsXG4gICAgcHJpbWFyeV9sb2dvOiAwLFxuICAgIHNlY29uZGFyeV9sb2dvOiAwLFxuICAgIGxvY2t1cDogMCxcbiAgICBoZWFkbGluZV90ZXh0Ym94OiAwLFxuICAgIG90aGVyX3RleHRib3g6IDAsXG4gICAgZ3JhcGhpY19hY2NlbnQ6IDAsXG4gICAgcGhvdG9fYWNjZW50OiAwLFxuICAgIGJ1dHRvbjogMCxcbiAgICBiYW5uZXI6IDAsXG4gICAgc3RpY2tlcjogMCxcbiAgICBmcmFtZTogMFxuICB9O1xuXG4gIHN0YXRpYyBzZXRDb2xvcihjYW52YXMsIGNvbG9yKSB7XG4gICAgY2FudmFzLmZyZWVEcmF3aW5nQnJ1c2guY29sb3IgPSBjb2xvcjtcbiAgfVxuXG4gIHN0YXRpYyBjbGVhckRyYXdpbmdUcmFuc3BhcmVudEJnKGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuY2xlYXJEcmF3aW5nKGNhbnZhcywgMSk7IC8vIE5PVEU6IGZpcnN0IG9iamVjdCBpcyB0aGUgaW1hZ2UgaXRzZWxmXG4gIH1cblxuICBzdGF0aWMgY2xlYXJEcmF3aW5nKGNhbnZhcywgb2Zmc2V0ID0gMCkge1xuICAgIGNhbnZhc1xuICAgICAgLmdldE9iamVjdHMoKVxuICAgICAgLnNsaWNlKG9mZnNldClcbiAgICAgIC5mb3JFYWNoKG8gPT4gY2FudmFzLnJlbW92ZShvKSk7XG4gIH1cblxuICBzdGF0aWMgdW5kb0RyYXdpbmdUcmFuc3BhcmVudEJnKGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UudW5kb0RyYXdpbmcoY2FudmFzLCAxKTsgLy8gTk9URTogZmlyc3Qgb2JqZWN0IGlzIHRoZSBpbWFnZSBpdHNlbGZcbiAgfVxuXG4gIHN0YXRpYyB1bmRvRHJhd2luZyhjYW52YXMsIG9mZnNldCA9IDApIHtcbiAgICBjb25zdCBvYmpzID0gY2FudmFzLmdldE9iamVjdHMoKTtcbiAgICBpZiAob2Jqcy5sZW5ndGggPiBvZmZzZXQpIHtcbiAgICAgIGNhbnZhcy5yZW1vdmUob2Jqc1tvYmpzLmxlbmd0aCAtIDFdKTtcbiAgICB9XG4gIH1cblxuICBzdGF0aWMgYWRkVGV4dChjYW52YXMsIHBhcmFtcykge1xuICAgIGNvbnN0IHsgdGV4dCwgY29uZmlnIH0gPSBwYXJhbXM7XG4gICAgY29uc3QgdGV4dGJveCA9IG5ldyBmYWJyaWMuVGV4dGJveCh0ZXh0LCBjb25maWcpO1xuICAgIGNhbnZhcy5hZGQodGV4dGJveCk7XG4gICAgY2FudmFzLmRpc2NhcmRBY3RpdmVPYmplY3QoKTtcbiAgfVxuXG4gIHN0YXRpYyBicmluZ0ZvcndhcmQoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY2FudmFzLmJyaW5nRm9yd2FyZChzZWxlY3RlZE9iamVjdCk7XG4gIH1cblxuICBzdGF0aWMgc2VuZEJhY2t3YXJkcyhjYW52YXMpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBjYW52YXMuc2VuZEJhY2t3YXJkcyhzZWxlY3RlZE9iamVjdCk7XG4gIH1cblxuICBzdGF0aWMgc2V0Qm9sZChjYW52YXMpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoXG4gICAgICAnZm9udFdlaWdodCcsXG4gICAgICBzZWxlY3RlZE9iamVjdC5mb250V2VpZ2h0ID09PSAnYm9sZCcgPyAnbm9ybWFsJyA6ICdib2xkJ1xuICAgICk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRJdGFsaWMoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KFxuICAgICAgJ2ZvbnRTdHlsZScsXG4gICAgICBzZWxlY3RlZE9iamVjdC5mb250U3R5bGUgPT09ICdpdGFsaWMnID8gJ25vcm1hbCcgOiAnaXRhbGljJ1xuICAgICk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRVbmRlcmxpbmUoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCd1bmRlcmxpbmUnLCAhc2VsZWN0ZWRPYmplY3QudW5kZXJsaW5lKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduTGVmdChjYW52YXMpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoJ3RleHRBbGlnbicsICdsZWZ0Jyk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnbkNlbnRlcihjYW52YXMpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoJ3RleHRBbGlnbicsICdjZW50ZXInKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduUmlnaHQoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCd0ZXh0QWxpZ24nLCAncmlnaHQnKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIHNldEZvbnRGYW1pbHkoY2FudmFzLCBmb250RmFtaWx5KSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCdmb250RmFtaWx5JywgZm9udEZhbWlseSk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRGaWxsQ29sb3IoY2FudmFzLCBjb2xvcikge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnZmlsbCcsIGNvbG9yKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICAgIHJldHVybiBjb2xvcjtcbiAgfVxuXG4gIHN0YXRpYyBzZXRGb250U2l6ZShjYW52YXMsIGZvbnRTaXplKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCdmb250U2l6ZScsIGZvbnRTaXplKTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG4gIFxuICBzdGF0aWMgc2V0TGluZUhlaWdodChjYW52YXMsIGxpbmVIZWlnaHQpIHtcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXQoJ2xpbmVIZWlnaHQnLCBsaW5lSGVpZ2h0KTtcbiAgICBjYW52YXMucmVxdWVzdFJlbmRlckFsbCgpO1xuICB9XG5cbiAgc3RhdGljIHNldENoYXJhY3RlclNwYWNpbmcoY2FudmFzLCBjaGFyYWN0ZXJTcGFjaW5nKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCdjaGFyU3BhY2luZycsIGNoYXJhY3RlclNwYWNpbmcpO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgYWRkSW1hZ2UoY2FudmFzLCBwYXJhbXMpIHtcbiAgICBmYWJyaWMuSW1hZ2UuZnJvbVVSTChcbiAgICAgIHBhcmFtcy5maWxlVXJsLFxuICAgICAgaW1nID0+IHtcbiAgICAgICAgaWYgKHBhcmFtcy5zY2FsaW5nKSB7XG4gICAgICAgICAgY29uc3Qgc2NhbGVGYWN0b3IgPVxuICAgICAgICAgICAgaW1nLmhlaWdodCA+IGltZy53aWR0aFxuICAgICAgICAgICAgICA/IChjYW52YXMuaGVpZ2h0ICogcGFyYW1zLnNjYWxpbmcpIC8gaW1nLmhlaWdodFxuICAgICAgICAgICAgICA6IChjYW52YXMud2lkdGggKiBwYXJhbXMuc2NhbGluZykgLyBpbWcud2lkdGg7XG4gICAgICAgICAgaW1nLnNldCh7XG4gICAgICAgICAgICBzY2FsZVg6IHNjYWxlRmFjdG9yLFxuICAgICAgICAgICAgc2NhbGVZOiBzY2FsZUZhY3RvclxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGNhbnZhcy5hZGQoaW1nKTtcbiAgICAgICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgIH0sXG4gICAgICBwYXJhbXMuY29uZmlnXG4gICAgKTtcblxuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgdXBkYXRlSW1hZ2UoY2FudmFzLCB1cmwsIGNhbGxiYWNrKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY29uc3QgeyB3aWR0aCwgaGVpZ2h0LCBzY2FsZVgsIHNjYWxlWSB9ID0gc2VsZWN0ZWRPYmplY3Q7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0U3JjKFxuICAgICAgdXJsLFxuICAgICAgaW1nID0+IHtcbiAgICAgICAgY29uc3Qgc2NhbGVGYWN0b3IgPSBfLm1pbihbXG4gICAgICAgICAgKHdpZHRoICogc2NhbGVYKSAvIGltZy53aWR0aCxcbiAgICAgICAgICAoaGVpZ2h0ICogc2NhbGVZKSAvIGltZy5oZWlnaHRcbiAgICAgICAgXSk7XG4gICAgICAgIGltZy5zZXQoe1xuICAgICAgICAgIHNjYWxlWDogc2NhbGVGYWN0b3IsXG4gICAgICAgICAgc2NhbGVZOiBzY2FsZUZhY3RvclxuICAgICAgICB9KTtcbiAgICAgICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgIH0sXG4gICAgICB7IGNyb3NzT3JpZ2luOiAnYW5vbnltb3VzJyB9XG4gICAgKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRTdHJva2VDb2xvcihjYW52YXMsIGNvbG9yKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgc2VsZWN0ZWRPYmplY3Quc2V0KCdzdHJva2UnLCBjb2xvcik7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgICByZXR1cm4gY29sb3I7XG4gIH1cblxuICBzdGF0aWMgc2V0U3Ryb2tlV2lkdGgoY2FudmFzLCB3aWR0aCkge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0LnNldCgnc3Ryb2tlV2lkdGgnLCB3aWR0aCk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBkZWxldGVTZWxlY3Rpb24oY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY2FudmFzLnJlbW92ZShzZWxlY3RlZE9iamVjdCk7XG4gIH1cblxuICBzdGF0aWMgZGVsZXRlU2VsZWN0aW9uR3JvdXAoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3RzID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIHNlbGVjdGVkT2JqZWN0cy5mb3JFYWNoT2JqZWN0KG8gPT4gY2FudmFzLnJlbW92ZShvKSk7XG4gICAgY2FudmFzLmRpc2NhcmRBY3RpdmVPYmplY3QoKTtcbiAgfVxuXG4gIHN0YXRpYyBkZWxldGVBbGxTZWxlY3Rpb25zKGNhbnZhcykge1xuICAgIGNhbnZhcy5yZW1vdmUoLi4uY2FudmFzLmdldE9iamVjdHMoKSk7XG5cbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLnJlc2V0T2JqZWN0VHlwZUNvdW50O1xuICB9XG5cbiAgc3RhdGljIHJlc2V0T2JqZWN0VHlwZUNvdW50KCkge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2Uub2JqZWN0VHlwZUNvdW50ID0ge1xuICAgICAgcHJvZHVjdDogMCxcbiAgICAgIHByaW1hcnlfbG9nbzogMCxcbiAgICAgIHNlY29uZGFyeV9sb2dvOiAwLFxuICAgICAgbG9ja3VwOiAwLFxuICAgICAgaGVhZGxpbmVfdGV4dGJveDogMCxcbiAgICAgIG90aGVyX3RleHRib3g6IDAsXG4gICAgICBncmFwaGljX2FjY2VudDogMCxcbiAgICAgIHBob3RvX2FjY2VudDogMCxcbiAgICAgIGJ1dHRvbjogMCxcbiAgICAgIGJhbm5lcjogMCxcbiAgICAgIHN0aWNrZXI6IDAsXG4gICAgICBmcmFtZTogMFxuICAgIH07XG4gIH1cblxuICBzdGF0aWMgYWRkUmVjdGFuZ2xlKGNhbnZhcywgcGFyYW1zKSB7XG4gICAgY29uc3QgeyBjb25maWcgfSA9IHBhcmFtcztcbiAgICBjb25zdCByZWN0YW5nbGUgPSBuZXcgZmFicmljLlJlY3QoY29uZmlnKTtcbiAgICBjYW52YXMuYWRkKHJlY3RhbmdsZSk7XG4gICAgY2FudmFzLmRpc2NhcmRBY3RpdmVPYmplY3QoKTtcbiAgfVxuXG4gIHN0YXRpYyBhZGRCb3VuZGluZ0JveChjYW52YXMsIHBhcmFtcykge1xuICAgIGNvbnN0IHsgY29uZmlnIH0gPSBwYXJhbXM7XG4gICAgY29uc3QgcmVjdGFuZ2xlID0gbmV3IGZhYnJpYy5SZWN0KGNvbmZpZyk7XG4gICAgY2FudmFzLmFkZChyZWN0YW5nbGUpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG5cbiAgICBjb25zdCBvYmplY3RUeXBlID0gcGFyYW1zLmFkZGl0aW9uYWxQcm9wZXJ0aWVzLm9iamVjdFR5cGU7XG4gICAgY29uc3Qgb2JqZWN0SWQgPSBWaXN1YWxFZGl0b3JTZXJ2aWNlLmNyZWF0ZU9iamVjdElkKG9iamVjdFR5cGUpO1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuZXh0ZW5kT2JqZWN0SnNvbihyZWN0YW5nbGUsIG9iamVjdElkLCBvYmplY3RUeXBlKTtcbiAgfVxuXG4gIHN0YXRpYyBhZGRMaW5lKGNhbnZhcywgcGFyYW1zKSB7XG4gICAgY29uc3QgeyBjb25maWcsIGNvb3JkcyB9ID0gcGFyYW1zO1xuICAgIGNvbnN0IGxpbmUgPSBuZXcgZmFicmljLkxpbmUoY29vcmRzLCBjb25maWcpO1xuICAgIGNhbnZhcy5hZGQobGluZSk7XG4gICAgY2FudmFzLmRpc2NhcmRBY3RpdmVPYmplY3QoKTtcbiAgfVxuXG4gIHN0YXRpYyBhZGRDaXJjbGUoY2FudmFzLCBwYXJhbXMpIHtcbiAgICBjb25zdCB7IGNvbmZpZyB9ID0gcGFyYW1zO1xuICAgIGNvbnN0IGNpcmNsZSA9IG5ldyBmYWJyaWMuQ2lyY2xlKGNvbmZpZyk7XG4gICAgY2FudmFzLmFkZChjaXJjbGUpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgYWRkVHJpYW5nbGUoY2FudmFzLCBwYXJhbXMpIHtcbiAgICBjb25zdCB7IGNvbmZpZywgY29vcmRzIH0gPSBwYXJhbXM7XG4gICAgY29uc3QgdHJpYW5nbGUgPSBuZXcgZmFicmljLlBvbHlnb24oY29vcmRzLCBjb25maWcpO1xuICAgIGNhbnZhcy5hZGQodHJpYW5nbGUpO1xuICAgIGNhbnZhcy5kaXNjYXJkQWN0aXZlT2JqZWN0KCk7XG4gIH1cblxuICBzdGF0aWMgY2xvbmVPYmplY3QoY2FudmFzKSB7XG4gICAgY29uc3Qgc2VsZWN0ZWRPYmplY3QgPSBjYW52YXMuZ2V0QWN0aXZlT2JqZWN0KCk7XG4gICAgY29uc3QgY2xvbmUgPSBuZXcgZmFicmljLlJlY3Qoc2VsZWN0ZWRPYmplY3QudG9PYmplY3QoKSk7XG4gICAgY2xvbmUuc2V0KHsgbGVmdDogMCwgdG9wOiAwIH0pO1xuXG4gICAgY2FudmFzLmFkZChjbG9uZSk7XG5cbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmV4dGVuZE9iamVjdEpzb24oXG4gICAgICBjbG9uZSxcbiAgICAgIHNlbGVjdGVkT2JqZWN0LnRvT2JqZWN0KCkuX29JZCxcbiAgICAgIHNlbGVjdGVkT2JqZWN0LnRvT2JqZWN0KCkub2JqZWN0VHlwZVxuICAgICk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb25MZWZ0KGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb24oY2FudmFzLCAnbGVmdCcpO1xuICB9XG5cbiAgc3RhdGljIGFsaWduU2VsZWN0aW9uSG9yaXpvbnRhbENlbnRlcihjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uKGNhbnZhcywgJ2hvcml6b250YWwtY2VudGVyJyk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb25SaWdodChjYW52YXMpIHtcbiAgICBWaXN1YWxFZGl0b3JTZXJ2aWNlLmFsaWduU2VsZWN0aW9uKGNhbnZhcywgJ3JpZ2h0Jyk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb25Ub3AoY2FudmFzKSB7XG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbihjYW52YXMsICd0b3AnKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnblNlbGVjdGlvblZlcnRpY2FsQ2VudGVyKGNhbnZhcykge1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2UuYWxpZ25TZWxlY3Rpb24oY2FudmFzLCAndmVydGljYWwtY2VudGVyJyk7XG4gIH1cblxuICBzdGF0aWMgYWxpZ25TZWxlY3Rpb25Cb3R0b20oY2FudmFzKSB7XG4gICAgVmlzdWFsRWRpdG9yU2VydmljZS5hbGlnblNlbGVjdGlvbihjYW52YXMsICdib3R0b20nKTtcbiAgfVxuXG4gIHN0YXRpYyBhbGlnblNlbGVjdGlvbihjYW52YXMsIGRpcmVjdGlvbikge1xuICAgIGNvbnN0IGdyb3VwID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNvbnN0IGdyb3VwQm91bmRpbmdSZWN0ID0gZ3JvdXAuZ2V0Qm91bmRpbmdSZWN0KHRydWUpO1xuXG4gICAgZ3JvdXAuZm9yRWFjaE9iamVjdChpdGVtID0+IHtcbiAgICAgIGNvbnN0IGl0ZW1Cb3VuZGluZ1JlY3QgPSBpdGVtLmdldEJvdW5kaW5nUmVjdCgpO1xuXG4gICAgICBzd2l0Y2ggKGRpcmVjdGlvbikge1xuICAgICAgICBjYXNlICdsZWZ0Jzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIGxlZnQ6XG4gICAgICAgICAgICAgIC1ncm91cEJvdW5kaW5nUmVjdC53aWR0aCAvIDIgKyAoaXRlbS5sZWZ0IC0gaXRlbUJvdW5kaW5nUmVjdC5sZWZ0KVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIGNhc2UgJ2hvcml6b250YWwtY2VudGVyJzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIGxlZnQ6IGl0ZW0ubGVmdCAtIGl0ZW1Cb3VuZGluZ1JlY3QubGVmdCAtIGl0ZW1Cb3VuZGluZ1JlY3Qud2lkdGggLyAyXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAncmlnaHQnOiB7XG4gICAgICAgICAgaXRlbS5zZXQoe1xuICAgICAgICAgICAgbGVmdDpcbiAgICAgICAgICAgICAgaXRlbS5sZWZ0ICtcbiAgICAgICAgICAgICAgKGdyb3VwQm91bmRpbmdSZWN0LndpZHRoIC8gMiAtIGl0ZW1Cb3VuZGluZ1JlY3QubGVmdCkgLVxuICAgICAgICAgICAgICBpdGVtQm91bmRpbmdSZWN0LndpZHRoXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAndG9wJzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIHRvcDpcbiAgICAgICAgICAgICAgLWdyb3VwQm91bmRpbmdSZWN0LmhlaWdodCAvIDIgKyAoaXRlbS50b3AgLSBpdGVtQm91bmRpbmdSZWN0LnRvcClcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBjYXNlICd2ZXJ0aWNhbC1jZW50ZXInOiB7XG4gICAgICAgICAgaXRlbS5zZXQoe1xuICAgICAgICAgICAgdG9wOiBpdGVtLnRvcCAtIGl0ZW1Cb3VuZGluZ1JlY3QudG9wIC0gaXRlbUJvdW5kaW5nUmVjdC5oZWlnaHQgLyAyXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgICAgY2FzZSAnYm90dG9tJzoge1xuICAgICAgICAgIGl0ZW0uc2V0KHtcbiAgICAgICAgICAgIHRvcDpcbiAgICAgICAgICAgICAgaXRlbS50b3AgK1xuICAgICAgICAgICAgICAoZ3JvdXBCb3VuZGluZ1JlY3QuaGVpZ2h0IC8gMiAtIGl0ZW1Cb3VuZGluZ1JlY3QudG9wKSAtXG4gICAgICAgICAgICAgIGl0ZW1Cb3VuZGluZ1JlY3QuaGVpZ2h0XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGdyb3VwLmZvckVhY2hPYmplY3QoaXRlbSA9PiB7XG4gICAgICBncm91cC5yZW1vdmVXaXRoVXBkYXRlKGl0ZW0pLmFkZFdpdGhVcGRhdGUoaXRlbSk7XG4gICAgfSk7XG4gICAgZ3JvdXAuc2V0Q29vcmRzKCk7XG4gICAgY2FudmFzLnJlcXVlc3RSZW5kZXJBbGwoKTtcbiAgfVxuXG4gIHN0YXRpYyBnZXRTaGFkb3dPZmZzZXQobGVuZ3RoOiBudW1iZXIsIGFuZ2xlOiBudW1iZXIsIHBvczogc3RyaW5nKSB7XG4gICAgbGV0IHg7XG4gICAgbGV0IHk7XG5cbiAgICBzd2l0Y2ggKHBvcykge1xuICAgICAgY2FzZSAnQk9UVE9NJzpcbiAgICAgICAgeCA9IDA7XG4gICAgICAgIHkgPSAxMDtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdCT1RUT01fTEVGVCc6XG4gICAgICAgIHggPSAtNjtcbiAgICAgICAgeSA9IDEwO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ0JPVFRPTV9SSUdIVCc6XG4gICAgICAgIHggPSA2O1xuICAgICAgICB5ID0gMTA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnVE9QJzpcbiAgICAgICAgeCA9IDA7XG4gICAgICAgIHkgPSAtMTA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnVE9QX0xFRlQnOlxuICAgICAgICB4ID0gLTY7XG4gICAgICAgIHkgPSAtMTA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnVE9QX1JJR0hUJzpcbiAgICAgICAgeCA9IDY7XG4gICAgICAgIHkgPSAtMTA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgeCA9IDA7XG4gICAgICAgIHkgPSAwO1xuICAgICAgICBicmVhaztcbiAgICB9XG5cbiAgICByZXR1cm4gZmFicmljLnV0aWwucm90YXRlUG9pbnQoXG4gICAgICBuZXcgZmFicmljLlBvaW50KHggKiBsZW5ndGgsIHkgKiBsZW5ndGgpLFxuICAgICAgbmV3IGZhYnJpYy5Qb2ludCgwLCAwKSxcbiAgICAgIGZhYnJpYy51dGlsLmRlZ3JlZXNUb1JhZGlhbnMoMzYwIC0gYW5nbGUpXG4gICAgKTtcbiAgfVxuXG4gIHN0YXRpYyBzZXRTaGFkb3coY2FudmFzLCBwYXJhbXMpIHtcbiAgICBjb25zdCB7IGFuZ2xlLCBsZW5ndGgsIGJsdXIgfSA9IHBhcmFtcztcbiAgICBjb25zdCBzZWxlY3RlZE9iamVjdCA9IGNhbnZhcy5nZXRBY3RpdmVPYmplY3QoKTtcbiAgICBjb25zdCBzaGFkb3dPZmZzZXQgPSBWaXN1YWxFZGl0b3JTZXJ2aWNlLmdldFNoYWRvd09mZnNldChcbiAgICAgIGxlbmd0aCxcbiAgICAgIC1hbmdsZSxcbiAgICAgICdCT1RUT00nXG4gICAgKTtcblxuICAgIGNvbnN0IGNvbG9yID1cbiAgICAgIHNlbGVjdGVkT2JqZWN0LnNoYWRvdyAmJiBzZWxlY3RlZE9iamVjdC5zaGFkb3cuY29sb3JcbiAgICAgICAgPyBzZWxlY3RlZE9iamVjdC5zaGFkb3cuY29sb3JcbiAgICAgICAgOiAncmdiYSgwLDAsMCwwLjUpJztcblxuICAgIHNlbGVjdGVkT2JqZWN0LnNldFNoYWRvdyh7XG4gICAgICAuLi5zZWxlY3RlZE9iamVjdC5zaGFkb3csXG4gICAgICBjb2xvcixcbiAgICAgIGJsdXIsXG4gICAgICBvZmZzZXRYOiBzaGFkb3dPZmZzZXQueCxcbiAgICAgIG9mZnNldFk6IHNoYWRvd09mZnNldC55LFxuICAgICAgYWZmZWN0U3Ryb2tlOiBmYWxzZVxuICAgIH0pO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG4gIH1cblxuICBzdGF0aWMgc2V0U2hhZG93Q29sb3IoY2FudmFzLCBjb2xvcikge1xuICAgIGNvbnN0IHNlbGVjdGVkT2JqZWN0ID0gY2FudmFzLmdldEFjdGl2ZU9iamVjdCgpO1xuICAgIGNvbnN0IGFscGhhID1cbiAgICAgIHNlbGVjdGVkT2JqZWN0LnNoYWRvdyAmJlxuICAgICAgc2VsZWN0ZWRPYmplY3Quc2hhZG93LmNvbG9yICYmXG4gICAgICBmYWJyaWMuQ29sb3IuZnJvbVJnYmEoc2VsZWN0ZWRPYmplY3Quc2hhZG93LmNvbG9yKS5nZXRBbHBoYSgpXG4gICAgICAgID8gZmFicmljLkNvbG9yLmZyb21SZ2JhKHNlbGVjdGVkT2JqZWN0LnNoYWRvdy5jb2xvcikuZ2V0QWxwaGEoKVxuICAgICAgICA6IDAuNTtcbiAgICBjb25zdCByZ2JhQ29sb3IgPSBmYWJyaWMuQ29sb3IuZnJvbUhleChjb2xvcilcbiAgICAgIC5zZXRBbHBoYShhbHBoYSlcbiAgICAgIC50b1JnYmEoKTtcbiAgICBzZWxlY3RlZE9iamVjdC5zZXRTaGFkb3coe1xuICAgICAgLi4uc2VsZWN0ZWRPYmplY3Quc2hhZG93LFxuICAgICAgY29sb3I6IHJnYmFDb2xvcixcbiAgICAgIGJsdXI6IDAuNVxuICAgIH0pO1xuICAgIGNhbnZhcy5yZXF1ZXN0UmVuZGVyQWxsKCk7XG5cbiAgICByZXR1cm4gY29sb3I7XG4gIH1cblxuICBzdGF0aWMgdHJhbnNmb3JtQ2FudmFzKGltZ1VybCwgY2FudmFzT2JqKTogYW55IHtcbiAgICBjb25zdCBiYWNrZ3JvdW5kUGxhY2Vob2xkZXJPYmplY3QgPSB7XG4gICAgICBzcmM6ICdiYWNrZ3JvdW5kX3BsYWNlaG9sZGVyJyxcbiAgICAgIHRvcDogMzAwLFxuICAgICAgX2tleTogJ2JhY2tncm91bmRfcGxhY2Vob2xkZXInLFxuICAgICAgZmlsbDogJ3JnYigwLDAsMCknLFxuICAgICAgbGVmdDogMzAwLFxuICAgICAgdHlwZTogJ2ltYWdlJyxcbiAgICAgIGFuZ2xlOiAwLFxuICAgICAgY3JvcFg6IDAsXG4gICAgICBjcm9wWTogMCxcbiAgICAgIGZsaXBYOiBmYWxzZSxcbiAgICAgIGZsaXBZOiBmYWxzZSxcbiAgICAgIHNrZXdYOiAwLFxuICAgICAgc2tld1k6IDAsXG4gICAgICB3aWR0aDogY2FudmFzT2JqLmJhY2tncm91bmRJbWFnZS53aWR0aCxcbiAgICAgIGNsaXBUbzogbnVsbCxcbiAgICAgIGhlaWdodDogY2FudmFzT2JqLmJhY2tncm91bmRJbWFnZS5oZWlnaHQsXG4gICAgICBzY2FsZVg6IDEsXG4gICAgICBzY2FsZVk6IDEsXG4gICAgICBzaGFkb3c6IG51bGwsXG4gICAgICBzdHJva2U6IG51bGwsXG4gICAgICBmaWx0ZXJzOiBbXSxcbiAgICAgIG9wYWNpdHk6IDEsXG4gICAgICBvcmlnaW5YOiAnY2VudGVyJyxcbiAgICAgIG9yaWdpblk6ICdjZW50ZXInLFxuICAgICAgdmVyc2lvbjogJzIuMy42JyxcbiAgICAgIHZpc2libGU6IHRydWUsXG4gICAgICBmaWxsUnVsZTogJ25vbnplcm8nLFxuICAgICAgcGFpbnRGaXJzdDogJ2ZpbGwnLFxuICAgICAgY3Jvc3NPcmlnaW46ICcnLFxuICAgICAgc3Ryb2tlV2lkdGg6IDAsXG4gICAgICBzdHJva2VMaW5lQ2FwOiAnYnV0dCcsXG4gICAgICBzdHJva2VMaW5lSm9pbjogJ21pdGVyJyxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJycsXG4gICAgICBzdHJva2VEYXNoQXJyYXk6IG51bGwsXG4gICAgICB0cmFuc2Zvcm1NYXRyaXg6IG51bGwsXG4gICAgICBzdHJva2VNaXRlckxpbWl0OiA0LFxuICAgICAgZ2xvYmFsQ29tcG9zaXRlT3BlcmF0aW9uOiAnc291cmNlLW92ZXInXG4gICAgfTtcblxuICAgIGNhbnZhc09iai5vYmplY3RzLmZvckVhY2gobyA9PiB7XG4gICAgICBvLnN0cm9rZSA9IG51bGw7XG4gICAgICBvLnN0cm9rZVdpZHRoID0gMDtcbiAgICAgIG8udHlwZSA9ICdpbWFnZSc7XG4gICAgICBvLndpZHRoID0gby53aWR0aCAqIG8uc2NhbGVYO1xuICAgICAgby5oZWlnaHQgPSBvLmhlaWdodCAqIG8uc2NhbGVZO1xuICAgICAgby5zY2FsZVggPSAxO1xuICAgICAgby5zY2FsZVkgPSAxO1xuICAgICAgby5zcmMgPSBvLl9rZXlcblxuICAgICAgZGVsZXRlIG8uX29JZDtcbiAgICAgIGRlbGV0ZSBvLm9iamVjdFR5cGU7XG4gICAgfSk7XG4gICAgY2FudmFzT2JqLm9iamVjdHMudW5zaGlmdChiYWNrZ3JvdW5kUGxhY2Vob2xkZXJPYmplY3QpO1xuICAgIGNhbnZhc09iai5iYWNrZ3JvdW5kSW1hZ2Uuc3JjID0gJyc7XG4gICAgY2FudmFzT2JqLmJhY2tncm91bmRJbWFnZS5jcm9zc09yaWdpbiA9ICdhbm9ueW1vdXMnO1xuICAgIGNhbnZhc09iai5fb3JpZ2luYWxJbWdVcmwgPSBpbWdVcmw7XG5cbiAgICBjb25zdCBjYW52YXNKc29uID0gSlNPTi5zdHJpbmdpZnkoY2FudmFzT2JqLCBudWxsLCAyKTtcbiAgICBjb25zb2xlLmxvZyhjYW52YXNKc29uKTtcbiAgICByZXR1cm4gY2FudmFzSnNvbjtcbiAgfVxuXG4gIHN0YXRpYyBjcmVhdGVPYmplY3RJZChvYmplY3RUeXBlOiBzdHJpbmcpIHtcbiAgICBjb25zdCBpZCA9IFZpc3VhbEVkaXRvclNlcnZpY2Uub2JqZWN0VHlwZUNvdW50W29iamVjdFR5cGVdO1xuICAgIFZpc3VhbEVkaXRvclNlcnZpY2Uub2JqZWN0VHlwZUNvdW50W29iamVjdFR5cGVdICs9IDE7XG4gICAgcmV0dXJuIGlkO1xuICB9XG5cbiAgc3RhdGljIGV4dGVuZE9iamVjdEpzb24ob2JqLCBpZCwgb2JqZWN0VHlwZSkge1xuICAgIG9iai50b09iamVjdCA9IChmdW5jdGlvbih0b09iamVjdCkge1xuICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4gZmFicmljLnV0aWwub2JqZWN0LmV4dGVuZCh0b09iamVjdC5jYWxsKHRoaXMpLCB7XG4gICAgICAgICAgX29JZDogaWQsXG4gICAgICAgICAgX2tleTogYCR7b2JqZWN0VHlwZX1fcGxhY2Vob2xkZXJfJHtpZH1gLFxuICAgICAgICAgIG9iamVjdFR5cGVcbiAgICAgICAgfSk7XG4gICAgICB9O1xuICAgIH0pKG9iai50b09iamVjdCk7XG4gIH1cbn1cbiJdfQ==