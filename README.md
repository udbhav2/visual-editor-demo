### Requirements

* Angular CLI 7.2.15

### How to use the Visual Editor Library in your Angular Application

* Specify the visual-editor library as a dependency in your main package.json file: 
```ruby
"dependencies": {
    ...,
    "visual-editor": "git+https://gitlab.com/pencil-ai/pencil-visual-editor.git#(desiredTagNumber)"
}
```
* Install the visual-editor library `npm install visual-editor`
* In your tsconfig.json file, configure a custom path to the visual-editor:
```ruby
"compilerOptions": {
    "baseUrl": "./", 
        ...
    "paths": {
        "visual-editor": [
            "node_modules/visual-editor/dist/visual-editor"
        ],
        "visual-editor/*": [
            "node_modules/visual-editor/dist/visual-editor/*"
        ]
    },
    ...
```
* In your app.module.ts file, you can now specify an import statement `import { VisualEditorModule } from 'visual-editor';`
* In the Angular component where you would like to use the visual editor library, import the relevant config file in your component.ts file eg. `import { TemplateEditorConfig } from 'visual-editor';`
* Declare a visualEditorConfig variable in your component.ts file with the relevant config `visualEditorConfig = new TemplateEditorConfig();` and a visualJson variable that is equal to the relevant Json if necessary
* You can now use the `<lib-visual-editor [visualJson]="visualJson" [visualEditorConfig]="visualEditorConfig"></lib-visual-editor>` html selector which takes in two input properties anywhere in your component.html file

### Available Config Files

* TemplateEditorConfig
* TemplateAnnotatorConfig
* SegmentationVisualEditorConfig
* BannerVisualEditorConfig
* AdVisualEditorConfig

### Things to Note
* Ensure you import/use a pre-built or custom [Angular Material Theme] (https://material.angular.io/guide/theming) in your main styles.scss file eg. `@import '@angular/material/prebuilt-themes/deeppurple-amber.css';`



